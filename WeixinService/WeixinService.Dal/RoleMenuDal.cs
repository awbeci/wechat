﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class RoleMenuDal : BaseDal<RoleMenu>
    {
        public override bool Exist(RoleMenu bean)
        {
            StringBuilder sql = new StringBuilder("select 1 from RoleMenu where 1=1 ");
            var i = 0;
            var list = new List<object>();

            sql.Append(" and menuid={" + i++ + "} and roleid={" + i++ + "}");
            list.Add(bean.MenuId);
            list.Add(bean.RoleId);
            
            DataTable dt = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return dt.Rows.Count > 0;
        }

        public override void Add(RoleMenu bean)
        {
            if (string.IsNullOrEmpty(bean.Id))
            {
                bean.Id = Guid.NewGuid().ToString();
            }
            var sql = "insert RoleMenu(id,roleid,menuid) values({0},{1},{2})";
            SqlServerOperator.Execute(sql, bean.Id, bean.RoleId, bean.MenuId);
        }

        public override void Del(RoleMenu bean)
        {
            var sql = "delete RoleMenu where roleid={0}";
            SqlServerOperator.Execute(sql, bean.RoleId);
        }

        public override void Modify(RoleMenu bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(RoleMenu bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RoleMenu bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RoleMenu bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }
    }
}
