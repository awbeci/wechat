﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using YOUO.Framework.DataAccess;
using log4net;

namespace WeixinService.Dal
{
    /// <summary>
    /// 数据操作层基类，对于没有实现的方法，一定要抛出异常。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseDal<T>
    {
        protected SQLServerOperator SqlServerOperator = new SQLServerOperator(System.Configuration.ConfigurationSettings.AppSettings["connstr"]);
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public abstract bool Exist(T bean);
        public abstract void Add(T bean);

        public abstract void Del(T bean);

        public abstract void Modify(T bean);

        public abstract DataTable Query(T bean);

        public abstract DataTable QueryByPage(T bean, int page, int rows);
        public abstract DataTable QueryByPage(T bean, int page, int rows, ref int count);
    }
}
