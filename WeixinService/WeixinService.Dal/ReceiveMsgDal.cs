﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class ReceiveMsgDal : BaseDal<ReceiveMsg>
    {

        public override bool Exist(ReceiveMsg receiveMsg)
        {
            throw new NotImplementedException();
        }

        public override void Add(ReceiveMsg receiveMsg)
        {
            Log.Debug("Add方法参数：" + receiveMsg.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            Guid gid = Guid.NewGuid();

            sql1.Append(" id,");
            sql2.Append(" {" + i++ + "},");
            list.Add(string.IsNullOrEmpty(receiveMsg.id) ? gid.ToString() : receiveMsg.id);

            sql1.Append(" FromUserName,");
            sql2.Append(" {" + i++ + "},");
            list.Add(receiveMsg.FromUserName);

            sql1.Append(" ToUserName,");
            sql2.Append(" {" + i++ + "},");
            list.Add(receiveMsg.ToUserName);

            sql1.Append(" CreateTime,");
            sql2.Append(" {" + i++ + "},");
            list.Add(DateTime.Now);


            sql1.Append(" MsgId,");
            sql2.Append(" {" + i++ + "},");
            list.Add(receiveMsg.MsgId);

            sql1.Append(" MsgType,");
            sql2.Append(" {" + i++ + "},");
            list.Add(receiveMsg.MsgType);

            if (!string.IsNullOrEmpty(receiveMsg.Title))
            {
                sql1.Append(" Title,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.Title);
            }
            if (!string.IsNullOrEmpty(receiveMsg.Description))
            {
                sql1.Append(" Description,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.Description);
            }
            if (!string.IsNullOrEmpty(receiveMsg.Url))
            {
                sql1.Append(" Url,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.Url);
            }

            if (!string.IsNullOrEmpty(receiveMsg.Content))
            {
                sql1.Append(" Content,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.Content);
            }
            if (!string.IsNullOrEmpty(receiveMsg.PicUrl))
            {
                sql1.Append(" PicUrl,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.PicUrl);
            }
            if (!string.IsNullOrEmpty(receiveMsg.Format))
            {
                sql1.Append(" Format,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.Format);
            }
            if (!string.IsNullOrEmpty(receiveMsg.MediaId))
            {
                sql1.Append(" MediaId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.MediaId);
            }
            if (!string.IsNullOrEmpty(receiveMsg.MediaIdURL))
            {
                sql1.Append(" MediaIdURL,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.MediaIdURL);
            }

            if (!string.IsNullOrEmpty(receiveMsg.ThumbMediaId))
            {
                sql1.Append(" ThumbMediaId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.ThumbMediaId);
            }
            if (!string.IsNullOrEmpty(receiveMsg.ThumbMediaIdURL))
            {
                sql1.Append(" ThumbMediaIdURL,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.ThumbMediaIdURL);
            }
            if (!string.IsNullOrEmpty(receiveMsg.bak1))
            {
                sql1.Append(" bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.bak1);
            }
            if (!string.IsNullOrEmpty(receiveMsg.bak2))
            {
                sql1.Append(" bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.bak2);
            }
            if (!string.IsNullOrEmpty(receiveMsg.bak3))
            {
                sql1.Append(" bak3,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.bak3);
            }
            if (!string.IsNullOrEmpty(receiveMsg.bak4))
            {
                sql1.Append(" bak4,");
                sql2.Append(" {" + i++ + "},");
                list.Add(receiveMsg.bak4);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into receiveMsg(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(ReceiveMsg receiveMsg)
        {
            throw new NotImplementedException();
        }

        public override void Modify(ReceiveMsg bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update receiveMsg set");
            var i = 0;
            var dList = new List<object>();

            if (!string.IsNullOrEmpty(bean.bak1))
            {
                sql.Append(" bak1={" + i++ + "},");
                dList.Add(bean.bak1);
            }
            if (!string.IsNullOrEmpty(bean.bak2))
            {
                sql.Append(" bak2={" + i++ + "},");
                dList.Add(bean.bak2);
            }

            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(ReceiveMsg receiveMsg)
        {
            throw new NotImplementedException();
        }


        public override DataTable QueryByPage(ReceiveMsg receiveMsg, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(ReceiveMsg receiveMsg, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：" + receiveMsg);
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            var sqlWhere = new StringBuilder();
            var i = 0;
            var paramList = new List<object>();
            sql.Append(@"SELECT [id],[ToUserName], [FromUserName]
      ,[CreateTime] ,[MsgType],[MsgId],[Content],[PicUrl],[Format]
      ,[ThumbMediaId],[ThumbMediaIdURL],[MediaId],[MediaIdURL] ,[Title],[Description],[Url]
      ,[bak1],[bak2], [bak3],
(SELECT top 1 [nickname] FROM UserInfo where openid=FromUserName) [bak4] FROM ReceiveMsg where 1=1 ");
            sqlcount.Append(@"SELECT count(*) count FROM ReceiveMsg where 1=1 ");

            if (receiveMsg.DateBegin != DateTime.MinValue)
            {
                sqlWhere.Append(" and CreateTime >= {" + i++ + "}");
                paramList.Add(receiveMsg.DateBegin);
            }
            if (receiveMsg.DateEnd != DateTime.MinValue)
            {
                sqlWhere.Append(" and CreateTime <= {" + i++ + "}");
                paramList.Add(receiveMsg.DateEnd.AddDays(1));
            }
            if (!string.IsNullOrEmpty(receiveMsg.KeyWords))
            {
                sqlWhere.Append(" and (Content like N'%" + receiveMsg.KeyWords + "%' or Description like N'%"
                           + receiveMsg.KeyWords + "%')");
            }
            if (!string.IsNullOrEmpty(receiveMsg.bak1))//是否回复：1-否，2-是
            {
                sqlWhere.Append(" and bak1 = {" + i++ + "}");
                paramList.Add(receiveMsg.bak1);
            }
            var data = SqlServerOperator.GetDataTableByPage(sql.Append(sqlWhere).Append(" order by CreateTime desc ").ToString(), (page - 1) * rows, rows, paramList.ToArray());

            var tmp = SqlServerOperator.GetDataTable(sqlcount.Append(sqlWhere).ToString(), paramList.ToArray());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

    }
}
