﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class RiskNoticeSignerDal : BaseDal<RiskNoticeSigner>
    {
        public override bool Exist(RiskNoticeSigner bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(RiskNoticeSigner bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            if (!string.IsNullOrEmpty(bean.ID))
            {
                sql1.Append(" ID,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.ID);
            }
            if (!string.IsNullOrEmpty(bean.WeiXin))
            {
                sql1.Append(" t_WeiXin,");
                sql2.Append(" {" + i++ + "} ,");
                list.Add(bean.WeiXin);
            }
            if (!string.IsNullOrEmpty(bean.PhoneNum))
            {
                sql1.Append(" t_PhoneNum,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.PhoneNum);
            }
            if (!string.IsNullOrEmpty(bean.RNID))
            {
                sql1.Append(" RNID,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.RNID);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" t_Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }
            if (bean.Type != null)
            {
                sql1.Append(" t_Type,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Type);
            }
            if (bean.State != null)
            {
                sql1.Append(" t_state,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.State);
            }
            if (!string.IsNullOrEmpty(bean.Img))
            {
                sql1.Append(" t_img,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Img);
            }
            if (bean.SignerDt != null)
            {
                sql1.Append(" t_signerdt,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.SignerDt);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [RiskNotice_Signer](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(RiskNoticeSigner bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from [RiskNotice_Signer] where [RNID]={0}";
            SqlServerOperator.Execute(sql, bean.RNID);
        }

        public override void Modify(RiskNoticeSigner bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(RiskNoticeSigner bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNoticeSigner bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNoticeSigner bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 根据通知单id获取签名人信息
        /// </summary>
        /// <param name="rnid"></param>
        /// <returns></returns>
        public DataTable QueryRiskNoticeSignerByRnid(string rnid)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(@"SELECT [ID]
                                          ,[t_phonenum]
                                          ,[t_weixin]
                                          ,[RNID]
                                          ,[t_name]
                                          ,[t_type]
                                          ,[t_state]
                                          ,[t_img]
                                          ,[t_signerdt]
                                      FROM [RiskNotice_Signer]
                                      where rnid={0}");
            return SqlServerOperator.GetDataTable(stringBuilder.ToString(), rnid);
        }

        /// <summary>
        /// 查询通知单编号和签名人微信
        /// </summary>
        /// <param name="id">通知单id</param>
        /// <returns></returns>
        public DataTable QueryRiskNoticeAndSigner(string id)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(@"SELECT [t_number]
	                             ,rns.rnid
                                 ,rns.[t_weixin]
                                 ,rns.[t_phonenum]
                                 ,rns.[t_type]
                              FROM [RiskNotice] rn
                              inner join riskNotice_signer rns on rn.id=rns.rnid
                              where rn.id={0} ");
            return SqlServerOperator.GetDataTable(stringBuilder.ToString(), new object[] {id });
        }
    }
}
