﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class UserRegDal : BaseDal<UserReg>
    {
        /// <summary>
        /// 检查是否已经存在数据
        /// </summary>
        /// <param name="bean"></param>
        /// <returns>TRUE-存在，FALSE-不存在</returns>
        public override bool Exist(UserReg bean)
        {
            var sql = "select 1 from UserReg where  RNumber={0}";
            DataTable dt = SqlServerOperator.GetDataTable(sql, bean.RNumber);
            return dt.Rows.Count > 0 ? true : false;
        }

        public override void Add(UserReg bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            if (!string.IsNullOrEmpty(bean.Wxid))
            {
                sql1.Append(" wxid,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Wxid);
            }

            if (!string.IsNullOrEmpty(bean.RNumber))
            {
                sql1.Append(" RNumber,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.RNumber);
            }
            if (!string.IsNullOrEmpty(bean.PhoneNumber))
            {
                sql1.Append(" PhoneNumber,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.PhoneNumber);
            }
            if (!string.IsNullOrEmpty(bean.Password))
            {
                sql1.Append(" Password,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Password);
            }
            if (!string.IsNullOrEmpty(bean.bak1))
            {
                sql1.Append(" bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.bak1);
            }
            if (!string.IsNullOrEmpty(bean.bak2))
            {
                sql1.Append(" bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.bak2);
            }
            if (!string.IsNullOrEmpty(bean.bak3))
            {
                sql1.Append(" bak3,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.bak3);
            }
            if (!string.IsNullOrEmpty(bean.bak4))
            {
                sql1.Append(" bak4,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.bak4);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into UserReg (" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(UserReg bean)
        {
            var sql = new StringBuilder("delete from UserReg where 1=1");
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Wxid))
            {
                sql.Append(" and wxid={" + i++ + "}");
                list.Add(bean.Wxid);
            }
            if (!string.IsNullOrEmpty(bean.bak1))
            {
                sql.Append(" and bak1={" + i++ + "}");
                list.Add(bean.bak1);
            }
            if (!string.IsNullOrEmpty(bean.bak2))
            {
                sql.Append(" and bak2={" + i++ + "}");
                list.Add(bean.bak2);
            }
            if (!string.IsNullOrEmpty(bean.bak3))
            {
                sql.Append(" and bak3={" + i++ + "}");
                list.Add(bean.bak3);
            }
            if (!string.IsNullOrEmpty(bean.bak4))
            {
                sql.Append(" and bak4={" + i++ + "}");
                list.Add(bean.bak4);
            }

            SqlServerOperator.Execute(sql.ToString(), list.ToArray());
        }

        public void Del2(UserReg bean)
        {
            var sql = @"delete from UserReg 
                        where wxid={0} and bak1={1} and bak2={2} and PhoneNumber={3}";
            SqlServerOperator.Execute(sql, new object[] { bean.Wxid, 
                bean.bak1, 
                bean.bak2, 
                bean.PhoneNumber 
            });
        }

        public override void Modify(UserReg bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(UserReg bean)
        {
            var sql = new StringBuilder("select * from UserReg where 1=1");
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Wxid))
            {
                sql.Append(" and wxid={" + i++ + "}");
                list.Add(bean.Wxid);
            }
            if (!string.IsNullOrEmpty(bean.PhoneNumber))
            {
                sql.Append(" and PhoneNumber={" + i++ + "}");
                list.Add(bean.PhoneNumber);
            }

            DataTable dt = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return dt;
        }

        public override System.Data.DataTable QueryByPage(UserReg bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataTable QueryByPage(UserReg bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 查询户号绑定
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserReg(string kssj, string jssj, string hh, int page, int rows, ref int count)
        {
            var i = -1;var j = -1;
            var sql = @"SELECT [RNumber],[PhoneNumber],[CreateDt]
                        FROM [UserReg] where 1=1 ";

            var sqlcount = @"SELECT count(*) count
                        FROM [UserReg] where 1=1 ";
            var list = new List<object>();

            if (!string.IsNullOrEmpty(hh))
            {
                sql += "and RNumber={" + ++i + "}";
                sqlcount += "and RNumber={" + ++j + "}";
                list.Add(hh);
            }

            if (!string.IsNullOrEmpty(kssj) && !string.IsNullOrEmpty(jssj))
            {
                jssj += " 23:59:59";
                sql += " and CreateDt between {" + ++i + "} and {" + ++i + "}";
                sqlcount += " and CreateDt between {" + ++j + "} and {" + ++j + "}";
                list.Add(kssj);
                list.Add(jssj);

            }
            sql += " order by CreateDt desc";

            var dtTable = SqlServerOperator.GetDataTableByPage(sql, (page - 1) * rows, rows, list.ToArray());

            DataTable dtCount = SqlServerOperator.GetDataTable(sqlcount, list.ToArray());
            count = int.Parse(dtCount.Rows[0]["count"].ToString());
            return dtTable;
        }

        /// <summary>
        /// 查询户号绑定2
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserReg(string kssj, string jssj, string hh)
        {
            var sql = @"SELECT [RNumber],[PhoneNumber],[CreateDt]
                        FROM [UserReg] where 1=1 ";

            var list = new List<object>();

            if (!string.IsNullOrEmpty(hh))
            {
                sql += "and RNumber={0}";
                list.Add(hh);
            }

            if (!string.IsNullOrEmpty(kssj) && !string.IsNullOrEmpty(jssj))
            {
                sql += " and CreateDt between {1} and {2}";
                list.Add(kssj);
                list.Add(jssj);

            }
            sql += " order by CreateDt";

            var dtTable = SqlServerOperator.GetDataTable(sql, list.ToArray());
            return dtTable;
        }

        /// <summary>
        /// 查询所有用户绑定数据
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserRegAll()
        {
            var sql = @"SELECT [RNumber],[PhoneNumber],[CreateDt]
                        FROM [UserReg] ";

            DataTable dtTable = SqlServerOperator.GetDataTable(sql);
            return dtTable;
        }
    }
}
