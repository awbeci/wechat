﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class UserInfoDal : BaseDal<UserInfo>
    {
        public override bool Exist(UserInfo user)
        {
            var sql = "select * from UserInfo where openid={0}";
            DataTable dtTable = SqlServerOperator.GetDataTable(sql.ToString(), new object[] { user.openid });
            if (dtTable.Rows.Count > 0)
                return true;
            return false;
        }

        public override void Add(UserInfo user)
        {
            Log.Debug("Add方法参数：" + user.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            sql1.Append(" subscribe,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.subscribe);

            sql1.Append(" openid,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.openid);

            sql1.Append(" nickname,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.nickname);

            sql1.Append(" sex,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.sex);

            sql1.Append(" language,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.language);

            sql1.Append(" city,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.city);

            sql1.Append(" province,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.province);


            sql1.Append(" country,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.country);

            sql1.Append(" headimgurl,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.headimgurl);

            sql1.Append(" subscribe_time,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.subscribe_time.ToString());

            sql1.Append(" unionid,");
            sql2.Append(" {" + i++ + "},");
            list.Add(user.unionid);


            if (!string.IsNullOrEmpty(user.bak3))
            {
                sql1.Append(" bak3,");
                sql2.Append(" {" + i++ + "},");
                list.Add(user.bak3);
            }

            if (!string.IsNullOrEmpty(user.Latitude))
            {
                sql1.Append(" Latitude,");
                sql2.Append(" {" + i++ + "},");
                list.Add(user.Latitude);
            }

            if (!string.IsNullOrEmpty(user.Longitude))
            {
                sql1.Append(" Longitude,");
                sql2.Append(" {" + i++ + "},");
                list.Add(user.Longitude);
            }

            if (!string.IsNullOrEmpty(user.Precision))
            {
                sql1.Append(" Precision,");
                sql2.Append(" {" + i++ + "},");
                list.Add(user.Precision);
            }

            if (user.UpdateDt != null)
            {
                sql1.Append(" UpdateDt,");
                sql2.Append(" {" + i++ + "},");
                list.Add(user.UpdateDt);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into UserInfo(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(UserInfo user)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from UserInfo where openid={0}";
            SqlServerOperator.Execute(sql, user.openid);
        }

        public override void Modify(UserInfo bean)
        {
            Log.Debug("Modify方法参数：" + bean.ToString());
            var sql = new StringBuilder();
            sql.Append(" update UserInfo set ");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.groupID))
            {
                sql.Append(" groupID={" + i++ + "},");
                dList.Add(bean.groupID);
            }
            if (!string.IsNullOrEmpty(bean.groupName))
            {
                sql.Append(" groupName={" + i++ + "},");
                dList.Add(bean.groupName);
            }
            if (!string.IsNullOrEmpty(bean.RNumber))
            {
                sql.Append(" RNumber={" + i++ + "},");
                dList.Add(bean.RNumber);
            }
            if (!string.IsNullOrEmpty(bean.Latitude))
            {
                sql.Append(" Latitude={" + i++ + "},");
                dList.Add(bean.Latitude);
            }
            if (!string.IsNullOrEmpty(bean.Longitude))
            {
                sql.Append(" Longitude={" + i++ + "},");
                dList.Add(bean.Longitude);
            }
            if (!string.IsNullOrEmpty(bean.Precision))
            {
                sql.Append(" Precision={" + i++ + "},");
                dList.Add(bean.Precision);
            }
            if (bean.UpdateDt != null)
            {
                sql.Append(" UpdateDt={" + i++ + "},");
                dList.Add(bean.UpdateDt);
            }

            if (sql.Length > 0)
            {
                sql = sql.Remove(sql.Length - 1, 1);
            }
            sql.Append(" where openid={" + i++ + "}");
            dList.Add(bean.openid);
            Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(UserInfo user)
        {
            var sql = new StringBuilder("select * from UserInfo where 1=1 ");
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(user.openid))
            {
                sql.Append(" and openid={" + i++ + "}");
                list.Add(user.openid);
            }
            DataTable dtTable = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return dtTable;
        }

        public override DataTable QueryByPage(UserInfo user, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserInfo user, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        public DataTable QueryAll()
        {
            var sql = @"SELECT [openid]
                        FROM [UserInfo]";
            DataTable dtTable = SqlServerOperator.GetDataTable(sql);
            return dtTable;
        }
    }
}
