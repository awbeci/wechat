﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeixinService.Model;
using System.Data;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class UsersInfoDal : BaseDal<UserInfo>
    {

        public override bool Exist(UserInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(UserInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Del(UserInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(UserInfo bean)
        {
            Log.Debug("Modify方法参数：" + bean.ToString());
            var sql = new StringBuilder();
            var i = 0;
            var dList = new List<object>();
            sql.Append(" update UserInfo set groupID={" + i++ + "}, groupName={" + i++ + "},");
            dList.Add(bean.groupID);
            dList.Add(bean.groupName);
            if (sql.Length > 0)
            {
                sql = sql.Remove(sql.Length - 1, 1);
            }
            sql.Append(" where openid={" + i++ + "}");
            dList.Add(bean.openid);
            Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(UserInfo bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserInfo bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserInfo bean, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：" + bean);
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();

            sql.Append(@"SELECT u.openid as userid,u.subscribe,u.nickname,u.sex,u.city,u.country,u.province,u.language,u.headimgurl,u.subscribe_time,u.unionid,u.groupID,ug.name as groupName,u.RNumber,u.msgFrom,u.TrueName,u.bak1 FROM [UserInfo] u left join [UserToGroup] utg on u.openid=utg.openid
  left join [UserGroup] ug on utg.groupid=ug.id where 1=1");
            sqlcount.Append(@"SELECT count(u.openid) count FROM [UserInfo] u left join [UserToGroup] utg on u.openid=utg.openid
  left join [UserGroup] ug on utg.groupid=ug.id where 1=1");
            if (!string.IsNullOrEmpty(bean.nickname))
            {
                sql.Append(" and nickname like N'%" + bean.nickname + "%' ");
                sqlcount.Append(" and nickname like N'%" + bean.nickname + "%' ");
            }
            if (!string.IsNullOrEmpty(bean.truename))
            {
                sql.Append(" and truename like N'%" + bean.truename + "%' ");
                sqlcount.Append(" and truename like N'%" + bean.truename + "%' ");
            }
            if (bean.begintime != new DateTime() && bean.endtime != new DateTime())
            {
                sql.Append(" and subscribe_time between '" + bean.begintime + "' and '" + bean.endtime + "' ");
                sqlcount.Append(" and subscribe_time between '" + bean.begintime + "' and '" + bean.endtime + "' ");
            }
            if (bean.groupID == "all" || bean.groupID == "-1")
            {
                sql.Append(" order by subscribe_time desc ");
            }
            else if (bean.groupID == "0")
            {
                sql.Append(" and (u.openid not in (select openid from   [UserToGroup] ) or utg.groupid='0') order by subscribe_time desc ");
                sqlcount.Append(" and (u.openid not in (select openid from [UserToGroup] ) or utg.groupid='0') ");
            }
            else if (bean.groupID == "0_0")
            {
                sql.Append(" and (u.openid not in (select openid from [UserToGroup] ) or utg.groupid='0') and u.openid in (select wxid from [UserReg] ) order by subscribe_time desc ");
                sqlcount.Append(" and (u.openid not in (select openid from [UserToGroup] ) or utg.groupid='0') and u.openid in (select wxid from [UserReg] ) ");
            }
            else
            {
                sql.Append(" and utg.groupid={0} order by subscribe_time desc ");
                sqlcount.Append(" and utg.groupid={0} ");
            }

            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows, bean.groupID);

            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString(), bean.groupID);
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }


        /// <summary>
        /// 查询用户分组
        /// </summary>
        /// <returns></returns>
        public DataTable QueryGroup()
        {
            Log.Debug("QueryGroup方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT * FROM [UserGroup] order by id ");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        /// <summary>
        /// 带条件查询用户分组
        /// </summary>
        /// <returns></returns>
        public DataTable QueryGroupTable()
        {
            Log.Debug("QueryGroupTable方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT * FROM [UserGroup] where id!='-1' and id!='0_0' order by id ");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        /// <summary>
        /// 新建用户分组
        /// </summary>
        /// <param name="bean"></param>
        public void AddFz(UserGroup bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.id))
            {
                sql1.Append(" id,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.id);
            }
            if (!string.IsNullOrEmpty(bean.pid))
            {
                sql1.Append(" pid,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.pid);
            }
            if (!string.IsNullOrEmpty(bean.name))
            {
                sql1.Append(" name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.name);
            }
            if (bean.count != 0)
            {
                sql1.Append(" count,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.count);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }
            var sql = "insert into usergroup(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        /// <summary>
        /// 编辑用户分组
        /// </summary>
        /// <param name="bean"></param>
        public void ModifyFz(UserGroup bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update usergroup set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.name))
            {
                sql.Append(" name={" + i++ + "},");
                dList.Add(bean.name);
            }
            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        /// <summary>
        /// 删除用户分组
        /// </summary>
        /// <param name="bean"></param>
        public void DelFz(UserGroup bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("delete from usergroup where id={0}");
            var dList = new List<object>();
            dList.Add(bean.id);
            Log.Debug("SQL :" + sql + ",params:" + dList.ToArray());
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        /// <summary>
        /// 查询用户信息和业务号
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserInfoAndYwh(string groupid, int page, int rows, ref int count)
        {
            Log.Debug("QueryUserInfoAndYwh方法参数：");
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            sql.Append(@"select ui.openid wxid,nickname,ur.phonenumber,dbo.[AsBak1](ui.openid) qy,dbo.[AsBak4](ui.openid) zw
                            from userinfo ui
                            inner join UserToGroup utg on utg.openid=ui.openid
                            inner join UserGroup ug on ug.id=utg.groupid
                            left join userreg ur on ui.openid=ur.wxid
                            where ug.iD={0}
                            group by ui.openid,nickname,ur.phonenumber ");

            sqlcount.Append(@"select count(*) count
                                from 
	                                (select ui.openid wxid,nickname,dbo.AggregateString(ui.openid) ywh
		                                from userinfo ui
		                                inner join UserToGroup utg on utg.openid=ui.openid
                                        inner join UserGroup ug on ug.id=utg.groupid
                                        left join userreg ur on ui.openid=ur.wxid
                                        where ug.iD={0}
		                                group by ui.openid,nickname,ur.phonenumber) tmpdb");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows, groupid);
            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString(), groupid);
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 查询用户信息和业务号
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserInfoAndYwh2(string groupid, int page, int rows, ref int count)
        {
            Log.Debug("QueryUserInfoAndYwh方法参数：");
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            sql.Append(@"select ui.openid wxid,ui.bak1 zw,nickname,ur.phonenumber
                            from userinfo ui
                            inner join UserToGroup utg on utg.openid=ui.openid
                            inner join UserGroup ug on ug.id=utg.groupid
                            left join userreg ur on ui.openid=ur.wxid
                            where ug.iD={0} ");

            sqlcount.Append(@"select count(*) count
                            from userinfo ui
                            inner join UserToGroup utg on utg.openid=ui.openid
                            inner join UserGroup ug on ug.id=utg.groupid
                            left join userreg ur on ui.openid=ur.wxid
                            where ug.iD={0}");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows, groupid);
            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString(), groupid);
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 查询人员
        /// </summary>
        /// <param name="txt">参数(可能是人员名，也有可能是公司名)</param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DataTable QueryPerson(string txt, int page, int rows, ref int count)
        {
            Log.Debug("QueryUserInfoAndYwh方法参数：");
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            sql.Append(@"select distinct ui.openid wxid,nickname,dbo.[AsBak1](ui.openid) qy,dbo.[AsBak4](ui.openid) zw
                            from userinfo ui
                            left join userreg ur on ui.openid=ur.wxid                            
                            group by ui.openid,nickname,ur.bak1
                            having ui.nickname like N'%" + txt + "%' or ur.bak1 like N'%" + txt + "%'");

            sqlcount.Append(@"select count(*) count
                                from 
	                                (select distinct ui.openid wxid,nickname,dbo.[AsBak1](ui.openid) qy,dbo.[AsBak4](ui.openid) zw
		                                from userinfo ui
		                                left join userreg ur on ui.openid=ur.wxid
		                                group by ui.openid,nickname,ur.bak1
                                        having ui.nickname like N'%" + txt + "%' or ur.bak1 like N'%" + txt + "%')tmpdb");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows);
            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 根据组id查询用户分组父id
        /// </summary>
        /// <returns></returns>
        public string QueryPidById(string id)
        {
            Log.Debug("QueryPidById方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT pid FROM UserGroup where id='" + id + "' ");
            Log.Debug("SQL :" + sql + ",params:" + id);
            return SqlServerOperator.GetScalar(sql.ToString()).ToString();
        }

        /// <summary>
        /// 根据组id查询用户分组名称
        /// </summary>
        /// <returns></returns>
        public string QueryNameById(string id)
        {
            Log.Debug("QueryPidById方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT name FROM UserGroup where id='" + id + "' ");
            Log.Debug("SQL :" + sql + ",params:" + id);
            return SqlServerOperator.GetScalar(sql.ToString()).ToString();
        }

        /// <summary>
        /// 实名编辑
        /// </summary>
        /// <param name="bean"></param>
        public void ModifyTrueName(UserInfo bean)
        {
            Log.Debug("ModifyTrueName方法参数：" + bean.ToString());
            var sql = new StringBuilder();
            var i = 0;
            var dList = new List<object>();

            sql.Append(" update UserInfo set truename={" + i++ + "},bak1={" + i++ + "} where openid={" + i++ + "}");
            dList.Add(bean.truename);
            dList.Add(bean.bak1);
            dList.Add(bean.openid);
            Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }


        /// <summary>
        /// 删除用户和分组关系
        /// </summary>
        /// <param name="bean"></param>
        public void DelUserToGruop(string openids)
        {
            Log.Debug("DelUserToGruop方法参数：" + openids);          
            var sql = new StringBuilder();
            sql.Append("delete from UserToGroup where openid in( ");
            if (!string.IsNullOrEmpty(openids))
            {
                string[] arropenids = openids.Split(':');
                for (int i = 0; i < arropenids.Length;i++ )
                {
                    sql.Append(" '"+arropenids[i]+"',");
                }
            }
            sql = sql.Remove(sql.Length-1,1); 
            sql.Append(" ) ");
            Log.Debug("SQL :" + sql + ",params:" + openids);
            SqlServerOperator.Execute(sql.ToString());
        }


        /// <summary>
        /// 插入用户和分组关系
        /// </summary>
        /// <param name="bean"></param>
        public void AddUserToGruop(UserToGruop bean)
        {
            Log.Debug("AddUserToGruop方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.openid))
            {
                sql1.Append(" openid,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.openid);
            }
            if (!string.IsNullOrEmpty(bean.groupid))
            {
                sql1.Append(" groupid,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.groupid);
            }
            if (bean.uploadwechat!=0)
            {
                sql1.Append(" uploadwechat,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.uploadwechat);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }
            var sql = "insert into [UserToGroup](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        /// <summary>
        /// 查询用户分组关系表
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserToGroup()
        {
            Log.Debug("QueryUserToGroup方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT * FROM [UserToGroup] where uploadwechat='1' ");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        /// <summary>
        /// 查询用户分组关系表openid不重复数据
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserToGroupByOne()
        {
            Log.Debug("QueryUserToGroup方法参数：");
            var sql = new StringBuilder();
            sql.Append(" SELECT  openid,(select top 1 groupid from [UserToGroup] a where a.openid=b.openid) as groupid  FROM [UserToGroup] b where uploadwechat='1' group by openid ");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }


        /// <summary>
        /// 实名编辑
        /// </summary>
        /// <param name="bean"></param>
        public void ModifyUploadWechat(string openid)
        {
            Log.Debug("ModifyUploadWechat方法参数：" + openid);
            var sql = new StringBuilder();
            sql.Append(" update [UserToGroup] set uploadwechat='2' where openid='"+openid+"' ");
            SqlServerOperator.Execute(sql.ToString());
        }

        /// <summary>
        /// 查询用户名称
        /// </summary>
        /// <returns></returns>
        public DataTable QueryUserName(string openid)
        {
            Log.Debug("QueryUserName方法参数：");
            var sql = new StringBuilder();
            sql.Append(" select * from [UserInfo] where openid ='" + openid + "' ");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

    }
}
