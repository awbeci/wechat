﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class UserGroupDal : BaseDal<UserGroup>
    {
        public override bool Exist(UserGroup bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(UserGroup bean)
        {
            throw new NotImplementedException();
        }

        public override void Del(UserGroup bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(UserGroup bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(UserGroup bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserGroup bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserGroup bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 查询所有组数据
        /// </summary>
        /// <returns></returns>
        public DataTable QueryAll()
        {
            Log.Debug("QueryAll方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [id]
                          ,[name]
                          ,[pid]
                      FROM [UserGroup] 
                      where (pid=103 or (pid =104 and id in ('e4af4532-552e-66c4-17ac-d32df13d5b58','edc96f81-4169-e5b9-3387-4bb4c89f29b6','813e3917-2ffe-f645-5985-8540e7a9b5a5')))
                        or id in('0','103','104')");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        /// <summary>
        /// 根据组id=104查找用户数据
        /// </summary>
        /// <returns></returns>
        public bool QueryPersonByGroupid(string wxid)
        {
            Log.Debug("QueryPersonByGroupid方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT count(*) count
                           FROM [UserInfo] ui
                           inner join [UserToGroup] utg on ui.openid=utg.openid
                           inner join [UserGroup] ug on utg.groupid=ug.id
                           where (utg.groupID='104' or ug.pid='104') and ui.openid={0}");
            var data = SqlServerOperator.GetDataTable(sql.ToString(), wxid);
            var count = int.Parse(data.Rows[0]["count"].ToString());
            return count > 0;
        }

        public DataTable QueryInternalUser()
        {
            var sql = @" SELECT distinct ui.openid FROM [UserInfo] ui
                           inner join [UserToGroup] utg on ui.openid=utg.openid
                           inner join [UserGroup] ug on utg.groupid=ug.id
                           where (utg.groupID='104' or ug.pid='104')";
            return SqlServerOperator.GetDataTable(sql);
        }

        public DataTable QueryUnbindInternalUser(string wxid)
        {
            var sql = @" SELECT distinct ui.*  FROM [UserInfo] ui
 inner join [UserToGroup] utg on ui.openid=utg.openid
 inner join [UserGroup] ug on utg.groupid=ug.id where (utg.groupID='104' or ug.pid='104') 
 and ui.openid not in  ( SELECT distinct [OpenId]
 FROM [Users] where openid is not null and openid != {0})";
            return SqlServerOperator.GetDataTable(sql, wxid);
        }

        public DataTable QueryUnbindInternalUser()
        {
            var sql = @" SELECT distinct ui.*,ur.Phonenumber
                             FROM [UserInfo] ui
                                     inner join [UserToGroup] utg on ui.openid=utg.openid
                                     inner join [UserGroup] ug on utg.groupid=ug.id 
                                     inner join [UserReg] ur on ur.wxid=ui.openid
                             where (utg.groupID='104' or ug.pid='104') ";
            return SqlServerOperator.GetDataTable(sql);
        }
    }
}
