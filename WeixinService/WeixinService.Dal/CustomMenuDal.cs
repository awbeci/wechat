﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class CustomMenuDal : BaseDal<CustomMenu>
    {
        public override bool Exist(CustomMenu bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(CustomMenu bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql1.Append(" Id,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.Pid))
            {
                sql1.Append(" Pid,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Pid);
            }
            if (!string.IsNullOrEmpty(bean.Type))
            {
                sql1.Append(" Type,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Type);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.Nkey))
            {
                sql1.Append(" nKey,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Nkey);
            }
            if (!string.IsNullOrEmpty(bean.Url))
            {
                sql1.Append(" Url,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Url);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into custommenu(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(CustomMenu bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from custommenu where Id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public override void Modify(CustomMenu bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update custommenu set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.Type))
            {
                sql.Append(" Type={" + i++ + "},");
                dList.Add(bean.Type);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql.Append(" Name={" + i++ + "},");
                dList.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.Nkey))
            {
                sql.Append(" Nkey={" + i++ + "},");
                dList.Add(bean.Nkey);
            }
            if (!string.IsNullOrEmpty(bean.Url))
            {
                sql.Append(" Url={" + i++ + "},");
                dList.Add(bean.Url);
            }
            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.Id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(CustomMenu bean)
        {
            Log.Debug("Query方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("select * from custommenu where 1=1 order by id");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        public DataTable QueryByFirstMenu()
        {
            Log.Debug("Query方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [id]
                          ,[pid]
                          ,[type]
                          ,[name]
                          ,[nkey]
                          ,[url]
                      FROM [CustomMenu]
                      where pid='0'");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        public DataTable QueryBySecondMenu(string str)
        {
            Log.Debug("Query方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [id]
                          ,[pid]
                          ,[type]
                          ,[name]
                          ,[nkey]
                          ,[url]
                      FROM [CustomMenu]
                      where pid='" + str + "'");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }

        public override DataTable QueryByPage(CustomMenu bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(CustomMenu bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }
    }
}
