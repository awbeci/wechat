﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class DepartmentDal : BaseDal<DepartmentModel>
    {
        public override bool Exist(DepartmentModel bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(DepartmentModel bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql1.Append(" Id,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.PId))
            {
                sql1.Append(" PId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.PId);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [Department](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(DepartmentModel bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from [Department] where Id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public override void Modify(DepartmentModel bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update [Department] set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql.Append(" Name={" + i++ + "},");
                dList.Add(bean.Name);
            }
            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.Id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(DepartmentModel bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(DepartmentModel bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(DepartmentModel bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        public DataTable QueryAll()
        {
            var sql = @"SELECT [Id]
                              ,[Name]
                              ,[PId]
                          FROM [Department]";
            DataTable dtTable = SqlServerOperator.GetDataTable(sql);
            return dtTable;
        }
    }
}
