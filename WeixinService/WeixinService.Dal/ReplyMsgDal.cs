﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class ReplyMsgDal : BaseDal<ReplyMsg>
    {

        public override bool Exist(ReplyMsg bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(ReplyMsg bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            sql1.Append(" id,");
            sql2.Append(" {" + i++ + "},");
            list.Add(Guid.NewGuid().ToString());
            if (!string.IsNullOrEmpty(bean.ReceiveMsgId))
            {
                sql1.Append(" ReceiveMsgId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.ReceiveMsgId);
            }
            if (!string.IsNullOrEmpty(bean.AcceptName))
            {
                sql1.Append(" AcceptName,");
                sql2.Append(" {" + i++ + "} ,");
                list.Add(bean.AcceptName);
            }
            if (!string.IsNullOrEmpty(bean.TextMsg))
            {
                sql1.Append(" TextMsg,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.TextMsg);
            }
            if (bean.IsSend != 0)
            {
                sql1.Append(" IsSend,");
                sql2.Append(" {" + i++ + "} ,");
                list.Add(bean.IsSend);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into ReplyMsg(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(ReplyMsg bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(ReplyMsg bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(ReplyMsg bean)
        {
            var sql = "select * from ReplyMsg where ReceiveMsgId={0} order by CreateTime desc";
            DataTable dtTable = SqlServerOperator.GetDataTable(sql, new object[] { bean.ReceiveMsgId });
            return dtTable;
        }

        public override DataTable QueryByPage(ReplyMsg bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(ReplyMsg bean, int page, int rows, ref int count)
        {
            var sql = "select * from ReplyMsg where ReceiveMsgId={0} order by CreateTime desc";
            var sqlCount = "select count(*) count from ReplyMsg where ReceiveMsgId={0} ";
            DataTable dtTable = SqlServerOperator.GetDataTable(sql, (page - 1) * rows, rows, new object[] { bean.ReceiveMsgId });
            var tmp = SqlServerOperator.GetDataTable(sqlCount, new object[] { bean.ReceiveMsgId });
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return dtTable;
            throw new NotImplementedException();
        }
    }
}
