﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class UserInOrOutDal:BaseDal<UserInOrOut>
    {
        public override bool Exist(UserInOrOut userInOut)
        {
            throw new NotImplementedException();
        }

        public override void Add(UserInOrOut userInOut)
        {
            Log.Debug("Add方法参数：" + userInOut.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            Guid gid = Guid.NewGuid();

            sql1.Append(" id,");
            sql2.Append(" {" + i++ + "},");
            list.Add(gid.ToString());

            sql1.Append(" userId,");
            sql2.Append(" {" + i++ + "},");
            list.Add(userInOut.userId);

            sql1.Append(" optTime,");
            sql2.Append(" {" + i++ + "},");
            list.Add(userInOut.optTime.ToString());

            sql1.Append(" type,");
            sql2.Append(" {" + i++ + "},");
            list.Add(userInOut.type);

            if (!string.IsNullOrEmpty(userInOut.userName))
            {
                sql1.Append(" userName,");
                sql2.Append(" {" + i++ + "},");
                list.Add(userInOut.userName);
            }
            if (!string.IsNullOrEmpty(userInOut.groupID))
            {
                sql1.Append(" groupID,");
                sql2.Append(" {" + i++ + "},");
                list.Add(userInOut.groupID);
            }
            if (!string.IsNullOrEmpty(userInOut.groupName))
            {
                sql1.Append(" groupName,");
                sql2.Append(" {" + i++ + "},");
                list.Add(userInOut.groupName);
            }

            if (!string.IsNullOrEmpty(userInOut.bak1))
            {
                sql1.Append(" bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(userInOut.bak1);
            }
            if (!string.IsNullOrEmpty(userInOut.bak2))
            {
                sql1.Append(" bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(userInOut.bak2);
            }


            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into userInOrOut(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(UserInOrOut userInOut)
        {

            throw new NotImplementedException();
        }

        public override void Modify(UserInOrOut userInOut)
        {

            throw new NotImplementedException();
        }

        public override DataTable Query(UserInOrOut userInOut)
        {

            throw new NotImplementedException();
        }


        public override DataTable QueryByPage(UserInOrOut userInOut, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(UserInOrOut userInOut, int page, int rows, ref int count)
        {
            throw new NotImplementedException();  
        }
    }
}
