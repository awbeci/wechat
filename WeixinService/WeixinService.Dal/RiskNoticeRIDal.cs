﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class RiskNoticeRIDal : BaseDal<RiskNoticeRI>
    {
        public override bool Exist(RiskNoticeRI bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(RiskNoticeRI bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            if (!string.IsNullOrEmpty(bean.ID))
            {
                sql1.Append(" ID,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.ID);
            }
            if (!string.IsNullOrEmpty(bean.WeiXin))
            {
                sql1.Append(" t_WeiXin,");
                sql2.Append(" {" + i++ + "} ,");
                list.Add(bean.WeiXin);
            }
            if (!string.IsNullOrEmpty(bean.PhoneNum))
            {
                sql1.Append(" t_PhoneNum,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.PhoneNum);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" t_Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }
            if (bean.Type != null)
            {
                sql1.Append(" t_Type,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Type);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql1.Append(" t_Bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql1.Append(" t_Bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak2);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [RiskNotice_R_I](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(RiskNoticeRI bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(RiskNoticeRI bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update [RiskNotice_R_I] set");
            var i = 0;
            var dList = new List<object>();

            if (!string.IsNullOrEmpty(bean.WeiXin))
            {
                sql.Append(" t_WeiXin={" + i++ + "},");
                dList.Add(bean.WeiXin);
            }
            if (!string.IsNullOrEmpty(bean.PhoneNum))
            {
                sql.Append(" t_PhoneNum={" + i++ + "},");
                dList.Add(bean.PhoneNum);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql.Append(" t_Name={" + i++ + "},");
                dList.Add(bean.Name);
            }
            if (bean.Type != null)
            {
                sql.Append(" t_Type={" + i++ + "},");
                dList.Add(bean.Type);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql.Append(" t_Bak1={" + i++ + "},");
                dList.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql.Append(" t_Bak2={" + i++ + "},");
                dList.Add(bean.Bak2);
            }

            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.ID);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(RiskNoticeRI bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNoticeRI bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNoticeRI bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 查找审核人、签发人
        /// </summary>
        /// <returns></returns>
        public DataTable QueryRiskNoticeRI()
        {
            try
            {
                var sql = @"SELECT [ID]
                              ,[t_weixin]
                              ,[t_phonenum]
                              ,[t_name]
                              ,[t_type]
                              ,[t_bak1]
                              ,[t_bak2]
                          FROM [RiskNotice_R_I]";
                DataTable dt = SqlServerOperator.GetDataTable(sql);
                return dt;
            }
            catch (Exception e)
            {
                Log.Debug("QueryRiskNoticeRI方法,错误原因：" + e.Message);
                return null;
            }
        }
    }
}
