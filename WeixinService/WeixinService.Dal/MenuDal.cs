﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class MenuDal : BaseDal<MenuModel>
    {
        public override bool Exist(MenuModel bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(MenuModel bean)
        {
            throw new NotImplementedException();
        }

        public override void Del(MenuModel bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(MenuModel bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(MenuModel bean)
        {
            var sql = "select * from Menu m1 where 1=1 and m1.enable=2";
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.PId))
            {
                sql += " and m1.pid={" + i++ + "}";
                list.Add(bean.PId);
            }
            return SqlServerOperator.GetDataTable(sql, list.ToArray());
        }

        public override DataTable QueryByPage(MenuModel bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(MenuModel bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 查询主菜单
        /// </summary>
        /// <returns></returns>
        public DataTable QueryMainMenu()
        {
            Log.Debug("QueryByWxid方法参数：");
            var sql = new StringBuilder();
            sql.Append(@"SELECT id,pid,title
                            FROM [Menu] m
                            where pid='0' order by id");
            var data = SqlServerOperator.GetDataTable(sql.ToString());
            return data;
        }

        /// <summary>
        /// 查询子菜单 
        /// </summary>
        /// <param name="loginname"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public DataTable QuerySubMenu(string loginname, string pid)
        {
            Log.Debug("QueryByWxid方法参数：");
            var sql = new StringBuilder();
            sql.Append(@"SELECT m.id,[Title],[Url],icon,[Pid]
                              FROM [Menu] m
                              inner join [RoleMenu] rm on m.id = rm.menuid
                              inner join [Roles] r on rm.roleid = r.id
                              inner join [users] u on u.roleid = r.id
                              where u.loginname={0} and pid={1} and enable !=1
                              order by m.id");
            var list = new List<object>();
            list.Add(loginname);
            list.Add(pid);
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data;
        }
    }
}
