﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class RolesDal : BaseDal<Roles>
    {
        public override bool Exist(Roles bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(Roles bean)
        {
            if (string.IsNullOrEmpty(bean.Id))
            {
                bean.Id = Guid.NewGuid().ToString();
            }
            var sql = "insert Roles(id,name) values({0},{1})";
            SqlServerOperator.Execute(sql, bean.Id, bean.Name);
        }

        public override void Del(Roles bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from [Users] where Id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public void Del1(Roles bean)
        {
            var sql = "delete Roles where id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public override void Modify(Roles bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(Roles bean)
        {
            var sql = "select * from Roles where 1=1 ";
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql += " and Id={" + i++ + "}";
                list.Add(bean.Id);
            }
            return SqlServerOperator.GetDataTable(sql, list.ToArray());
        }

        public override DataTable QueryByPage(Roles bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(Roles bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        public DataTable QueryAll()
        {
            Log.Debug("QueryAll方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT   [Id]
                                  ,[Name]
                              FROM [Roles]");
            return SqlServerOperator.GetDataTable(sql.ToString());
        }
    }
}
