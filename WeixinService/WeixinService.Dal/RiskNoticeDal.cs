﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;
using WeixinService.Model.common;

namespace WeixinService.Dal
{
    public class RiskNoticeDal : BaseDal<RiskNotice>
    {
        string imgurl = System.Configuration.ConfigurationSettings.AppSettings["imgurl"];
        public override bool Exist(RiskNotice bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(RiskNotice bean)
        {
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            if (!string.IsNullOrEmpty(bean.ID))
            {
                sql1.Append(" ID,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.ID);
            }
            if (!string.IsNullOrEmpty(bean.Number))
            {
                sql1.Append(" t_number,");
                sql2.Append(" {" + i++ + "} ,");
                list.Add(bean.Number);
            }
            if (!string.IsNullOrEmpty(bean.Rq))
            {
                sql1.Append(" t_date,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Rq);
            }
            if (!string.IsNullOrEmpty(bean.Zs))
            {
                sql1.Append(" t_zs,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Zs);
            }
            if (!string.IsNullOrEmpty(bean.Tdsb_Gq))
            {
                sql1.Append(" t_tdsb_gq,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Tdsb_Gq);
            }
            if (!string.IsNullOrEmpty(bean.Yxfxfx))
            {
                sql1.Append(" t_yxfxfx,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Yxfxfx);
            }
            if (!string.IsNullOrEmpty(bean.Fxykcs))
            {
                sql1.Append(" t_fxykcs,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Fxykcs);
            }
            if (bean.State != null)
            {
                sql1.Append(" t_state,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.State);
            }
            if (bean.Send_Dt != null)
            {
                sql1.Append(" t_send_dt,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Send_Dt);
            }
            if (bean.CreateDt != null)
            {
                sql1.Append(" t_createdt,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.CreateDt);
            }
            if (!string.IsNullOrEmpty(bean.Operator))
            {
                sql1.Append(" t_Operator,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Operator);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [RiskNotice](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list);
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(RiskNotice bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from RiskNotice where id={0}";
            SqlServerOperator.Execute(sql, bean.ID);
        }

        public override void Modify(RiskNotice bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update [RiskNotice] set");
            var i = 0;
            var dList = new List<object>();

            if (!string.IsNullOrEmpty(bean.Number))
            {
                sql.Append(" [t_number]={" + i++ + "},");
                dList.Add(bean.Number);
            }
            if (!string.IsNullOrEmpty(bean.Rq))
            {
                sql.Append(" [t_date]={" + i++ + "},");
                dList.Add(bean.Rq);
            }
            if (!string.IsNullOrEmpty(bean.Zs))
            {
                sql.Append(" [t_zs]={" + i++ + "},");
                dList.Add(bean.Zs);
            }
            if (!string.IsNullOrEmpty(bean.Tdsb_Gq))
            {
                sql.Append(" [t_tdsb_gq]={" + i++ + "},");
                dList.Add(bean.Tdsb_Gq);
            }
            if (!string.IsNullOrEmpty(bean.Yxfxfx))
            {
                sql.Append(" [t_yxfxfx]={" + i++ + "},");
                dList.Add(bean.Yxfxfx);
            }
            if (!string.IsNullOrEmpty(bean.Fxykcs))
            {
                sql.Append(" [t_fxykcs]={" + i++ + "},");
                dList.Add(bean.Fxykcs);
            }
            if (!string.IsNullOrEmpty(bean.Operator))
            {
                sql.Append(" [t_Operator]={" + i++ + "},");
                dList.Add(bean.Operator);
            }
            if (bean.Send_Dt != null)
            {
                sql.Append(" [t_send_dt]={" + i++ + "},");
                dList.Add(bean.Send_Dt);
            }
            if (bean.CreateDt != null)
            {
                sql.Append(" [t_createdt]={" + i++ + "},");
                dList.Add(bean.CreateDt);
            }
            if (bean.State != null)
            {
                sql.Append(" [t_state]={" + i++ + "},");
                dList.Add(bean.State);
            }

            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.ID);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(RiskNotice bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNotice bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RiskNotice bean, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：" + bean);
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();

            sql.Append(@"SELECT *
                  FROM [RiskNotice]
                  order by t_createdt desc");

            sqlcount.Append(@"SELECT count(id) count
                  FROM [RiskNotice]");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows);

            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        public DataTable QueryByPage(RiskNoticeTmp3 riskNoticeTmp3, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：");
            var i = -1; var j = -1;
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            var list = new List<object>();
            sql.Append(@"SELECT *
                  FROM [RiskNotice]
                  where 1=1");

            sqlcount.Append(@"SELECT count(id) count
                  FROM [RiskNotice]
                  where 1=1");

            if (!string.IsNullOrEmpty(riskNoticeTmp3.kssj) && !string.IsNullOrEmpty(riskNoticeTmp3.jssj.ToString()))
            {
                riskNoticeTmp3.jssj = riskNoticeTmp3.jssj + " 23:59:59";
                sql.Append(" and t_createdt between {" + ++i + "} and {" + ++i + "}");
                sqlcount.Append(" and t_createdt between {" + ++j + "} and {" + ++j + "}");
                list.Add(riskNoticeTmp3.kssj);
                list.Add(riskNoticeTmp3.jssj);
            }

            if (!string.IsNullOrEmpty(riskNoticeTmp3.bh))
            {
                sql.Append(" and t_number like '%" + riskNoticeTmp3.bh + "%'");
                sqlcount.Append(" and t_number like '%" + riskNoticeTmp3.bh + "%'");
                list.Add(riskNoticeTmp3.bh);
            }
            sql.Append(" order by t_createdt desc");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows, list.ToArray());

            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString(), list.ToArray());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 根据手机号查询通知单数据
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DataTable QueryRiskNotice(string phone, int page, int rows, ref int count)
        {
            Log.Debug("QueryAll方法参数：");
            var sql = new StringBuilder();
            var sqlCount = new StringBuilder();
            sql.Append(@" SELECT   rn.id,
                                   [t_number]
                                  ,[t_date]
                                  ,[t_zs]
                                  ,[t_tdsb_gq]
                                  ,[t_yxfxfx]
                                  ,[t_fxykcs]
                                  ,rn.[t_state] zt
                                  ,[t_send_dt]
                                  ,[t_createdt]
                                  ,rns.t_state qzzt
                                  ,rns.t_type 
                              FROM [RiskNotice] rn
                              inner join risknotice_Signer rns on rns.rnid=rn.id
                              where rns.[t_phonenum]={0} and (rn.[t_state]>=3 or (rn.[t_state]=2 and (rns.t_type=2 or rns.t_type=1))or (rn.[t_state]=1 and rns.t_type=1) )
                              order by rn.t_createdt desc");

            sqlCount.Append(@" SELECT count(*) count
                               FROM [RiskNotice] rn
                               inner join risknotice_Signer rns on rns.rnid=rn.id
                               where rns.[t_phonenum]={0} and (rn.[t_state]>=3 or (rn.[t_state]=2 and (rns.t_type=2 or rns.t_type=1))or (rn.[t_state]=1 and rns.t_type=1) )");

            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows, phone);

            var tmp = SqlServerOperator.GetDataTable(sqlCount.ToString(), phone);
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 根据id查询编制人
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable QueryBzr(string id)
        {
            try
            {
                var sql = @"SELECT [t_operator]
                          FROM [RiskNotice]
                          where id={0}";
                DataTable dt = SqlServerOperator.GetDataTable(sql, id);
                return dt;
            }
            catch (Exception e)
            {
                Log.Debug("QueryBzr方法,错误原因：" + e.Message);
                return null;
            }
        }

        /// <summary>
        /// 根据id修改风险告知单状态
        /// </summary>
        /// <param name="Number"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public bool ModifyRNState(string id, int State)
        {
            try
            {
                var sql = new StringBuilder();
                var i = 0;
                var dList = new List<object>();
                sql.Append(" update RiskNotice set t_state={" + i++ + "},");

                dList.Add(State);
                if (sql.Length > 0)
                {
                    sql = sql.Remove(sql.Length - 1, 1);
                }
                sql.Append(" where ID={" + i++ + "}");
                dList.Add(id);
                Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
                SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
                return true;
            }
            catch (Exception e)
            {
                Log.Debug("ModifyRNState方法,错误原因：" + e.Message);
                return false;
            }
        }

        /// <summary>
        /// 根据手机号修改签字状态
        /// </summary>
        /// <param name="phoneNum"></param>
        /// <param name="rnid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool ModifySignerState(string phoneNum, string rnid)
        {
            Log.Debug("手机号：" + phoneNum + ",rnid=" + rnid);
            string imgname = phoneNum + "-" + rnid + ".png";
            string imgUrl = imgurl + "/" + imgname;
            try
            {
                var sql = new StringBuilder();
                var dList = new List<object>();
                sql.Append(" update RiskNotice_Signer set t_state=1,t_img={0},t_signerdt=getdate() ");
                sql.Append(" where t_phonenum={1} and RNID={2}");
                dList.Add(imgUrl);
                dList.Add(phoneNum);
                dList.Add(rnid);
                Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
                SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
                return true;
            }
            catch (Exception e)
            {
                Log.Debug("ModifySignerState方法,错误原因：" + e.Message);
                return false;
            }
        }


        /// <summary>
        /// 检测电话是否存在
        /// </summary>
        /// <param name="PhoneNum"></param>
        /// <returns></returns>
        public DataTable CheckPhone(string PhoneNum)
        {
            try
            {
                var sql = new StringBuilder();
                var i = 0;
                var dList = new List<object>();
                sql.Append(" select * from UserReg where PhoneNumber={" + i++ + "} ");
                dList.Add(PhoneNum);
                Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
                return SqlServerOperator.GetDataTable(sql.ToString(), dList.ToArray());
            }
            catch (Exception e)
            {
                Log.Debug("CheckPhone方法,错误原因：" + e.Message);
                return new DataTable();
            }
        }

        /// <summary>
        /// 根据通知单id查询签字信息
        /// </summary>
        /// <param name="PhoneNum"></param>
        /// <returns></returns>
        public DataTable QuerySinger(string id)
        {
            try
            {
                var sql = new StringBuilder();
                var i = 0;
                var dList = new List<object>();
                sql.Append(" select * from RiskNotice_Signer where RNID={" + i++ + "} ");
                dList.Add(id);
                Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
                return SqlServerOperator.GetDataTable(sql.ToString(), dList.ToArray());

            }
            catch (Exception e)
            {
                Log.Debug("QuerySinger方法,错误原因：" + e.Message);
                return new DataTable();
            }
        }

        public DataTable QueryRiskNoticeByID(string id)
        {
            try
            {
                var sql = new StringBuilder();
                var i = 0;
                var dList = new List<object>();
                sql.Append(" select * from RiskNotice where ID={" + i++ + "} ");
                dList.Add(id);
                Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
                return SqlServerOperator.GetDataTable(sql.ToString(), dList.ToArray());

            }
            catch (Exception e)
            {
                Log.Debug("QueryRiskNoticeByID方法,错误原因：" + e.Message);
                return new DataTable();
            }
        }


        /// <summary>
        /// 根据通知单编号修改通知单状态为[已发布]
        /// </summary>
        /// <param name="riskNotice"></param>
        /// <returns></returns>
        public bool UpdateRiskNoticeState(RiskNotice riskNotice)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("update RiskNotice set [t_state]={0},[t_send_dt]={1} where [id]={2}");
            try
            {
                SqlServerOperator.Execute(stringBuilder.ToString(), new object[] { riskNotice.State, riskNotice.Send_Dt, riskNotice.ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }


    }
}
