﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class UsersDal : BaseDal<Users>
    {
        public override bool Exist(Users bean)
        {
            Log.Debug("Exist方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [Id]
                          ,[loginname]
                          ,[password]
                      FROM [Users]
                      where loginname={0} and password={1}");
            var list = new List<object>();
            list.Add(bean.LoginName);
            list.Add(bean.Password);
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data.Rows.Count > 0;
        }

        public bool ExistLoginName(Users bean)
        {
            Log.Debug("Exist方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [Id]
                          ,[loginname]
                          ,[password]
                      FROM [Users]
                      where loginname={0}");
            var list = new List<object>();
            list.Add(bean.LoginName);
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data.Rows.Count > 0;
        }

        public DataTable QueryUserByLName(Users bean)
        {
            Log.Debug("Exist方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [Id]
                          ,[loginname]
                          ,[password]
                      FROM [Users]
                      where loginname={0}");
            var list = new List<object>();
            list.Add(bean.LoginName);
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data;
        }

        public override void Add(Users bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql1.Append(" Id,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.OpenId))
            {
                sql1.Append(" OpenId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.OpenId);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.LoginName))
            {
                sql1.Append(" LoginName,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.LoginName);
            }
            if (!string.IsNullOrEmpty(bean.Password))
            {
                sql1.Append(" Password,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Password);
            }
            if (!string.IsNullOrEmpty(bean.DepartmentId))
            {
                sql1.Append(" DepartmentId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.DepartmentId);
            }
            if (!string.IsNullOrEmpty(bean.RoleId))
            {
                sql1.Append(" RoleId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.RoleId);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [Users](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(Users bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(Users bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update [Users] set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.OpenId))
            {
                sql.Append(" OpenId={" + i++ + "},");
                dList.Add(bean.OpenId);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql.Append(" Name={" + i++ + "},");
                dList.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.Password))
            {
                sql.Append(" Password={" + i++ + "},");
                dList.Add(bean.Password);
            }
            if (!string.IsNullOrEmpty(bean.LoginName))
            {
                sql.Append(" LoginName={" + i++ + "},");
                dList.Add(bean.LoginName);
            }
            if (!string.IsNullOrEmpty(bean.RoleId))
            {
                sql.Append(" RoleId={" + i++ + "},");
                dList.Add(bean.RoleId);
            }

            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.Id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(Users bean)
        {
            var sql = new StringBuilder("select * from [Users] where 1=1");
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.LoginName))
            {
                sql.Append(" and LoginName={" + i++ + "}");
                list.Add(bean.LoginName);
            }
            return SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
        }

        public override DataTable QueryByPage(Users bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(Users bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }

        public DataTable QueryPageByDepartmentId(string departmentId, int page, int rows, ref int count)
        {
            var sql = @"SELECT u.[Id],u.[OpenId]
                              ,u.[name]
                                 ,u.loginname
                                ,[password]
                                ,d.name deptname
                                ,r.name rolename ";

            var sqlcount = @"SELECT count(*) count ";

            var sqlcommond = @" FROM [Users] u
                              inner join [Department] d on u.departmentid = d.id 
                              inner join [Roles] r on u.roleid = r.id
                              where departmentid={0}";
            var tmp = SqlServerOperator.GetDataTable(sqlcount + sqlcommond, departmentId);
            count = int.Parse(tmp.Rows[0]["count"].ToString());

            DataTable dtTable = SqlServerOperator.GetDataTableByPage(sql + sqlcommond, (page - 1) * rows, rows, departmentId);
            return dtTable;
        }

        /// <summary>
        /// 根据微信id查询
        /// </summary>
        /// <param name="wxid"></param>
        /// <returns></returns>
        public DataTable QueryByWxidAndMenuId(string wxid)
        {
            Log.Debug("QueryByWxidAndMenuId方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT distinct m.id,u.OpenId,u.name
                          FROM [Users] u
                          left join [Roles] r on u.RoleId=r.Id
                          left join [RoleMenu] rm on rm.RoleId = r.Id
                          left join [Menu] m on m.Id=rm.MenuId
                          where u.OpenId={0} and 
                          m.Id in ('3080','3081','3082')");
            var data = SqlServerOperator.GetDataTable(sql.ToString(), wxid);
            return data;
        }

        /// <summary>
        /// 根据登录名查询用户数据
        /// </summary>
        /// <param name="loginname"></param>
        /// <returns></returns>
        public DataTable QueryUserInfo(string loginname)
        {
            Log.Debug("QueryUserInfo方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" SELECT [name]
                          ,[loginname]
                          ,[password]
                      FROM [Users]
                      where loginname={0}");
            var data = SqlServerOperator.GetDataTable(sql.ToString(), loginname);
            return data;
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="bean"></param>
        public void ModifyPassword(Users bean)
        {
            Log.Debug("ModifyPassword方法参数：");
            var sql = new StringBuilder();
            sql.Append("update [Users] set");
            sql.Append(" Password={0}");
            sql.Append(" where loginname={1}");
            SqlServerOperator.Execute(sql.ToString(), new object[] { bean.Password, bean.LoginName });
        }

        /// <summary>
        /// 根据用户id重置密码
        /// </summary>
        /// <param name="id"></param>
        public void ResetPass(string id)
        {
            Log.Debug("ResetPass方法参数：");
            var sql = new StringBuilder();
            sql.Append(@" update Users set password='888888' where id={0}");
            SqlServerOperator.Execute(sql.ToString(), id);
        }
    }

}
