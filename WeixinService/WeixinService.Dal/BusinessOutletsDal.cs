﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class BusinessOutletsDal : BaseDal<BusinessOutlets>
    {
        public override bool Exist(BusinessOutlets bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(BusinessOutlets bean)
        {
            Log.Debug("Add方法参数：");
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql1.Append(" Id,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.AreaId))
            {
                sql1.Append(" AreaId,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.AreaId);
            }
            if (!string.IsNullOrEmpty(bean.Address))
            {
                sql1.Append(" Address,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Address);
            }
            if (!string.IsNullOrEmpty(bean.AreaName))
            {
                sql1.Append(" AreaName,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.AreaName);
            }
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql1.Append(" Name,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.Latitude))
            {
                sql1.Append(" Latitude,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Latitude);
            }
            if (!string.IsNullOrEmpty(bean.Longitude))
            {
                sql1.Append(" Longitude,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Longitude);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql1.Append(" Bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql1.Append(" Bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak2);
            }
            if (!string.IsNullOrEmpty(bean.Bak3))
            {
                sql1.Append(" Bak3,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak3);
            }
            if (!string.IsNullOrEmpty(bean.Bak4))
            {
                sql1.Append(" Bak4,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak4);
            }
            if (bean.Type != null)
            {
                sql1.Append(" Type,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Type);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into [BusinessOutlets](" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(BusinessOutlets bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from BusinessOutlets where Id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public override void Modify(BusinessOutlets bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update BusinessOutlets set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.Name))
            {
                sql.Append(" Name={" + i++ + "},");
                dList.Add(bean.Name);
            }
            if (!string.IsNullOrEmpty(bean.Latitude))
            {
                sql.Append(" Latitude={" + i++ + "},");
                dList.Add(bean.Latitude);
            }
            if (!string.IsNullOrEmpty(bean.Longitude))
            {
                sql.Append(" Longitude={" + i++ + "},");
                dList.Add(bean.Longitude);
            }
            if (!string.IsNullOrEmpty(bean.Address))
            {
                sql.Append(" Address={" + i++ + "},");
                dList.Add(bean.Address);
            }
            if (!string.IsNullOrEmpty(bean.AreaId))
            {
                sql.Append(" AreaId={" + i++ + "},");
                dList.Add(bean.AreaId);
            }
            if (!string.IsNullOrEmpty(bean.AreaName))
            {
                sql.Append(" AreaName={" + i++ + "},");
                dList.Add(bean.AreaName);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql.Append(" Bak1={" + i++ + "},");
                dList.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql.Append(" Bak2={" + i++ + "},");
                dList.Add(bean.Bak2);
            }
            if (!string.IsNullOrEmpty(bean.Bak3))
            {
                sql.Append(" Bak3={" + i++ + "},");
                dList.Add(bean.Bak3);
            }
            if (!string.IsNullOrEmpty(bean.Bak4))
            {
                sql.Append(" Bak4={" + i++ + "},");
                dList.Add(bean.Bak4);
            }
            if (bean.Type != null)
            {
                sql.Append(" Type={" + i++ + "},");
                dList.Add(bean.Type);
            }
            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.Id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(BusinessOutlets bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(BusinessOutlets bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(BusinessOutlets bean, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：" + bean);
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();

            sql.Append(@"SELECT [Id]
                          ,[AreaId]
                          ,[AreaName]
                          ,[Address]
                          ,[Name]
                          ,[Longitude]
                          ,[Latitude]
                          ,[bak1]
                          ,[bak2]
                          ,[bak3]
                          ,[bak4]
                      FROM [BusinessOutlets]");

            sqlcount.Append(@"SELECT count(id) count
                  FROM [BusinessOutlets]");
            var data = SqlServerOperator.GetDataTableByPage(sql.ToString(), (page - 1) * rows, rows);

            var tmp = SqlServerOperator.GetDataTable(sqlcount.ToString());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 根据经纬度查询营业网点信息
        /// </summary>
        /// <param name="jd">经度</param>
        /// <param name="wd">纬度</param>
        /// <returns></returns>
        public DataTable QueryByJdAndWd(float jd, float wd)
        {
            Log.Debug("QueryByWxid方法参数：");
            var sql = new StringBuilder();
            var list = new List<object>();
            sql.Append(@"SELECT [Id]
                          ,[AreaName]
                          ,[Address]
                          ,[Name]
                          ,[Longitude]
                          ,[Latitude]
                      FROM [anqingweixin].[dbo].[BusinessOutlets] bo
                      where ({0}-CONVERT(float, bo.Longitude)<=0.1 and {1}-CONVERT(float, bo.Longitude)>=-0.1)
                      and ({2}-CONVERT(float, bo.Latitude)<=0.1 and {3}-CONVERT(float, bo.Latitude)>=-0.1)");
            list.Add(jd);
            list.Add(jd);
            list.Add(wd);
            list.Add(wd);
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data;
        }
    }
}
