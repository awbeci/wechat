﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class PowerCutDal : BaseDal<PowerCut>
    {

        public override bool Exist(PowerCut bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(PowerCut bean)
        {
            Log.Debug("Add方法参数：" + bean.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            sql1.Append(" Id,");
            sql2.Append(" {" + i++ + "},");
            list.Add(string.IsNullOrEmpty(bean.Id) ? Guid.NewGuid().ToString() : bean.Id);

            if (!string.IsNullOrEmpty(bean.Title))
            {
                sql1.Append(" Title,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Title);
            }
            if (!string.IsNullOrEmpty(bean.State))
            {
                sql1.Append(" State,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.State);
            }
            if (!string.IsNullOrEmpty(bean.FlagRelease))
            {
                sql1.Append(" FlagRelease,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.FlagRelease);
            }
            if (!string.IsNullOrEmpty(bean.BusinessType))
            {
                sql1.Append(" BusinessType,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.BusinessType);
            }
            if (bean.CreateDt != DateTime.MinValue)
            {
                sql1.Append(" CreateDt,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.CreateDt);
            }
            if (!string.IsNullOrEmpty(bean.CreatePerson))
            {
                sql1.Append(" CreatePerson,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.CreatePerson);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql1.Append(" Bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql1.Append(" Bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak2);
            }
            if (!string.IsNullOrEmpty(bean.Bak3))
            {
                sql1.Append(" Bak3,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak3);
            }
            if (!string.IsNullOrEmpty(bean.Bak4))
            {
                sql1.Append(" Bak4,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Bak4);
            }
            if (!string.IsNullOrEmpty(bean.SendGroup))
            {
                sql1.Append(" SendGroup,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.SendGroup);
            }
            if (!string.IsNullOrEmpty(bean.SendPerson))
            {
                sql1.Append(" SendPerson,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.SendPerson);
            }
            if (bean.PowerCutTime != DateTime.MinValue)
            {
                sql1.Append(" PowerCutTIme,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.PowerCutTime);
            }
            if (!string.IsNullOrEmpty(bean.TimeArea))
            {
                sql1.Append(" TimeArea,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.TimeArea);
            }
            if (!string.IsNullOrEmpty(bean.Area))
            {
                sql1.Append(" Area,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Area);
            }
            if (!string.IsNullOrEmpty(bean.Device))
            {
                sql1.Append(" Device,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Device);
            }
            if (!string.IsNullOrEmpty(bean.Jd))
            {
                sql1.Append(" Jd,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Jd);
            }
            if (!string.IsNullOrEmpty(bean.Wd))
            {
                sql1.Append(" Wd,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Wd);
            }
            if (!string.IsNullOrEmpty(bean.Description))
            {
                sql1.Append(" Description,");
                sql2.Append(" {" + i++ + "},");
                list.Add(bean.Description);
            }

            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into PowerCut(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(PowerCut bean)
        {
            Log.Debug("del方法参数：");
            var sql = "delete from PowerCut where Id={0}";
            SqlServerOperator.Execute(sql, bean.Id);
        }

        public override void Modify(PowerCut bean)
        {
            Log.Debug("Modify方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append("update PowerCut set");
            var i = 0;
            var dList = new List<object>();
            if (!string.IsNullOrEmpty(bean.Title))
            {
                sql.Append(" Title={" + i++ + "},");
                dList.Add(bean.Title);
            }
            if (!string.IsNullOrEmpty(bean.State))
            {
                sql.Append(" State={" + i++ + "},");
                dList.Add(bean.State);
            }
            if (!string.IsNullOrEmpty(bean.FlagRelease))
            {
                sql.Append(" FlagRelease={" + i++ + "},");
                dList.Add(bean.FlagRelease);
            }
            if (!string.IsNullOrEmpty(bean.BusinessType))
            {
                sql.Append(" BusinessType={" + i++ + "},");
                dList.Add(bean.BusinessType);
            }
            if (bean.CreateDt != null)
            {
                sql.Append(" CreateDt={" + i++ + "},");
                dList.Add(bean.CreateDt);
            }
            if (!string.IsNullOrEmpty(bean.CreatePerson))
            {
                sql.Append(" CreatePerson={" + i++ + "},");
                dList.Add(bean.CreatePerson);
            }
            if (!string.IsNullOrEmpty(bean.Bak1))
            {
                sql.Append(" Bak1={" + i++ + "},");
                dList.Add(bean.Bak1);
            }
            if (!string.IsNullOrEmpty(bean.Bak2))
            {
                sql.Append(" Bak2={" + i++ + "},");
                dList.Add(bean.Bak2);
            }
            if (!string.IsNullOrEmpty(bean.Bak3))
            {
                sql.Append(" Bak3={" + i++ + "},");
                dList.Add(bean.Bak3);
            }
            if (!string.IsNullOrEmpty(bean.Bak4))
            {
                sql.Append(" Bak4={" + i++ + "},");
                dList.Add(bean.Bak4);
            }
            if (!string.IsNullOrEmpty(bean.SendGroup))
            {
                sql.Append(" SendGroup={" + i++ + "},");
                dList.Add(bean.SendGroup);
            }
            if (!string.IsNullOrEmpty(bean.SendPerson))
            {
                sql.Append(" SendPerson={" + i++ + "},");
                dList.Add(bean.SendPerson);
            }

            if (bean.PowerCutTime != DateTime.MinValue)
            {
                sql.Append(" PowerCutTime={" + i++ + "},");
                dList.Add(bean.PowerCutTime);
            }
            if (!string.IsNullOrEmpty(bean.TimeArea))
            {
                sql.Append(" TimeArea={" + i++ + "},");
                dList.Add(bean.TimeArea);
            }
            if (!string.IsNullOrEmpty(bean.Area))
            {
                sql.Append(" Area={" + i++ + "},");
                dList.Add(bean.Area);
            }
            if (!string.IsNullOrEmpty(bean.Device))
            {
                sql.Append(" Device={" + i++ + "},");
                dList.Add(bean.Device);
            }
            if (!string.IsNullOrEmpty(bean.Jd))
            {
                sql.Append(" Jd={" + i++ + "},");
                dList.Add(bean.Jd);
            }
            if (!string.IsNullOrEmpty(bean.Wd))
            {
                sql.Append(" Wd={" + i++ + "},");
                dList.Add(bean.Wd);
            }
            if (!string.IsNullOrEmpty(bean.Description))
            {
                sql.Append(" Description={" + i++ + "},");
                dList.Add(bean.Description);
            }

            sql = sql.Remove(sql.Length - 1, 1);
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.Id);
            Log.Debug("SQL :" + sql + ",params:" + dList);
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override DataTable Query(PowerCut bean)
        {
            Log.Debug("Query方法参数：" + bean);
            var sql = new StringBuilder();
            var i = 0;
            var list = new List<object>();
            sql.Append(@"SELECT * FROM [PowerCut] where 1=1 ");
            if (!string.IsNullOrEmpty(bean.Id))
            {
                sql.Append(" and id={" + i++ + "}");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.FlagRelease))
            {
                sql.Append(" and FlagRelease in (" + bean.FlagRelease + ")");
            }
            if (!string.IsNullOrEmpty(bean.State))
            {
                sql.Append(" and State={" + i++ + "}");
                list.Add(bean.State);
            }
            if (!string.IsNullOrEmpty(bean.BusinessType))
            {
                sql.Append(" and [BusinessType]={" + i++ + "} ");
                list.Add(bean.BusinessType);
            }
            if (bean.DateBegin != DateTime.MinValue)
            {
                sql.Append(" and [PowerCutTime]>={" + i++ + "} ");
                list.Add(bean.DateBegin);
            }
            if (bean.DateEnd != DateTime.MinValue)
            {
                sql.Append(" and [PowerCutTime]<{" + i++ + "} ");
                list.Add(bean.DateEnd.AddDays(1));
            }
            sql.Append(" order by PowerCutTime asc ");
            var data = SqlServerOperator.GetDataTable(sql.ToString(), list.ToArray());
            return data;
        }

        public override DataTable QueryByPage(PowerCut bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(PowerCut bean, int page, int rows, ref int count)
        {
            Log.Debug("QueryByPage方法参数：" + bean);
            var sql = new StringBuilder();
            var sqlcount = new StringBuilder();
            var list = new List<object>();
            var i = 0;

            sql.Append(@"SELECT * FROM [PowerCut] where 1=1 ");
            var sqlWhere = new StringBuilder();

            if (!string.IsNullOrEmpty(bean.Id))
            {
                sqlWhere.Append(" and id={" + i++ + "}");
                list.Add(bean.Id);
            }
            if (!string.IsNullOrEmpty(bean.FlagRelease))
            {
                sqlWhere.Append(" and FlagRelease in (" + bean.FlagRelease + ")");
            }
            if (!string.IsNullOrEmpty(bean.State))
            {
                sqlWhere.Append(" and State={" + i++ + "}");
                list.Add(bean.State);
            }
            if (!string.IsNullOrEmpty(bean.BusinessType))
            {
                sqlWhere.Append(" and [BusinessType]={" + i++ + "} ");
                list.Add(bean.BusinessType);
            }
            if (bean.DateBegin != DateTime.MinValue)
            {
                sqlWhere.Append(" and [PowerCutTime]>={" + i++ + "} ");
                list.Add(bean.DateBegin);
            }
            if (bean.DateEnd != DateTime.MinValue)
            {
                sqlWhere.Append(" and [PowerCutTime]<{" + i++ + "} ");
                list.Add(bean.DateEnd.AddDays(1));
            }

            sqlcount.Append(@"SELECT count(id) count FROM [PowerCut] where 1=1");
            var data = SqlServerOperator.GetDataTableByPage(sql.Append(sqlWhere).Append("order by PowerCutTime desc").ToString(), (page - 1) * rows, rows, list.ToArray());
            Log.Debug(sql.ToString());
            var tmp = SqlServerOperator.GetDataTable(sqlcount.Append(sqlWhere).ToString(), list.ToArray());
            count = int.Parse(tmp.Rows[0]["count"].ToString());
            return data;
        }

        /// <summary>
        /// 根据id查询停电信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public DataTable QueryDataById(string id, string key)
        {
            Log.Debug("QueryDataById方法参数：");
            var sql = new StringBuilder();
            sql.Append(@"   select *
                              from PowerCut pc
                              where pc.PowerCutTime in (select PowerCutTime 
                              from PowerCut pcs 
                              where Id={0}
                              ) and BusinessType={1}
                            ");
            return SqlServerOperator.GetDataTable(sql.ToString(), new object[] { id, key });
        }
    }
}
