﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WeixinService.Model.common;
using System.Data;

namespace WeixinService.Dal
{
    public class MenuEventLogDal : BaseDal<MenuEventLog>
    {
        public override bool Exist(MenuEventLog menuLog)
        {
            throw new NotImplementedException();
        }

        public override void Add(MenuEventLog menuLog)
        {
            Log.Debug("Add方法参数：" + menuLog.ToString());
            var sql1 = new StringBuilder();
            var sql2 = new StringBuilder();
            var i = 0;
            var list = new List<object>();

            Guid gid = Guid.NewGuid();

            sql1.Append(" id,");
            sql2.Append(" {" + i++ + "},");
            list.Add(gid.ToString());

            sql1.Append(" FromUserName,");
            sql2.Append(" {" + i++ + "},");
            list.Add(menuLog.FromUserName);

            sql1.Append(" clickTime,");
            sql2.Append(" {" + i++ + "},");
            list.Add(menuLog.clickTime.ToString());


            sql1.Append(" menuId,");
            sql2.Append(" {" + i++ + "},");
            list.Add(menuLog.menuId);


            if (!string.IsNullOrEmpty(menuLog.bak1))
            {
                sql1.Append(" bak1,");
                sql2.Append(" {" + i++ + "},");
                list.Add(menuLog.bak1);
            }
            if (!string.IsNullOrEmpty(menuLog.bak2))
            {
                sql1.Append(" bak2,");
                sql2.Append(" {" + i++ + "},");
                list.Add(menuLog.bak2);
            }
            if (sql1.Length > 0)
            {
                sql1 = sql1.Remove(sql1.Length - 1, 1);
                sql2 = sql2.Remove(sql2.Length - 1, 1);
            }

            var sql = "insert into MenuEventLog(" + sql1 + ") values(" + sql2 + ")";
            Log.Debug("SQL :" + sql + ",params:" + list.ToString());
            SqlServerOperator.Execute(sql, list.ToArray());
        }

        public override void Del(MenuEventLog menuLog)
        {
            throw new NotImplementedException();
        }

        public override void Modify(MenuEventLog menuLog)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(MenuEventLog menuLog)
        {
            throw new NotImplementedException();
        }


        public override DataTable QueryByPage(MenuEventLog menuLog, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(MenuEventLog menuLog, int page, int rows, ref int count)
        {
            throw new NotImplementedException();

        }

    }
}
