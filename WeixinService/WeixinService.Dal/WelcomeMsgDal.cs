﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeixinService.Model;

namespace WeixinService.Dal
{
    public class WelcomeMsgDal : BaseDal<WelcomeMsg>
    {
        public override bool Exist(WelcomeMsg bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(WelcomeMsg bean)
        {
            throw new NotImplementedException();
        }

        public override void Del(WelcomeMsg bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(WelcomeMsg bean)
        {
            Log.Debug("Modify方法参数：" + bean.ToString());
            var sql = new StringBuilder();
            var i = 0;
            var dList = new List<object>();
            sql.Append(" update SysConfig set [content]={" + i++ + "},");
            dList.Add(bean.content);
            if (sql.Length > 0)
            {
                sql = sql.Remove(sql.Length - 1, 1);
            }
            sql.Append(" where id={" + i++ + "}");
            dList.Add(bean.id);
            Log.Debug("SQL :" + sql + ",params:" + dList.ToString());
            SqlServerOperator.Execute(sql.ToString(), dList.ToArray());
        }

        public override System.Data.DataTable Query(WelcomeMsg bean)
        {
            Log.Debug("QueryByPage方法参数：" + bean);
            var sql = new StringBuilder();
            sql.Append(@"SELECT TOP 1000 [id]
                                      ,[type]
                                      ,[name]
                                      ,[Content]
                                      ,[bak1]
                                      ,[bak2]
                                  FROM [SysConfig] where [type]='1'");
            var data = SqlServerOperator.GetDataTable(sql.ToString());
            return data;
        }

        public override System.Data.DataTable QueryByPage(WelcomeMsg bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override System.Data.DataTable QueryByPage(WelcomeMsg bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }
    }
}
