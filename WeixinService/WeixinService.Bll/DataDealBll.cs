﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Xml;
using WeixinService.Dal;
using WeixinService.Model.common;
using log4net;
using WeixinService.Model;

namespace WeixinService.Bll
{
    public class DataDealBll
    {
        readonly string _ip1 = System.Configuration.ConfigurationManager.AppSettings["ip1"];
        readonly string _ip2 = System.Configuration.ConfigurationManager.AppSettings["ip2"];

        /// <summary>
        /// 获取的数据是否正确
        /// </summary>
        private bool isMsgTrue { get; set; }

        /// <summary>
        /// 接收到的消息的类型
        /// </summary>
        private MessageType msgType { get; set; }

        /// <summary>
        /// xml文档
        /// </summary>
        private XmlDocument doc { get; set; }

        /// <summary>
        /// 接收到的信息
        /// </summary>
        public ReceiveMsg ReceiveMsgModel { get; set; }

        private static ILog log = log4net.LogManager.GetLogger("DataDealBll");

        /// <summary>
        /// 构造函数，
        /// </summary>
        /// <param name="xmlData">xml数据</param>
        public DataDealBll(string xmlData)
        {
            doc = new XmlDocument();
            try
            {
                xmlData = xmlData.Replace("<![CDATA[", "");
                xmlData = xmlData.Replace("]]>", "");
                if (xmlData.Contains("&"))
                    xmlData = xmlData.Replace("&", "&amp;");
                doc.LoadXml(xmlData);
                msgType = AnalyseMsgType();
                isMsgTrue = true;
            }
            catch (System.Exception ex)
            {
                log.Error(ex.Message);
                isMsgTrue = false;
            }
        }

        public MessageType GetMsgType()
        {
            return msgType;
        }

        public bool IsMessageTrue()
        {
            return isMsgTrue;
        }

        /********** 主方法：判断消息类型、获取消息信息model*******/
        /// <summary>
        /// 分析接受过来的xml数据
        /// </summary>
        private MessageType AnalyseMsgType()
        {
            var root = doc.DocumentElement;
            XmlNode msgTypeNode = root.SelectSingleNode("MsgType");
            string msgTypeValue = msgTypeNode.InnerText;
            DataType dataType = GetMsgOrEventType(msgTypeValue);
            MessageType msgType = MessageType.NULL;
            if (dataType.Equals(DataType.Message))
            {
                msgType = GetMsgDataType(msgTypeValue);
            }
            else if (dataType.Equals(DataType.Event))
            {
                XmlNode eventNode = root.SelectSingleNode("Event");
                string strEvent = eventNode.InnerText;
                msgType = GetEventDataType(strEvent);
            }
            else
            {
                msgType = MessageType.NULL;
            }
            return msgType;

        }


        /*********** 具体实现的方法 **************/

        /*********** 判断具体的消息类型 **************/

        /// <summary>
        /// 判断post的数据类别是事件还是消息
        /// </summary>
        /// <param name="data">事件的数据值</param>
        /// <returns></returns>
        private DataType GetMsgOrEventType(string data)
        {
            DataType resultType = DataType.NULL;
            switch (data)
            {
                case "event":
                    {
                        resultType = DataType.Event;
                        break;
                    }
                default:
                    {
                        resultType = DataType.Message;
                        break;
                    }
            }
            return resultType;
        }

        /// <summary>
        /// 获取消息的具体类别
        /// </summary>
        /// <param name="msgData">消息的类别值</param>
        /// <returns></returns>
        private MessageType GetMsgDataType(string msgData)
        {
            ReceiveMsgBll recMsgBll = new ReceiveMsgBll();
            ReceiveMsg receiveMsgModel = null;
            MessageType resultType = MessageType.MsgText;
            switch (msgData)
            {
                case "text":
                    {
                        resultType = MessageType.MsgText;
                        receiveMsgModel = GetTextModel();
                        break;
                    }
                case "image":
                    {
                        resultType = MessageType.MsgImage;
                        receiveMsgModel = GetImageModel();
                        break;
                    }
                case "voice":
                    {
                        resultType = MessageType.MsgVoice;
                        receiveMsgModel = GetVoiceModel();
                        break;
                    }
                case "video":
                    {
                        resultType = MessageType.MsgVideo;
                        receiveMsgModel = GetVideoModel();
                        break;
                    }
                case "location":
                    {
                        resultType = MessageType.MsgLocation;
                        receiveMsgModel = GetLocationModel();
                        break;
                    }
                case "link":
                    {
                        resultType = MessageType.MsgLink;
                        break;
                    }
                default:
                    {
                        resultType = MessageType.NULL;
                        break;
                    }
            }
            if (receiveMsgModel != null)
            {
                recMsgBll.Add(receiveMsgModel);
                ReceiveMsgModel = receiveMsgModel;
            }
            return resultType;
        }

        /// <summary>
        /// 获取事件的类别
        /// </summary>
        /// <param name="eventData">事件的类别值</param>
        /// <returns></returns>
        private MessageType GetEventDataType(string eventData)
        {
            MessageType resultType = MessageType.EventClick;
            var sendMessage = new SendMessage();
            switch (eventData)
            {
                case "CLICK":
                    {
                        resultType = MessageType.EventClick;
                        MenuEventLog menuEventLogModel = GetMenuEventLogModel(resultType);
                        MenuEventLogBll menuBll = new MenuEventLogBll();
                        menuBll.Add(menuEventLogModel);
                        log.Debug("菜单key：" + menuEventLogModel.menuId);
                        log.Debug("用户id：" + menuEventLogModel.FromUserName);
                        var listnews = new List<Articles>();
                        var articles = new Articles();
                        sendMessage.SendTxtMessage(menuEventLogModel.FromUserName, "系统升级中。");
                        //switch (menuEventLogModel.menuId)
                        //{
                        //    //这里风险告知信息和高压用户注册合并了，可以看case "025_youo_thzc":那行代码
                        //    case "023_youo_ydfx":
                        //        //sendMessage.SendNewsMessageFxgz(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "风险告知信息", "1", "5");
                        //        break;
                        //    case "011_youo_tdtz":
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "计划停电信息", "1", "5");
                        //        break;
                        //    case "011_youo_tdtz2":
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "故障停电信息", "1", "5");
                        //        break;
                        //    case "012_youo_djzc":
                        //        //注意：电价政策，用电知识和营业网点三个菜单现在已经合并到 key="013_youo_jdzs",详细请见key="013_youo_jdzs"
                        //        // sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "电价政策信息", "1", "5");
                        //        break;
                        //    case "013_youo_jdzs":
                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "用电知识",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            description = ""
                        //        });

                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "最新资讯",
                        //            description = "查询最新资讯信息",
                        //            url = _ip1 + "/views/messagelist/messagelist.htm?key=091_youo_zxzx&wxid=" + menuEventLogModel.FromUserName + "&name=最新资讯信息&flag=1",
                        //            picurl = _ip1 + "/UploadImages/091_youo_zxzx.jpg"
                        //        });

                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "电价政策",
                        //            description = "查询电价政策信息",
                        //            url = _ip1 + "/views/messagelist/messagelist.htm?key=012_youo_djzc&wxid=" + menuEventLogModel.FromUserName + "&name=电价政策信息&flag=1",
                        //            picurl = _ip1 + "/UploadImages/012_youo_djzc.jpg"
                        //        });

                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "用电常识",
                        //            description = "查询更多用电常识信息",
                        //            url = _ip1 + "/views/messagelist/messagelist.htm?key=013_youo_jdzs&wxid=" + menuEventLogModel.FromUserName + "&name=用电常识信息&flag=1",
                        //            picurl = _ip1 + "/UploadImages/013_youo_jdzs.jpg"
                        //        });

                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "营业网点",
                        //            description = "查询更多营业网点信息",
                        //            url = _ip1 + "/views/messagelist/messagelist.htm?key=015_youo_yywd&wxid=" + menuEventLogModel.FromUserName + "&name=营业网点信息&flag=1",
                        //            picurl = _ip1 + "/UploadImages/015_youo_yywd.jpg"
                        //        });
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, listnews);
                        //        //sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "用电知识信息", "1", "5");
                        //        break;
                        //    case "015_youo_yywd":
                        //        //注意：电价政策，用电知识和营业网点三个菜单现在已经合并到 key="013_youo_jdzs",详细请见key="013_youo_jdzs"
                        //        //sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, menuEventLogModel.menuId, "营业网点信息", "1", "5");
                        //        break;
                        //    case "025_youo_thzc":

                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "风险告知",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            description = ""
                        //        });

                        //        UserRegBll userRegBLL = new UserRegBll();
                        //        UserReg userRegModel = new UserReg();
                        //        userRegModel.Wxid = menuEventLogModel.FromUserName;
                        //        DataTable dt = userRegBLL.Query(userRegModel);
                        //        string strPhone = "";
                        //        if (dt != null && dt.Rows.Count > 0)
                        //        {
                        //            strPhone = dt.Rows[0]["PhoneNumber"].ToString();
                        //        }
                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "风险预警通知单列表",
                        //            description = "风险预警通知单列表",
                        //            //url = _ip1 + "/views/messagelist/messagelist.htm?key=023_youo_ydfx&wxid=" + menuEventLogModel.FromUserName + "&name=风险告知信息&flag=0",
                        //            //url = _ip1 + "/views/app/app.htm",
                        //            url = _ip1 + "/views/qm/tzdlist.htm?phone=" + strPhone,
                        //            picurl = _ip1 + "/UploadImages/023_youo_ydfx.jpg"
                        //        });

                        //        //高压用户注册之前是写在 click事件里面的，现在放到风险告知里面
                        //        listnews.Add(new Articles()
                        //        {
                        //            title = "高压用户注册",
                        //            description = @"温馨提示:尊敬的客户：为保证您的用电安全，方便我们及时告知您用电风险，请专线大客户注册。请您按要求录入相关信息，每个单位请至少两人（用电分管领导、具体负责人）注册。谢谢！",
                        //            picurl = _ip1 + "/UploadImages/025_youo_thzc.jpg",
                        //            url = _ip1 + "/views/User/UserReg.html?key=025_youo_thzc&wxid=" + menuEventLogModel.FromUserName
                        //        });
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, listnews);
                        //        break;
                        //    case "101_kh_dyxx":
                        //        articles = new Articles()
                        //        {
                        //            title = "短信订阅",
                        //            description = @"温馨提示: 您关注“国网铜陵供电公司”微信公众号为您短信自助订阅，订阅的客户，我们将为您提供电费信息自动发送，欠费信息自主提醒，让您及时方便了解用电信息！",
                        //            picurl = _ip1 + "/UploadImages/016_kh_qfcx.jpg",
                        //            url = _ip2 + "/SubscribeInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    case "011_kh_hhbd":
                        //        articles = new Articles()
                        //        {
                        //            title = "户号绑定",
                        //            description = @"温馨提示: 您关注“国网铜陵供电公司”微信公众号并申请户号绑定后，可进行电量电费、阶梯电量、停电信息等服务信息查询。",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip2 + "/WeixinBind.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    case "012_kh_yfdl":
                        //        articles = new Articles()
                        //        {
                        //            title = "电费电量",
                        //            description = @"温馨提示: 电量电费栏目可以清晰查看到您的用电状况，包括电表指数、合计电量、应缴电费及当期（两月一计）居民阶梯用电、分时用电等状况。",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip2 + "/ElecBillInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    case "013_kh_yhda":
                        //        articles = new Articles()
                        //        {
                        //            title = "用户档案",
                        //            description = @"温馨提示: 用户档案栏目提供客户用电资料的基本信息查询，包括用电地址、用户分类、用电类别、电压等级、计量点信息等。",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip2 + "/ElecConsInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    //case "014_kh_jtsf":
                        //    //    articles = new Articles()
                        //    //    {
                        //    //        title = "阶梯算费",
                        //    //        description = @"温馨提示: 阶梯算费栏目根据安徽省阶梯电价政策及您的用电量进行电费模拟计算。",
                        //    //        picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //    //        url = @"http://testweixin.anhuihuahong.com/hbdl_wxyx/hbdl_wap/hbdlPowerPrice?openid=" + menuEventLogModel.FromUserName
                        //    //    };
                        //    //    sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //    //    break;
                        //    //case "015_kh_jtdl":
                        //    //    articles = new Articles()
                        //    //    {
                        //    //        title = "阶梯电量",
                        //    //        description = @"温馨提示: 阶梯电量按年累计结算，居民用户年累计用电量在2160度(180度×12月)以内部分，按第一档电价执行;2161度～4200度(350度×12月)部分，每度电加价5分钱;年累计用电量超过4200度后，则每度电加价0.3元。",
                        //    //        picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //    //        url = @"http://218.22.27.253:8893/ElecLevelInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //    //    };
                        //    //    sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //    //    break;
                        //    case "016_kh_qfcx":
                        //        articles = new Articles()
                        //        {
                        //            title = "欠费查询",
                        //            description = @"温馨提示: 欠费信息查询。",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip2 + "/OweAmtInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    case "017_kh_jfjl":
                        //        articles = new Articles()
                        //        {
                        //            title = "缴费记录",
                        //            description = @"温馨提示: 缴费信息查询。",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip2 + "/ChargeAmtInfo.aspx?openid=" + menuEventLogModel.FromUserName
                        //        };
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, articles);
                        //        break;
                        //    case "320_youo_nbbg":
                        //        var userGroupDal = new UserGroupDal();
                        //        var usersDal = new UsersDal();
                        //        articles = new Articles()
                        //            {
                        //                title = "内部办公",
                        //                description = "",
                        //                picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg"
                        //            };
                        //        listnews.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "内部用户注册",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/User/InternalUserReg.htm?key=" + menuEventLogModel.menuId + "&wxid=" + menuEventLogModel.FromUserName
                        //        };
                        //        listnews.Add(articles);
                        //        var data = userGroupDal.QueryPersonByGroupid(menuEventLogModel.FromUserName);
                        //        if (data)
                        //        {
                        //            articles = new Articles()
                        //            {
                        //                title = "主网跳闸信息列表",
                        //                description = "",
                        //                url = _ip1 + "/views/messagelist/messagelist.htm?key=3080_youo_zwtz&name=主网跳闸信息列表&flag=1"
                        //            };
                        //            listnews.Add(articles);
                        //            articles = new Articles()
                        //            {
                        //                title = "配网跳闸信息列表",
                        //                description = "",
                        //                url = _ip1 + "/views/messagelist/messagelist.htm?key=3081_youo_pwtz&name=配网跳闸信息列表&flag=1"
                        //            };
                        //            listnews.Add(articles);
                        //            articles = new Articles()
                        //            {
                        //                title = "监控信息列表",
                        //                description = "",
                        //                url = _ip1 + "/views/messagelist/messagelist.htm?key=3082_youo_jkxx&name=监控信息列表&flag=1"
                        //            };
                        //            listnews.Add(articles);
                        //            var data2 = usersDal.QueryByWxidAndMenuId(menuEventLogModel.FromUserName);
                        //            foreach (DataRow dataRow in data2.Rows)
                        //            {
                        //                switch (dataRow["id"].ToString())
                        //                {
                        //                    case "3080":
                        //                        articles = new Articles()
                        //                        {
                        //                            title = "主网跳闸信息发布",
                        //                            description = "",
                        //                            url = _ip1 + "/views/publishinfoTophone/publishTophone.htm?key=3080_youo_zwtz&name=主网跳闸信息发布&people=" + dataRow["name"]
                        //                        };
                        //                        listnews.Add(articles);
                        //                        break;
                        //                    case "3081":
                        //                        articles = new Articles()
                        //                           {
                        //                               title = "配网跳闸信息发布",
                        //                               description = "",
                        //                               url = _ip1 + "/views/publishinfoTophone/publishTophone.htm?key=3081_youo_pwtz&name=配网跳闸信息发布&people=" + dataRow["name"]
                        //                           };
                        //                        listnews.Add(articles);
                        //                        break;
                        //                    case "3082":
                        //                        articles = new Articles()
                        //                        {
                        //                            title = "监控信息发布",
                        //                            description = "",
                        //                            url = _ip1 + "/views/publishinfoTophone/publishTophone.htm?key=3082_youo_jkxx&name=监控信息发布&people=" + dataRow["name"]
                        //                        };
                        //                        listnews.Add(articles);
                        //                        break;
                        //                    default: break;
                        //                }
                        //            }
                        //        }
                        //        //else
                        //        //{
                        //        //    sendMessage.SendTxtMessage(menuEventLogModel.FromUserName, "您还不是内部用户，不支持此功能");
                        //        //}
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, listnews);

                        //        break;
                        //    case "000_youo_help":
                        //        var listnewshelp = new List<Articles>();
                        //        articles = new Articles()
                        //        {
                        //            title = "新手指导",
                        //            description = "",
                        //            picurl = _ip1 + "/UploadImages/" + menuEventLogModel.menuId + ".jpg"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何查找客户编号",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/serchCustomerNumber.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何获取用电查询密码",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/GetSelectPassword.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何绑定客户编号",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/HowBindUser.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何设置默认客户",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/HowBindDefaultUser.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何查询计划停电",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/PlanCutPower.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何查询故障停电",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/DefaultCutPower.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        articles = new Articles()
                        //        {
                        //            title = "如何注册高压用户",
                        //            description = "",
                        //            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                        //            url = _ip1 + "/views/help/HighVoltageUserReg.htm"
                        //        };
                        //        listnewshelp.Add(articles);
                        //        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, listnewshelp);
                        //        break;
                        //}//switch
                        break;
                    }
                case "VIEW":
                    {
                        resultType = MessageType.EventView;
                        MenuEventLog menuEventLogModel = GetMenuEventLogModel(resultType);
                        MenuEventLogBll menuBll = new MenuEventLogBll();
                        menuBll.Add(menuEventLogModel);
                        break;
                    }
                case "subscribe":
                    {
                        resultType = MessageType.EventSubscribe;
                        Subscribe();//todo:关注事件
                        var listnewshelp = new List<Articles>();
                        var articles = new Articles();
                        articles = new Articles()
                        {
                            title = "新手指导",
                            description = "",
                            picurl = _ip1 + "/UploadImages/000_youo_help.jpg"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何查找客户编号",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/serchCustomerNumber.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何获取用电查询密码",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/GetSelectPassword.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何绑定客户编号",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/HowBindUser.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何设置默认客户",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/HowBindDefaultUser.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何查询计划停电",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/PlanCutPower.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何查询故障停电",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/DefaultCutPower.htm"
                        };
                        listnewshelp.Add(articles);
                        articles = new Articles()
                        {
                            title = "如何注册高压用户",
                            description = "",
                            // picurl = "http://218.22.27.236/tl/UploadImages/" + menuEventLogModel.menuId + ".jpg",
                            url = _ip1 + "/views/help/HighVoltageUserReg.htm"
                        };
                        listnewshelp.Add(articles);
                        MenuEventLog menuEventLogModel = GetMenuEventLogModel(resultType);
                        sendMessage.SendNewsMessage(menuEventLogModel.FromUserName, listnewshelp);
                        break;
                    }
                case "unsubscribe":
                    {
                        resultType = MessageType.EventUnsubscribe;
                        UnSubscribe();
                        break;
                    }
                case "SCAN":
                    {
                        resultType = MessageType.EventScan;
                        break;
                    }
                case "LOCATION"://todo:地理位置推送事件用户同意上报地理位置后，每次进入公众号会话时，都会在进入时上报地理位置，或在进入会话后每5秒上报一次地理位置，公众号可以在公众平台网站中修改以上设置。上报地理位置时，微信会将上报地理位置事件推送到开发者填写的URL。
                    {
                        resultType = MessageType.EventLocation;
                        //GetLocationModel();
                        break;
                    }

                default:
                    {
                        resultType = MessageType.NULL;
                        break;
                    }
            }
            return resultType;
        }


        /**********  事件处理方法 **************/
        /// <summary>
        /// 关注时候的操作
        /// </summary>
        private void Subscribe()
        {
            if (IsExistUser())
                return;
            UserInOrOut userInorOutModel = GetUserInModel();

            UserInfoBll userBll = new UserInfoBll();
            UserInfo user = userBll.GetUserInfo(userInorOutModel.userId);
            userInorOutModel.userName = user.nickname;

            UserInOrOutBll userInOrOutBll = new UserInOrOutBll();
            userInOrOutBll.Add(userInorOutModel);

            user = new UserInfo();
            user = userBll.GetUserInfo(userInorOutModel.userId);
            userBll.Add(user);
        }

        /// <summary>
        /// 取消关注的方法
        /// </summary>
        private void UnSubscribe()
        {
            UserInOrOut userInorOutModel = GetUserOutModel();

            UserInfo user = new UserInfo();
            user.openid = userInorOutModel.userId;

            UserInfoBll userBll = new UserInfoBll();
            DataTable userTable = userBll.GetUserTable(user);

            userInorOutModel.userName = userTable.Rows[0]["nickname"].ToString();
            userInorOutModel.groupID = userTable.Rows[0]["groupId"].ToString();
            userInorOutModel.groupName = userTable.Rows[0]["groupName"].ToString();
            UserInOrOutBll userInOrOutBll = new UserInOrOutBll();
            userInOrOutBll.Add(userInorOutModel);

            user = new UserInfo();
            user.openid = userInorOutModel.userId;
            userBll.Del(user);
        }

        /********** 获取具体的消息model **********/

        /// <summary>
        /// 获取文本消息
        /// </summary>
        /// <returns></returns>
        private ReceiveMsg GetTextModel()
        {
            ReceiveMsg receiveMsg = new ReceiveMsg();
            var root = doc.DocumentElement;
            receiveMsg.ToUserName = root.SelectSingleNode("ToUserName").InnerText;
            receiveMsg.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            receiveMsg.CreateTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText.Trim());
            receiveMsg.MsgType = root.SelectSingleNode("MsgType").InnerText;
            receiveMsg.Content = root.SelectSingleNode("Content").InnerText;
            receiveMsg.MsgId = root.SelectSingleNode("MsgId").InnerText;

            return receiveMsg;
        }

        /// <summary>
        /// 获取图片消息
        /// </summary>
        /// <returns></returns>
        private ReceiveMsg GetImageModel()
        {
            ReceiveMsg receiveMsg = new ReceiveMsg();
            var root = doc.DocumentElement;
            receiveMsg.ToUserName = root.SelectSingleNode("ToUserName").InnerText;
            receiveMsg.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            receiveMsg.CreateTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText.Trim());
            receiveMsg.MsgType = root.SelectSingleNode("MsgType").InnerText;
            receiveMsg.PicUrl = root.SelectSingleNode("PicUrl").InnerText;
            receiveMsg.MediaId = root.SelectSingleNode("MediaId").InnerText;
            receiveMsg.MsgId = root.SelectSingleNode("MsgId").InnerText;

            return receiveMsg;
        }

        /// <summary>
        /// 获取语音消息
        /// </summary>
        /// <returns></returns>
        private ReceiveMsg GetVoiceModel()
        {
            ReceiveMsg receiveMsg = new ReceiveMsg();
            var root = doc.DocumentElement;
            receiveMsg.id = Guid.NewGuid().ToString();
            receiveMsg.ToUserName = root.SelectSingleNode("ToUserName").InnerText;
            receiveMsg.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            receiveMsg.CreateTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText.Trim());
            receiveMsg.MsgType = root.SelectSingleNode("MsgType").InnerText;
            receiveMsg.MediaId = root.SelectSingleNode("MediaId").InnerText;
            receiveMsg.Format = root.SelectSingleNode("Format").InnerText;
            receiveMsg.MsgId = root.SelectSingleNode("MsgId").InnerText;

            return receiveMsg;
        }

        /// <summary>
        /// 获取视频消息
        /// </summary>
        /// <returns></returns>
        private ReceiveMsg GetVideoModel()
        {
            ReceiveMsg receiveMsg = new ReceiveMsg();
            var root = doc.DocumentElement;
            receiveMsg.ToUserName = root.SelectSingleNode("ToUserName").InnerText;
            receiveMsg.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            receiveMsg.CreateTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText.Trim());
            receiveMsg.MsgType = root.SelectSingleNode("MsgType").InnerText;
            receiveMsg.MediaId = root.SelectSingleNode("MediaId").InnerText;
            receiveMsg.ThumbMediaId = root.SelectSingleNode("ThumbMediaId").InnerText;
            receiveMsg.MsgId = root.SelectSingleNode("MsgId").InnerText;

            return receiveMsg;
        }

        /// <summary>
        /// 获取地理位置消息
        /// </summary>
        /// <returns></returns>
        private ReceiveMsg GetLocationModel()
        {
            ReceiveMsg receiveMsg = new ReceiveMsg();
            //todo:这里要添加接收的地理位置数据到用户信息表里
            //var root = doc.DocumentElement;
            //receiveMsg.ToUserName = root.SelectSingleNode("ToUserName").InnerText;
            //receiveMsg.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            //receiveMsg.CreateTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText.Trim());
            //receiveMsg.MsgType = root.SelectSingleNode("MsgType").InnerText;
            //receiveMsg.Location_X = root.SelectSingleNode("Location_X").InnerText;
            //receiveMsg.Location_Y = root.SelectSingleNode("Location_Y").InnerText;
            //receiveMsg.Scale = root.SelectSingleNode("Scale").InnerText;
            //receiveMsg.Label = root.SelectSingleNode("Label").InnerText;
            //receiveMsg.MsgId = root.SelectSingleNode("MsgId").InnerText;

            return receiveMsg;
        }

        /****** 获取用户关注、退出关注的信息model **************/

        private bool IsExistUser()
        {
            UserInfo user = new UserInfo();
            var root = doc.DocumentElement;
            user.openid = root.SelectSingleNode("FromUserName").InnerText;
            UserInfoBll userBll = new UserInfoBll();
            return userBll.IsExist(user);
        }

        private UserInOrOut GetUserInModel()
        {
            UserInOrOut userInorOut = new UserInOrOut();
            var root = doc.DocumentElement;
            userInorOut.userId = root.SelectSingleNode("FromUserName").InnerText;
            userInorOut.optTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText);
            userInorOut.type = 1;
            XmlNode ticket = root.SelectSingleNode("Ticket");

            return userInorOut;
        }

        private UserInOrOut GetUserOutModel()
        {
            UserInOrOut userInorOut = new UserInOrOut();
            var root = doc.DocumentElement;
            userInorOut.userId = root.SelectSingleNode("FromUserName").InnerText;
            userInorOut.optTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText);
            userInorOut.type = 2;

            return userInorOut;
        }

        /****** 获取菜单点击的信息 **************/
        private MenuEventLog GetMenuEventLogModel(MessageType msgType)
        {
            MenuEventLog menuLog = new MenuEventLog();
            var root = doc.DocumentElement;
            menuLog.FromUserName = root.SelectSingleNode("FromUserName").InnerText;
            menuLog.clickTime = FormatTime(root.SelectSingleNode("CreateTime").InnerText);
            string menuKey = root.SelectSingleNode("EventKey").InnerText;
            if (msgType.Equals(MessageType.EventView))
            {
                int index = menuKey.IndexOf("=");
                int endIndex = menuKey.IndexOf("&");
                if (endIndex > index)
                    menuKey = menuKey.Substring(index + 1, endIndex - index - 1);
            }
            menuLog.menuId = menuKey;
            return menuLog;
        }

        /// <summary>
        /// 将微信的时间转化为当前时间
        /// </summary>
        /// <param name="createTime"></param>
        /// <returns></returns>
        private DateTime FormatTime(string createTime)
        {
            // 将微信传入的CreateTime的起始时间
            string time = "1970-01-01 00:00:00";
            DateTime dt = Convert.ToDateTime(time);
            double val = Convert.ToDouble(createTime);
            DateTime logTime = dt.AddSeconds(val);

            return logTime;

        }

    }
}
