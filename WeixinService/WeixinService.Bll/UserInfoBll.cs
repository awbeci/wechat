﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WeixinService.Dal;
using WeixinService.Model.common;
using WeixinService.Model;
using Newtonsoft.Json;
using System.Data;

namespace WeixinService.Bll
{
    public class UserInfoBll
    {
        public string subscribe { get; set; }
        public string openid { get; set; }
        public string nickname { get; set; }
        public string sex { get; set; }
        public string city { get; set; }

        public string country { get; set; }
        public string province { get; set; }
        public string language { get; set; }
        public string headimgurl { get; set; }
        public string subscribe_time { get; set; }
        public string unionid { get; set; }
        public string remark { get; set; }

        public UserInfo GetUserInfo(string userId)
        {
            AccessToken AcToken = new AccessToken();
            string token = AcToken.GetExistAccessToken();
            string url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + token + "&openid=" + userId + "&lang=zh_CN";

            SendDataToWeChat sender = new SendDataToWeChat();
            string data = sender.GetPage(url);
            var userBll = JsonConvert.DeserializeObject<UserInfoBll>(data);

            UserInfo user = new UserInfo();
            user.subscribe = Convert.ToInt32(userBll.subscribe);
            user.openid = userBll.openid;
            user.nickname = userBll.nickname;
            user.sex = Convert.ToInt32(userBll.sex);
            user.city = userBll.city;
            user.country = userBll.country;
            user.province = userBll.province;
            user.language = userBll.language;
            user.headimgurl = userBll.headimgurl;
            user.subscribe_time = FormatTime(userBll.subscribe_time);
            user.unionid = userBll.unionid;

            return user;
        }

        public bool IsExist(UserInfo user)
        {
            UserInfoDal dal = new UserInfoDal();
            return dal.Exist(user);
        }

        public void Add(UserInfo user)
        {
            UserInfoDal dal = new UserInfoDal();
            dal.Add(user);
        }

        public void Modify(UserInfo user)
        {
            UserInfoDal dal = new UserInfoDal();
            dal.Modify(user);
        }

        public void Del(UserInfo user)
        {
            UserInfoDal dal = new UserInfoDal();
            dal.Del(user);
        }

        public DataTable GetUserTable(UserInfo user)
        {
            UserInfoDal dal = new UserInfoDal();
            return dal.Query(user);
        }
        
        /// <summary>
        /// 将微信的时间转化为当前时间
        /// </summary>
        /// <param name="createTime"></param>
        /// <returns></returns>
        private DateTime FormatTime(string createTime)
        {
            // 将微信传入的CreateTime的起始时间
            string time = "1970-01-01 00:00:00";
            DateTime dt = Convert.ToDateTime(time);
            double val = Convert.ToDouble(createTime);
            DateTime logTime = dt.AddSeconds(val);

            return logTime;

        }

    }
}
