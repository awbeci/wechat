﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Newtonsoft.Json;

namespace WeixinService.Bll
{
    public class AccessToken
    {
        public string access_token { get; set; }

        public string expires_in { get; set; }

        /// <summary>
        /// 获取令牌
        /// </summary>
        private AccessToken GetToken()
        {
            var sendDataToWeChat = new SendDataToWeChat();
            var tmp = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxafebc80a58de29b8&secret=35e2abe7503bffa6a95049684dfd5e0e");
            var accessToken = JsonConvert.DeserializeObject<AccessToken>(tmp);
            return accessToken;
        }

        /// <summary>
        /// 获取微信令牌
        /// </summary>
        /// <returns></returns>
        public string GetExistAccessToken()
        {
            // 读取XML文件中的数据
            string filepath = System.Web.HttpContext.Current.Server.MapPath("/WeChatToken.xml");
            var str = new StreamReader(filepath, Encoding.UTF8);
            var xml = new XmlDocument();
            xml.Load(str);
            str.Close();
            str.Dispose();
            var token = xml.SelectSingleNode("xml").SelectSingleNode("AccessToken").InnerText;
            var accessExpires = Convert.ToDateTime(xml.SelectSingleNode("xml").SelectSingleNode("AccessExpires").InnerText);

            if (DateTime.Now >= accessExpires)
            {
                AccessToken mode = GetToken();
                xml.SelectSingleNode("xml").SelectSingleNode("AccessToken").InnerText = mode.access_token;
                //DateTime _accessExpires = DateTime.Now.AddSeconds(int.Parse(mode.expires_in));
                DateTime _accessExpires = DateTime.Now.AddSeconds(3600);
                xml.SelectSingleNode("xml").SelectSingleNode("AccessExpires").InnerText = _accessExpires.ToString();
                xml.Save(filepath);
                token = mode.access_token;
            }
            return token;
        }
    }
}
