﻿using System.Data;
using WeixinService.Dal;
using WeixinService.Model;

namespace WeixinService.Bll
{
    public class UserRegBll
    {
        readonly UserRegDal _dal = new UserRegDal();

        public bool Exist(UserReg bean)
        {
            return _dal.Exist(bean);
        }

        public void Add(UserReg bean)
        {
            _dal.Add(bean);
        }

        public void Del(UserReg bean)
        {
            _dal.Del(bean);
        }

        /// <summary>
        /// 获取微信绑定用户列表
        /// </summary>
        /// <returns></returns>
        public DataTable Query(UserReg bean)
        {
            return _dal.Query(bean);
        }
    }
}
