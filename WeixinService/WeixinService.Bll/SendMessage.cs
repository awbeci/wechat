﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Bll
{
    public class SendMessage
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        readonly string _ip1 = System.Configuration.ConfigurationManager.AppSettings["ip1"];
        /// <summary>
        /// 发送图文信息
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <param name="key">菜单id</param>
        /// <param name="name">菜单名称</param>
        /// <param name="type">业务类型</param>
        /// <param name="count">查询数据数量</param>
        public void SendNewsMessage(string userid, string key, string name, string type, string count)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var infoReleaseDal = new InfoReleaseDal();
            var listnews = new List<object>();
            try
            {
                //获取发布信息数据
                var dataDt = infoReleaseDal.QueryInfo(new InfoRelease()
                {
                    BusinessType = key,
                    FlagRelease = type
                }, count);
                var listDt = from ldt in dataDt.AsEnumerable()
                             select new
                             {
                                 Id = ldt.Field<string>("Id"),
                                 Title = ldt.Field<string>("Title"),
                                 MessageDescription = ldt.Field<string>("MessageDescription")
                             };
                if (dataDt.Rows.Count > 1)
                {
                    var name2 = HttpContext.Current.Server.UrlEncode(name);
                    listnews.Add(new
                    {
                        title = name + "更多列表...",
                        description = "",
                        url = _ip1 + "/views/messagelist/messagelist.htm?key=" + key + "&name=" + name2 + "&flag=1",
                        picurl = _ip1 + "/UploadImages/" + key + ".jpg"
                    });
                }
                foreach (var ldt in listDt)
                {
                    if (dataDt.Rows.Count > 1)
                    {
                        listnews.Add(new
                        {
                            title = ldt.Title,
                            description = ldt.MessageDescription,
                            url = _ip1 + "/views/messagelist/messagedetail.htm?id=" + ldt.Id
                        });
                    }
                    else
                    {
                        listnews.Add(new
                        {
                            title = ldt.Title,
                            description = ldt.MessageDescription,
                            url = _ip1 + "/views/messagelist/messagedetail.htm?id=" + ldt.Id,
                            picurl = _ip1 + "/UploadImages/" + key + ".jpg"
                        });
                    }
                }
                hashTable["touser"] = userid;
                hashTable["msgtype"] = "news";
                hashTable["news"] = new
                {
                    articles = listnews
                };
                var json = _jss.Serialize(hashTable);
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }

        /// <summary>
        /// 查询风险告知数据推送给用户(已经弃用)
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <param name="key">菜单id</param>
        /// <param name="name">菜单名称</param>
        /// <param name="type">业务类型</param>
        /// <param name="count">查询数据数量</param>
        public void SendNewsMessageFxgz(string userid, string key, string name, string type, string count)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var infoReleaseDal = new InfoReleaseDal();
            var listnews = new List<object>();
            try
            {
                //获取发布信息数据
                var dataDt = infoReleaseDal.QueryInfo(new InfoRelease()
                {
                    BusinessType = key
                }, userid, count);

                if (dataDt.Rows.Count <= 0)
                {
                    hashTable["touser"] = userid;
                    hashTable["msgtype"] = "text";
                    hashTable["text"] = new
                    {
                        content = "没有风险告知信息"
                    };
                    var json = _jss.Serialize(hashTable);
                    var token = accessToken.GetExistAccessToken();
                    var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                    Log.Debug("调试信息:" + back);
                }
                else
                {
                    var listDt = from ldt in dataDt.AsEnumerable()
                                 select new
                                 {
                                     Id = ldt.Field<string>("Id"),
                                     Title = ldt.Field<string>("Title"),
                                     MessageDescription = ldt.Field<string>("MessageDescription")
                                 };
                    //有风险告知信息就往后执行，如果没有就推送一个文本信息给用户，提示：没有风险告知信息
                    if (dataDt.Rows.Count > 1)
                    {
                        name = HttpContext.Current.Server.UrlEncode(name);
                        listnews.Add(new
                        {
                            title = "风险告知更多列表...",
                            description = "",
                            url = _ip1 + "/views/messagelist/messagelist.htm?key=" + key + "&wxid=" + userid + "&name=" + name + "&flag=0",
                            picurl = _ip1 + "/UploadImages/" + key + ".jpg"
                        });
                    }
                    foreach (var ldt in listDt)
                    {
                        if (dataDt.Rows.Count > 1)
                        {
                            listnews.Add(new
                            {
                                title = ldt.Title,
                                description = ldt.MessageDescription,
                                url = _ip1 + "/views/messagelist/messagedetail_fxgz.htm?id=" + ldt.Id + "&wxid=" + userid
                            });
                        }
                        else
                        {
                            listnews.Add(new
                            {
                                title = ldt.Title,
                                description = ldt.MessageDescription,
                                url = _ip1 + "/views/messagelist/messagedetail_fxgz.htm?id=" + ldt.Id + "&wxid=" + userid,
                                picurl = _ip1 + "/UploadImages/" + key + ".jpg"
                            });
                        }
                    }

                    hashTable["touser"] = userid;
                    hashTable["msgtype"] = "news";
                    hashTable["news"] = new
                    {
                        articles = listnews
                    };
                    var json = _jss.Serialize(hashTable);
                    var token = accessToken.GetExistAccessToken();
                    var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                    Log.Debug("调试信息:" + back);
                }
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }


        /// <summary>
        /// 发送图文信息
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="articles"></param>
        public void SendNewsMessage(string userid, Articles articles)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var listnews = new List<Articles>();
            try
            {
                listnews.Add(articles);
                hashTable["touser"] = userid;
                hashTable["msgtype"] = "news";
                hashTable["news"] = new
                {
                    articles = listnews
                };
                var json = _jss.Serialize(hashTable);
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }

        /// <summary>
        /// 发送图文信息
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="articles"></param>
        public void SendNewsMessage(string userid, List<Articles> articles)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            try
            {
                hashTable["touser"] = userid;
                hashTable["msgtype"] = "news";
                hashTable["news"] = new
                {
                    articles
                };
                var json = _jss.Serialize(hashTable);
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }
        /// <summary>
        /// 发送图文信息
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="userid">微信号</param>
        /// <param name="articles"></param>
        public void SendNewsMessage(string token, string userid, List<Articles> articles)
        {
            var hashTable = new Hashtable();
            var sendDataToUser = new SendDataToWeChat();
            try
            {
                hashTable["touser"] = userid;
                hashTable["msgtype"] = "news";
                hashTable["news"] = new
                {
                    articles
                };
                var json = _jss.Serialize(hashTable);
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }

        /// <summary>
        /// 发送文本信息
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="msg">消息文字</param>
        public void SendTxtMessage(string userid, string msg)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            try
            {
                hashTable["touser"] = userid;
                hashTable["msgtype"] = "text";
                hashTable["text"] = new
                {
                    content = msg
                };
                var json = _jss.Serialize(hashTable);
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }
    }
}
