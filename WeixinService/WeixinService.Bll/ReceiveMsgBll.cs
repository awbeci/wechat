﻿using System.Data;

using WeixinService.Dal;
using WeixinService.Model.common;

namespace WeixinService.Bll
{
    public class ReceiveMsgBll
    {
        readonly ReceiveMsgDal _dal = new ReceiveMsgDal();
        public void Add(ReceiveMsg receiveMsg)
        {
            _dal.Add(receiveMsg);
        }
        public DataTable QueryByPage(ReceiveMsg receiveMsg, int page, int rows, ref int count)
        {
            return _dal.QueryByPage(receiveMsg, page, rows, ref count);
        }
    }
}
