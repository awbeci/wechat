﻿using System.Data;
using WeixinService.Dal;
using WeixinService.Model.common;

namespace WeixinService.Bll
{
    public class ReplyMsgBll
    {
        readonly ReplyMsgDal _dal = new ReplyMsgDal();
        public void Add(ReplyMsg bean)
        {
            if (!string.IsNullOrEmpty(bean.ReceiveMsgId))
            {
                ReceiveMsgDal receiveMsgDal = new ReceiveMsgDal();
                receiveMsgDal.Modify(new ReceiveMsg()
                    {
                        id = bean.ReceiveMsgId,
                        bak1 = "2",
                    });
            }
            _dal.Add(bean);
        }

        public DataTable Query(ReplyMsg bean)
        {
            return _dal.Query(bean);
        }

        public DataTable QueryByPage(ReplyMsg bean, int page, int rows, ref int count)
        {
            return _dal.QueryByPage(bean, page, rows, ref count);
        }
    }
}
