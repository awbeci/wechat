﻿using System;
using System.Web;
using System.Net;

namespace WeixinService.Bll
{
    public class GetMultimedia
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 下载保存多媒体文件,返回多媒体保存路径 
        /// </summary>
        /// <param name="ACCESS_TOKEN">令牌</param>
        /// <param name="MEDIA_ID">媒体ID</param>
        /// <param name="SavePath">保存文件的文件夹路径，例如：E:\abc</param>
        /// <returns>保存文件路径</returns>
        public string GetMedia(string ACCESS_TOKEN, string MEDIA_ID, string SavePath)
        {
            string file = string.Empty;
            string strpath = string.Empty;
            string savepath = string.Empty;
            string stUrl = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=" + ACCESS_TOKEN + "&media_id=" + MEDIA_ID;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(stUrl);

            req.Method = "GET";
            using (WebResponse wr = req.GetResponse())
            {
                HttpWebResponse myResponse = (HttpWebResponse)req.GetResponse();

                strpath = myResponse.ResponseUri.ToString();
                Log.Debug("接收类别://" + myResponse.ContentType);
                WebClient mywebclient = new WebClient();
                savepath = SavePath + "\\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + (new Random()).Next().ToString().Substring(0, 4) + ".amr";
                Log.Debug("路径://" + savepath);
                try
                {
                    mywebclient.DownloadFile(strpath, savepath);
                    file = savepath;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
            return file;
        }
    }
}
