﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
   public class Users:BaseModel
    {
       public string Id { get; set; }

       /// <summary>
       /// 微信号
       /// </summary>
       public string OpenId { get; set; }

       /// <summary>
       /// 名称
       /// </summary>
       public string Name { get; set; }

       /// <summary>
       /// 登录名称
       /// </summary>
       public string LoginName { get; set; }

       /// <summary>
       /// 密码
       /// </summary>
       public string Password { get; set; }

       /// <summary>
       /// 部门id
       /// </summary>
       public string DepartmentId { get; set; }

       /// <summary>
       /// 角色id
       /// </summary>
       public string RoleId { get; set; }

    }
}
