﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class DepartmentModel:BaseModel
    {
        public string Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 父部门节点id
        /// </summary>
        public string PId { get; set; }
    }
}
