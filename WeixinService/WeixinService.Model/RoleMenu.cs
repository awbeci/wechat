﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class RoleMenu : BaseModel
    {
        public string Id { get; set; }

        /// <summary>
        /// 菜单id
        /// </summary>
        public string MenuId { get; set; }

        /// <summary>
        /// 角色id
        /// </summary>
        public string RoleId { get; set; }
    }
}
