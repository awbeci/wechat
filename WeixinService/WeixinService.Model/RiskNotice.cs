﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class RiskNotice
    {
        public string ID { get; set; }

        /// <summary>
        /// 编号 
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public string Rq { get; set; }

        /// <summary>
        /// 主送
        /// </summary>
        public string Zs { get; set; }

        /// <summary>
        /// 停电设备及工期
        /// </summary>
        public string Tdsb_Gq { get; set; }

        /// <summary>
        /// 运行风险分析
        /// </summary>
        public string Yxfxfx { get; set; }

        /// <summary>
        /// 风险预控措施
        /// </summary>
        public string Fxykcs { get; set; }

        /// <summary>
        /// 状态,0:未发布 1:已发布 2:已审核 3:已签发 4:已确认 5:已备案
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 发送日期
        /// </summary>
        public DateTime? Send_Dt { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CreateDt { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string Operator { get; set; }
    }
}
