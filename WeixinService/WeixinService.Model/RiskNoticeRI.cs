﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class RiskNoticeRI
    {
        public string ID { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        public string WeiXin { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNum { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 人员类型1:审核人，2:签发人
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 预留字段
        /// </summary>
        public string Bak1 { get; set; }

        /// <summary>
        /// 预留字段
        /// </summary>
        public string Bak2 { get; set; }
    }
}
