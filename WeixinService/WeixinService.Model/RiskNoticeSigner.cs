﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class RiskNoticeSigner
    {
        public string ID { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNum { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        public string WeiXin { get; set; }

        /// <summary>
        /// 风险告知通知单编号
        /// </summary>
        public string RNID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 人员类型:1:审核人 2:签发人 3:运检部 4:营销部 5:安质部 6:客户
        /// </summary>
        public int? Type { get; set; }

        /// <summary>
        /// 状态0:未签字，1:已签字
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 图片路径
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 签字日期
        /// </summary>
        public DateTime? SignerDt { get; set; }
    }
}
