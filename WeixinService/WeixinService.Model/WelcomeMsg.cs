﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class WelcomeMsg : BaseModel
    {
        /// <summary>
        /// id 唯一id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 类别id（1：欢迎语）
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 类别名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string content { get; set; }
        /// <summary>
        /// 备注1
        /// </summary>
        public string bak1 { get; set; }
        /// <summary>
        /// 备注2
        /// </summary>
        public string bak2 { get; set; }
    }
}
