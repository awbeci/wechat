﻿
using System;

namespace WeixinService.Model
{
    public class UserReg : BaseModel
    {
        /// <summary>
        /// 微信ID
        /// </summary>
        public string Wxid { get; set; }

        /// <summary>
        /// 用户号
        /// </summary>
        public string RNumber { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string bak1 { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string bak2 { get; set; }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string bak3 { get; set; }

        /// <summary>
        /// 职务
        /// </summary>
        public string bak4 { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDt { get; set; }

    }
}
