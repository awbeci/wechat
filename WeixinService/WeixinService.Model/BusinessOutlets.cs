﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    /// <summary>
    /// 营业网点信息类
    /// </summary>
    public class BusinessOutlets : BaseModel
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 区域id
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 区域名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Bak1 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Bak2 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Bak3 { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Bak4 { get; set; }

        /// <summary>
        /// 营业网点类型
        /// </summary>
        public int? Type { get; set; }
    }
}
