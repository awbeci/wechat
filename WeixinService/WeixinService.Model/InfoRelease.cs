﻿using System;

namespace WeixinService.Model
{
    public class InfoRelease : BaseModel
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 素材内容
        /// </summary>
        public string MaterialContent { get; set; }

        /// <summary>
        /// 素材类型
        /// </summary>
        public string MaterialType { get; set; }

        /// <summary>
        /// 发布标识：0：未发布 ，1：已发布,2:已推送
        /// </summary>
        public string FlagRelease { get; set; }

        /// <summary>
        /// 信息描述
        /// </summary>
        public string MessageDescription { get; set; }

        /// <summary>
        /// 业务类型，指的是微信菜单内容
        /// </summary>
        public string BusinessType { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CreateDt { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatePerson { get; set; }

        /// <summary>
        /// 发布的欠费文件有效月份
        /// </summary>
        public string Bak1 { get; set; }

        /// <summary>
        /// 预留字段2
        /// </summary>
        public string Bak2 { get; set; }

        /// <summary>
        /// 预留字段3
        /// </summary>
        public string Bak3 { get; set; }

        /// <summary>
        /// 预留字段4
        /// </summary>
        public string Bak4 { get; set; }

        /// <summary>
        /// 发送组别
        /// </summary>
        public string SendGroup { get; set; }

        /// <summary>
        /// 发送人
        /// </summary>
        public string SendPerson { get; set; }
    }
}
