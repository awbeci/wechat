﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class InfoReleaseDetailsModel:BaseModel
    {
        public string Id { get; set; }

        /// <summary>
        /// 状态:1,待推送;2,待确认;3,已确认
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 微信id
        /// </summary>
        public string OpenId { get; set; }

        /// <summary>
        /// 客户号
        /// </summary>
        public string RNumber { get; set; }

        /// <summary>
        /// 确认时间
        /// </summary>
        public DateTime? ConfirmDt { get; set; }

        /// <summary>
        /// 语音确认,0:未确认,1:已确认
        /// </summary>
        public string Bk1 { get; set; }

        /// <summary>
        ///  ReceiveMsg表id
        /// </summary>
        public string Bk2 { get; set; }

        /// <summary>
        /// 语音地址
        /// </summary>
        public string Bk3 { get; set; }

        /// <summary>
        /// 信息发布表id
        /// </summary>
        public string InfoReleaseId { get; set; }

       
    }
}
