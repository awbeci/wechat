﻿using System;

namespace WeixinService.Model
{
    public class MenuModel : BaseModel
    {
        public string Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 是否可用,1-无效，2-有效
        /// </summary>
        public string Enable { get; set; }

        /// <summary>
        /// 父菜单id
        /// </summary>
        public string PId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDt { get; set; }
    }
}
