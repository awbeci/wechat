﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model
{
    public class CustomMenu : BaseModel
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父菜单id
        /// </summary>
        public string Pid { get; set; }

        /// <summary>
        /// 菜单类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// key
        /// </summary>
        public string Nkey { get; set; }

        /// <summary>
        /// url
        /// </summary>
        public string Url { get; set; }

    }
}
