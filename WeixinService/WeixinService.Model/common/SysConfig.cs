﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace WeixinService.Model.common
{
	 	//SysConfig
		public class SysConfig
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private string _id;
        public string id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// type
        /// </summary>		
		private int _type;
        public int type
        {
            get{ return _type; }
            set{ _type = value; }
        }        
		/// <summary>
		/// name
        /// </summary>		
		private string _name;
        public string name
        {
            get{ return _name; }
            set{ _name = value; }
        }        
		/// <summary>
		/// Content
        /// </summary>		
		private string _content;
        public string Content
        {
            get{ return _content; }
            set{ _content = value; }
        }        
		/// <summary>
		/// bak1
        /// </summary>		
		private string _bak1;
        public string bak1
        {
            get{ return _bak1; }
            set{ _bak1 = value; }
        }        
		/// <summary>
		/// bak2
        /// </summary>		
		private string _bak2;
        public string bak2
        {
            get{ return _bak2; }
            set{ _bak2 = value; }
        }        
		   
	}
}

