﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model.common
{
    public class MenuView
    {
        public string type { get; set; }

        public string name { get; set; }

        public string url { get; set; }
    }
}
