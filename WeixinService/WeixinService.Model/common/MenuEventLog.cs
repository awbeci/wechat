﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;

namespace WeixinService.Model.common
{
    //MenuEventLog
    public class MenuEventLog
    {

        /// <summary>
        /// id
        /// </summary>		
        private string _id;
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// menuId（对应菜单id和key）
        /// </summary>		
        private string _menuid;
        public string menuId
        {
            get { return _menuid; }
            set { _menuid = value; }
        }
        /// <summary>
        /// menuName
        /// </summary>		
        private string _menuname;
        public string menuName
        {
            get { return _menuname; }
            set { _menuname = value; }
        }
        /// <summary>
        /// clickTime
        /// </summary>		
        private DateTime _clicktime;
        public DateTime clickTime
        {
            get { return _clicktime; }
            set { _clicktime = value; }
        }
        /// <summary>
        /// FromUserName(用户微信账号id)
        /// </summary>		
        private string _fromusername;
        public string FromUserName
        {
            get { return _fromusername; }
            set { _fromusername = value; }
        }
        /// <summary>
        /// bak1
        /// </summary>		
        private string _bak1;
        public string bak1
        {
            get { return _bak1; }
            set { _bak1 = value; }
        }
        /// <summary>
        /// bak2
        /// </summary>		
        private string _bak2;
        public string bak2
        {
            get { return _bak2; }
            set { _bak2 = value; }
        }

    }
}

