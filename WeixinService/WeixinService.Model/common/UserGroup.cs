﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;

namespace WeixinService.Model.common
{
    //UserGroup
    public class UserGroup
    {

        /// <summary>
        /// id 分组id
        /// </summary>		
        private string _id;
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// name 分组名称
        /// </summary>		
        private string _name;
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        /// <summary>
        /// count 分组用户数量
        /// </summary>		
        private int _count;
        public int count
        {
            get { return _count; }
            set { _count = value; }
        }
        /// <summary>
        /// pid 
        /// </summary>		
        private string _pid;
        public string pid
        {
            get { return _pid; }
            set { _pid = value; }
        }
        /// <summary>
        /// bak2
        /// </summary>		
        private string _bak2;
        public string bak2
        {
            get { return _bak2; }
            set { _bak2 = value; }
        }
        /// <summary>
        /// bak3
        /// </summary>		
        private string _bak3;
        public string bak3
        {
            get { return _bak3; }
            set { _bak3 = value; }
        }
        /// <summary>
        /// bak4
        /// </summary>		
        private int _bak4;
        public int bak4
        {
            get { return _bak4; }
            set { _bak4 = value; }
        }

    }
}

