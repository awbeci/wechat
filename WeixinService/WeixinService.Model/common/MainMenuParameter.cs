﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model.common
{
    public class MainMenuParameter
    {
        public string name { get; set; }

        public List<object> sub_button { get; set; } 
    }
}
