﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model.common
{
    public enum MessageType
    {
        /// <summary>
        /// 未知
        /// </summary>
        NULL = 0,
        /// <summary>
        /// 文本消息
        /// </summary>
        MsgText = 1,
        /// <summary>
        /// 图片消息
        /// </summary>
        MsgImage = 2,
        /// <summary>
        /// 语音消息
        /// </summary>
        MsgVoice = 3,
        /// <summary>
        /// 视频消息
        /// </summary>
        MsgVideo = 4,
        /// <summary>
        /// 地理位置消息
        /// </summary>
        MsgLocation = 5,
        /// <summary>
        /// 链接消息
        /// </summary>
        MsgLink = 6,

        /// <summary>
        /// 关注事件
        /// </summary>
        EventSubscribe = 7,
        /// <summary>
        ///取消关注事件
        /// </summary>
        EventUnsubscribe = 8,
        /// <summary>
        /// 扫描带参数二维码事件,用户已关注时的事件推送
        /// </summary>
        EventScan = 9,
        /// <summary>
        /// 上报地理位置事件
        /// </summary>
        EventLocation = 10,
        /// <summary>
        /// 点击菜单拉取消息时的事件推送
        /// </summary>
        EventClick = 11,
        /// <summary>
        /// 点击菜单跳转链接时的事件推送
        /// </summary>
        EventView = 12
    }

    public enum DataType
    {
        /// <summary>
        /// 未知
        /// </summary>
        NULL = 0,
        /// <summary>
        /// post数据的类型是事件
        /// </summary>
        Event = 1,
        /// <summary>
        /// post数据的类型是消息
        /// </summary>
        Message = 2
    }

    public class TypeModel
    {

    }
}
