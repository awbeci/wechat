﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeixinService.Model.common
{
    public class Articles
    {
        public string title { get; set; }

        public string description { get; set; }

        public string url { get; set; }

        public string picurl { get; set; }
    }
}
