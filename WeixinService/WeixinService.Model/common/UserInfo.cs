﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;

namespace WeixinService.Model.common
{
    //UserInfo
    public class UserInfo
    {

        /// <summary>
        /// subscribe 关注
        /// </summary>		
        private int _subscribe;
        public int subscribe
        {
            get { return _subscribe; }
            set { _subscribe = value; }
        }
        /// <summary>
        /// openid 微信用户号
        /// </summary>		
        private string _openid;
        public string openid
        {
            get { return _openid; }
            set { _openid = value; }
        }
        /// <summary>
        /// nickname 微信用户昵称
        /// </summary>		
        private string _nickname;
        public string nickname
        {
            get { return _nickname; }
            set { _nickname = value; }
        }
        /// <summary>
        /// sex 性别
        /// </summary>		
        private int _sex;
        public int sex
        {
            get { return _sex; }
            set { _sex = value; }
        }
        /// <summary>
        /// city 城市
        /// </summary>		
        private string _city;
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }
        /// <summary>
        /// country 国家
        /// </summary>		
        private string _country;
        public string country
        {
            get { return _country; }
            set { _country = value; }
        }
        /// <summary>
        /// province 省份
        /// </summary>		
        private string _province;
        public string province
        {
            get { return _province; }
            set { _province = value; }
        }
        /// <summary>
        /// language 语言
        /// </summary>		
        private string _language;
        public string language
        {
            get { return _language; }
            set { _language = value; }
        }
        /// <summary>
        /// headimgurl 用户头像
        /// </summary>		
        private string _headimgurl;
        public string headimgurl
        {
            get { return _headimgurl; }
            set { _headimgurl = value; }
        }
        /// <summary>
        /// subscribe_time 关注时间
        /// </summary>		
        private DateTime _subscribe_time;
        public DateTime subscribe_time
        {
            get { return _subscribe_time; }
            set { _subscribe_time = value; }
        }
        /// <summary>
        /// unionid
        /// </summary>		
        private string _unionid;
        public string unionid
        {
            get { return _unionid; }
            set { _unionid = value; }
        }
        /// <summary>
        /// groupID 分组id
        /// </summary>		
        private string _groupid;
        public string groupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }
        /// <summary>
        /// groupName 分组名称
        /// </summary>		
        private string _groupname;
        public string groupName
        {
            get { return _groupname; }
            set { _groupname = value; }
        }
        /// <summary>
        /// RNumber 关联钞资产号
        /// </summary>		
        private string _rnumber;
        public string RNumber
        {
            get { return _rnumber; }
            set { _rnumber = value; }
        }
        /// <summary>
        /// msgFrom 用户来源
        /// </summary>		
        private string _msgfrom;
        public string msgFrom
        {
            get { return _msgfrom; }
            set { _msgfrom = value; }
        }
        /// <summary>
        /// 开始时间
        /// </summary>		
        private DateTime _begintime;
        public DateTime begintime
        {
            get { return _begintime; }
            set { _begintime = value; }
        }
        /// <summary>
        /// 结束时间
        /// </summary>		
        private DateTime _endtime;
        public DateTime endtime
        {
            get { return _endtime; }
            set { _endtime = value; }
        }

        /// <summary>
        /// 人员职位，1正职，2副职，默认为0：无职位
        /// </summary>
        public int bak1 { get; set; }

        public string bak2 { get; set; }

        /// <summary>
        /// bak3
        /// </summary>		
        private string _bak3;
        public string bak3
        {
            get { return _bak3; }
            set { _bak3 = value; }
        }
        /// <summary>
        /// bak4
        /// </summary>		
        private int _bak4;
        public int bak4
        {
            get { return _bak4; }
            set { _bak4 = value; }
        }
        /// <summary>
        /// 实名
        /// </summary>		
        private string _truename;
        public string truename
        {
            get { return _truename; }
            set { _truename = value; }
        }

        /// <summary>
        /// 用户地理位置纬度
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// 用户地理位置经度
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 用户地理位置精度
        /// </summary>
        public string Precision { get; set; }

        /// <summary>
        /// 用户地理位置更新时间
        /// </summary>
        public DateTime? UpdateDt { get; set; }
    }
}

