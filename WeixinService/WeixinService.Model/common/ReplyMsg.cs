﻿using System;

namespace WeixinService.Model.common
{
    public class ReplyMsg
    {
        public string Id { get; set; }

        public string ReceiveMsgId { get; set; }

        public string AcceptName { get; set; }

        public DateTime CreateTime { get; set; }

        public string TextMsg { get; set; }

        public int IsSend { get; set; }

        public string Bak1 { get; set; }
        public string Bak2 { get; set; }
        public string Bak3 { get; set; }
        public string Bak4 { get; set; }
    }
}
