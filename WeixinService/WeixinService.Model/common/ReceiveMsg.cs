﻿using System;

namespace WeixinService.Model.common
{
    //ReceiveMsg
    public class ReceiveMsg
    {

        /// <summary>
        /// id
        /// </summary>		
        private string _id;
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// ToUserName
        /// </summary>		
        private string _tousername;
        public string ToUserName
        {
            get { return _tousername; }
            set { _tousername = value; }
        }
        /// <summary>
        /// FromUserName
        /// </summary>		
        private string _fromusername;
        public string FromUserName
        {
            get { return _fromusername; }
            set { _fromusername = value; }
        }
        /// <summary>
        /// CreateTime
        /// </summary>		
        private DateTime _createtime;
        public DateTime CreateTime
        {
            get { return _createtime; }
            set { _createtime = value; }
        }
        /// <summary>
        /// MsgType
        /// </summary>		
        private string _msgtype;
        public string MsgType
        {
            get { return _msgtype; }
            set { _msgtype = value; }
        }
        /// <summary>
        /// MsgId
        /// </summary>		
        private string _msgid;
        public string MsgId
        {
            get { return _msgid; }
            set { _msgid = value; }
        }
        /// <summary>
        /// Content
        /// </summary>		
        private string _content;
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        /// <summary>
        /// PicUrl
        /// </summary>		
        private string _picurl;
        public string PicUrl
        {
            get { return _picurl; }
            set { _picurl = value; }
        }
        /// <summary>
        /// Format
        /// </summary>		
        private string _format;
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }
        /// <summary>
        /// ThumbMediaId
        /// </summary>		
        private string _thumbmediaid;
        public string ThumbMediaId
        {
            get { return _thumbmediaid; }
            set { _thumbmediaid = value; }
        }
        /// <summary>
        /// ThumbMediaIdURL
        /// </summary>		
        private string _thumbmediaidurl;
        public string ThumbMediaIdURL
        {
            get { return _thumbmediaidurl; }
            set { _thumbmediaidurl = value; }
        }
        /// <summary>
        /// MediaId
        /// </summary>		
        private string _mediaid;
        public string MediaId
        {
            get { return _mediaid; }
            set { _mediaid = value; }
        }
        /// <summary>
        /// MediaIdURL
        /// </summary>		
        private string _mediaidurl;
        public string MediaIdURL
        {
            get { return _mediaidurl; }
            set { _mediaidurl = value; }
        }
        /// <summary>
        /// Title
        /// </summary>		
        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        /// <summary>
        /// Description
        /// </summary>		
        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        /// <summary>
        /// Url
        /// </summary>		
        private string _url;
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        /// <summary>
        /// 是否回复，1-否，2-是
        /// </summary>		
        private string _bak1;
        public string bak1
        {
            get { return _bak1; }
            set { _bak1 = value; }
        }
        /// <summary>
        /// 语音文件存放路径
        /// </summary>		
        private string _bak2;
        public string bak2
        {
            get { return _bak2; }
            set { _bak2 = value; }
        }
        /// <summary>
        /// bak3
        /// </summary>		
        private string _bak3;
        public string bak3
        {
            get { return _bak3; }
            set { _bak3 = value; }
        }
        /// <summary>
        /// bak4
        /// </summary>		
        private string _bak4;
        public string bak4
        {
            get { return _bak4; }
            set { _bak4 = value; }
        }

        public DateTime DateBegin { get; set; }

        public DateTime DateEnd { get; set; }

        public string KeyWords { get; set; }
    }
}

