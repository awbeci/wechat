﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;

namespace WeixinService.Model.common
{
    //UserInOrOut
    public class UserInOrOut
    {

        /// <summary>
        /// id
        /// </summary>		
        private string _id;
        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// userId
        /// </summary>		
        private string _userid;
        public string userId
        {
            get { return _userid; }
            set { _userid = value; }
        }
        /// <summary>
        /// userName
        /// </summary>		
        private string _username;
        public string userName
        {
            get { return _username; }
            set { _username = value; }
        }
        /// <summary>
        /// type
        /// </summary>		
        private int _type;
        public int type
        {
            get { return _type; }
            set { _type = value; }
        }
        /// <summary>
        /// optTime
        /// </summary>		
        private DateTime _opttime;
        public DateTime optTime
        {
            get { return _opttime; }
            set { _opttime = value; }
        }
        /// <summary>
        /// groupID
        /// </summary>		
        private string _groupid;
        public string groupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }
        /// <summary>
        /// groupNmae
        /// </summary>		
        private string _groupname;
        public string groupName
        {
            get { return _groupname; }
            set { _groupname = value; }
        }
        /// <summary>
        /// bak1
        /// </summary>		
        private string _bak1;
        public string bak1
        {
            get { return _bak1; }
            set { _bak1 = value; }
        }
        /// <summary>
        /// bak2
        /// </summary>		
        private string _bak2;
        public string bak2
        {
            get { return _bak2; }
            set { _bak2 = value; }
        }

    }
}

