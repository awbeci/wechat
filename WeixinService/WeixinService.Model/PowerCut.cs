﻿using System;

namespace WeixinService.Model
{
    /// <summary>
    /// 停电信息
    /// </summary>
    public class PowerCut:BaseModel
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 送电状态，1：已送电，2：未送电
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 业务类型，指的是微信菜单内容
        /// </summary>
        public string BusinessType { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CreateDt { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatePerson { get; set; }

        /// <summary>
        /// 预留字段1
        /// </summary>
        public string Bak1 { get; set; }

        /// <summary>
        /// 预留字段2
        /// </summary>
        public string Bak2 { get; set; }

        /// <summary>
        /// 预留字段3
        /// </summary>
        public string Bak3 { get; set; }

        /// <summary>
        /// 预留字段4
        /// </summary>
        public string Bak4 { get; set; }

        /// <summary>
        /// 发送组别
        /// </summary>
        public string SendGroup { get; set; }

        /// <summary>
        /// 发送人
        /// </summary>
        public string SendPerson { get; set; }

        /// <summary>
        /// 停电时间
        /// </summary>
        public DateTime PowerCutTime { get; set; }

        /// <summary>
        /// 停电时间范围
        /// </summary>
        public string TimeArea { get; set; }

        /// <summary>
        /// 停电区域
        /// </summary>
        public string Area { set; get; }

        /// <summary>
        /// 停电设备
        /// </summary>
        public string Device { get; set; }

        /// <summary>
        /// 是否发布，1-否，2-是
        /// </summary>
        public string FlagRelease { get; set; }

        public DateTime DateBegin { get; set; }

        public DateTime DateEnd { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public string Jd { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public string Wd { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

    }
}
