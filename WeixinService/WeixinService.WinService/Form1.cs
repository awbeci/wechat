﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using System.Data;
using WeixinService.WinService.WebServ;
using WeixinService.Model.common;

namespace WeixinService.WinService
{
    public partial class Form1 : Form
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool Success = false;
        public Form1()
        {
            InitializeComponent();
            var thread = new Thread(SendToWeixin);
            //thread.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var thread = new Thread(Send);
            thread.Start();
            button1.Visible = false;
        }

        public void Send()
        {
            while (true)
            {
                try
                {
                    Log.Debug("主网、配网、监控 推送。");
                    InfoReleaseDal dal = new InfoReleaseDal();
                    DataTable dt = dal.QueryByTypes(new string[] { "3080_youo_zwtz", "3081_youo_pwtz", "3082_youo_jkxx" });

                    UserGroupDal userGroupDal = new UserGroupDal();
                    DataTable dtUser = userGroupDal.QueryInternalUser();
                    //发送：主网、配网、监控
                    foreach (DataRow row in dt.Rows)
                    {
                        Success = false;
                        var title = row["Title"].ToString();
                        var MessageDescription = row["MaterialContent"].ToString();
                        //var content = "标题：" + title + "\n内容：" + MessageDescription + "\n发布人：" + row["CreatePerson"].ToString();
                        var content = "标题：" + title + "\n内容：" + MessageDescription + "\n\r       国网铜陵供电公司";

                        foreach (DataRow row1 in dtUser.Rows)
                        {
                            var openId = row1["openid"].ToString();
                            SendMsg(openId, content);
                        }
                        if (Success)
                        {
                            dal.Modify(new InfoRelease()
                                {
                                    Id = row["id"].ToString(),
                                    FlagRelease = "2",
                                });
                        }
                    }

                    PowerCutDal powerCutDal = new PowerCutDal();
                    DataTable dtPowerCut = powerCutDal.Query(new PowerCut()
                        {
                            State = "1", //未送电
                            FlagRelease = "'2'", //已发布
                            DateBegin = DateTime.Now.Date,
                            DateEnd = DateTime.MaxValue.AddDays(-2),
                            BusinessType = "011_youo_tdtz2",
                        });
                    //发送：故障停电
                    Log.Debug("故障停电");
                    foreach (DataRow row in dtPowerCut.Rows)
                    {
                        Success = false;
                        var content = "尊敬的电力客户：\n\r       因突发电力故障，安排以下故障抢修工作安排。未经铜陵供电公司有关部门许可，严禁任何单位和个人在停电线路及设备上工作。为此造成的不便，敬请各客户给予谅解和支持。如有任何疑问，请致电供电公司24小时服务热线95598。\n\r       停电时间：" + row["PowerCutTime"].ToString().Split(' ')[0] + " " + row["TimeArea"].ToString() +
                          "\n\r       停电设备：" + row["Device"].ToString() + "\n\r       停电区域：" + row["Area"].ToString() + "\n\r       国网铜陵供电公司";

                        foreach (DataRow row1 in dtUser.Rows)
                        {
                            var openId = row1["openid"].ToString();
                            SendMsg(openId, content);
                        }
                        if (Success)
                        {
                            powerCutDal.Modify(new PowerCut()
                            {
                                Id = row["id"].ToString(),
                                FlagRelease = "3",
                            });
                            Log.Debug("修改状态为已推送--" + row["id"].ToString());
                        }
                    }


                    #region  发送最新资讯091_youo_zxzx

                    var sendMessage = new SendMessage();
                    var listnews = new List<Articles>();
                    var articles = new Articles();
                    InfoReleaseDal infoReleaseDal = new InfoReleaseDal();
                    DataTable yhcsDt = infoReleaseDal.Query(new InfoRelease()
                        {
                            FlagRelease = "1",
                            BusinessType = "091_youo_zxzx",
                        });

                    DataTable userDt = new UserInfoDal().Query(new UserInfo());//获取所有关注的微信用户
                    WebServ.WebService1 service1 = new WebService1();
                    var token = service1.GetToken();
                    foreach (DataRow row in yhcsDt.Rows)
                    {
                        var id = row["id"].ToString();
                        var discrip = row["MessageDescription"].ToString();
                        if (discrip.Length > 200)
                        {
                            discrip = discrip.Substring(0, 190) + "...";
                        }
                        articles = new Articles()
                        {
                            title = row["title"].ToString(),
                            description = discrip,
                            picurl = "http://60.173.29.191/UploadImages/091_youo_zxzx.jpg",
                            url = "http://60.173.29.191/views/messagelist/messagedetail.htm?id=" + id,
                        };
                        listnews.Add(articles);
                        foreach (DataRow uRow in userDt.Rows)
                        {
                            var openid = uRow["openid"].ToString();
                            sendMessage.SendNewsMessage(token, openid, listnews);
                        }
                        infoReleaseDal.Modify(new InfoRelease()
                            {
                                Id = id,
                                FlagRelease = "2",
                            });
                        Log.Debug("最新资讯-修改状态为已推送--" + row["id"].ToString());
                    }
                    #endregion

                    Thread.Sleep(10000);
                }
                catch (Exception ex)
                {
                    Log.Debug("发送故障" + ex);
                    Thread.Sleep(100000);
                }
            }
        }

        private void SendMsg(string openId, string replyContent)
        {
            var jsonWx = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
                         replyContent + "\"}}";

            var sendDataToWeChat = new SendDataToWeChat();
            WebServ.WebService1 service1 = new WebService1();
            var token = service1.GetToken();

            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token,
                                                jsonWx);
            var backData = JsonConvert.DeserializeObject<WeChatBackData>(back);
            if (backData.errcode == 0)
            {
                Success = true;
                Log.Debug("发送成功，返回：" + back);
            }
            else
            {
                Log.Debug("发送失败，返回：" + back);
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        public class WeChatBackData
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public void SendToWeixin()
        {
            while (true)
            {
                try
                {
                    WebService1 service1 = new WebService1();
                    var msg = service1.GetTokenToUpload();
                    Log.Debug(msg);
                    Thread.Sleep(15 * 60 * 100);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }
    }
}
