﻿var objlist;                       //保存的通知单列表
var phonenum;                      //手机号
var currpage = 1;                          //当前页数
var oldlist = "";                       //旧数据
$(function () {
    phonenum  = decodeURI(queryUrlName("phone")); 
    if(queryUrlName("page")!=null && queryUrlName("page")!='' && typeof(queryUrlName("page"))!='undefined'){
    currpage = queryUrlName("page");
    }
    $("#jzgd").bind("click",function(){
        currpage ++;
    	oldlist  =  $("#myul").html();
    	queryMessageList();
    	
    });
    $("#listtitle").html("铜陵电网运行风险预警通知单");
    queryMessageList();
  
});

/*
 * 通过Ajax访问并获取数据
 */
function queryMessageList() {
    $.ajax({
        url: "../../handlers/RiskNotice.asmx/QueryRiskNotice",
        type: "post",
        timeout: 2000,
        async: false,
        data: {"phone":phonenum,"page":currpage},
        dataType: "json",
        success: function(data){
        	ajax_QueryMessageList(data);
        },
        error:function(){
            alert("获取列表失败");
        }
    });
}
/*
 * 处理获取到的数据
 */

function ajax_QueryMessageList(obj) {
    var html = "";
    var olength = 0;
    if (obj == null || obj.data == null || obj.data.length <= 0) {
        //alert("数据为空，请联系管理人员");
        return false;
    }
    if(objlist==null || typeof(objlist)=='undefined' || objlist==''){
    objlist = obj;
    }else{
      olength  = objlist.data.length;
      var zz  = JSON.stringify(objlist.data).substr(1,JSON.stringify(objlist.data).length-2);
    	  for (var i = 0; i < obj.data.length; i++) {
                 zz  += ','+JSON.stringify(obj.data[i]);
    	  }
          zz  = '['+ zz +']';
          objlist.data = JSON.parse(zz);
    }
    for (var i = 0; i < obj.data.length; i++) {
        html += '<li onclick="myclick(\'' + (olength+i) + '\')"><div style="font-size:14px;font-weight:bold;margin-left:5px;margin-bottom:5px;">铜陵电网运行风险预警通知单' + 
        obj.data[i].number + '</div>';
        if (obj.data[i].date != null) {
            html += '<div class="mydescription" style="font-size:12px;margin-left:5px;" ><span style="float:left;">' + obj.data[i].date + '</span>'
            if(obj.data[i].qzzt==0){                   //0未签字  1已签
            	  html += '<span style="float:right;">未签</span>';
            }else{
            	  html += '<span style="float:right;">已签</span>';
            }
            html += '</div></li>';
        }
    }
    if(obj.dataCount/currpage <=10){
    	$("#jzgd").css("display","none"); 
    }else{
    	$("#jzgd").css("display","block"); 
    	
    }
    
    	$("#myul").html(oldlist+html);
  
    
}
/*
 * 通知单的点击事件 
 */
function myclick(i) {
	var id  = objlist.data[i].id;       
 //   var number = objlist.data[i].number;   //编号
 //   var date = objlist.data[i].date;     //日期
 //   var zs = objlist.data[i].zs;       //主送
 //   var tdsb_gq = objlist.data[i].tdsb_gq;  //停电设备及工期
 //   var yxfxfx = objlist.data[i].yxfxfx;   //运行风险分析
 //   var fxykcs = objlist.data[i].fxykcs;   //风险预控措施
    var zt = objlist.data[i].zt;       //通知单状态（0未发布  1已发布 2已审核 3已签发 4已确认 5已备案）
 //   var send_dt = objlist.data[i].send_dt;  //发送日期
 //   var createdt = objlist.data[i].createdt; //创建日期
      var qzzt = objlist.data[i].qzzt;     //当前手机号  对于当前通知单的  签字状态  0未签 1已签
    location.href = "detail.htm?phonenum="+phonenum+"&id="+id+"&qzzt="+qzzt+"&zt="+zt;
   // location.href = "detail.htm?id="+id+"&number="+number+"&date="+date+"&zs="+zs+"&tdsb_gq="+tdsb_gq+"&yxfxfx="+
   // yxfxfx+"&fxykcs="+fxykcs+"&zt="+zt+"&send_dt="+send_dt+"&createdt="+createdt+"&qzzt="+qzzt+"&phonenum="+phonenum;
  
}