﻿//ajax对象
var zwobj = {
    url: "",
    data: {},
    post: "POST",
    get: "GET",
    json: "json",
    html: "html",
    contentype: "application/json; charset=utf-8",
    flag: "",
    formname: ""
};

var formobj = {
    target: '#output2',
    beforeSubmit: showRequest,
    success: showResponse,
    data: {},
    url: '',
    type: 'post',
    dataType: 'json',
    clearForm: true,
    resetForm: true,
    timeout: 3000
};
//计算数值在数组里的位置
Array.prototype.indexOf = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};
//数组方法(删除数组元素)
Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};

/*
* 获取url参数值 
*/
function queryUrlName(name) {
    var url = location.href.replace(/\\u0026/g, "&");
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}
 
/**
* 日期时间格式化方法，
* 可以格式化年、月、日、时、分、秒、周
**/
Date.prototype.Format = function (formatStr) {
    var week = ['日', '一', '二', '三', '四', '五', '六'];
    return formatStr.replace(/yyyy|YYYY/, this.getFullYear())
 	             .replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
 	             .replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1)).replace(/M/g, (this.getMonth() + 1))
 	             .replace(/w|W/g, week[this.getDay()])
 	             .replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate()).replace(/d|D/g, this.getDate())
 	             .replace(/HH|hh/g, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours()).replace(/H|h/g, this.getHours())
 	             .replace(/mm/g, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes()).replace(/m/g, this.getMinutes())
 	             .replace(/ss/g, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds()).replace(/S|s/g, this.getSeconds());
};

// 生成Guid字符串
function Guid() {
    var guid = "";
    for (var i = 1; i <= 32; i++) {
        var n = Math.floor(Math.random() * 16.0).toString(16);
        guid += n;
        if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
            guid += "-";
    }
    return guid;
}

//json格式{data:[{id:123,val:'123'}]}
function bindSelectDom(id, data) {
    if (data.length <= 0 || data == null || typeof data == "undefined") {
        return;
    }
    $("#" + id).empty();
    var l = data.length;
    $("#" + id).append("<option value=''>—请选择—</option>");
    for (var i = 0; i < l; i++) {
        if (data[i].id == null) {
            continue;
        }
        $("#" + id).append("<option value='" + data[i].id + "'>" + data[i].val + "</option>");
    }
    return true;
}

function ZwDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth();
    var mymonth;
    var day = date.getDate();

    this.getZwDate = function () {
        this.getMonth();
        if (day < 10) {
            return year + "-" + mymonth + "-0" + day;
        }
        return year + "-" + mymonth + "-" + day;
    };

    this.getDay = function () {
        return date.getDate();
    };

    this.setDay = function (myday) {
        date.setDate(day + myday);
        day = date.getDate();
    };

    this.getMonth = function () {
        month = date.getMonth();
        switch (month) {
            case 0:
                mymonth = "01";
                break;
            case 1:
                mymonth = "02";
                break;
            case 2:
                mymonth = "03";
                break;
            case 3:
                mymonth = "04";
                break;
            case 4:
                mymonth = "05";
                break;
            case 5:
                mymonth = "06";
                break;
            case 6:
                mymonth = "07";
                break;
            case 7:
                mymonth = "08";
                break;
            case 8:
                mymonth = "09";
                break;
            case 9:
                mymonth = "10";
                break;
            case 10:
                mymonth = "11";
                break;
            case 11:
                mymonth = "12";
                break;
            default:
        }
        return mymonth;
    };
}

//绑定select下拉列表的某一个option
function bindSelectOption(id, val) {
    $("#" + id + " option").each(function () {
        if ($(this).text() === val) {
            $(this).attr('selected', 'selected');
        }
    });
}

//----------------------------  ajax  ---------------------------------

/**
* *ajax增删改查方法
**/
function ajaxData() {
    $.ajax({
        url: zwobj.url,
        type: zwobj.post,
        timeout: 2000,
        async: false,
        data: zwobj.data,
        dataType: zwobj.json,
        success: serviceSuccess,
        error: serviceError
    });
}

/**
* *ajax增删改查方法
**/
function ajaxData2() {
    $.ajax({
        url: zwobj.url,
        type: zwobj.post,
        timeout: 30000,
        async: true,
        data: zwobj.data,
        dataType: zwobj.json,
        success: serviceSuccess,
        error: serviceError
    });
}

/**
* *ajax成功时返回resultObject是json数据
**/
function serviceSuccess(resultObject, status) {
    if (resultObject == null || resultObject == "") {
        //alert("返回数据为空");
        return false;
    }
    if (resultObject == "timeout") {
        top.window.location.replace("/views/main/login.html");
    }
    if (resultObject.isSuccess) {
        eval(resultObject.jsMethod + "(resultObject)");
    } else {
        top.window.location.replace("/views/main/login.html");
    }
    return true;
}

/**
* *ajax失败时返回
**/
function serviceError(result, status) {
    if (status == "timeout") {
        top.window.location.replace("/views/main/login.html");
    }
    //alert("请求数据超时，请重试！");
    //location.href = "../views/main/login.html";
    top.window.location.replace("/views/main/login.html");
    return false;
}

//------------------------------------ ajax form js -----------------------------------------------

function showRequest(formData, jqForm, options) {
    var queryString = $.param(formData);
    return true;
}

function showResponse(resultObject, statusText, xhr, $form) {
    if (resultObject == null) {
        return true;
    }
    if (resultObject.isSuccess) {
        eval(resultObject.jsMethod + "(resultObject)");
    } else {
        alert(resultObject.msg);
        return false;
    }
    return true;
}


//typeof obj.sendGroup != "undefined"//判断undefined
//$.parser.parse();//easyui重新渲染
//replace(/"/g,'');//删除所有";(注意：正则表达式结合replace非常有用)