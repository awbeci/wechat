﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;

namespace WeixinService.Web
{
    /// <summary>
    /// Summary description for Main
    /// </summary>
    public class Main : IHttpHandler
    {
        private string _token = "E9c50ulVR0QZWng6frbnQV3oVfDjp0fYaV8c9eiEj41xVQqAtTJxit8T9NOW-b5wtRBREgCysy7NIyS8L8vw7A";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            method.Invoke(this, new object[] { HttpContext.Current });
        }

        /// <summary>
        /// 获取微信token数据
        /// </summary>
        /// <param name="httpContext"></param>
        public void GetToken(HttpContext httpContext)
        {
            var tmp = GetPage("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxafebc80a58de29b8&secret=35e2abe7503bffa6a95049684dfd5e0e");
            _token = tmp;
            httpContext.Response.Write(tmp);
        }

        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void GetMenu(HttpContext httpContext)
        {
            var tmp = GetPage("https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" + _token);
            httpContext.Response.Write(tmp);
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void AddMenu(HttpContext httpContext)
        {
            var fs1 = new FileStream(httpContext.Server.MapPath(".") + "\\data\\menu.data", FileMode.Open);
            var sr = new StreamReader(fs1, Encoding.GetEncoding("GBK"));
            var menu = sr.ReadToEnd();
            sr.Close();
            fs1.Close();
            var tmp = GetPage("https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + _token, menu);
            httpContext.Response.Write(tmp);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        public void DelMenu(HttpContext httpContext)
        {
            var tmp = GetPage("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + _token);
            httpContext.Response.Write(tmp);
        }

        /// <summary>
        /// 根据url和data数据返回微信数据
        /// </summary>
        /// <param name="postUrl"></param>
        /// <param name="postData"></param>
        /// <returns></returns>
        public string GetPage(string postUrl, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...  
            try
            {
                // 设置参数  
                request = WebRequest.Create(postUrl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据  
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求  
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码  
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }

        /// <summary>
        /// 通过url返回微信数据
        /// </summary>
        /// <param name="postUrl"></param>
        /// <returns></returns>
        public string GetPage(string postUrl)
        {
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            // 准备请求...  
            try
            {
                // 设置参数  
                request = WebRequest.Create(postUrl) as HttpWebRequest;
                var cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                //发送请求并获取相应回应数据  
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求  
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码  
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}