﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using WeixinService.Model.common;

namespace WeixinService.Web
{
    public partial class Default : System.Web.UI.Page
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        const string Token = "youotech";//定义一个局部变量不可以被修改，这里定义的变量要与接口配置信息中填写的Token一致
        protected void Page_Load(object sender, EventArgs e)
        {
            //校验签名,当填入的信息提交之后页面有提示“你已成功成为公众平台开发者，
            //可以使用公众平台的开发功能”这个的时候，接下来你就需要注释掉这个校验的方法，使得后面的消息回复得以正常运作
            //Valid();

            if (Request.HttpMethod.ToLower() == "post")//当普通微信用户向公众账号发消息时，微信服务器将POST该消息到填写的URL上
            {
                var postStr = PostInput();
                //     WriteLog(postStr, Server);//计入日记
                if (postStr.Contains("xml"))
                {
                    Response.Write("");
                    Response.Flush();//立即输出
                    Response.Close();
                    DataDealBll dataDeal = new DataDealBll(postStr);
                    Log.Debug("dataDeal.ReceiveMsgModel.MsgType:" + dataDeal.ReceiveMsgModel.MsgType);

                    if ("voice".Equals(dataDeal.ReceiveMsgModel.MsgType))
                    {
                        try
                        {
                            //在添加到数据库后开始下载。
                            InfoReleaseDetailsDal infoReleaseDetailsDal = new InfoReleaseDetailsDal();
                            DataTable dt = infoReleaseDetailsDal.QueryMaxdt(new InfoReleaseDetailsModel()
                            {
                                OpenId = dataDeal.ReceiveMsgModel.FromUserName,
                            });
                            bool FlagModify = false;
                            if (dt.Rows.Count > 0)
                            {
                                var DtStr = dt.Rows[0]["confirmdt"].ToString();
                                Log.Debug("DtStr:" + DtStr);
                                var confirmDt = DateTime.MinValue;
                                if (!string.IsNullOrEmpty(DtStr))
                                {
                                    confirmDt = DateTime.Parse(DtStr);
                                }
                                if (confirmDt >= DateTime.Now.AddMinutes(-30) && (dt.Rows[0]["bk1"].ToString().Length == 0 || dt.Rows[0]["bk1"].ToString() != "1"))
                                {
                                    FlagModify = true;
                                    infoReleaseDetailsDal.Modify(new InfoReleaseDetailsModel()
                                        {
                                            Id = dt.Rows[0]["id"].ToString(),
                                            Bk2 = dataDeal.ReceiveMsgModel.id,
                                        });
                                }
                            }
                            AccessToken AcToken = new AccessToken();
                            string token = AcToken.GetExistAccessToken();
                            Log.Debug("GetMedia参数，token：" + token + ",dataDeal.MediaId:" + dataDeal.ReceiveMsgModel.MediaId + ",path:" + Server.MapPath("/"));
                            GetMultimedia getMultimedia = new GetMultimedia();
                            string filePath = GetMedia(token, dataDeal.ReceiveMsgModel.MediaId, Server.MapPath("/voice"));
                            new ReceiveMsgDal().Modify(new ReceiveMsg()
                                {
                                    id = dataDeal.ReceiveMsgModel.id,
                                    bak2 = "/voice/" + filePath.Substring(filePath.LastIndexOf('\\') + 1),
                                });
                            if (FlagModify)
                            {
                                infoReleaseDetailsDal.Modify(new InfoReleaseDetailsModel()
                                {
                                    Id = dt.Rows[0]["id"].ToString(),
                                    Bk1 = "1",
                                    Bk3 = "/voice/" + filePath.Substring(filePath.LastIndexOf('\\') + 1),
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                        }
                    }

                    if (!dataDeal.IsMessageTrue())
                    {
                        WriteLog(postStr, Server);//计入日记
                    }
                    else
                    {
                        MessageType msgType = dataDeal.GetMsgType();
                        if (msgType.Equals(MessageType.EventSubscribe))
                        {
                            /******* 添加首次关注的回复 ***/
                        }
                    }
                }
                Response.End();
            }
            else
            {
                var postStr = PostInput();
                if (postStr.Contains("xml"))
                {
                    Response.Write("");
                    DataDealBll dataDeal = new DataDealBll(postStr);
                    if (!dataDeal.IsMessageTrue())
                    {
                        WriteLog(postStr, Server);//计入日记
                    }
                }
            }
        }

        private void Valid()
        {
            var echoStr = Request.QueryString["echoStr"];
            if (CheckSignature())
            {
                if (!string.IsNullOrEmpty(echoStr))
                {
                    Response.Write(echoStr);
                    Response.End();
                }
            }
        }

        /// <summary>
        /// 验证微信签名
        /// </summary>
        /// <returns></returns>
        private bool CheckSignature()
        {
            string signature = Request.QueryString["signature"];
            string timestamp = Request.QueryString["timestamp"];
            string nonce = Request.QueryString["nonce"];
            string[] arrTmp = { Token, timestamp, nonce };
            Array.Sort(arrTmp);//字典排序
            var tmpStr = string.Join("", arrTmp);
            tmpStr = FormsAuthentication.HashPasswordForStoringInConfigFile(tmpStr, "SHA1");//对该字符串进行sha1加密
            tmpStr = tmpStr.ToLower();//对字符串中的字母部分进行小写转换，非字母字符不作处理
            WriteLog(tmpStr, Server);//计入日志
            if (tmpStr == signature)//开发者获得加密后的字符串可与signature对比，标识该请求来源于微信。开发者通过检验signature对请求进行校验，若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，否则接入失败
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 获取post返回来的数据
        /// </summary>
        /// <returns></returns>
        private string PostInput()
        {
            var s = HttpContext.Current.Request.InputStream;
            var b = new byte[s.Length];
            s.Read(b, 0, (int)s.Length);
            return Encoding.UTF8.GetString(b);
        }

        /// <summary>
        ///返回微信信息结果
        /// </summary>
        /// <param name="weixinXml"></param>
        private void ResponseMsg(string weixinXml)
        {
            try
            {
                var doc = new XmlDocument();
                doc.LoadXml(weixinXml);//读取XML字符串
                var rootElement = doc.DocumentElement;

                if (rootElement != null)
                {
                    XmlNode msgType = rootElement.SelectSingleNode("MsgType");//获取字符串中的消息类型

                    var resxml = "";
                    if (msgType.InnerText == "text")//如果消息类型为文本消息
                    {
                        var model = new
                            {
                                ToUserName = rootElement.SelectSingleNode("ToUserName").InnerText,
                                FromUserName = rootElement.SelectSingleNode("FromUserName").InnerText,
                                CreateTime = rootElement.SelectSingleNode("CreateTime").InnerText,
                                MsgType = msgType.InnerText,
                                Content = rootElement.SelectSingleNode("Content").InnerText,
                                MsgId = rootElement.SelectSingleNode("MsgId").InnerText
                            };
                        resxml += "<xml><ToUserName><![CDATA[" + model.FromUserName + "]]></ToUserName><FromUserName><![CDATA[" + model.ToUserName + "]]></FromUserName><CreateTime>" + ConvertDateTimeInt(DateTime.Now) + "</CreateTime>";
                        if (!string.IsNullOrEmpty(model.Content))//如果接收到消息
                        {
                            if (model.Content.Contains(" 你好") || model.Content.Contains(" 好") || model.Content.Contains("hi") || model.Content.Contains("hello"))// 你好
                            {
                                resxml += "<MsgType><![CDATA[text]]></MsgType><Content><![CDATA[你好，有事请留言，偶会及时回复你的。]]></Content><FuncFlag>0</FuncFlag></xml>";
                            }

                        }

                        else//没有接收到消息
                        {
                            resxml += "<MsgType><![CDATA[text]]></MsgType><Content><![CDATA[亲，感谢您对我的关注，有事请留言。]]></Content><FuncFlag>0</FuncFlag></xml>";
                        }

                        Response.Write(resxml);
                    }
                    if (msgType.InnerText == "image")//如果消息类型为图片消息
                    {
                        var model = new
                            {
                                ToUserName = rootElement.SelectSingleNode("ToUserName").InnerText,
                                FromUserName = rootElement.SelectSingleNode("FromUserName").InnerText,
                                CreateTime = rootElement.SelectSingleNode("CreateTime").InnerText,
                                MsgType = msgType.InnerText,
                                PicUrl = rootElement.SelectSingleNode("PicUrl").InnerText,
                                MsgId = rootElement.SelectSingleNode("MsgId").InnerText
                            };
                        resxml += "<xml><ToUserName><![CDATA[" + model.FromUserName + "]]></ToUserName><FromUserName><![CDATA[" + model.ToUserName + "]]></FromUserName><CreateTime>" + ConvertDateTimeInt(DateTime.Now) + "</CreateTime><MsgType><![CDATA[news]]></MsgType><ArticleCount>1</ArticleCount><Articles><item><Title><![CDATA[欢迎您的光临！]]></Title><Description><![CDATA[非常感谢您的关注！]]></Description><PicUrl><![CDATA[http://...jpg]]></PicUrl><Url><![CDATA[http://www.baidu.com/]]></Url></item></Articles><FuncFlag>0</FuncFlag></xml>";
                        Response.Write(resxml);
                    }
                    else//如果是其余的消息类型
                    {
                        var model = new
                            {
                                ToUserName = rootElement.SelectSingleNode("ToUserName").InnerText,
                                FromUserName = rootElement.SelectSingleNode("FromUserName").InnerText,
                                CreateTime = rootElement.SelectSingleNode("CreateTime").InnerText,
                            };
                        resxml += "<xml><ToUserName><![CDATA[" + model.FromUserName + "]]></ToUserName><FromUserName><![CDATA[" + model.ToUserName + "]]></FromUserName><CreateTime>" + ConvertDateTimeInt(DateTime.Now) + "</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[亲，感谢您对我的关注，有事请留言，我会及时回复你的哦。]]></Content><FuncFlag>0</FuncFlag></xml>";
                        Response.Write(resxml);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Response.End();

        }

        /// <summary>
        /// datetime转换成unixtime
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private int ConvertDateTimeInt(DateTime time)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }

        /// <summary>
        /// 写日志(用于跟踪)，可以将想打印出的内容计入一个文本文件里面，便于测试
        /// </summary>
        public static void WriteLog(string strMemo, HttpServerUtility server)
        {
            string filename = server.MapPath("/log/log.txt");//在网站项目中建立一个文件夹命名logs（然后在文件夹中随便建立一个web页面文件，避免网站在发布到服务器之后看不到预定文件）
            if (!Directory.Exists(server.MapPath("//log//")))
                Directory.CreateDirectory("//log//");
            StreamWriter sr = null;
            try
            {
                if (!File.Exists(filename))
                {
                    sr = File.CreateText(filename);
                }
                else
                {
                    sr = File.AppendText(filename);
                }
                sr.WriteLine(strMemo);
            }
            catch
            {
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }


        public string GetMedia(string ACCESS_TOKEN, string MEDIA_ID, string SavePath)
        {
            string file = string.Empty;
            string strpath = string.Empty;
            string savepath = string.Empty;
            string stUrl = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=" + ACCESS_TOKEN + "&media_id=" + MEDIA_ID;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(stUrl);

            req.Method = "GET";
            using (WebResponse wr = req.GetResponse())
            {
                HttpWebResponse myResponse = (HttpWebResponse)req.GetResponse();

                strpath = myResponse.ResponseUri.ToString();
                Log.Debug("接收类别://" + myResponse.ContentType);
                WebClient mywebclient = new WebClient();
                savepath = SavePath + "\\" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + (new Random()).Next().ToString().Substring(0, 4) + ".amr";
                Log.Debug("路径://" + savepath);
                try
                {
                    mywebclient.DownloadFile(strpath, savepath);
                    file = savepath;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
            return file;
        }
    }
}