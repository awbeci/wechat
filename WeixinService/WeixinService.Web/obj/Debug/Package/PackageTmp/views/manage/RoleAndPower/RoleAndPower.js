﻿var myurl;
var mydata;
var mytype = "POST";
var htmlType = "html";
var commonType = "application/json; charset=utf-8";

$(function () {
    $("#d_tb").fadeIn(1000);
    $.fn.zTree.init($("#treeRole"), settingRole); //加载角色树
    $.fn.zTree.init($("#treeMenu"), settingMenu); //加载菜单树
});

//获取菜单树
var settingMenu = {
    async: {
        enable: true,
        url: "../../../handlers/ManageService.ashx",
        autoParam: ["id", "name=n"],
        otherParam: { "action": "Menu" }
    },
    check: {
        enable: true,
        chkStyle: "checkbox"
    }, 
    callback: {
        onAsyncError: function (event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus == "error") {
                top.location = "../../main/Login.html";
            }
        }
    },
    data: {
        key: {
            name: "name"
        },
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "pId",
            rootPId: 0
        }
    }
};

//获取角色树
var settingRole = {
    async: {
        enable: true,
        url: "../../../handlers/ManageService.ashx",
        autoParam: ["id", "name=n"],
        otherParam: { "action": "QueryRoles" }
    },
    callback: {
        onClick: onClick,
        callback: {
        onAsyncError: function (event, treeId, treeNode, XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus == "error") {
                top.location = "../../main/Login.html";
            }
        }
    },
    },
    data: {
        simpleData: {
            enable: true
        }
    }
};

//节点点击方法
function onClick(event, treeId, treeNode, clickFlag) {
    settingMenu.async.otherParam.roleid = treeNode.id;
    $.fn.zTree.init($("#treeMenu"), settingMenu); //加载树
}

function getAllNodes() {
    var treeObj = $.fn.zTree.getZTreeObj("treeRole");
    var nodes = treeObj.getNodes();
    return nodes;
}


//获取选中节点的对象
function getSelectNode() {
    var selectnode = null;
    var zTree = $.fn.zTree.getZTreeObj("treeRole");
    var nodes = zTree.getSelectedNodes();
    var length = nodes.length;
    if (length == 1) {
        selectnode = nodes[0];
    }
    return selectnode;
}

//获取选中节点的对象
function getCheckNode() {
    var zTree = $.fn.zTree.getZTreeObj("treeMenu");
    var nodes = zTree.getCheckedNodes(true);
    return nodes;
}

function clearForm() {
    $("form").clearForm();
}

//----------------------------------- CLick 事件 -----------------------------------------------
function addRole_click() {
    clearForm();
    $("#dlg").children().children().css("display", "block");
    $("#dlg").dialog("open").dialog("setTitle", "角色编辑");
}

function save_click() {
    var selectedNode = getSelectNode();
    if (!selectedNode) {
        $.messager.alert("提示", "请先点击选择角色！");
        return false;
    }

    var checkedNode = getCheckNode();
    if (checkedNode.length <= 0) {
        $.messager.alert("提示", "请给角色分配权限！");
        return false;
    }

    var menuid = "";

    for (var i = 0; i < checkedNode.length; i++) {
        if (!checkedNode[i].isParent) {
            menuid += checkedNode[i].id + ",";
        }
    }
    menuid = menuid.substring(0, menuid.length - 1);
    myurl = "../../../handlers/ManageService.ashx";
    mydata = { action: 'SaveMenuRole', roleid: selectedNode.id, menuid: menuid };
    save();
    return true;
}

function delRole_click() {
    myurl = "../../../handlers/ManageService.ashx";
    var selectNode = getSelectNode();
    if (selectNode == null) {
        $.messager.alert("提示", "请点击角色列表中的角色！");
        return;
    }
    var nodeId = selectNode.id; //选中节点的类型
    mydata = { action: 'DelRole', nodeid: nodeId };
    delrole();
}


function roleForm_Save_click() {
    var nodes = getAllNodes();
    var name = $("#jsmc").val();
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].name == name) {
            $.messager.alert("提示", "已存在该角色！");
            return false;
        }
    }
    myurl = "../../../handlers/ManageService.ashx";
    mydata = { action: 'SaveRole' };
    saverole();
    return true;
}

//----------------------------------------------------------------------------------

function save() {
    $.ajax({
        url: myurl, // ajax 调用后台方法
        type: mytype,
        data: mydata, // 参数
        dataType: "json",
        success: function (responseText, statusText) {
            var val = responseText;
            if (!val.error) {
                $('#dlg').dialog('close');
                $.fn.zTree.init($("#treeRole"), settingRole); //加载树
                settingMenu.async.otherParam.roleid = "";
                $.fn.zTree.init($("#treeMenu"), settingMenu); //加载树
            } else {
                alert(responseText);
            }
        }
    });
}

//保存角色信息
function saverole() {
    $("form").ajaxSubmit({
        url: myurl,
        data: mydata,
        complete: function () {
        },
        beforeSubmit: function () {
            return $("form").form('validate');
        },
        success: function (responseText, statusText) {
            var val = null;
            eval("val=" + responseText);
            if (!val.error) {
                $('#dlg').dialog('close');
                $.fn.zTree.init($("#treeRole"), settingRole); //加载树
                settingMenu.async.otherParam.roleid = "";
                $.fn.zTree.init($("#treeMenu"), settingMenu); //加载树
            } else {
                alert(responseText);
            }
        },
        error: function (e) {
            alert("error");
        }
    });

}


//删除角色信息

function delrole() {
    $.messager.confirm('提示', '确定删除选中的数据?<br />如果删除，将会删除该角色与人员的对应关系！',
        function (r) {
            if (r) {
                $.ajax({
                    url: myurl,
                    type: mytype,
                    data: mydata,
                    dataType: htmlType,
                    success: function (data, status) {
                        var val = null;
                        eval("val=" + data);
                        if (!val.error) {
                            $('#dlg').dialog('close');
                            $.fn.zTree.init($("#treeRole"), settingRole); //加载树
                            settingMenu.async.otherParam.roleid = "";
                            $.fn.zTree.init($("#treeMenu"), settingMenu); //加载树
                        } else {
                            alert(val.msg);
                        }
                    }
                });
            }
        });
}
