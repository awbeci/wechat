﻿var flagTree;
var setting = {
    view: {
        removeHoverDom: removeHoverDom,
        selectedMulti: false
    },
    edit: {
        enable: true,
        showRemoveBtn: showRemoveBtn,
        showRenameBtn: showRenameBtn
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeRemove: beforeRemove,
        beforeRename: beforeRename,
        onRemove: onRemove,
        onRename: onRename,
        onClick: onClick
    }
};

function beforeRemove(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("departmentTree");
    zTree.selectNode(treeNode);
    onClick(null, treeId, treeNode);
    var data = $("#dg").datagrid('getData');
    if (data.total > 0) {
        alert("请先删除部门人员，再点击删除部门");
        return false;
    }
    return confirm("确认删除部门 -- " + treeNode.name + " 吗？");
}

function onRemove(e, treeId, treeNode) {
    zwobj.data = { action: "DelDepartment", id: treeNode.id, name: treeNode.name };
    zwobj.url = "../../../handlers/ManageService.ashx";
    ajaxData();
}

function beforeRename(treeId, treeNode, newName, isCancel) {
    flagTree = "edit";
}

function onRename(e, treeId, treeNode, isCancel) {
    addDom(treeId, treeNode);
    zwobj.data = { action: "UpdateDepartment", id: treeNode.id, name: treeNode.name };
    zwobj.url = "../../../handlers/ManageService.ashx";
    ajaxData();
}

function ajax_AddDepartment(obj) {
    //alert("添加成功");
}

function ajax_UpdateDepartment(obj) {
    //alert("更新成功");
}

function ajax_DelDepartment(obj) {
    alert("删除成功");
}

function showRemoveBtn(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("departmentTree");
    var nodes = zTree.getSelectedNodes();
    if (nodes.length == 0) {
        return false;
    } else if (nodes[0].id == treeNode.id) {
        return !treeNode.isParent && !(treeNode.level == 0);
    }
    return false;
}

function showRenameBtn(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("departmentTree");
    var nodes = zTree.getSelectedNodes();
    if (nodes.length == 0) {
        return false;
    } else if (nodes[0].id == treeNode.id) {
        return true;
    }
    return false;
}

function removeHoverDom(treeId, treeNode) {
    $("#addBtn_" + treeNode.tId).unbind().remove();
}

function onClick(event, treeId, treeNode) {
    addDom(treeId, treeNode);
    $("#dg").datagrid("load", {
        action: "QueryUser",
        uid: treeNode.id
    });
};

function addDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='add node' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var btn = $("#addBtn_" + treeNode.tId);
    if (btn) btn.bind("click", function () {
        var zTree = $.fn.zTree.getZTreeObj("departmentTree");
        var id = Guid();
        zTree.addNodes(treeNode, { id: id, pId: treeNode.id, name: "部门名称", icon: "../../../js/jquery-easyui-1.4/themes/icons/group.png" });
        zwobj.url = "../../../handlers/ManageService.ashx";
        zwobj.data = { action: "AddDepartment", id: id, pId: treeNode.id, name: "部门名称" };
        ajaxData2();
        var nodes = zTree.getNodesByParam("id", id, null);
        zTree.editName(nodes[0]);
        return false;
    });
}

function initDepartmentTree() {
    zwobj.url = "../../../handlers/ManageService.ashx?action=QueryDepartment";
    zwobj.data = {};
    ajaxData();
}

function ajax_QueryDepartment(obj) {
    var len = obj.data.length;
    var departmentNodes = [];
    for (var i = 0; i < len; i++) {
        if (obj.data[i].id == "0") {
            departmentNodes.push({ id: obj.data[i].id, pId: obj.data[i].pid, name: obj.data[i].name, open: true,
                icon: "../../../images/house.png"
            });
        } else {
            departmentNodes.push({ id: obj.data[i].id, pId: obj.data[i].pid, name: obj.data[i].name, open: true,
                icon: "../../../js/jquery-easyui-1.4/themes/icons/group.png"
            });
        }
    }
    $.fn.zTree.init($("#departmentTree"), setting, departmentNodes);
    $("#dg").datagrid(dgObj); //初始化人员表格，是第一次执行
}

$(function () {
    initDepartmentTree();
    initDlg();
    $('#openid').combogrid({
        panelWidth: 500,
        idField: 'openid',
        textField: 'openid',
        editable: false,
        url: '../../../handlers/ManageService.ashx',
        queryParams: { action: 'QueryUnbindWeixin', wxid: wxid, r: Math.random() },
        columns: [[
            { field: 'openid', title: '微信ID', width: 220, align: 'left' },
            { field: 'TrueName', title: '真实姓名', width: 80 },
            { field: 'nickname', title: '昵称', width: 80 },
            { field: 'sex', title: '性别', width: 50,
                formatter: function (val, obj) {
                    if (val == 1) {
                        return '男';
                    } else {
                        return '女';
                    }
                }
            }
        ]],
        onShowPanel: function () {
            var g = $('#openid').combogrid('grid');
            g.datagrid('load', { action: 'QueryUnbindWeixin', wxid: wxid, r: Math.random() });
        },
        fitColumns: true
    });
});

var dgObj = {
    url: "../../../handlers/ManageService.ashx",
    queryParams: { action: "QueryUser", uid: "" },
    fit: true,
    title: '用户详细列表',
    border: false,
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: [{
        text: '新建',
        iconCls: 'icon-add',
        handler: function () {
            addDlg();
        }
    }, '-', {
        text: '编辑',
        iconCls: 'icon-edit',
        handler: function () {
            editDlg();
        }
    }, '-', {
        text: '删除',
        iconCls: 'icon-remove',
        handler: function () {
            var selected = $("#dg").datagrid("getSelected");
            if (selected == null) {
                alert("请选择一条数据进行删除");
                return false;
            }
            if (confirm("确认删除该人员吗？")) {
                zwobj.url = "../../../handlers/ManageService.ashx";
                zwobj.data = { id: selected.id, action: "DelUser" };
                ajaxData2();
                $("#dg").datagrid("reload");
            }
        }
    }, '-', {
        text: '重置密码',
        iconCls: 'icon-key',
        handler: function () {
            var selected = $("#dg").datagrid("getSelected");
            if (selected == null) {
                alert("请选择一条数据进行重置");
                return false;
            }

            zwobj.url = "../../../handlers/ManageService.ashx";
            zwobj.data = { id: selected.id, action: "ResetPass" };
            ajaxData2();
            $("#dg").datagrid("reload");
        }
    }
    ],
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
         { field: "id", hidden: true },
         { field: "openid", title: "微信用户号", width: 200, align: 'center' },
         { field: "name", title: "人员名称", width: 100, align: 'center' },
         { field: "loginname", title: "登录名称", width: 100, align: 'center' },
         { field: "deptname", title: "所属部门", width: 150, align: 'center' },
         { field: "rolename", title: "所属角色", width: 100, align: 'center' }
    ]]
};

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: [{
            text: '确 定',
            iconCls: 'icon-ok',
            handler: function () {
                $("#fm-users").form('submit', {
                    url: "../../../handlers/ManageService.ashx",
                    onSubmit: function (data) {
                        if ($("#name").val().length <= 0) {
                            alert("请输入人员名称");
                            return false;
                        }
                        if ($("#loginname").val().length <= 0) {
                            alert("请输入登录名称");
                            return false;
                        }
                        if ($("#sel_role").val().length <= 0) {
                            alert("请输入角色");
                            return false;
                        }
                        var zTree = $.fn.zTree.getZTreeObj("departmentTree");
                        var nodes = zTree.getSelectedNodes();
                        if (dlgFlag == "add") {
                            data.bmid = nodes[0].id;
                            data.action = "AddUser";
                        }
                        if (dlgFlag == "edit") {
                            var selected = $("#dg").datagrid("getSelected");
                            data.action = "UpdateUser";
                            data.id = selected.id;
                        }
                        return true;
                    }, success: function (result) {
                        var data = eval('(' + result + ')');  // change the JSON string to javascript object
                        if (data.isSuccess) {
                            alert("添加成功");
                            $("#dg").datagrid('reload');
                            $('#dlg').dialog('close');
                        } else {
                            alert(data.msg);
                        }
                    }
                });
            }
        }, {
            text: '取 消',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#dlg').dialog('close');
            }
        }]
    });
}

var dlgFlag;
var wxid = "";

function addDlg() {
    if ($(".panel-icon").hasClass("icon-edit")) {
        $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
    }
    dlgFlag = "add";
    var zTree = $.fn.zTree.getZTreeObj("departmentTree");
    var nodes = zTree.getSelectedNodes();
    if (nodes.length <= 0) {
        alert("请选择部门后再点击添加人员");
        return false;
    }
    wxid = "";
    zwobj.url = "../../../handlers/ManageService.ashx?action=QueryRole";
    zwobj.data = {};
    ajaxData();
}

function editDlg() {
    if ($(".panel-icon").hasClass("icon-add")) {
        $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
    }
    dlgFlag = "edit";
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行编辑");
        return false;
    }
    wxid = selected.openid;
    zwobj.url = "../../../handlers/ManageService.ashx?action=QueryRole";
    zwobj.data = {};
    ajaxData();
}

function ajax_QueryRoles(obj) {
    $("#fm-users").form('reset');
    bindSelectDom("sel_role", obj.data);
    $('#dlg').dialog("open");
    if (dlgFlag == "edit") {
        var selected = $("#dg").datagrid("getSelected");
        $("#name").val(selected.name);
        $("#loginname").val(selected.loginname);
        $("#openid").val(selected.openid);
        bindSelectOption("sel_role", selected.rolename);
        $('#openid').combogrid('setValue', wxid);
    }
}