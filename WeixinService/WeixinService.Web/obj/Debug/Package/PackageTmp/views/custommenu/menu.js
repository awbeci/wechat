﻿
var setting = {
    view: {
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        selectedMulti: false
    },
    edit: {
        enable: true,
        editNameSelectAll: true,
        showRemoveBtn: showRemoveBtn,
        showRenameBtn: showRenameBtn
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeDrag: beforeDrag,
        beforeEditName: beforeEditName,
        beforeRemove: beforeRemove,
        beforeRename: beforeRename,
        onRemove: onRemove,
        onRename: onRename
    }
};

var zNodes = []; //ztree节点数组
var log, className = "dark";
var mynode = null;
var openDialogType = "";

function beforeDrag(treeId, treeNodes) {
    return false;
}
function beforeEditName(treeId, treeNode) {
    $(".myhiddentype").show();
    $(".myhiddenkey").show();
    $(".myhiddenurl").show();
    if ($(".panel-icon").hasClass("icon-add")) {
        $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
    }
    mynode = treeNode; //注意这里！
    $("#dlg").dialog("open");
    openDialogType = "edit";
    $("#name").val(treeNode.name);
    $("#type").val(treeNode.type);
    $("#nkey").val(treeNode.nkey);
    $("#nurl").val(treeNode.url);
    typechange();
    if (treeNode.level == 1) {
        $(".myhiddentype").hide();
        $(".myhiddenkey").hide();
        $(".myhiddenurl").hide();
    }
    if (treeNode.level == 2) {
        $("#nkey").attr("disabled", "disabled");
    }
    return false;
}

function beforeRemove(treeId, treeNode) {
    className = (className === "dark" ? "" : "dark");
    showLog("[ " + getTime() + " beforeRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.selectNode(treeNode);
    return confirm("确认删除菜单：[ " + treeNode.name + " ] 吗？"); //这里删除的只是前台数据，没有真正
}

function onRemove(e, treeId, treeNode) {
    //showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    $.post('../../handlers/CustomMenu.ashx?action=DelMenu&id=' + treeNode.id, function (data) {

    });
}

function beforeRename(treeId, treeNode, newName, isCancel) {
    className = (className === "dark" ? "" : "dark");
    showLog((isCancel ? "<span style='color:red'>" : "") + "[ " + getTime() + " beforeRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>" : ""));
    if (newName.length == 0) {
        alert("节点名称不能为空.");
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        setTimeout(function () { zTree.editName(treeNode); }, 10);
        return false;
    }
    return true;
}
function onRename(e, treeId, treeNode, isCancel) {
    showLog((isCancel ? "<span style='color:red'>" : "") + "[ " + getTime() + " onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>" : ""));
}
function showRemoveBtn(treeId, treeNode) {
    return !treeNode.isParent;
}
function showRenameBtn(treeId, treeNode) {
    return !(treeNode.level == 0);
}
function showLog(str) {
    if (!log) log = $("#log");
    log.append("<li class='" + className + "'>" + str + "</li>");
    if (log.children("li").length > 8) {
        log.get(0).removeChild(log.children("li")[0]);
    }
}
function getTime() {
    var now = new Date(),
			h = now.getHours(),
			m = now.getMinutes(),
			s = now.getSeconds(),
			ms = now.getMilliseconds();
    return (h + ":" + m + ":" + s + " " + ms);
}

var newCount = 1;

function addHoverDom(treeId, treeNode) {
    var tmp = treeNode.children == null ? true : false;
    if (treeNode.level == 0) {
        if (tmp) {
            showAddBtn(treeNode);
        } else if (treeNode.children.length < 3) {
            showAddBtn(treeNode);
        }
    }
    if (treeNode.level == 1) {
        if (tmp) {
            showAddBtn(treeNode);
        } else if (treeNode.children.length < 5) {
            showAddBtn(treeNode);
        }
    }
}

function showAddBtn(treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add node' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var btn = $("#addBtn_" + treeNode.tId);
    if (btn) btn.bind("click", function () {
        $(".myhiddentype").show();
        $(".myhiddenkey").show();
        $(".myhiddenurl").show();
        $("#nkey").removeAttr("disabled");
        $('#fm-menu').form('reset');
        typechange();
        if ($(".panel-icon").hasClass("icon-edit")) {
            $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
        }
        $("#dlg").dialog("open");
        openDialogType = "add";
        mynode = treeNode; //注意这里！
        return false;
    });
}
function removeHoverDom(treeId, treeNode) {
    if (treeNode.level != 0) {
        $("#addBtn_" + treeNode.tId).unbind().remove();
    }
}

function selectAll() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.setting.edit.editNameSelectAll = $("#selectAll").attr("checked");
}

$(document).ready(function () {
    queryMenu();
    $.fn.zTree.init($("#treeDemo"), setting, zNodes);
    $("#selectAll").bind("click", selectAll);
    typechange();
});

function typechange() {
    if ($("#type").val() == "view") {
        $(".myhiddenurl").show();
    } else if ($("#type").val() == "click") {
        $(".myhiddenurl").hide();
    }
}

function queryMenu() {
    zwobj.url = "../../handlers/CustomMenu.ashx?action=QueryMenuAddStitching";
    ajaxData();
}

function ajax_QueryMenuAddStitching(obj) {
    var len = obj.data.length;
    for (var i = 0; i < len; i++) {
        zNodes.push({
            id: obj.data[i].Id,
            pId: obj.data[i].Pid,
            name: obj.data[i].Name,
            type: obj.data[i].Type,
            nkey: obj.data[i].Nkey,
            url: obj.data[i].Url,
            icon: "../../images/page.png",
            open: true
        });
    }
}

function saveMenu() {
    var url = "";
    if (openDialogType == "add") {
        url = "../../handlers/CustomMenu.ashx?action=AddMenu&pid=" + mynode.id;
    }
    else if (openDialogType == "edit") {
        url = "../../handlers/CustomMenu.ashx?action=EditMenu&id=" + mynode.id;
    }
    $('#fm-menu').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                location.reload();
            } else {
                $.messager.alert("提示", "添加失败");
            }
        }
    });

    $("#dlg").dialog("close");
}

function ajax_AddMenu(obj) {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.refresh();
}

function sendMenu() {
    zwobj.url = "../../handlers/CustomMenu.ashx?action=SendMenu";
    ajaxData();
}

function ajax_SendMenu(obj) {
    data = eval('(' + obj.data + ')');
    if (data.errcode == 0 && data.errmsg == "ok") {
        alert("发送菜单成功");
    }
    else{
        alert("发送菜单失败，可能令牌已过期");
    }
}