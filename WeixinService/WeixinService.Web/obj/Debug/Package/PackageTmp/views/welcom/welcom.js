﻿$(function () {
    initTable();
});

var dgObj = {
    url: "",
    queryParams: { action: "queryproject" },
    fit: true,
    pagination: true,
    nowrap: false,
    fitColumns: true,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: "#toolbar",
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "ProjectFrontNum", title: "前缀码", width: 60, align: 'center' },
        { field: "InternetGroupId", title: "团购商id", width: 10, align: 'center', hidden: true },
        { field: "InternetGroup", title: "团购商", width: 70, align: 'center' },
        { field: "ProjectName", title: "项目名称", width: 70, align: 'center' },
        { field: "RoomTypeId", title: "房间类型id", width: 10, align: 'center', hidden: true },
        { field: "RoomTypeName", title: "房间类型", width: 120, align: 'center' }
    ]]
};

function initTable() {
    $("#dg").datagrid(dgObj);
}

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: '项目基本信息编辑',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
}

//添加方法
function addMenu() {
    $('#fm').form('clear');
    $('#dlg').dialog('open');
}
//修改方法
function editfunc() {
    $("#dlg").children().children().css("display", "block");
    var selected = $('#t_projectgroup').datagrid('getSelected');
    if (!selected) {
        $.messager.alert('提示', '请选中一行数据！');
        return;
    }
    $("#dlg").dialog("open");
    $("#hotel").val(selected.HotelId);
    $("#tgs").val(selected.InternetGroupId);
    $("#tgs").attr("disabled", "disabled");
    $("#htgs").val(selected.InternetGroupId);
    $("#qzm").val(selected.ProjectFrontNum);
    $("#qzm").attr("disabled", "disabled");
    $("#hqzm").val(selected.ProjectFrontNum);
    $("#projecmc").val(selected.ProjectName);
    $("#roomtype").val(selected.RoomTypeId + "#" + selected.RoomTypeName);
    if (selected.Rate == 0) {
        $("#rate").val("");
    } else { $("#rate").val(selected.Rate); }

    $("#rateCode").val(selected.RateCode);
    $("#bz").val(selected.Remarks);
    url = "/DataService/WebService/InternetGroup/InternetGroup.ashx?action=editproject";

}
//保存方法
var url;
function save() {
    var hotel = $("#hotel").val();
    if (hotel == "") {
        $.messager.alert('提示', '请选择酒店！');
        return;
    }
    var tgs = $("#tgs").val();
    if (tgs == "") {
        $.messager.alert('提示', '请选择团购商！');
        return;
    }
    var roomtype = $("#roomtype").val();
    if (roomtype == "") {
        $.messager.alert('提示', '请选择房间类型！');
        return;
    }
    $('#fm').form('submit', {
        url: url,
        onSubmit: function () {
            return $(this).form('validate');
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.success) {
                $('#dlg').dialog('close');
                $("#t_projectgroup").datagrid("reload");
                $.messager.alert('提示', result.msg);
            } else {
                $.messager.alert("提示", result.msg);
            }
        }
    });


}
//删除方法
function delfunc() {
    var selected = $('#t_projectgroup').datagrid('getSelected');
    if (!selected) {
        $.messager.alert('提示', '请选中一行数据！');
        return;
    }
    $.messager.confirm('提示', '确定删除?',
                function (r) {
                    if (r) {
                        $.ajax({
                            url: "/DataService/WebService/InternetGroup/InternetGroup.ashx?action=delproject",
                            type: "post",
                            data: { "qzm": selected.ProjectFrontNum, "tgsid": selected.InternetGroupId },
                            datatype: "json",
                            success: function (result) {
                                result = eval('(' + result + ')');
                                if (result.success) {
                                    $('#dlg').dialog('close');
                                    $("#t_projectgroup").datagrid("reload");
                                    $.messager.alert('提示', result.msg);
                                } else {
                                    $.messager.alert("提示", result.msg);
                                }
                            }
                        });
                    }
                });
}

//加载房间属性
function getRoomQuality() {
    $.ajax({
        url: "/DataService/WebService/InternetGroup/InternetGroup.ashx",
        type: "post",
        async: false,
        data: "action=queryroomquality&typeid=" + 1,
        dataType: "html",
        success: function (html) {
            var val = "";
            eval("val=" + html);
            var length = val.rows.length;
            $("#roomtype").html("<option value='' >—请选择—</option>");
            for (var i = 0; i < length; i++) {
                $("#roomtype").append("<option value=" + val.rows[i].TypeId + "#" + val.rows[i].Name + " >" + val.rows[i].Name + "</option>");
            }
        }
    });
}
