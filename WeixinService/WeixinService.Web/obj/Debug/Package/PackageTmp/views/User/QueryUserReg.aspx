﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QueryUserReg.aspx.cs" Inherits="WeixinService.Web.views.User.QueryUserReg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>信息发布</title>
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <!--easyui css-->
    <link href="../../js/jquery-easyui-1.4/themes/default/easyui.css" rel="stylesheet"
        type="text/css" />
    <link href="../../js/jquery-easyui-1.4/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-easyui-1.4/jquery.min.js" type="text/javascript"></script>
    <!--easyui js-->
    <script src="../../js/jquery-easyui-1.4/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-easyui-1.4/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <script src="QueryUserReg.js" type="text/javascript"></script>
    <script src="../../js/zwjs.js" type="text/javascript"></script>
    <script src="../../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <style>
        body
        {
            margin: 1px !important;
        }
    </style>
</head>
<body class="easyui-layout">
    <form id="form1" runat="server">
    <div id="mainPanle" region="center" border="false">
        <table id="dg" class="easyui-datagrid" data-options="toolbar:'#d_tb'">
        </table>
    </div>
    <div id="d_tb" style="padding: 3px; height: 30px;">
        <table>
            <tr style="height: 28px;">
                <td>
                    户号:
                </td>
                <td>
                    <input type="text" id="hh" name="hh"/>
                </td>
                <td>
                    开始时间:
                </td>
                <td>
                    <input type="text" placeholder=" —请选择— " id="kssj" name="kssj" style="width: 100px;"
                        class="Wdate" onfocus="WdatePicker({isShowClear:false,dateFmt:'yyyy-MM-dd',startDate:'%y-%M-%d',maxDate:'#F{$dp.$D(\'jssj\')||\'%y-%M-%d\'}'})" />
                </td>
                <td>
                    结束时间:
                </td>
                <td>
                    <input type="text" placeholder=" —请选择— " id="jssj" name="jssj" style="width: 100px;"
                        class="Wdate" onfocus="WdatePicker({isShowClear:false,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'kssj\')}',maxDate:'%y-%M-%d',startDate:'%y-%M-%d'})" />
                </td>
                <td>
                    <a href="javascript:search()" class="easyui-linkbutton" plain="true" iconcls="icon-search">
                        查询</a>
                </td>
                <td>
                    <asp:LinkButton ID="ExportQueryExcel" CssClass="easyui-linkbutton" plain="true" iconcls="icon-excel"
                        runat="server" OnClick="ExportQueryExcel_Click">1、导出查询数据</asp:LinkButton>
                </td>
                <td>
                    <asp:LinkButton ID="ExportAllExcel" CssClass="easyui-linkbutton" plain="true" iconcls="icon-excel"
                        runat="server" OnClick="ExportAllExcel_Click">2、全部导出</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
