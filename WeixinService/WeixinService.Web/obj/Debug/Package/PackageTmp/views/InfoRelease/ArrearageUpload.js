﻿var actiontype = "";
var key = "";
$(function () {
    key = queryUrlName("key");
    $("#dg").datagrid({
        url: "../../handlers/PowerInfo.ashx",
        queryParams: { action: "QueryPowerInfo", businesstype: '011_youo_dfzd' },
        fit: true,
        fitColumns: true,
        title: '欠费Excel列表',
        pagination: true,
        iconCls: 'icon-applicationviewcolumns',
        nowrap: false,
        border: true,
        rownumbers: true,
        singleSelect: true,
        striped: true,
        toolbar: "#toolbar",
        pageList: [10, 20, 30, 50],
        pageSize: 20,
        columns: [[
            { field: "Id", hidden: true },
            { field: "Title", title: "标题", width: 150, align: 'left' },
            {
                field: "MaterialContent",
                title: "内容",
                width: 300,
                align: 'left',
                formatter: function (val, obj) {
                    return encodeURI(val).substr(0, 80);
                }
            },
            { field: "Bak1", title: "文件月份", width: 130, align: 'left' },
            {
                field: "FlagRelease",
                title: "发布标识",
                width: 70,
                align: 'center',
                formatter: function (val, obj) {
                    switch (val) {
                        case "0":
                            return "未发布";
                        case "1":
                            return "已发布";
                        case "2":
                            return "已推送";
                        default:
                            return "未知";
                    }
                }
            },
            {
                field: "CreateDt",
                title: "创建日期",
                width: 150,
                align: 'center',
                formatter: function (val, obj) {
                    if (val != null) {
                        return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                    }
                }
            },
            { field: "CreatePerson", title: "创建人", width: 120, align: 'center' }
        ]]
    });
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
});

//打开添加\修改界面
function addDlg(data) {
    actiontype = data;
    $('#fm').form('reset');
    if (data == "edit") {
        if ($(".panel-icon").hasClass("icon-add")) {
            $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
        }
        var selected = $("#dg").datagrid("getSelected");
        if (selected == null) {
            alert("请选择一条数据进行编辑");
            return false;
        }
        $("#messageDescription").val(selected.MessageDescription);
        $("#title").val(selected.Title);
        $("#month").val(selected.Bak1);
    }
    if (data == "add") {
        if ($(".panel-icon").hasClass("icon-edit")) {
            $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
        }
        $.getJSON("../../handlers/Common.ashx", { action: "GetDate", r: Math.random() }, function (data1) {
            $("#month").val(data1.monthNow);
        });
    }
    $('#dlg').dialog("open");
}

//保存方法

function save() {
    var myurl;
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        myurl = '../../handlers/ArrearageUpload.ashx?action=Add&key=' + key;
    } else if (actiontype == "edit") {
        myurl = '../../handlers/ArrearageUpload.ashx?action=Edit&key=' + key + '&id=' + selected.Id;
    }

    $('#fm').form('submit', {
        url: myurl,
        data: { "key": queryUrlName("key") },
        onSubmit: function (data) {
            if ($("#title").val().length <= 0) {
                alert("请输入标题。");
                return false;
            }
            if ($("#month").val().length <= 0) {
                alert("请输入文件月份。");
                return false;
            }
            if ($("#Exclefile").val().length <= 0 && actiontype == "add") {
                alert("请输入内容。");
                return false;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (!result.error) {
                $('#dlg').dialog('close');
                $("#dg").datagrid("reload");
            } else {
                alert(result.msg);
            }
        }
    });
}

function del() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    if (confirm("确认删除此条数据吗？")) {
        $.getJSON('../../handlers/ArrearageUpload.ashx',
            { action: "Del", id: selected.Id, r: Math.random() },
            function (data) {
                if (!data.error) {
                    $("#dg").datagrid("reload");
                } else {
                    alert(data.msg);
                }
            });
    }
    return false;
}

function Release() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行发布");
        return false;
    }
    if (confirm("确认发布此条数据吗？")) {
        $.getJSON('../../handlers/ArrearageUpload.ashx',
            { action: "Release", id: selected.Id, r: Math.random() },
            function (data) {
                if (!data.error) {
                    $("#dg").datagrid("reload");
                } else {
                    alert(data.msg);
                }
            });
    }
    return false;
}

/**
* 日期时间格式化方法，
* 可以格式化年、月、日、时、分、秒、周
**/
Date.prototype.Format = function (formatStr) {
    var week = ['日', '一', '二', '三', '四', '五', '六'];
    return formatStr.replace(/yyyy|YYYY/, this.getFullYear())
 	             .replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
 	             .replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1)).replace(/M/g, (this.getMonth() + 1))
 	             .replace(/w|W/g, week[this.getDay()])
 	             .replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate()).replace(/d|D/g, this.getDate())
 	             .replace(/HH|hh/g, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours()).replace(/H|h/g, this.getHours())
 	             .replace(/mm/g, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes()).replace(/m/g, this.getMinutes())
 	             .replace(/ss/g, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds()).replace(/S|s/g, this.getSeconds());
};

/*
* 获取url参数值 
*/
function queryUrlName(name) {
    var url = location.href.replace(/\\u0026/g, "&");
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}