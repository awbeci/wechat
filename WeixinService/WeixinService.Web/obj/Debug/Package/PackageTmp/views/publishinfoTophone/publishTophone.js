﻿//全局变量
var key;
var name;
var people;

$(function () {
    key = queryUrlName("key");
    people = queryUrlName("people");
    people = decodeURI(people);
    name = queryUrlName("name");
    name = decodeURI(name);
    var date = new ZwGetDate();
    var dt = date.getZwDate();
    $("#title").val(dt + name);
    $("#listtitle").html(name);
});

function publish() {
    $('#publishfm').form('submit', {
        url: '../../handlers/PublishInfo.ashx?action=SavePublishInfoPhone',
        onSubmit: function (data) {
            data.key = key;
            data.people = people;
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                alert("添加成功");
            }
        }
    });
}

function ZwGetDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth();
    var mymonth;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();

    this.getZwDate = function () {
        switch (month) {
            case 0:
                mymonth = "01";
                break;
            case 1:
                mymonth = "02";
                break;
            case 2:
                mymonth = "03";
                break;
            case 3:
                mymonth = "04";
                break;
            case 4:
                mymonth = "05";
                break;
            case 5:
                mymonth = "06";
                break;
            case 6:
                mymonth = "07";
                break;
            case 7:
                mymonth = "08";
                break;
            case 8:
                mymonth = "09";
                break;
            case 9:
                mymonth = "10";
                break;
            case 10:
                mymonth = "11";
                break;
            case 11:
                mymonth = "12";
                break;
            default:
        }
        return year + "年" + mymonth + "月" + day + "日" + hours + "时" + minutes + "分 ";
    };
}