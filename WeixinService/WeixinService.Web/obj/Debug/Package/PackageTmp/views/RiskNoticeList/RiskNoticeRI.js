﻿$(function () {
    showWxGrid();
    initForm();
});

function initForm() {
    zwobj.url = "../../handlers/RiskNoticeService.ashx";
    zwobj.data = { action: 'QueryRiskNoticeRI' };
    ajaxData();
}

function ajax_QueryRiskNoticeRI(data) {
    if (data != null && typeof data != "undefined") {
        for (var i = 0, len = data.data.length; i < len; i++) {
            if (data.data[i].type == "1") {
                $("#wxid").combogrid('setValue', data.data[i].openid);
                $("#phoneNumber").textbox('setText', data.data[i].phonename);
                $("#name").val(data.data[i].name);
            }
            if (data.data[i].type == "2") {
                $("#wxid2").combogrid('setValue', data.data[i].openid);
                $("#phoneNumber2").textbox('setText', data.data[i].phonename);
                $("#name2").val(data.data[i].name);
            }
        }
    }
}

function clearForm(type) {
    if (type == 1) {
        $("#form-shr").form('reset');
    }
    if (type == 2) {
        $("#form-qfr").form('reset');
    }
}

function submitForm(type) {
    if (type == 1) {
        $('#form-shr').form('submit', {
            url: '../../handlers/RiskNoticeService.ashx?action=UpdateRiskNoticeRI',
            onSubmit: function (data) {
                var wxid = $("#wxid").textbox("getText");
                var wxid2 = $("#wxid2").textbox("getText");

                var sjh = $("#phoneNumber").textbox("getText");
                var name = $("#name").val();
                if (wxid.length <= 0) {
                    alert("请选择微信ID");
                    return false;
                }
                if (sjh == null || sjh == "") {
                    alert("请输入手机号");
                    return false;
                }
                if (name == null || name == "") {
                    alert("请输入姓名");
                    return false;
                }
                if (wxid == wxid2) {
                    alert("审核人和签发人不能相同，请重新选择！");
                    return false;
                }
                data.sjh = sjh;
                data.type = "1";
                return true;
            },
            success: function (result) {
                result = eval('(' + result + ')');
                if (result.isSuccess) {
                    alert("审核人保存成功");
                }
            }
        });
    }
    if (type == 2) {
        $('#form-qfr').form('submit', {
            url: '../../handlers/RiskNoticeService.ashx?action=UpdateRiskNoticeRI',
            onSubmit: function (data) {
                var wxid2 = $("#wxid2").textbox("getText");
                var wxid = $("#wxid").textbox("getText");
                var sjh = $("#phoneNumber2").textbox("getText");
                var name = $("#name2").val();
                if (wxid2.length <= 0) {
                    alert("请选择微信ID");
                    return false;
                }
                if (sjh == null || sjh == "") {
                    alert("请输入手机号");
                    return false;
                }
                if (name == null || name == "") {
                    alert("请输入姓名");
                    return false;
                }
                if (wxid == wxid2) {
                    alert("审核人和签发人不能相同，请重新选择！");
                    return false;
                }
                data.sjh = sjh;
                data.type = "2";
                return true;
            },
            success: function (result) {
                result = eval('(' + result + ')');
                if (result.isSuccess) {
                    alert("签发人保存成功");
                }
            }
        });
    }
}

function showWxGrid() {
    $('#wxid').combogrid({
        panelWidth: 400,
        idField: 'openid',
        textField: 'openid',
        editable: false,
        url: '../../handlers/ManageService.ashx',
        queryParams: { action: 'QueryUnbindWeixin2' },
        columns: [[
            { field: 'openid', title: '微信ID', width: 50, align: 'center' },
            { field: 'trueName', title: '真实姓名', align: 'center', width: 50 },
            { field: 'nickname', title: '昵称', align: 'center', width: 50 },
            { field: 'phonenumber', title: '手机号', align: 'center', width: 60 },
            { field: 'sex', title: '性别', align: 'center', width: 30,
                formatter: function (val, obj) {
                    if (val == 1) {
                        return '男';
                    } else {
                        return '女';
                    }
                }
            }
        ]],
        onClickRow: function (rowIndex, rowData) {
            $("#name").val(rowData.trueName);
            $("#phoneNumber").textbox("setText", rowData.phonenumber);
        },
        fitColumns: true
    });


    $('#wxid2').combogrid({
        panelWidth: 400,
        idField: 'openid',
        textField: 'openid',
        editable: false,
        url: '../../handlers/ManageService.ashx',
        queryParams: { action: 'QueryUnbindWeixin2' },
        columns: [[
            { field: 'openid', title: '微信ID', width: 50, align: 'center' },
            { field: 'trueName', title: '真实姓名', align: 'center', width: 50 },
            { field: 'nickname', title: '昵称', align: 'center', width: 50 },
            { field: 'phonenumber', title: '手机号', align: 'center', width: 60 },
            { field: 'sex', title: '性别', align: 'center', width: 30,
                formatter: function (val, obj) {
                    if (val == 1) {
                        return '男';
                    } else {
                        return '女';
                    }
                }
            }
        ]],
        onClickRow: function (rowIndex, rowData) {
            $("#name2").val(rowData.trueName);
            $("#phoneNumber2").textbox("setText", rowData.phonenumber);
        },
        fitColumns: true
    });
}