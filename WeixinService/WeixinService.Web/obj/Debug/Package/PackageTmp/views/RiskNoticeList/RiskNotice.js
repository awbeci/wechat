﻿var settingWest = {
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: zTreeOnClick
    }
};

var settingEast = {
    data: {
        simpleData: {
            enable: true
        }
    },
    edit: {
        enable: true,
        showRemoveBtn: true,
        showRenameBtn: false
    },
    callback: {
        beforeRemove: eastBeforeRemove
    }
};

function eastBeforeRemove(treeId, treeNode) {
    if (treeNode.type == 1) {
        alert("审核人不能删除");
        return false;
    }
    if (treeNode.type == 2) {
        alert("签发人不能删除");
        return false;
    }
    return true;
}

function zTreeOnClick(event, treeId, treeNode) {
    $('#dg-pp').datagrid('load', {
        action: "QueryUserInfoAndYwh",
        groupid: treeNode.id
    });
};

var dgPpObj = {
    url: "../../handlers/UsersInfo.ashx",
    queryParams: { action: "QueryUserInfoAndYwh", groupid: '' },
    fit: true,
    border: false,
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    title: '用户列表',
    //rownumbers: true,
    singleSelect: true,
    striped: true,
    pageList: [10, 20, 30, 50],
    pageSize: 10,
    columns: [[
         { field: "chk", checkbox: true },
         { field: "wxid", title: "微信号", width: 200, align: 'center' },
         { field: "nickname", title: "用户名", width: 100, align: 'center' },
        { field: "sjh", title: "手机号", width: 100, align: 'center' },
    //         { field: "qy", title: "企业名称", width: 100, align: 'center' },
        {field: "zw", title: "职位", width: 100, align: 'center',
        formatter: function (val, obj) {
            if (val == 1) {
                return "正职";
            }
            if (val == 2) {
                return "副职";
            }
        }
    }
    ]],
    onSelect: function (rowIndex, rowData) {
        var selected = $("#dg-pp").datagrid("getSelected");
        var westTreeObj = $.fn.zTree.getZTreeObj("westTree");
        var eastTreeObj = $.fn.zTree.getZTreeObj("eastTree");

        var westTreeNodes = westTreeObj.getSelectedNodes();

        if (eastTreeObj != null) {
            if (rowData.sjh == null || typeof rowData.sjh == "undefined") {
                alert("该用户没有手机号不能选择发送！");
                return false;
            }
            var typeObj;
            var nodes = eastTreeObj.getNodesByParam("id", rowData.wxid, null);
            if (nodes.length == 0) {
                switch (westTreeNodes[0].id) {
                    //运检部id                 
                    case "e4af4532-552e-66c4-17ac-d32df13d5b58":
                        if (selected.zw == null || selected.zw == "") {
                            alert("该用户没有职位不能选择发送！");
                            return false;
                        }
                        nodes = eastTreeObj.getNodesByParam("type", 3, null);
                        if (nodes.length > 0) {
                            alert("您选择的人员里面已经存在运检部人员，请先删除再点击添加！");
                            return false;
                        }
                        typeObj = {
                            id: rowData.wxid,
                            name: rowData.nickname,
                            sjh: rowData.sjh,
                            type: 3,
                            icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
                        };
                        break;
                    //营销部id                 
                    case "edc96f81-4169-e5b9-3387-4bb4c89f29b6":
                        if (selected.zw == null || selected.zw == "") {
                            alert("该用户没有职位不能选择发送！");
                            return false;
                        }
                        nodes = eastTreeObj.getNodesByParam("type", 4, null);
                        if (nodes.length > 0) {
                            alert("您选择的人员里面已经存在营销部人员，请先删除再点击添加！");
                            return false;
                        }
                        typeObj = {
                            id: rowData.wxid,
                            name: rowData.nickname,
                            sjh: rowData.sjh,
                            type: 4,
                            icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
                        };
                        break;
                    //安质部id                  
                    case "813e3917-2ffe-f645-5985-8540e7a9b5a5":
                        if (selected.zw == null || selected.zw == "") {
                            alert("该用户没有职位不能选择发送！");
                            return false;
                        }
                        nodes = eastTreeObj.getNodesByParam("type", 5, null);
                        if (nodes.length > 0) {
                            alert("您选择的人员里面已经存在安质部人员，请先删除再点击添加！");
                            return false;
                        }
                        typeObj = {
                            id: rowData.wxid,
                            name: rowData.nickname,
                            sjh: rowData.sjh,
                            type: 5,
                            icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
                        };
                        break;
                    default:
                        //用户(在高压用户里面选择) 
                        if (westTreeNodes[0].id == "103" || westTreeNodes[0].pId == "103") {
                            nodes = eastTreeObj.getNodesByParam("type", 6, null);
                            if (nodes.length > 0) {
                                alert("您选择的人员里面已经存在用户人员，请先删除再点击添加！");
                                return false;
                            }
                            typeObj = {
                                id: rowData.wxid,
                                name: rowData.nickname,
                                sjh: rowData.sjh,
                                type: 6,
                                icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
                            };
                        } else {
                            alert("您不应该在此部门下选择人员，请重新选择部门点击添加！");
                            return false;
                        }
                        break;
                }
                eastTreeObj.addNodes(null, typeObj);
            }
        }
    }
};

function addPpDlg() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条风险告知单进行发送！");
        return false;
    }
    switch (selected.State) {
        case 2:
            alert("数据已经审核，不能再发送！");
            return false;
        case 3:
            alert("数据已经签发，不能再发送！");
            return false;
        case 4:
            alert("数据已经确认，不能再发送！");
            return false;
        case 5:
            alert("数据已经备案，不能再发送！");
            return false;
        default:
    }

    $("#dg-pp").datagrid(dgPpObj);
    zwobj.url = "../../handlers/UserGroup.ashx?action=QuerySelectedNodes";
    zwobj.data = { id: selected.Id };
    ajaxData();
}

function ajax_QuerySelectedNodes(obj) {
    if (!obj.data3) {
        alert("审核人或签发人已经修改，请删除该条通知单或再新建一条通知单！");
        return false;
    }

    if (typeof (obj) == "undefined" || obj == null) {
        return false;
    }
    $('#dlg-pp').dialog("open");
    var zNodesGroup = [];
    for (var i = 0; i < obj.data.length; i++) {
        if (obj.data[i].id == 0)
            continue;
        zNodesGroup.push({
            id: obj.data[i].id,
            pId: obj.data[i].pid,
            name: obj.data[i].name,
            icon: "../../js/jquery-easyui-1.4/themes/icons/group.png",
            open: true
        });
    }

    var zNodesPp = [];
    if (typeof (obj.data2) != "undefined" || obj.data2 != null || obj.data2.length > 0) {
        for (var j = 0; j < obj.data2.length; j++) {
            zNodesPp.push({
                id: obj.data2[j].openid,
                name: obj.data2[j].nickname,
                sjh: obj.data2[j].sjh,
                type: obj.data2[j].type,
                icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
            });
        }
    }
    $.fn.zTree.init($("#westTree"), settingWest, zNodesGroup);
    $.fn.zTree.init($("#eastTree"), settingEast, zNodesPp);
    $("#dg-pp").datagrid(dgPpObj);
}
//-------------------------------以上是操作发布组的westTree和easyui datagrid-------------------------------------------------

function savePp() {
    var eastTreeObj = $.fn.zTree.getZTreeObj("eastTree");
    for (var j = 1; j <= 6; j++) {
        nodes = eastTreeObj.getNodesByParam("type", j, null);
        if (nodes.length == 0) {
            switch (j) {
                //                case 1:  
                //                    alert("请先维护好审核人再点击发送！");  
                //                    return;  
                //                case 2:  
                //                    alert("请先维护好签发人再点击发送！");  
                //                    return;  
                case 3:
                    alert("您没有选择运检部人员，请先选择好再点击发送！");
                    return;
                case 4:
                    alert("您没有选择营销部人员，请先选择好再点击发送！");
                    return;
                case 5:
                    alert("您没有选择安质部人员，请先选择好再点击发送！");
                    return;
                case 6:
                    alert("您没有选择用户，请先选择好再点击发送！");
                    return;
                default:
            }
        }
    }

    var nodes = eastTreeObj.getNodes();
    var nodesArrary = [];
    var selected = $("#dg").datagrid("getSelected");
    var id = selected.Id; //通知单id
    for (var i = 0; i < nodes.length; i++) {
        nodesArrary.push({ id: id, wxid: nodes[i].id, nickname: nodes[i].name, sjh: nodes[i].sjh, type: nodes[i].type });
    }

    zwobj.url = "../../handlers/RiskNoticeService.ashx";
    zwobj.data = { action: "AddRiskNoticeSigner", id: id, data: JSON.stringify(nodesArrary), num: selected.Number };
    ajaxData();
}

function ajax_AddRiskNoticeSigner(obj) {
    if (obj.isSuccess) {
        alert("发送成功");
    } else {
        alert("发送失败");
    }
    $('#dlg-pp').dialog("close");
    $("#dg").datagrid("reload");
}
//-------------------------------以上是操作发布人的eastTree和easyui datagrid-------------------------------------------------

$(function () {
    initTable();
    initDlg();
});

function addRiskNotice_click() {
    $("#dlg").dialog({ title: "新建", iconCls: "icon-add" });
    OpenDlgForm("add");
}

function editRiskNotice_click() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行编辑");
        return false;
    }
    switch (selected.State) {
        case 2:
            alert("数据已经审核，不能编辑！");
            return false;
        case 3:
            alert("数据已经签发，不能编辑！");
            return false;
        case 4:
            alert("数据已经确认，不能编辑！");
            return false;
        case 5:
            alert("数据已经备案，不能编辑！");
            return false;
        default:
    }
    $("#dlg").dialog({ title: "编辑", iconCls: "icon-edit" });
    OpenDlgForm("edit");

}

function delRiskNotice_click() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    switch (selected.State) {
        case 2:
            alert("数据已经审核，不能删除！");
            return false;
        case 3:
            alert("数据已经签发，不能删除！");
            return false;
        case 4:
            alert("数据已经确认，不能删除！");
            return false;
        case 5:
            alert("数据已经备案，不能删除！");
            return false;
        default:
    }
    del();
}

function sendRiskNotice_click() {
    addPpDlg();
}

function exportWord_click() {
    exportWord();
}

function viewRiskNotice() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行预览");
        return false;
    }
    $("#view-bh").empty();
    $("#view-rq").empty();
    $("#view-zs").empty();
    $("#view_Tdsb_Gq").empty();
    $("#view_Yxfxfx").empty();
    $("#view_Fxykcs").empty();
    $("#view_bz").empty();
    $("#view_sh").empty();
    $("#view_qf").empty();
    $("#view_yjb").empty().append("运检部：<br>");
    $("#view_yxb").empty().append("营销部：<br>");
    $("#view_azb").empty().append(" 安质部：<br>");
    $("#view_kh").empty().append("客&nbsp;&nbsp;户：<br>");

    $("#view-bh").text("编 号：" + selected.Number);
    $("#view-rq").text(selected.Rq);
    $("#view-zs").append("主 送" + selected.Zs);
    $("#view_Tdsb_Gq").append(selected.Tdsb_Gq);
    $("#view_Yxfxfx").append(selected.Yxfxfx);
    $("#view_Fxykcs").append(selected.Fxykcs.replace(/\r\n/g, '<br>'));
    $("#view_bz").append(selected.Operator);
    zwobj.url = "../../handlers/RiskNoticeService.ashx";
    zwobj.data = { action: "ViewRiskNotice", id: selected.Id };
    ajaxData2();
    $('#dlg-yl').dialog("open");
}

function ajax_ViewRiskNotice(obj) {
    if (obj == null) {
        return false;
    }
    var len = obj.data.length;
    for (var i = 0; i < len; i++) {
        switch (obj.data[i].Type) {
            case 1:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_sh").append("<img src='" + obj.data[i].Img + "' width='50' height='50'>");
                }
                break;
            case 2:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_qf").append("<img src='" + obj.data[i].Img + "' width='50' height='50' style='margin-left:30px;'>");
                }
                break;
            case 3:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_yjb").append("<img src='" + obj.data[i].Img + "' width='50' height='50'>");
                }
                break;
            case 4:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_yxb").append("<img src='" + obj.data[i].Img + "' width='50' height='50'>");
                }
                break;
            case 5:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_azb").append("<img src='" + obj.data[i].Img + "' width='50' height='50'>");
                }
                break;
            case 6:
                if (obj.data[i].Img != null && obj.data[i].Img != "") {
                    $("#view_kh").append("<img src='" + obj.data[i].Img + "' width='50' height='50'>");
                }
                break;
            default:
        }
    }
}

function search_click() {
    if ($("#kssj").val() == "") {
        alert("请先选择开始时间再点击查询！");
        return;
    }
    if ($("#jssj").val() == "") {
        alert("请先选择结束时间再点击查询！");
        return;
    }
    $("#dg").datagrid('load', {
        action: "QueryRiskNotice",
        kssj: $("#kssj").val(),
        jssj: $("#jssj").val(),
        bh: $("#number").val()
    });
}

var actiontype; //全局变量

var dgObj = {
    view: detailview,
    url: "../../handlers/RiskNoticeService.ashx",
    queryParams: { action: "QueryRiskNotice" },
    fit: true,
    title: '信息列表',
    fitColumns: false,
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: true,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: "#tb",
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "Id", hidden: true },
        { field: "Number", title: "编号", width: 100, align: 'center' },
        { field: "Rq", title: "日期", width: 100, align: 'center' },
        { field: "Zs", title: "主送", width: 200, align: 'left' },
        { field: "Tdsb_Gq", title: "停电设备及工期", width: 200, align: 'left' },
        { field: "Yxfxfx", title: "运行风险分析", width: 200, align: 'left' },
        { field: "Fxykcs", title: "风险预控措施", width: 200, align: 'left' },
        { field: "State", title: "状态", width: 80, align: 'center',
            formatter: function (val, obj) {
                switch (val) {
                    case 0:
                        return "未发送";
                    case 1:
                        return "已发送";
                    case 2:
                        return "已审核";
                    case 3:
                        return "已签发";
                    case 4:
                        return "已确认";
                    case 5:
                        return "已备案";
                    default:
                        return "未知错误";
                }
            }
        },
        {
            field: "Send_Dt",
            title: "发送日期",
            width: 150,
            align: 'center',
            formatter: function (val, obj) {
                if (val != null) {
                    return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                }
            }
        }, {
            field: "CreateDt",
            title: "创建日期",
            width: 150,
            align: 'center',
            formatter: function (val, obj) {
                if (val != null) {
                    return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                }
            }
        }
    ]],
    detailFormatter: function (index, row) {
        return '<div style="padding:2px"><table class="ddv"></table></div>';
    },
    onExpandRow: function (index, row) {
        var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
        ddv.datagrid({
            url: "../../handlers/RiskNoticeService.ashx",
            queryParams: { action: "QueryRiskNoticeDetail", id: row.Id },
            singleSelect: true,
            rownumbers: false,
            loadMsg: '正在加载，请稍等！',
            height: 'auto',
            columns: [[
                            { field: 'name', title: '人员名称', width: 100, align: 'center' },
                            { field: 'sjh', title: '手机号', width: 100, align: 'center' },
                            { field: 'type', title: '人员类型', width: 100, align: 'center',
                                formatter: function (val, obj) {
                                    switch (val) {
                                        case 1:
                                            return "审核人";
                                        case 2:
                                            return "签发人";
                                        case 3:
                                            return "运检部";
                                        case 4:
                                            return "营销部";
                                        case 5:
                                            return "安质部";
                                        case 6:
                                            return "客户";
                                        default:
                                            return "未知";
                                    }
                                }
                            },
                            { field: 'state', title: '签字状态', width: 100, align: 'center',
                                formatter: function (val, obj) {
                                    if (val == "0") {
                                        return "未签字";
                                    } else {
                                        return "已签字";
                                    }
                                }
                            },
                            { field: 'dt', title: '签字日期', width: 150, align: 'center',
                                formatter: function (val, obj) {
                                    if (val != null) {
                                        return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                                    }
                                }
                            }
                        ]],
            onResize: function () {
                $('#dg').datagrid('fixDetailRowHeight', index);
            },
            onLoadSuccess: function () {
                setTimeout(function () {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                }, 0);
            }
        });
        $('#dg').datagrid('fixDetailRowHeight', index);
    }
};

function initTable() {
    $("#dg").datagrid(dgObj);
}

function exportWord() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行导出！");
        return false;
    }

    zwobj.url = "../../handlers/RiskNoticeService.ashx";
    zwobj.data = { action: "ExportWord", data: JSON.stringify(selected) };
    ajaxData2();
}

function ajax_ExportWord(obj) {
    if (obj.isSuccess) {
        location.href = obj.data;
    } else {
        alert("导出word失败");
    }
}

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
    $('#fm-dlg').dialog({
        title: '编 辑',
        modal: true,
        closed: true,
        buttons: "#fm-dlg-btn"
    });
    $('#dlg-yl').dialog({
        title: '预 览',
        iconCls: "icon-publish",
        modal: true,
        closed: true,
        buttons: "#dlg-yl-btnTool"
    });
    $('#dlg-word').dialog({
        title: '导出word',
        iconCls: "icon-word",
        modal: true,
        closed: true,
        buttons: "#dlg-word-btn"
    });

    $('#dlg-pp').dialog({
        title: '选择人并发送',
        modal: true,
        iconCls: "icon-send",
        closed: true,
        buttons: [{
            text: '发 送',
            iconCls: 'icon-ok',
            handler: function () {
                savePp();
            }
        }, {
            text: '取 消',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#dlg-pp').dialog('close');
            }
        }]
    });
}

function OpenDlgForm(type) {
    actiontype = type;
    var selected = $("#dg").datagrid("getSelected");
    zwobj.url = "../../handlers/RiskNoticeService.ashx";
    if (type == "add") {
        zwobj.data = { action: "QueryRiskNoticeRIB", type: type };
    }
    if (type == "edit") {
        zwobj.data = { action: "QueryRiskNoticeRIB", type: type, id: selected.Id };
    }
    ajaxData();
}

function ajax_QueryRiskNoticeRIB(obj) {
    $("#fm").form("reset");
    for (var i = 0, len = obj.data.length; i < len; i++) {
        if (obj.data[i].type == "1") {
            $("#shr").val(obj.data[i].name);
        }
        if (obj.data[i].type == "2") {
            $("#qfr").val(obj.data[i].name);
        }
    }

    $("#dlg").dialog("open");
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        $("#zs").val("安质部、运检部、营销部、");
        $("#bzr").val(obj.bzr);
    }
    if (actiontype == "edit") {
        $("#bh").val(selected.Number);
        $("#rq").val(selected.Rq);
        $("#zs").val(selected.Zs);
        $("#tdsbgq").val(selected.Tdsb_Gq);
        $("#yxfxfx").val(selected.Yxfxfx);
        $("#fxykcs").val(selected.Fxykcs);
        $("#bzr").val(obj.bzr[0].bzr);
    }
    $("#bh").focus();
}

function save() {
    $('#fm').form('submit', {
        url: '../../handlers/RiskNoticeService.ashx?action=SaveRiskNotice',
        onSubmit: function (data) {
            var bhtxt = $("#bh").val();
            var bh = "第 " + (new Date).getFullYear() + "-" + bhtxt + " 号";
            if (bhtxt.length <= 0) {
                alert("请先输入编号");
                return false;
            }
            if ($("#rq").val().length <= 0) {
                alert("请先选择日期");
                return false;
            }
            if ($("#zs").val().length <= 0) {
                alert("请先输入主送");
                return false;
            }
            if ($("#tdsbgq").val().length <= 0) {
                alert("请先输入停电设备及工期");
                return false;
            }
            if ($("#yxfxfx").val().length <= 0) {
                alert("请先输入运行风险分析");
                return false;
            }
            if ($("#fxykcs").val().length <= 0) {
                alert("请先输入风险预控措施");
                return false;
            }
            if ($("#bzr").val().length <= 0) {
                alert("请先输入编制人");
                return false;
            }
            if (actiontype == "add") {
                data.type = "add";
                data.bh2 = bh;
                if (isNaN(parseInt(bhtxt))) {
                    alert("您输入的编号不符合规范，编号应该是数字如：012");
                    return false;
                }
                var getData = $("#dg").datagrid("getRows");
                if (typeof getData != "undefined") {
                    if (parseInt(bhtxt) <= parseInt(getData[0].Number.split('-')[1].split(' ')[0])) {
                        alert("已经存在相同编号通知单，或编号不能小于已经存在的编号！");
                        $("#bh").val("");
                        $("#bh").focus();
                        return false;
                    }
                }
            }
            if (actiontype == "edit") {
                data.type = "edit";
                var selected = $("#dg").datagrid("getSelected");
                data.id = selected.Id;
                data.bh2 = bhtxt;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                if (actiontype == "add") {
                    alert("新建成功");
                    $("#dg").datagrid("reload");
                }
                if (actiontype == "edit") {
                    alert("编辑成功");
                    $("#dg").datagrid("reload");
                }
                $("#dlg").dialog("close");
            }
        }
    });
}

function del() {
    if (confirm("确认删除此条数据吗？")) {
        var selected = $("#dg").datagrid("getSelected");
        zwobj.url = "../../handlers/RiskNoticeService.ashx";
        zwobj.data = { action: "DelRiskNotice", id: selected.Id };
        ajaxData();
    }
}

function ajax_DelRiskNotice(obj) {
    if (obj.isSuccess) {
        alert("删除成功");
    } else {
        alert("删除失败");
    }
    $("#dg").datagrid("reload");
}