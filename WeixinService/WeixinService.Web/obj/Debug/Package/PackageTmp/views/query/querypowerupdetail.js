﻿var url_id;
var key;
$(function () {
    url_id = queryUrlName("id");
    key = queryUrlName("key");
    queryDetailInfo();
});

function queryDetailInfo() {
    zwobj.url = "../../handlers/PowerCutService.ashx?action=GetInfo2&key=" + key + "&id=" + url_id;
    ajaxData();
}

function ajax_QueryDetailInfo(obj) {
    var html = "";
    $("#title").html(obj.data[0].Title);
    $("#time").html(eval("new " + obj.data[0].CreateDt.split('/')[1]).Format("yyyy-MM-dd"));
    if (key == "011_youo_tdtz") {
        html += "尊敬的电力客户：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因供电公司电力施工，安排以下计划停电工作安排，未经铜陵供电公司有关部门许可，严禁任何单位和个人在停电线路及设备上工作。为此造成的不便，敬请各客户给予谅解和支持。如有任何疑问，请致电供电公司24小时服务热线95598。";
    } else if (key == "011_youo_tdtz2") {
        html += "尊敬的电力客户：<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因突发电力故障，安排以下故障抢修工作安排。未经铜陵供电公司有关部门许可，严禁任何单位和个人在停电线路及设备上工作。为此造成的不便，敬请各客户给予谅解和支持。如有任何疑问，请致电供电公司24小时服务热线95598。";
    }
    for (var i = 0, len = obj.data.length; i < len; i++) {
        html += "<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;停电日期：" + eval("new " + obj.data[i].PowerCutTime.split('/')[1]).Format("yyyy-MM-dd")
        + "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时间范围：" + obj.data[i].TimeArea
        + "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;停电设备：" + obj.data[i].Device + "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;停电范围：" + obj.data[i].Area;
    } 
    $("#content").html(html);
}