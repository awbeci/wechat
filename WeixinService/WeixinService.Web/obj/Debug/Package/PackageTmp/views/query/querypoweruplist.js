﻿var key;
var name;
var kssj, jssj;
$(function () {
    name = queryUrlName("name");
    name = decodeURI(name);
    key = queryUrlName("key");
    kssj = queryUrlName("kssj");
    jssj = queryUrlName("jssj");
    $("#listtitle").html(name + "信息列表");
    queryMessageList();
});

function queryMessageList() {
    zwobj.url = "../../handlers/PowerCutService.ashx?action=Query";
    zwobj.data = { key: key, kssj: kssj, jssj: jssj };
    ajaxData();
}

function ajax_QueryMessageList(obj) {
    if (obj == null || obj.data == null || obj.data.length <= 0) {
        //alert("数据为空，请联系管理人员");
        $("#myul").html("没有查询到数据");
        return false;
    }
    var html = "";
    var itime = "";
    var icount = 0;
    for (var i = 0; i < obj.data.length; i++) {
        if (itime == obj.data[i].PowerCutTime) {
            icount = icount + 1;
            if (icount != 0) {
                itime = obj.data[i].PowerCutTime;
                continue;
            }
        }
        else if (itime != obj.data[i].PowerCutTime) {
            icount = 0;
            itime = obj.data[i].PowerCutTime;
        }
        html += '<li onclick="myclick(\'' + obj.data[i].Id + '\')"><div style="font-size:16px;font-weight:bold;margin-left:15px;margin-bottom:2px;">' + obj.data[i].Title + '</div>';
        html += '<div class="mydescription">' + obj.data[i].Area + '</div></li>';
    }
    $("#myul").html(html);
}

function myclick(id) {
    location.href = "querypowerupdetail.htm?key=" + key + "&id=" + id;
}