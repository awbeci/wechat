﻿$(function () {
    $("#dg").datagrid({
        url: "../../handlers/PublishInfo.ashx",
        queryParams: {
            action: "GetUserRiskMessage",
            state: $("#s_status").val(),
            r: Math.random()
        },
        fit: true,
        title: '用户风险告知',
        pagination: true,
        iconCls: 'icon-applicationviewcolumns',
        nowrap: false,
        rownumbers: true,
        singleSelect: true,
        border: false,
        striped: true,
        toolbar: "#toolbar",
        pageList: [10, 20, 30, 50],
        pageSize: 20,
        columns: [[
            { field: "Id", hidden: true },
            { field: "Title", title: "标 题", width: 150, align: 'left' },
            { field: "MessageDescription", title: "描 述", width: 200, align: 'left' },
            {
                field: "State",
                title: "按钮状态",
                width: 70,
                align: 'center',
                formatter: function (val, obj) {
                    switch (val) {
                        case "1":
                            return "待推送";
                        case "2":
                            return "待确认";
                        case "3":
                            return "已确认";
                        default:
                            return "未知";
                    }
                }
            },
            {
                field: 'bak5',
                title: '语音状态',
                width: 80,
                align: 'center',
                formatter: function (val, data) {
                    switch (val) {
                        case "1":
                            return "已确认";
                        default:
                            return "未知";
                    }
                }
            },
            {
                field: 'bak4',
                title: '音频文件',
                width: 80,
                align: 'center',
                formatter: function (val, data) {
                    if (val == null) {
                        return null;
                    }
                    return "<span class='myvoice' onclick='play(\"" + val + "\")'></span>";
                }
            },
            { field: "Nickname", title: "昵称", width: 100, align: 'center' },
            { field: "Openid", title: "微信号", width: 200, align: 'center' },
            { field: "bak1", title: "企业名称", width: 200, align: 'left' },
            { field: "bak2", title: "联系人", width: 200, align: 'left' },
            { field: "bak3", title: "固话", width: 230, align: 'left' },
            { field: "PhoneNumber", title: "手机号", width: 230, align: 'left' },
            {
                field: "confirmdt",
                title: "确认日期",
                width: 150,
                align: 'center',
                formatter: function (val, obj) {
                    if (val != null) {
                        return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                    }
                }
            }
        ]],
        onLoadSuccess: function (data) {
            if (data.error) {
                alert(data.msg);
            }
        }
    });
});

function query() {
    $("#dg").datagrid("load", {
        action: "GetUserRiskMessage",
        state: $("#s_status").val(),
        r: Math.random()
    });
}

function select_change() {
    query();
}

//发送
function Send() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行编辑。");
        return false;
    }
    if (selected.State == "3") {
        alert("已确认消息，不能再一次发送。");
        return false;
    }
    $.getJSON("../../handlers/PublishInfo.ashx", {
        action: "Send",
        key: queryUrlName("key"),
        Id: selected.Id,
        title: selected.Title,
        userid: selected.Openid,
        MessageDescription: selected.MessageDescription,
        r: Math.random()
    }, function (data) {
        if (data.error) {
            alert(data.msg);
        } else {
            $("#dg").datagrid("reload");
        }
    });
}

/**
* 日期时间格式化方法，
* 可以格式化年、月、日、时、分、秒、周
**/
Date.prototype.Format = function (formatStr) {
    var week = ['日', '一', '二', '三', '四', '五', '六'];
    return formatStr.replace(/yyyy|YYYY/, this.getFullYear())
 	             .replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
 	             .replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1)).replace(/M/g, (this.getMonth() + 1))
 	             .replace(/w|W/g, week[this.getDay()])
 	             .replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate()).replace(/d|D/g, this.getDate())
 	             .replace(/HH|hh/g, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours()).replace(/H|h/g, this.getHours())
 	             .replace(/mm/g, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes()).replace(/m/g, this.getMinutes())
 	             .replace(/ss/g, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds()).replace(/S|s/g, this.getSeconds());
};

function queryUrlName(name) {
    var url = location.href.replace(/\\u0026/g, "&");
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}

function play(val) {
    var html = playVoice('../..' + val, 200, 30);
    $("#dlg-voice").html(html);
}

function playVoice(u, w, h) {
    var pv = '';
    pv += '<object width="' + w + '" height="' + h + '" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab">';
    pv += '<param name="src" value="' + u + '">';
    pv += '<param name="controller" value="true">';
    pv += '<param name="type" value="video/quicktime">';
    pv += '<param name="autoplay" value="true">';
    pv += '<param name="target" value="myself">';
    pv += '<param name="bgcolor" value="black">';
    pv += '<param name="pluginspage" value="http://www.apple.com/quicktime/download/index.html">';
    pv += '<embed AUTOSTART="true" src="' + u + '" width="' + w + '" height="' + h + '" controller="true" align="middle" bgcolor="white" target="myself" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/index.html"></embed>';
    pv += '</object>';
    return pv;
}