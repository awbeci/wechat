﻿var settingWest = {
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: zTreeOnClick
    }
};

var settingEast = {
    data: {
        simpleData: {
            enable: true
        }
    },
    edit: {
        enable: true,
        showRemoveBtn: true,
        showRenameBtn: false
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    $('#dg-pp').datagrid('load', {
        action: "QueryUserInfoAndYwh",
        groupid: treeNode.id
    });
};

var dgPpObj = {
    url: "../../handlers/UsersInfo.ashx",
    queryParams: { action: "QueryUserInfoAndYwh", groupid: '' },
    fit: true,
    border: false,
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    title: '用户列表',
    //rownumbers: true,
    singleSelect: true,
    striped: true,
    pageList: [10, 20, 30, 50],
    pageSize: 10,
    columns: [[
         { field: "chk", checkbox: true },
         { field: "wxid", title: "微信号", width: 200, align: 'left' },
         { field: "nickname", title: "用户名", width: 100, align: 'center' },
         { field: "qy", title: "企业名称", width: 200, align: 'left' },
        { field: "zw", title: "职务名称", width: 200, align: 'left' }
    ]],
    onSelect: function (rowIndex, rowData) {
        var eastTreeObj = $.fn.zTree.getZTreeObj("eastTree");
        if (eastTreeObj != null) {
            var nodes = eastTreeObj.getNodesByParam("id", rowData.wxid, null);
            if (nodes.length == 0) {
                eastTreeObj.addNodes(null, {
                    id: rowData.wxid,
                    name: rowData.nickname,
                    ywh: rowData.ywh,
                    icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
                });
            }
        }
    }
};

function addPpDlg() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行发布");
        return false;
    }
    $('#dlg-pp').dialog("open");
    $("#dg-pp").datagrid(dgPpObj);
    zwobj.url = "../../handlers/UserGroup.ashx?action=QuerySelectedNodes";
    zwobj.data = { id: selected.Id };
    ajaxData();
}

function ajax_QuerySelectedNodes(obj) {
    if (typeof (obj) == "undefined" || obj == null) {
        return false;
    }

    var zNodesGroup = [];
    for (var i = 0; i < obj.data.length; i++) {
        if (obj.data[i].id == 0)
            continue;
        zNodesGroup.push({
            id: obj.data[i].id,
            pId: obj.data[i].pid,
            name: obj.data[i].name,
            icon: "../../js/jquery-easyui-1.4/themes/icons/group.png",
            open: true
        });
    }

    var zNodesPp = [];
    if (typeof (obj.data2) != "undefined" || obj.data2 != null || obj.data2.length > 0) {
        for (var j = 0; j < obj.data2.length; j++) {
            zNodesPp.push({
                id: obj.data2[j].openid,
                name: obj.data2[j].nickname,
                ywh: obj.data2[j].rnumber,
                icon: "../../js/jquery-easyui-1.4/themes/icons/user.png"
            });
        }
    }
    $.fn.zTree.init($("#westTree"), settingWest, zNodesGroup);
    $.fn.zTree.init($("#eastTree"), settingEast, zNodesPp);
    $("#dg-pp").datagrid(dgPpObj);
}
//-------------------------------以上是操作发布组的westTree和easyui datagrid-------------------------------------------------

function savePp() {
    var eastTreeObj = $.fn.zTree.getZTreeObj("eastTree");
    var nodes = eastTreeObj.getNodes();
    var nodesArrary = [];
    for (var i = 0; i < nodes.length; i++) {
        nodesArrary.push({ wxid: nodes[i].id, nickname: nodes[i].name, ywh: nodes[i].ywh });
    }
    zwobj.url = "../../handlers/UsersInfo.ashx?action=AddPersonToInfoRelease";
    zwobj.data = {
        id: $("#dg").datagrid("getSelected").Id,
        ppid: JSON.stringify(nodesArrary),
        key: key,
        title: $("#dg").datagrid("getSelected").Title,
        ms: $("#dg").datagrid("getSelected").MessageDescription
    };
    ajaxData2();
}

function ajax_AddPersonToInfoRelease(obj) {
    if (obj.isSuccess) {
        alert("发布成功");
    } else {
        alert("发布失败");
    }
    $('#dlg-pp').dialog("close");
    $("#dg").datagrid("reload");
}
//-------------------------------以上是操作发布人的eastTree和easyui datagrid-------------------------------------------------
$(function () {
    key = queryUrlName("key");
    name = queryUrlName("name");
    name = decodeURI(name);
    initTable();
    initDlg();
    $("#ilable").text(name + "内容");
});
//全局变量
var editor;
var actiontype;
var key;
var name;
KindEditor.ready(function (k) {
    editor = k.create('textarea[name="content"]', {
        width: "500px",
        minWidth: "500px",
        resizeType: 1,
        allowPreviewEmoticons: false,
        //是否可以浏览上传文件
        allowFileManager: true,
        //是否可以上传
        allowUpload: true,
        //浏览文件方法
        fileManagerJson: '../../handlers/KindEditor.ashx?action=KindEditorRequest',
        //上传文件方法(注意这两个路径),
        uploadJson: '../../handlers/KindEditor.ashx?action=UploadImage',
        items: [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'image', 'multiimage']
    });
});

var dgObj = {
    url: "../../handlers/PublishInfo.ashx",
    queryParams: { action: "QueryPublishInfo", businesstype: key },
    fit: true,
    fitColumns: true,
    title: '信息列表',
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: [{
        text: '新建',
        iconCls: 'icon-add',
        handler: function () {
            addDlg('add');
        }
    }, '-', {
        text: '编辑',
        iconCls: 'icon-edit',
        handler: function () {
            addDlg('edit');
        }
    }, '-', {
        text: '删除',
        iconCls: 'icon-remove',
        handler: function () {
            del();
        }
    }, '-', {
        text: '发布并推送',
        iconCls: 'icon-world',
        handler: function () {
            addPpDlg();
        }
    }
    ],
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "Id", hidden: true },
        {
            field: "Title",
            title: "标题",
            width: 220,
            align: 'left',
            formatter: function (val, obj) {
                return name + ":" + val;
            }
        },
        {
            field: "MessageDescription",
            title: "描述",
            width: 300,
            align: 'left',
            formatter: function (val, obj) {
                if (val == null) {
                    return null;
                }
                return val.substr(0, 25);
            }
        },
        {
            field: "MaterialType",
            title: "类型",
            width: 70,
            align: 'center',
            formatter: function (val, obj) {
                switch (val) {
                    case "text":
                        return "文本";
                    case "image":
                        return "图片";
                    case "voice":
                        return "语音";
                    case "video":
                        return "视频";
                    case "music":
                        return "视频";
                    case "news":
                        return "图文";
                    default:
                        return "未知";
                }
            }
        },
        {
            field: "FlagRelease",
            title: "发布标识",
            width: 70,
            align: 'center',
            formatter: function (val, obj) {
                switch (val) {
                    case "0":
                        return "未发布";
                    case "1":
                        return "已发布";
                    case "2":
                        return "已推送";
                    case "3":
                        return "用户确认";
                    case "4":
                        return "推送失败";
                    default:
                        return "未知";
                }
            }
        },
        {
            field: "CreateDt",
            title: "创建日期",
            width: 120,
            align: 'center',
            formatter: function (val, obj) {
                if (val != null) {
                    return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                }
            }
        },
        { field: "CreatePerson", title: "创建人", width: 120, align: 'center' }
    ]],
    view: detailview,
    detailFormatter: function (index, row) {
        return '<div style="padding:2px"><table class="ddv"></table></div>';
    },
    onExpandRow: function (index, row) {
        var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
        ddv.datagrid({
            url: '../../handlers/PublishInfo.ashx',
            queryParams: {
                action: 'QueryInfoReleaseDetails',
                id: row.Id
            },
            fitColumns: false,
            singleSelect: true,
            loadMsg: '正在加载，请稍候...',
            height: 'auto',
            columns: [[
                { field: 'wxh', title: '微信号', width: 200, align: 'left' },
                { field: 'nickname', title: '昵称', width: 150, align: 'center' },
                { field: 'yyzt', title: '语音状态', width: 80, align: 'center',
                    formatter: function (val, data) {
                        switch (val) {
                            case "0":
                                return "未确认";
                            case "1":
                                return "已确认";
                            default:
                                return "未知";
                        }
                    }
                },
                { field: 'yy', title: '音频文件', width: 80, align: 'center', formatter: function (val, data) {
                    if (val == null) {
                        return null;
                    }
                    return "<span class='myvoice' onclick='play(\"" + val + "\")'></span>";
                }
                },
                {
                    field: 'state',
                    title: '按钮状态',
                    width: 80,
                    align: 'center',
                    formatter: function (val, data) {
                        switch (val) {
                            case "1":
                                return "待推送";
                            case "2":
                                return "待确认";
                            case "3":
                                return "已确认";
                            default:
                                return "未知";
                        }
                    }
                },
                {
                    field: 'confirmdt',
                    title: '确认时间',
                    width: 150,
                    align: 'center',
                    formatter: function (val, obj) {
                        if (val != null) {
                            return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                        }
                    }
                }
            ]],
            onResize: function () {
                $('#dg').datagrid('fixDetailRowHeight', index);
            },
            onLoadSuccess: function () {
                setTimeout(function () {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                }, 0);
            }
        });
        $('#dg').datagrid('fixDetailRowHeight', index);
    }
};

function play(val) {
    var html = playVoice('../..' + val, 200, 30);
    $("#dlg-voice").html(html);
}

function playVoice(u, w, h) {
    var pv = '';
    pv += '<object width="' + w + '" height="' + h + '" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab">';
    pv += '<param name="src" value="' + u + '">';
    pv += '<param name="controller" value="true">';
    pv += '<param name="type" value="video/quicktime">';
    pv += '<param name="autoplay" value="true">';
    pv += '<param name="target" value="myself">';
    pv += '<param name="bgcolor" value="black">';
    pv += '<param name="pluginspage" value="http://www.apple.com/quicktime/download/index.html">';
    pv += '<embed AUTOSTART="true" src="' + u + '" width="' + w + '" height="' + h + '" controller="true" align="middle" bgcolor="white" target="myself" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/index.html"></embed>';
    pv += '</object>';
    return pv;
}

function initTable() {
    dgObj.queryParams.businesstype = key;
    dgObj.title = name + "列表";
    $("#dg").datagrid(dgObj);
}

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });


    $('#dlg-pp').dialog({
        title: ' 添加到人',
        modal: true,
        iconCls: "icon-user",
        closed: true,
        buttons: [{
            text: '确 定',
            iconCls: 'icon-ok',
            handler: function () {
                savePp();
            }
        }, {
            text: '取 消',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#dlg-pp').dialog('close');
            }
        }]
    });
}

function addDlg(data) {
    actiontype = data;
    editor.html("");
    $('#fm').form('reset');
    if (data == "edit") {
        if ($(".panel-icon").hasClass("icon-add")) {
            $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
        }
        var selected = $("#dg").datagrid("getSelected");
        if (selected == null) {
            alert("请选择一条数据进行编辑");
            return false;
        }
        $("#messageDescription").val(selected.MessageDescription);
        $("#title").val(selected.Title);
        editor.html(selected.MaterialContent);
    }
    if (data == "add") {
        if ($(".panel-icon").hasClass("icon-edit")) {
            $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
        }
    }
    $("#title").val("高压用户风险告知通知书");
    $('#dlg').dialog("open");
}

function del() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    if (confirm("确认删除此条数据吗？")) {
        zwobj.url = "../../handlers/PublishInfo.ashx?action=DelPublishInfo";
        zwobj.data = { id: selected.Id };
        ajaxData();
    }
}

//保存方法
function save() {
    var myurl;
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        myurl = '../../handlers/PublishInfo.ashx?action=SavePublishInfo&key=' + key;
    } else if (actiontype == "edit") {
        myurl = '../../handlers/PublishInfo.ashx?action=EditPublishInfo&key=' + key + '&id=' + selected.Id;
    }

    $('#fm').form('submit', {
        url: myurl,
        onSubmit: function (data) {
            data.contents = editor.html(); //这里加ajax data数据
            if ($("#title").val().length <= 0) {
                alert("请输入标题");
                return false;
            }
            if (editor.html().length <= 0) {
                alert("请输入内容");
                return false;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                $('#dlg').dialog('close');
                $("#dg").datagrid("reload");
            }
        }
    });
}

function ajax_DelPowerInfo(obj) {
    $("#dg").datagrid("reload");
}

function query() {
    var txt = $("#txtPara").val();
    $('#dg-pp').datagrid('load', {
        action: "QueryPerson",
        txt: txt
    });
}