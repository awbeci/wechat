﻿$(function () {
    $("#dg").datagrid({
        url: "../../handlers/ReplyMsg.ashx",
        queryParams: {
            action: "QueryReceiveInfo",
            dateBegin: $("#dateBegin").val(),
            dateEnd: $("#dateEnd").val(),
            keyWords: $("#keyWords").val(),
            UnReply: $("input[id='UnReply']").attr("checked") ? "1" : "2",
            r: Math.random()
        },
        fit: true,
        fitColumns: true,
        title: '用户消息',
        pagination: true,
        iconCls: 'icon-applicationviewcolumns',
        nowrap: false,
        rownumbers: true,
        singleSelect: true,
        border: false,
        striped: true,
        toolbar: "#toolbar",
        pageList: [10, 20, 30, 50],
        pageSize: 20,
        columns: [[
            { field: "id", hidden: true },
            { field: "bak4", title: "用户昵称", width: 150, align: 'left' },
            {
                field: "MsgType",
                title: "类型",
                width: 70,
                align: 'center',
                formatter: function (val, obj) {
                    switch (val) {
                        case "text":
                            return "文本";
                        case "image":
                            return "图片";
                        case "voice":
                            return "语音";
                        case "video":
                            return "视频";
                        case "music":
                            return "视频";
                        case "news":
                            return "图文";
                        default:
                            return "未知";
                    }
                }
            },
            { field: "Content", title: "内容", width: 170, align: 'center' },
            {
                field: 'bak2',
                title: '音频文件',
                width: 210,
                align: 'center',
                formatter: function (val, data) {
                    if (!val) {
                        return null;
                    }
                    return "<span class='myvoice' onclick='play(\"" + val + "\")'></span>";
                }
            },
            { field: "Title", title: "标题", width: 70, align: 'center' },
            { field: "Description", title: "描述", width: 200, align: 'left' },
            {
                field: "bak1",
                title: "是否回复",
                width: 200,
                align: 'center',
                formatter: function (val, obj) {
                    if (val == 2) {
                        return "是";
                    } else {
                        return "否";
                    }
                }
            },
            {
                field: "CreateTime",
                title: "接收日期",
                width: 150,
                align: 'center',
                formatter: function (val, obj) {
                    if (val != null) {
                        return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                    }
                }
            }
        ]],
        view: detailview,
        detailFormatter: function (index, row) {
            return "<div style='padding:2px;'><table id='ddv-" + index + "'></table></div>";
        },
        onExpandRow: function (index, row) {
            $("#ddv-" + index).datagrid({
                url: "../../handlers/ReplyMsg.ashx",
                queryParams: {
                    action: "QueryReplyInfo",
                    msgId: row.id,
                    r: Math.random()
                },
                fitColums: true,
                singleSelect: true,
                rownumbers: true,
                loadMsg: "加载中...",
                height: "auto",
                columns: [[
                    {
                        field: "CreateTime",
                        title: "回复时间",
                        width: 150,
                        align: 'center',
                        formatter: function (val, obj) {
                            if (val != null) {
                                return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                            }
                        }
                    },
                    {
                        field: "IsSend",
                        title: "是否发送",
                        width: 100,
                        align: 'center',
                        formatter: function (val, obj) {
                            if (val == 1) {
                                return "否";
                            } else {
                                return "是";
                            }
                        }
                    },
                    {
                        field: "TextMsg",
                        title: "回复内容",
                        width: 700,
                        formatter: function (val, obj) {
                            return "<label title='" + val + "'>" + val + "</label>";
                        }
                    }
                ]],
                onLoadSuccess: function () {
                    setTimeout(function () {
                        $("#dg").datagrid("fixDetailRowHeight", index);
                    });
                },
                onResize: function () {
                    $("#dg").datagrid("fixDetailRowHeight", index);
                }
            });
        }
    });
    $('#dlg').dialog({
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
    $.getJSON("../../handlers/Common.ashx", { action: "GetDate", r: Math.random() }, function (data) {
        $("#dateBegin").val(data.d1ago);
        $("#dateEnd").val(data.now);
    });
});

function play(val) {
    var html = playVoice('../..' + val, 200, 30);
    $("#dlg-voice").html(html);
}

function playVoice(u, w, h) {
    var pv = '';
    pv += '<object width="' + w + '" height="' + h + '" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab">';
    pv += '<param name="src" value="' + u + '">';
    pv += '<param name="controller" value="true">';
    pv += '<param name="type" value="video/quicktime">';
    pv += '<param name="autoplay" value="true">';
    pv += '<param name="target" value="myself">';
    pv += '<param name="bgcolor" value="black">';
    pv += '<param name="pluginspage" value="http://www.apple.com/quicktime/download/index.html">';
    pv += '<embed AUTOSTART="true" src="' + u + '" width="' + w + '" height="' + h + '" controller="true" align="middle" bgcolor="white" target="myself" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/index.html"></embed>';
    pv += '</object>';
    return pv;
}

function query() {
    $("#dg").datagrid("load", {
        action: "QueryReceiveInfo",
        dateBegin: $("#dateBegin").val(),
        dateEnd: $("#dateEnd").val(),
        keyWords: $("#keyWords").val(),
        UnReply: $("input[id='UnReply']").attr("checked") ? "1" : "2",
        r: Math.random()
    });
}

//打开添加\修改界面
function Reply() {
    $('#fm').form('reset');
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行编辑。");
        return false;
    }
    $("#msgId").val(selected.id);
    $("#nickName").val(selected.bak4);
    $("#openId").val(selected.FromUserName);
    $("#content").val(selected.Content);
    $('#dlg').dialog("open");
}

function save() {
    $.getJSON("../../handlers/ReplyMsg.ashx", {
        action: "ReplyMsg1",
        msgId: $("#msgId").val(),
        openId: $("#openId").val(),
        replyContent: $("#replyContent").val(),
        r: Math.random()
    }, function (data) {
        if (!data.error) {
            alert("回复成功。");
            $("#dg").datagrid("reload");
            $('#dlg').dialog("close");
        } else {
            alert("保存失败。请刷新页面后重试。");
        }
    });
}

/**
* 日期时间格式化方法，
* 可以格式化年、月、日、时、分、秒、周
**/
Date.prototype.Format = function (formatStr) {
    var week = ['日', '一', '二', '三', '四', '五', '六'];
    return formatStr.replace(/yyyy|YYYY/, this.getFullYear())
 	             .replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
 	             .replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1)).replace(/M/g, (this.getMonth() + 1))
 	             .replace(/w|W/g, week[this.getDay()])
 	             .replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate()).replace(/d|D/g, this.getDate())
 	             .replace(/HH|hh/g, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours()).replace(/H|h/g, this.getHours())
 	             .replace(/mm/g, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes()).replace(/m/g, this.getMinutes())
 	             .replace(/ss/g, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds()).replace(/S|s/g, this.getSeconds());
};