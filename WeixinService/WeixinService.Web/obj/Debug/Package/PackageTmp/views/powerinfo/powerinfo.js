﻿$(function () {
    key = queryUrlName("key");
    initTable();
    initDlg();
});

var editor; //全局变量
var actiontype; //全局变量
var key; //全局变量
KindEditor.ready(function (k) {
    editor = k.create('textarea[name="content"]', {
        width: "500px",
        minWidth: "500px",
        resizeType: 1,
        allowPreviewEmoticons: false,
        allowFileManager: true, //是否可以浏览上传文件
        allowUpload: true, //是否可以上传
        fileManagerJson: '../../handlers/KindEditor.ashx?action=KindEditorRequest', //浏览文件方法
        uploadJson: '../../handlers/KindEditor.ashx?action=UploadImage', //上传文件方法(注意这两个路径),
        items: [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'image', 'multiimage']
    });
});

var dgObj = {
    url: "../../handlers/PowerInfo.ashx",
    queryParams: { action: "QueryPowerInfo", businesstype: key },
    fit: true,
    title: '信息列表',
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: "#toolbar",
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
         { field: "Id", hidden: true },
        { field: "Title", title: "标题", width: 180, align: 'left' },
        { field: "MaterialContent", title: "内容", width: 500, align: 'left', formatter: function (val, obj) {
            if (val == null) {
                return null;
            }
            return encodeURI(val).substr(0, 80);
        }
        },
        { field: "MaterialType", title: "类型", width: 70, align: 'center', formatter: function (val, obj) {
            switch (val) {

                case "text":
                    return "文本";
                case "image":
                    return "图片";
                case "voice":
                    return "语音";
                case "video":
                    return "视频";
                case "music":
                    return "视频";
                case "news":
                    return "图文";
                default: return "未知";
            }
        }
        },
        { field: "FlagRelease", title: "发布标识", width: 70, align: 'center', formatter: function (val, obj) {
            switch (val) {
                case "0":
                    return "未发布";
                case "1":
                    return "已发布";
                case "2":
                    return "已推送";
                default: return "未知";
            }
        }
        },
        { field: "MessageDescription", title: "描述", width: 300, align: 'left', formatter: function (val, obj) {
            if (val == null) {
                return null;
            }
            return val.substr(0, 25);
        }
        },
        { field: "CreateDt", title: "创建日期", width: 150, align: 'center', formatter: function (val, obj) {
            if (val != null) {
                return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
            }
        }
        },
        { field: "CreatePerson", title: "创建人", width: 120, align: 'center' }
    ]]
};

function initTable() {
    dgObj.queryParams.businesstype = key;
    $("#dg").datagrid(dgObj);
}

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
}

function addDlg(data) {
    actiontype = data;
    editor.html("");
    $('#fm').form('reset');
    if (data == "edit") {
        if ($(".panel-icon").hasClass("icon-add")) {
            $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
        }
        var selected = $("#dg").datagrid("getSelected");
        if (selected == null) {
            alert("请选择一条数据进行编辑");
            return false;
        }
        $("#messageDescription").val(selected.MessageDescription);
        $("#title").val(selected.Title);
        editor.html(selected.MaterialContent);
    }
    if (data == "add") {
        if ($(".panel-icon").hasClass("icon-edit")) {
            $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
        }
    }
    $('#dlg').dialog("open");
}

function del() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    if (confirm("确认删除此条数据吗？")) {
        zwobj.url = "../../handlers/PowerInfo.ashx?action=DelPowerInfo";
        zwobj.data = { id: selected.Id };
        ajaxData();
    }
}

//保存方法
function save() {
    var myurl;
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        myurl = '../../handlers/PowerInfo.ashx?action=SavePowerInfo&key=' + key;
    } else if (actiontype == "edit") {
        myurl = '../../handlers/PowerInfo.ashx?action=EditPowerInfo&key=' + key + '&id=' + selected.Id;
    }

    $('#fm').form('submit', {
        url: myurl,
        onSubmit: function (data) {
            data.contents = editor.html();//这里加ajax data数据
            if ($("#title").val().trim().length <= 0) {
                alert("请输入标题");
                return false;
            }
            if (editor.html().trim().length <= 0) {
                alert("请输入内容");
                return false;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                $('#dlg').dialog('close');
                $("#dg").datagrid("reload");
            }
        }
    });
}

function ajax_DelPowerInfo(obj) {
    $("#dg").datagrid("reload");
}
