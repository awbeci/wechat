﻿
function getToken() {
    $.ajax({
        url: '../Main.ashx?action=GetToken',
        type: 'GET',
        async: false,
        dataType: 'json',
        success: function (data) {

        },
        error: function () {
            alert("error");
        }
    });
}

function getMenu() {
    $.ajax({
        url: '../Main.ashx?action=GetMenu',
        type: 'GET',
        async: false,
        dataType: 'json',
        success: function (data) {

        },
        error: function () {
            alert("error");
        }
    });
}

function addMenu() {
    $.ajax({
        url: '../Main.ashx?action=AddMenu',
        type: 'GET',
        async: false,
        dataType: 'json',
        success: function (data) {

        },
        error: function () {
            alert("error");
        }
    });
}

function delMenu() {
    $.ajax({
        url: '../Main.ashx?action=DelMenu',
        type: 'GET',
        async: false,
        dataType: 'json',
        success: function (data) {

        },
        error: function () {
            alert("error");
        }
    });
}