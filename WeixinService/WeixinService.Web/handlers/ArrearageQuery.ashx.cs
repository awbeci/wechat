﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using Microsoft.Office.Interop.Excel;
using WeixinService.Dal;
using WeixinService.Model;
using DataTable = System.Data.DataTable;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// ArrearageQuery 的摘要说明
    /// </summary>
    public class ArrearageQuery : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        UsersInfoDal userdal = new UsersInfoDal();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("ArrearageQuery.ashx,描述:映射方法名出错,错误原因:" + e.Message);
            }
        }

        private void Query(HttpContext context)
        {
            try
            {
                //先查询有没有注册。对于已注册的，查询欠费列表；对于没有注册的，返回{"error":true,"msg":"Unreg"},前台判断后跳转到注册界面
                var phone = context.Request["phone"] ?? "";
                UserRegDal userRegDal = new UserRegDal();
                DataTable dtReg = userRegDal.Query(new UserReg() { PhoneNumber = phone });
                if (dtReg != null && dtReg.Rows.Count > 0)//已注册
                {
                    var yhid = "";
                    foreach (DataRow row in dtReg.Rows)
                    {
                        yhid += row["RNumber"].ToString() + ",";
                    }
                    yhid = yhid.Remove(yhid.Length - 1, 1);

                    InfoReleaseDal dal = new InfoReleaseDal();
                    var dt = dal.Query(new InfoRelease() { FlagRelease = "1", BusinessType = "011_youo_dfzd" });
                    string file = "";
                    string createDt = "";
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        file = dt.Rows[0]["MaterialContent"].ToString();
                        createDt = dt.Rows[0]["CreateDt"].ToString();
                        context.Response.Write(string.IsNullOrEmpty(yhid) ? "{\"error\":true,\"msg\":\"用户号为空。\"}" : ReadExcel(yhid, file, createDt));
                    }
                    else
                    {
                        context.Response.Write("{\"error\":true,\"msg\":\"没有数据。\"}");
                        return;
                    }
                }
                else
                {
                    context.Response.Write("{\"error\":true,\"msg\":\"unreg\"}");//未注册
                    return;
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");//未注册
                Log.Error(ex);
            }
        }

        /// <summary>
        /// 读取一个XLS文件并显示出来
        /// </summary>
        /// <param name="yhid">用户号</param>
        /// <param name="file">文件路径，包括文件名称</param>
        /// <returns></returns>
        public string ReadExcel(string yhid, string file, string createDt)
        {
            string sheetName = GetExcelSheetNames(file)[0];
            string strSql = "select * from [" + sheetName + "$] where 用户编号 in (" + yhid + ")";
            DataSet ds = GetDataTable(file, strSql);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var ret = "{\"createDt\":\"" + createDt + "\",\"rows\":[";
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ret += "{\"key\":\"用户编号\",\"value\":\"" + row["用户编号"].ToString()
                           + "\"},{\"key\":\"电费年月\",\"value\":\"" + row["电费年月"].ToString()
                           + "\"},{\"key\":\"欠费\",\"value\":\"" + row["欠费金额"].ToString()
                           + "\"},";
                }
                ret = ret.Remove(ret.Length - 1, 1);
                ret += "]}";
                return ret;
            }
            else
            {
                return "{\"error\":true,\"msg\":\"没有欠费记录或用户编号错误。\"}";
            }
        }

        private void GetUserName(HttpContext context)
        {
            try
            {
                var yhid = context.Request["yhid"] ?? "";
                InfoReleaseDal dal = new InfoReleaseDal();
                var dt = dal.Query(new InfoRelease() { FlagRelease = "1", BusinessType = "011_youo_dfzd" });
                string file = "";
                if (dt != null && dt.Rows.Count > 0)
                {
                    file = dt.Rows[0]["MaterialContent"].ToString();
                }
                string sheetName = GetExcelSheetNames(file)[0];
                string strSql = "select * from [" + sheetName + "$] where 用户编号 =" + yhid + "";
                DataSet ds = GetDataTable(file, strSql);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    context.Response.Write("{\"error\":false,\"name\":\"" + ds.Tables[0].Rows[0]["用户名称"].ToString() +
                                           "\"}");
                }
                else
                {
                    context.Response.Write("{\"error\":true,\"msg\":\"用户号输入错误。\"}");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");
            }
        }

        private void GetUserNameByDb(HttpContext context)
        {
            try
            {
                var yhid = context.Request["yhid"] ?? "";
                //InfoReleaseDal dal = new InfoReleaseDal();
                //var dt = dal.Query(new InfoRelease() { FlagRelease = "1", BusinessType = "011_youo_dfzd" });
                //string file = "";
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    file = dt.Rows[0]["MaterialContent"].ToString();
                //}
                var da = userdal.QueryUserName(yhid);

                if (da != null && da.Rows.Count > 0)
                {
                    context.Response.Write("{\"error\":false,\"name\":\"" + da.Rows[0]["nickname"].ToString() +
                                           "\"}");
                }
                else
                {
                    context.Response.Write("{\"error\":true,\"msg\":\"用户号输入错误。\"}");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");
            }
        }

        /// <summary>
        /// 根据文件名查询指定sql语句，并返回结果
        /// </summary>
        /// <param name="file">文件名</param>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        private DataSet GetDataTable(string file, string sql)
        {
            var dic = HttpRuntime.AppDomainAppPath + file;
            string oleconn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dic + ";Extended Properties='Excel 8.0;HDR=yes'";
            //string oleconn = "Provider=Microsoft.Ace.OleDb.12.0;" + "data source=" + dic + ";Extended Properties='Excel 12.0; HDR=Yes; IMEX=1'"; //此连接可以操作.xls与.xlsx文件 (支持Excel2003 和 Excel2007 的连接字符串)
            //  HDR=NO　即无字段 
            //   HDR=yes　即有字段，一般默认excel表中第1行的列标题为字段名，如姓名、年龄等 
            //如果您在连接字符串中指定 HDR=NO，Jet OLE DB 提供程序将自动为您命名字段（F1 表示第一个字段，F2 表示第二个字段，依此类推）； 
            // IMEX　表示是否强制转换为文本 
            //   Excel 驱动程序读取指定源中一定数量的行（默认情况下为 8 行）以推测每列的数据类型。 
            //如果推测出列可能包含混合数据类型（尤其是混合了文本数据的数值数据时）， 
            //驱动程序将决定采用占多数的数据类型，并对包含其他类型数据的单元返回空值。 
            //（如果各种数据类型的数量相当，则采用数值类型。） 
            //Excel 工作表中大部分单元格格式设置选项不会影响此数据类型判断。 
            //可以通过指定导入模式来修改 Excel 驱动程序的此行为。 
            //若要指定导入模式，请在“属性”窗口中将 IMEX=1 添加到 Excel 
            //连接管理器的连接字符串内的扩展属性值中。 

            OleDbConnection conn = new OleDbConnection(oleconn);
            conn.Open();
            DataSet ds = new DataSet();
            try
            {
                OleDbDataAdapter oda = new OleDbDataAdapter(sql, conn);

                oda.Fill(ds);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            finally
            {
                conn.Close();
            }
            return ds;
        }

        /// <summary> 
        /// 获取获得当前你选择的Excel Sheet的所有名字
        /// </summary> 
        /// <param name="filePath"></param> 
        /// <returns></returns> 
        public static string[] GetExcelSheetNames(string filePath)
        {
            Microsoft.Office.Interop.Excel.ApplicationClass excelApp = new Microsoft.Office.Interop.Excel.ApplicationClass();
            Workbooks wbs = excelApp.Workbooks;
            Workbook wb = wbs.Open(filePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            int count = wb.Worksheets.Count;
            string[] names = new string[count];
            for (int i = 1; i <= count; i++)
            {
                names[i - 1] = ((Worksheet)wb.Worksheets[i]).Name;
            }
            wb.Close(null, null, null);
            excelApp.Quit();
            wbs.Close();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(wbs);

            excelApp = null;
            wbs = null;
            wb = null;
            return names;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}