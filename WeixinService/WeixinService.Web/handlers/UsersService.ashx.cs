﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for UsersService
    /// </summary>
    public class UsersService : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        Hashtable _hashtable = new Hashtable();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        public void Login(HttpContext context)
        {
            var name = context.Request.Params["name"];
            var pwd = context.Request.Params["pwd"];
            var jzmm = context.Request.Params["jzmm"];
            context.Session["user"] = null;
            var usersDal = new UsersDal();
            try
            {
                var tmp = usersDal.Exist(new Users()
                    {
                        LoginName = name,
                        Password = pwd
                    });
                if (tmp)
                {
                    context.Session["user"] = name;
                    if (jzmm == "1")
                    {
                        context.Session.Timeout = 60 * 24 * 7;//保存7天
                    }
                    _hashtable["isSuccess"] = true;
                    _hashtable["jsMethod"] = "ajax_Login";
                    var json = _jss.Serialize(_hashtable);
                    context.Response.Write(json);
                }
                else
                {
                    _hashtable["isSuccess"] = false;
                    _hashtable["msg"] = "用户名或密码错误，请重新输入";
                    var json = _jss.Serialize(_hashtable);
                    context.Response.Write(json);
                }
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 验证session
        /// </summary>
        /// <param name="context"></param>
        public void VerifySession(HttpContext context)
        {
            var user = context.Session["user"];
            if (user == null)
            {
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_VerifySession";
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            else
            {
                _hashtable["isSuccess"] = true;
                _hashtable["data"] = user.ToString();
                _hashtable["jsMethod"] = "ajax_VerifySession";
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
        }

        /// <summary>
        /// 根据登录名获取信息
        /// </summary>
        /// <param name="context"></param>
        public void QueryUserInfo(HttpContext context)
        {
            var usersDal = new UsersDal();
            var user = context.Session["user"];
            if (user != null)
            {
                var data = usersDal.QueryUserInfo(user.ToString());
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   name = da.Field<string>("name"),
                                   loginname = da.Field<string>("loginname"),
                                   password = da.Field<string>("password"),
                               };
                _hashtable["data"] = list.ToList();
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_QueryUserInfo";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            else
            {
                _hashtable["data"] = "session";
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_QueryUserInfo";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="context"></param>
        public void ModifyPassword(HttpContext context)
        {
            var usersDal = new UsersDal();
            var oldpass = context.Request.Params["oldpass"];
            var newpass = context.Request.Params["newpass"];
            var newpass2 = context.Request.Params["newpass2"];
            var user = context.Session["user"];
            if (user != null)
            {
                var data = usersDal.QueryUserInfo(user.ToString());
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               name = da.Field<string>("name"),
                               loginname = da.Field<string>("loginname"),
                               password = da.Field<string>("password"),
                           };
                if (list.First().password != oldpass)
                {
                    _hashtable["data"] = "msg";
                    _hashtable["msg"] = "原始密码错误";
                }
                else
                {
                    usersDal.ModifyPassword(new Users()
                        {
                            LoginName = user.ToString(),
                            Password = newpass
                        });
                    _hashtable["data"] = "null";
                }
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}