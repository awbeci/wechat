﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for MessageList
    /// </summary>
    public class MessageList : IHttpHandler, IRequiresSessionState
    {

        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //var session = context.Session["user"];
            //if (session == null)
            //{
            //    context.Response.Write("location.href='../views/main/login.html';");
            //    return;
            //}
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询信息列表
        /// </summary>
        /// <param name="context"></param>
        public void QueryMessageList(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var key = context.Request.Params["key"];
            var flag = context.Request.Params["flag"];
            var wxid = context.Request.Params["wxid"];
            try
            {
                var data = new DataTable();
                switch (flag)
                {
                    case "0"://0表示风险告知数据
                        data = infoReleaseDal.QueryByWxid(wxid);
                        break;
                    case "1"://表示停电告知等数据
                        if (key == "013_youo_jdzs")//这里用电知识只显示前20条数据
                        {
                            data = infoReleaseDal.Query2(new InfoRelease()
                            {
                                BusinessType = key,
                                FlagRelease = "'1','2'"
                            });
                            break;
                        }
                        else
                        {
                            data = infoReleaseDal.Query(new InfoRelease()
                            {
                                BusinessType = key,
                                FlagRelease = "'1','2'"
                            });
                            break;
                        }
                }
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               Title = da.Field<string>("Title"),
                               MessageDescription = da.Field<string>("MessageDescription")
                           };
                hashtable["data"] = list.ToList();
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryMessageList";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("错误原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询详细信息
        /// </summary>
        /// <param name="context"></param>
        public void QueryDetailInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
            var id = context.Request.Params["id"];
            var wxid = context.Request.Params["wxid"];
            try
            {
                var data = infoReleaseDal.QueryDetailInfo(new InfoRelease()
                {
                    Id = id
                });
                //查询状态
                var stateDt = infoReleaseDetailsDal.QueryState(wxid, id);
                var state = stateDt.Rows.Count > 0 ? stateDt.Rows[0]["state"].ToString() : "2";

                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Title = da.Field<string>("Title"),
                               MaterialContent = da.Field<string>("MaterialContent"),
                               CreateDt = da.Field<DateTime?>("CreateDt")
                           };
                hashtable["data"] = list.ToList();
                hashtable["state"] = state;
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryDetailInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("错误原因：" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}