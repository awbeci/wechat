﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;
using System.Web.SessionState;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for BusinessOutletsService
    /// </summary>
    public class BusinessOutletsService : IHttpHandler
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        private BusinessOutletsDal _businessOutletsDal = new BusinessOutletsDal();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //var session = context.Session["user"];
            //if (session == null)
            //{
            //    context.Response.Write("location.href='../views/main/login.html';");
            //    return;
            //}
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询营业网点信息列表
        /// </summary>
        /// <param name="context"></param>
        public void QueryBusinessOutlets(HttpContext context)
        {
            var hashtable = new Hashtable();

            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var data = _businessOutletsDal.QueryByPage(new BusinessOutlets(), page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               AreaId = da.Field<string>("AreaId"),
                               AreaName = da.Field<string>("AreaName"),
                               Address = da.Field<string>("Address"),
                               Name = da.Field<string>("Name"),
                               Longitude = da.Field<string>("Longitude"),
                               Latitude = da.Field<string>("Latitude"),
                               Bak1 = da.Field<string>("Bak1"),
                               Bak2 = da.Field<string>("Bak2"),
                               Bak3 = da.Field<string>("Bak3"),
                               Bak4 = da.Field<string>("Bak4")
                           };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryBusinessOutlets,描述:查询营业网点信息方法出错,错误原因:" + e.Message);
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
        }

        /// <summary>
        /// 新增营业网点
        /// </summary>
        /// <param name="context"></param>
        public void AddBusinessOutlets(HttpContext context)
        {
            var hashtable = new Hashtable();
            var qy = context.Request.Params["qy"];
            var dz = context.Request.Params["dz"];
            var name = context.Request.Params["name"];
            var jd = context.Request.Params["jd"];
            var wd = context.Request.Params["wd"];

            var businessOutlets = new BusinessOutlets();
            businessOutlets.Id = Guid.NewGuid().ToString();
            businessOutlets.Name = name;
            businessOutlets.Latitude = wd;
            businessOutlets.Longitude = jd;
            businessOutlets.Address = dz;
            businessOutlets.AreaName = qy;

            try
            {
                _businessOutletsDal.Add(businessOutlets);
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_AddBusinessOutlets";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 编辑营业网点
        /// </summary>
        /// <param name="context"></param>
        public void EditBusinessOutlets(HttpContext context)
        {
            var hashtable = new Hashtable();
            var id = context.Request.Params["id"];
            var qy = context.Request.Params["qy"];
            var dz = context.Request.Params["dz"];
            var name = context.Request.Params["name"];
            var jd = context.Request.Params["jd"];
            var wd = context.Request.Params["wd"];

            BusinessOutlets businessOutlets = new BusinessOutlets();
            businessOutlets.Id = id;
            businessOutlets.Longitude = jd;
            businessOutlets.Latitude = wd;
            businessOutlets.Name = name;
            businessOutlets.Address = dz;
            businessOutlets.AreaName = qy;
            try
            {
                _businessOutletsDal.Modify(businessOutlets);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 删除营业网点
        /// </summary>
        /// <param name="context"></param>
        public void DelBusinessOutlets(HttpContext context)
        {
            var hashtable = new Hashtable();
            var businessOutletsDal = new BusinessOutletsDal();
            var id = context.Request.Params["id"];

            try
            {
                businessOutletsDal.Del(new BusinessOutlets()
                    {
                        Id = id
                    });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_DelBusinessOutlets";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 根据经纬度查询营业网点信息
        /// </summary>
        /// <param name="context"></param>
        public void QueryBusinessOutByJdAndWd(HttpContext context)
        {
            Log.Debug("in QueryBusinessOutByJdAndWd-----------------");
            var hashtable = new Hashtable();
            var businessOutletsDal = new BusinessOutletsDal();
            var jd = context.Request.Params["jd"] ?? "0";
            var wd = context.Request.Params["wd"] ?? "0";
            Log.Debug("jd:" + jd + "wd:" + wd);
            try
            {
                var data = businessOutletsDal.QueryByJdAndWd(float.Parse(jd), float.Parse(wd));
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   id = da.Field<string>("id"),
                                   areaName = da.Field<string>("AreaName"),
                                   address = da.Field<string>("Address"),
                                   name = da.Field<string>("Name"),
                                   longitude = da.Field<string>("Longitude"),
                                   latitude = da.Field<string>("Latitude"),
                               };
                hashtable["isSuccess"] = true;
                hashtable["data"] = list.ToList();
                hashtable["jsMethod"] = "ajax_QueryBusinessOutByJdAndWd";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                //context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Debug("方法名:QueryBusinessOutByJdAndWd,描述:根据经纬度查询营业网点信息,错误原因:" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}