﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Common 的摘要说明
    /// </summary>
    public class Common : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("描述:映射方法名出错,错误原因:" + e);
            }
        }

        public void GetDate(HttpContext context)
        {
            DateTime now = DateTime.Now;
            string ret = "{\"now\":\"" + now.ToString("yyyy-MM-dd") + "\",\"d1ago\":\"" +
                         now.AddDays(-1).ToString("yyyy-MM-dd") + "\",\"d7ago\":\"" +
                         now.AddDays(-7).ToString("yyyy-MM-dd") + "\",\"monthNow\":\"" +
                         now.ToString("yyyy-MM") + "\"}";
            context.Response.Write(ret);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}