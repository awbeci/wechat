﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using WeixinService.Model;
using WeixinService.Dal;
using System.Runtime.Remoting.Contexts;
using System.IO;
using System.Drawing;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for RiskNotice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RiskNotice : System.Web.Services.WebService
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        RiskNoticeDal _riskNoticeDal = new RiskNoticeDal();
        RiskNoticeRIDal _riskNoticeRiDal = new RiskNoticeRIDal();
        RiskNoticeSignerDal _signerDal = new RiskNoticeSignerDal();
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        private readonly Hashtable _hashtable = new Hashtable();
        readonly Bll.SendMessage _sendMessage = new Bll.SendMessage();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// 根据手机号查询风险告知通知单
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="page">当前页</param>
        [WebMethod]
        public void QueryRiskNotice(string phone, int page)
        {
            const int rows = 10; //每页显示条数
            int total = 0;//总数量
            try
            {
                var data = _riskNoticeDal.QueryRiskNotice(phone, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   id = da.Field<string>("id"),
                                   //通知单 编号
                                   number = da.Field<string>("t_number"),
                                   //通知单 日期
                                   date = da.Field<string>("t_date"),
                                   //通知单 主送
                                   zs = da.Field<string>("t_zs"),
                                   //通知单 停电设备及工期
                                   tdsb_gq = da.Field<string>("t_tdsb_gq"),
                                   //通知单 运行风险分析
                                   yxfxfx = da.Field<string>("t_yxfxfx"),
                                   //通知单 风险预控措施
                                   fxykcs = da.Field<string>("t_fxykcs"),
                                   //通知单 通知单状态：0:未发布 1:已发布 2:已审核 3:已签发 4:已确认 5:已备案
                                   zt = da.Field<int?>("zt"),
                                   //通知单 发送日期
                                   send_dt = da.Field<DateTime?>("t_send_dt"),
                                   //通知单 创建日期
                                   createdt = da.Field<DateTime?>("t_createdt"),
                                   qzzt = da.Field<int?>("qzzt"),
                                   rylx = da.Field<int?>("t_type")
                               };
                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "获取成功";
                _hashtable["data"] = list.ToList();
                _hashtable["dataCount"] = total;
                //解决乱码代码：
                Context.Response.ContentType = "text/plain;charset=utf-8";
                Context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryRiskNotice,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "获取失败";
                _hashtable["data"] = null;
                var json = _jss.Serialize(_hashtable);
                Context.Response.Write(json);
            }
        }
        
        /// <summary>
        /// 根据ID查询通知单信息
        /// </summary>
        /// <param name="id"></param>
        [WebMethod]
        public void QueryRiskNoticebyID(string id)
        {
            try
            {
                var dt = _riskNoticeDal.QueryRiskNoticeByID(id);
                var list = from da in dt.AsEnumerable()
                           select new
                           {
                               ID = da.Field<string>("ID"),
                               t_number = da.Field<string>("t_number"),
                               t_date = da.Field<string>("t_date"),
                               t_zs = da.Field<string>("t_zs"),
                               t_tdsb_gq = da.Field<string>("t_tdsb_gq"),
                               t_yxfxfx = da.Field<string>("t_yxfxfx"),
                               t_fxykcs = da.Field<string>("t_fxykcs"),
                               t_operator = da.Field<string>("t_operator"),
                               t_send_dt = da.Field<DateTime?>("t_send_dt"),
                               t_createdt = da.Field<DateTime?>("t_createdt"),
                               t_state = da.Field<int?>("t_state")
                           };
                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "获取成功";
                _hashtable["data"] = list.ToList();
                //解决乱码代码：
                Context.Response.ContentType = "text/plain;charset=utf-8";
                Context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryRiskNotice,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "获取失败";
                _hashtable["data"] = null;
                var json = _jss.Serialize(_hashtable);
                Context.Response.Write(json);
            }
        }
        /// <summary>
        /// 根据ID查询签字信息
        /// </summary>
        /// <param name="id"></param>
        [WebMethod]
        public void QueryRiskNoticeSignerByID(string id)
        {
            RiskNoticeSignerDal _riskNoticeSignerDal = new RiskNoticeSignerDal();
            
            try
            {
                var dt = _riskNoticeSignerDal.QueryRiskNoticeSignerByRnid(id);
                var list = from da in dt.AsEnumerable()
                           select new
                           {
                               Img = da.Field<string>("t_img"),
                               Type = da.Field<int?>("t_type")
                           };
                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "获取成功";
                _hashtable["data"] = list.ToList();
                //解决乱码代码：
                Context.Response.ContentType = "text/plain;charset=utf-8";
                Context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryRiskNotice,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "获取失败";
                _hashtable["data"] = null;
                var json = _jss.Serialize(_hashtable);
                Context.Response.Write(json);
            }
        }

        /// <summary>
        /// 根据id更新风险告知通知单状态
        /// </summary>
        /// <param name="Number"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ModifyRNState(string id, int State)
        {
            bool sign = true;
            bool mresult = true;
            try
            {
                if (State == 4)
                {
                    var datatable = _riskNoticeDal.QuerySinger(id);
                    if (datatable != null)
                    {
                        if (datatable.Rows.Count == 6)
                        {
                            for (int i = 0; i < datatable.Rows.Count; i++)
                            {
                                if (datatable.Rows[i]["t_state"].ToString() != "1")
                                {
                                    sign = false;
                                }
                            }
                            if (sign == true)
                            {
                                mresult = _riskNoticeDal.ModifyRNState(id, State);
                            }
                        }
                    }
                }
                else
                {
                    mresult = _riskNoticeDal.ModifyRNState(id, State);
                }
                return mresult;
            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifyRNState,错误原因:" + e.Message);
                return false;
            }
        }

        /// <summary>
        /// 根据手机号修改签字状态
        /// </summary>
        /// <param name="PhoneNum"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        [WebMethod]
        public bool ModifySignerState(string PhoneNum, string RNID, string Type)
        {
            try
            {
                bool mresult = _riskNoticeDal.ModifySignerState(PhoneNum, RNID);
                if (mresult)
                {
                    //查询编号id的通知单签名人信息，有1:审核人 2:签发人 3:运检部 4:营销部 5:安质部 6:客户
                    var data = _signerDal.QueryRiskNoticeAndSigner(RNID);
                    var list = from da in data.AsEnumerable()
                               select new
                                   {
                                       bh = da.Field<string>("t_number"),
                                       wxid = da.Field<string>("t_weixin"),
                                       type = da.Field<int?>("t_type")
                                   };
                    switch (Type)
                    {
                        //如果是审核人已签字，那么就要发送给签发人让其签字
                        case "1":
                            Log.Debug("执行了这里，是审核人的，count=" + list.Count());
                            var query1 = list.Where(s => s.type == 2).ToList();
                            if (query1.Count > 0)
                            {
                                Log.Debug("给签发人发送微信");
                                _sendMessage.SendTxtMessage(query1[0].wxid, "尊敬的用户您好：您有铜陵电网运行风险预警通知单，编号：" + query1[0].bh + "，请您签字确认！");
                            }
                            break;
                        //如果是签发人已签字，那么就要发送给运检部、安质部、营销部和用户
                        case "2":
                            Log.Debug("执行了这里，是签发人的count=" + list.Count());
                            var query2 = list.Where(s => s.type != 1 && s.type != 2).ToList();
                            foreach (var q in query2)
                            {
                                Log.Debug("给运检部、营销部、安质部和用户发送微信");
                                _sendMessage.SendTxtMessage(q.wxid, "尊敬的用户您好：您有铜陵电网运行风险预警通知单，编号：" + q.bh + "，请您签字确认！");
                            }
                            break;
                        default: break;
                    }
                }
                return mresult;
            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifySignerState,错误原因:" + e.Message);
                return false;
            }
        }

        /// <summary>
        /// 检测电话是否存在
        /// </summary>
        /// <param name="PhoneNum"></param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckPhone(string PhoneNum)
        {
            try
            {
                DataTable dt = _riskNoticeDal.CheckPhone(PhoneNum);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Log.Debug("方法名:CheckPhone,错误原因:" + e.Message);
                return false;
            }
        }

        /// <summary>
        /// 根据通知单id查询签字信息
        /// </summary>
        /// <param name="rnid"></param>
        [WebMethod]
        public void QuerySinger(string rnid)
        {
            try
            {
                var data = _riskNoticeDal.QuerySinger(rnid);
                if (data != null && data.Rows.Count > 0)
                {
                    var list = from da in data.AsEnumerable()
                               select new
                               {
                                   //通知单 人员类型:1:审核人 2:签发人 3:运检部 4:营销部 5:安质部 6:客户
                                   type = da.Field<int?>("t_type"),
                                   //通知单 图片路径
                                   img = da.Field<string>("t_img")
                               };
                    _hashtable["isSuccess"] = true;
                    _hashtable["msg"] = "获取成功";
                    _hashtable["data"] = list.ToList();
                    //解决乱码代码：
                    Context.Response.ContentType = "text/plain;charset=utf-8";
                    Context.Response.Write(_jss.Serialize(_hashtable));
                }
                else
                {
                    _hashtable["isSuccess"] = false;
                    _hashtable["msg"] = "获取失败";
                    _hashtable["data"] = null;
                    var json = _jss.Serialize(_hashtable);
                    Context.Response.Write(json);
                }
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QuerySinger,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "获取失败";
                _hashtable["data"] = null;
                var json = _jss.Serialize(_hashtable);
                Context.Response.Write(json);
            }
        }


        /// <summary>
        ///  保存图片到指定文件夹
        /// </summary>
        /// <param name="picStream"></param>
        /// <param name="picName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool SavePicture(string picName)
        {
            var picStream = HttpContext.Current.Request.InputStream;
            if (picStream.Length <= 0)
            {
                Log.Debug("上传图片为空，不能保存");
                return false;
            }
            string savePath = "/RiskNoticeImg"; ;//目标图片路径
            string dirPath = HttpContext.Current.Server.MapPath(savePath);
            string filePath = dirPath + @"\" + picName;
            Log.Debug("**********************************filePath=" + filePath);
            try
            {
                byte[] buffer = new byte[4096];
                int bytesRead = 0;
                using (FileStream fs = File.Create(filePath, 4096))
                {
                    while ((bytesRead = picStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        //向文件中写信息  
                        fs.Write(buffer, 0, bytesRead);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePicture,错误原因:" + e.Message);
                return false;
            }
        }

        /// <summary>
        ///  保存图片到指定文件夹Base64编码
        /// </summary>
        /// <param name="picStream"></param>
        /// <param name="picName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool SavePictureBase64()
        {
            return false;
        }
    }
}
