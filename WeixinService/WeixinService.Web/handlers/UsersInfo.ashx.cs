﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using WeixinService.Model.common;
using Newtonsoft.Json;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// UsersInfo 的摘要说明
    /// </summary>
    public class UsersInfo : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        UsersInfoDal usersInfodal = new UsersInfoDal();
        UserFzBll userfzbll = new UserFzBll();
        Hashtable hashtable = new Hashtable();
        WelcomeMsgDal wmsgdal = new WelcomeMsgDal();
        UsersInfoDal userdal = new UsersInfoDal();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("UsersInfo.ashx,描述:反射方法名出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 查询欢迎语
        /// </summary>
        /// <param name="context"></param>
        private void QueryWelMsg(HttpContext context)
        {
            try
            {
                var bean = new WelcomeMsg();
                DataTable dt = wmsgdal.Query(bean);
                var list = ConvertHelper<WelcomeMsg>.ConvertToList(dt);
                hashtable["rows"] = list.ToList();
                hashtable["total"] = list.Count;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryWelMsg,描述:查询欢迎语方法出错,错误原因:" + e.Message); ;
            }
        }

        /// <summary>
        /// 编辑欢迎语
        /// </summary>
        /// <param name="httpContext"></param>
        public void ModifyWelMsg(HttpContext context)
        {
            var id = context.Request["id"];
            var content = context.Request["content"];
            WelcomeMsg wmsg = new WelcomeMsg() { id = id, content = content };
            try
            {
                wmsgdal.Modify(wmsg);
                context.Response.Write("{\"success\":true,\"msg\":\"保存成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifyWelMsg,描述:编辑欢迎语方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            }
        }


        /// <summary>
        /// 实名编辑
        /// </summary>
        /// <param name="httpContext"></param>
        public void SaveTrueName(HttpContext context)
        {
            var openid = context.Request["openid"];
            var truename = context.Request["truename"];
            var zw = context.Request["zw"];

            try
            {
                if (!string.IsNullOrEmpty(zw))
                {
                    usersInfodal.ModifyTrueName(new UserInfo()
                    {
                        openid = openid,
                        truename = truename,
                        bak1 = int.Parse(zw)
                    });
                }
                else
                {
                    usersInfodal.ModifyTrueName(new UserInfo()
                    {
                        openid = openid,
                        truename = truename,
                        bak1 = 0
                    });
                }
                context.Response.Write("{\"success\":true,\"msg\":\"保存成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifyWelMsg,描述:编辑欢迎语方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            }
        }

        /// <summary>
        /// 查询微信用户信息
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryUserInfo(HttpContext httpContext)
        {
            var groupId = httpContext.Request.Params["fz"];
            var nickname = httpContext.Request.Params["nickname"];
            var truename = httpContext.Request.Params["truename"];
            var begintime = new DateTime();
            var endtime = new DateTime();
            string sj = httpContext.Request["begintime"] ?? "";
            if (sj.Length > 0)
            {
                begintime = DateTime.Parse(sj);
            }
            string ej = httpContext.Request["endtime"] ?? "";
            if (ej.Length > 0)
            {
                endtime = DateTime.Parse(ej).AddDays(1);
            }

            //获取分页数据
            var total = 0;
            var page = int.Parse(httpContext.Request["page"] ?? "1");
            var rows = int.Parse(httpContext.Request["rows"] ?? "10");
            try
            {
                var data = usersInfodal.QueryByPage(new UserInfo()
                {
                    groupID = groupId,
                    nickname = nickname,
                    truename = truename,
                    begintime = begintime,
                    endtime = endtime
                }, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               openid = da.Field<string>("userid"),
                               nickname = da.Field<string>("nickname"),
                               truename = da.Field<string>("TrueName"),
                               sex = da.Field<int>("sex"),
                               city = da.Field<string>("city"),
                               country = da.Field<string>("country"),
                               province = da.Field<string>("province"),
                               language = da.Field<string>("language"),
                               subscribe_time = da.Field<DateTime?>("subscribe_time"),
                               groupID = da.Field<string>("groupID"),
                               groupName = da.Field<string>("groupName"),
                               RNumber = da.Field<string>("RNumber"),
                               msgFrom = da.Field<string>("msgFrom"),
                               zw = da.Field<int?>("bak1")
                           };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryUserInfo,描述:查询微信用户信息方法出错,错误原因:" + e.Message);
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }

        }

        /// <summary>
        /// 查询分组
        /// </summary>
        /// <param name="context"></param>
        private void QueryGroup(HttpContext context)
        {
            var fz = context.Request["fz"];
            try
            {
                var bean = new Model.common.UserGroup();
                DataTable dt = new DataTable();
                if (fz == "-1")
                {
                    dt = usersInfodal.QueryGroupTable();
                }
                else
                {
                    dt = usersInfodal.QueryGroup();
                }
                var list = ConvertHelper<Model.common.UserGroup>.ConvertToList(dt);
                hashtable["rows"] = list.ToList();
                hashtable["total"] = list.Count;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryGroup,描述:查询分组信息方法出错,错误原因:" + e.Message); ;
            }
        }

        /// <summary>
        /// 推送分组信息
        /// </summary>
        public void UploadGroup(string token)
        {
            var dataone = userdal.QueryUserToGroupByOne();
            if (dataone.Rows.Count > 0)
            {
                for (int i = 0; i < dataone.Rows.Count; i++)
                {
                    string openid = dataone.Rows[i]["openid"].ToString();
                    string groupid = dataone.Rows[i]["groupid"].ToString();

                    //查询目标分组的父id
                    var groupPid = userdal.QueryPidById(groupid);
                    var groupname = userdal.QueryNameById(groupid);
                    if (groupPid == "-1")
                    {
                        string replyContent = "";
                        if (groupname.Contains("高压"))
                            replyContent = "高压客户,您的资料已审核通过！";
                        else if (groupname.Contains("未分组"))
                            replyContent = "";
                        else
                            replyContent = "您的客户资料已审核通过！";

                        try
                        {
                            //分组时，先发送分组信息到微信服务器，返回成功再更新本地数据库
                            var jsonWx = "{\"openid\":\"" + openid + "\",\"to_groupid\":\"" + groupid + "\"}";
                            var accessToken = new AccessToken();
                            var sendDataToWeChat = new SendDataToWeChat();

                            //var token = accessToken.GetExistAccessToken();
                            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token, jsonWx);
                            var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
                            if (backData.errcode != "0")
                            {
                                Log.Debug("给" + openid + "用户分到" + groupid + "_" + groupname + "组失败！");
                            }
                            else
                            {
                                userdal.ModifyUploadWechat(openid);

                                //分组成功后，给用户发送消息
                                var jsonSend = "{\"touser\":\"" + openid + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
                                         replyContent + "\"}}";
                                var backU = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonSend);
                                var backUData = JsonConvert.DeserializeObject<GroupBackData>(back);
                                if (backUData.errcode != "0")
                                    Log.Debug("给" + openid + "用户发送消息失败！");
                            }

                        }
                        catch (Exception e)
                        {
                            Log.Debug("方法名:UploadGroup,描述:分组信息推送方法出错,错误原因:" + e.Message);
                            Log.Debug("给" + openid + "用户分到" + groupid + "_" + groupname + "组失败！");
                        }
                    }
                    else if (groupPid != "-1")
                    {
                        string replyContent = "";
                        if (groupname.Contains("高压"))
                            replyContent = "高压客户,您的资料已审核通过！";
                        else if (groupname.Contains("未分组"))
                            replyContent = "";
                        else
                            replyContent = "您的客户资料已审核通过！";

                        try
                        {
                            //分组时，先发送分组信息到微信服务器，返回成功再更新本地数据库
                            var jsonWx = "{\"openid\":\"" + openid + "\",\"to_groupid\":\"" + groupPid + "\"}";
                            var accessToken = new AccessToken();
                            var sendDataToWeChat = new SendDataToWeChat();

                            //var token = accessToken.GetExistAccessToken();
                            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token, jsonWx);
                            var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
                            if (backData.errcode != "0")
                            {
                                Log.Debug("给" + openid + "用户分到" + groupid + "_" + groupname + "组失败！");
                            }
                            else
                            {
                                userdal.ModifyUploadWechat(openid);

                                //分组成功后，给用户发送消息
                                var jsonSend = "{\"touser\":\"" + openid + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
                                         replyContent + "\"}}";
                                var backU = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonSend);
                                var backUData = JsonConvert.DeserializeObject<GroupBackData>(back);
                                if (backUData.errcode != "0")
                                    Log.Debug("给" + openid + "用户发送消息失败！");
                            }

                        }
                        catch (Exception e)
                        {
                            Log.Debug("方法名:UploadGroup,描述:分组信息推送方法出错,错误原因:" + e.Message);
                            Log.Debug("给" + openid + "用户分到" + groupid + "_" + groupname + "组失败！");
                        }

                    }

                }
            }

        }


        /// <summary>
        /// 保存分组
        /// </summary>
        /// <param name="httpContext"></param>
        public void SaveFz(HttpContext context)
        {
            UserToGruop usertogroup = new UserToGruop();
            var openids = context.Request["openids"];
            var groupids = context.Request["groupids"];

            try
            {
                //删除用户以前的分组关系
                userdal.DelUserToGruop(openids);

                string[] arropenids = openids.Split(':');
                string[] arrgroupids = groupids.Split(':');
                //int truecount = 0;

                for (var i = 0; i < arrgroupids.Length; i++)
                {
                    usertogroup.groupid = arrgroupids[i];

                    //查询目标分组的名称
                    //var groupname = userdal.QueryNameById(arrgroupids[i]);
                    //string replyContent = "";
                    //if (groupname.Contains("高压"))
                    //    replyContent = "高压客户,您的资料已审核通过！";
                    //else if (groupname.Contains("未分组"))
                    //    replyContent = "";
                    //else
                    //    replyContent = "您的客户资料已审核通过！";


                    for (var j = 0; j < arropenids.Length; j++)
                    {
                        usertogroup.openid = arropenids[j];
                        usertogroup.uploadwechat = 1;
                        userdal.AddUserToGruop(usertogroup);
                        ////分组时，先发送分组信息到微信服务器，返回成功再更新本地数据库
                        //var jsonWx = "{\"openid\":\"" + arropenids[j] + "\",\"to_groupid\":\"" + arrgroupids[i] + "\"}";
                        //var accessToken = new AccessToken();
                        //var sendDataToWeChat = new SendDataToWeChat();

                        //var token = accessToken.GetExistAccessToken();
                        //var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token, jsonWx);
                        //var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
                        //if (backData.errcode != "0")
                        //{
                        //    context.Response.Write("{\"success\":true,\"msg\":\"给" + arropenids[j] + "用户分组失败！\"}");
                        //}
                        //else
                        //{
                        //    truecount = truecount + 1;
                        //    userdal.AddUserToGruop(usertogroup);

                        //    //分组成功后，给用户发送消息
                        //    var jsonSend = "{\"touser\":\"" + arropenids[j] + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
                        //             replyContent + "\"}}";
                        //    var backU = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonSend);
                        //    var backUData = JsonConvert.DeserializeObject<GroupBackData>(back);
                        //    if (backUData.errcode != "0")
                        //        context.Response.Write("{\"success\":true,\"msg\":\"给" + arropenids[j] + "用户发送消息失败！\"}");

                        //}                    
                    }
                }
                context.Response.Write("{\"success\":true,\"msg\":\"分组成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SaveFz,描述:保存分组方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            }
            //if (truecount == arropenids.Length*arrgroupids.Length)
            //    context.Response.Write("{\"success\":true,\"msg\":\"分组成功！\"}");


            //查询目标分组的父id
            //var groupPid = userdal.QueryPidById(groupid);
            //var groupPname = userdal.QueryNameById(groupPid);
            //if (groupPid == "-1")
            //{

            //    string replyContent = "";
            //    if (groupname.Contains("高压"))
            //        replyContent = "高压客户,您的资料已审核通过！";
            //    else if (groupname.Contains("未分组"))
            //        replyContent = "";
            //    else
            //        replyContent = "您的客户资料已审核通过！";

            //    try
            //    {
            //        int truecount = 0;
            //        string[] arropenids = openids.Split(':');
            //        for (var i = 0; i < arropenids.Length; i++)
            //        {
            //            userinfo.openid = arropenids[i];
            //            userinfo.groupID = groupid;
            //            userinfo.groupName = groupname;


            //            //分组时，先发送分组信息到微信服务器，返回成功再更新本地数据库
            //            var jsonWx = "{\"openid\":\"" + userinfo.openid + "\",\"to_groupid\":\"" + groupid + "\"}";
            //            var accessToken = new AccessToken();
            //            var sendDataToWeChat = new SendDataToWeChat();

            //            var token = accessToken.GetExistAccessToken();
            //            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token, jsonWx);
            //            var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
            //            if (backData.errcode != "0")
            //            {
            //                context.Response.Write("{\"success\":true,\"msg\":\"给" + userinfo.openid + "用户分组失败！\"}");
            //            }
            //            else
            //            {
            //                truecount = truecount + 1;
            //                userdal.Modify(userinfo);
            //            }

            //            //分组成功后，给用户发送消息
            //            var jsonSend = "{\"touser\":\"" + userinfo.openid + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
            //                     replyContent + "\"}}";
            //            var backU = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonSend);
            //            var backUData = JsonConvert.DeserializeObject<GroupBackData>(back);
            //            if (backUData.errcode != "0")
            //                context.Response.Write("{\"success\":true,\"msg\":\"给" + userinfo.openid + "用户发送消息失败！\"}");
            //        }

            //        if (truecount == arropenids.Length)
            //            context.Response.Write("{\"success\":true,\"msg\":\"分组成功！\"}");
            //    }
            //    catch (Exception e)
            //    {
            //        Log.Debug("方法名:SaveFz,描述:保存分组方法出错,错误原因:" + e.Message);
            //        context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            //    }
            //}
            //else if (groupPid != "-1")
            //{
            //    string replyContent = "";
            //    if (groupname.Contains("高压"))
            //        replyContent = "高压客户,您的资料已审核通过！";
            //    else if (groupname.Contains("未分组"))
            //        replyContent = "";
            //    else
            //        replyContent = "您的客户资料已审核通过！";

            //    try
            //    {
            //        int truecount = 0;
            //        string[] arropenids = openids.Split(':');
            //        for (var i = 0; i < arropenids.Length; i++)
            //        {
            //            userinfo.openid = arropenids[i];
            //            userinfo.groupID = groupid;
            //            userinfo.groupName = groupname;

            //            //分组时，先发送分组信息到微信服务器，返回成功再更新本地数据库
            //            var jsonWx = "{\"openid\":\"" + userinfo.openid + "\",\"to_groupid\":\"" + groupPid + "\"}";
            //            var accessToken = new AccessToken();
            //            var sendDataToWeChat = new SendDataToWeChat();

            //            var token = accessToken.GetExistAccessToken();
            //            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=" + token, jsonWx);
            //            var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
            //            if (backData.errcode != "0")
            //            {
            //                context.Response.Write("{\"success\":true,\"msg\":\"给" + userinfo.openid + "用户分组失败！\"}");
            //            }
            //            else
            //            {
            //                truecount = truecount + 1;
            //                userdal.Modify(userinfo);
            //            }

            //            //分组成功后，给用户发送消息
            //            var jsonSend = "{\"touser\":\"" + userinfo.openid + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
            //                     replyContent + "\"}}";
            //            var backU = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonSend);
            //            var backUData = JsonConvert.DeserializeObject<GroupBackData>(back);
            //            if (backUData.errcode != "0")
            //                context.Response.Write("{\"success\":true,\"msg\":\"给" + userinfo.openid + "用户发送消息失败！\"}");

            //        }

            //        if (truecount == arropenids.Length)
            //            context.Response.Write("{\"success\":true,\"msg\":\"分组成功！\"}");
            //    }
            //    catch (Exception e)
            //    {
            //        Log.Debug("方法名:SaveFz,描述:保存分组方法出错,错误原因:" + e.Message);
            //        context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            //    }

            //}

        }

        /// <summary>
        /// 新建分组
        /// </summary>
        /// <param name="httpContext"></param>
        public void AddFz(HttpContext context)
        {
            var id = context.Request["id"];
            var pid = context.Request["pid"];
            var name = context.Request["name"];
            Model.common.UserGroup usergroup = new Model.common.UserGroup() { id = id, pid = pid, name = name };
            try
            {
                usersInfodal.AddFz(usergroup);
                context.Response.Write("{\"success\":true,\"msg\":\"添加成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:AddFz,描述:新建分组方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"添加失败！\"}");
            }
        }


        /// <summary>
        /// 编辑分组
        /// </summary>
        /// <param name="httpContext"></param>
        public void ModifyFz(HttpContext context)
        {
            var id = context.Request["id"];
            var name = context.Request["name"];
            Model.common.UserGroup usergroup = new Model.common.UserGroup() { id = id, name = name };
            var pid = userdal.QueryPidById(id);
            try
            {
                if (pid == "-1")
                {
                    var jsonWx = "{\"group\":{\"id\":\"" + id + "\",\"name\":\"" + name + "\"}}";

                    var accessToken = new AccessToken();
                    var sendDataToWeChat = new SendDataToWeChat();

                    var token = accessToken.GetExistAccessToken();
                    var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/update?access_token=" + token, jsonWx);
                    var backData = JsonConvert.DeserializeObject<GroupBackData>(back);
                    if (backData.errcode != "0")
                    {
                        context.Response.Write("{\"success\":true,\"msg\":\"保存失败！\"}");
                    }
                    else
                    {
                        usersInfodal.ModifyFz(usergroup);
                        context.Response.Write("{\"success\":true,\"msg\":\"保存成功！\"}");
                    }
                }
                else if (pid != "-1")
                {
                    usersInfodal.ModifyFz(usergroup);
                    context.Response.Write("{\"success\":true,\"msg\":\"保存成功！\"}");

                }

            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifyFz,描述:编辑分组方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"保存失败！\"}");
            }
        }

        /// <summary>
        /// 获取微信服务器传来的组id
        /// </summary>
        /// <param name="context"></param>
        public void GetId(HttpContext context)
        {
            string name = context.Request["name"];
            var jsonWx = "{\"group\":{\"name\":\"" +
                       name + "\"}}";

            var accessToken = new AccessToken();
            var sendDataToWeChat = new SendDataToWeChat();

            var token = accessToken.GetExistAccessToken();
            var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/groups/create?access_token=" + token, jsonWx);
            //var backData = JsonConvert.DeserializeObject<GroupBackData>(back);

            context.Response.Write(back);

        }

        /// <summary>
        /// 删除分组
        /// </summary>
        /// <param name="context"></param>
        public void DelFz(HttpContext context)
        {
            var id = context.Request["id"];
            Model.common.UserGroup usergroup = new Model.common.UserGroup() { id = id };
            try
            {
                usersInfodal.DelFz(usergroup);
                context.Response.Write("{\"success\":true,\"msg\":\"删除成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:DelFz,描述:删除分组方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"删除失败！\"}");
            }
        }


        /// <summary>
        /// 查询用户组别并拼接数据
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryUserGroup(HttpContext context)
        {
            var hashTable = new Hashtable();
            var data = usersInfodal.QueryGroup();
            var tmp = from da in data.AsEnumerable()
                      select new
                      {
                          Id = da.Field<string>("id"),
                          Pid = da.Field<string>("pid"),
                          Name = da.Field<string>("name")
                      };
            hashTable["isSuccess"] = true;
            hashTable["data"] = tmp.ToList();
            hashTable["jsMethod"] = "ajax_QueryUserGroup";
            var json = _jss.Serialize(hashTable);
            context.Response.Write(json);
        }


        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="context"></param>
        public void UserReg(HttpContext context)
        {
            var bll = new UserRegBll();
            var wxid = context.Request["wxid"] ?? "";
            var qymc = context.Request["qymc"] ?? "";
            var lxr = context.Request["lxr"] ?? "";
            var phonenumber = context.Request["phonenumber"] ?? "";
            var tel = context.Request["tel"] ?? "";
            var zhiwu = context.Request["zhiwu"] ?? "";
            try
            {
                var bean = new UserReg()
                {
                    Wxid = wxid,
                    RNumber = qymc,//用户编号
                    bak1 = qymc,//企业名称
                    bak2 = lxr,//用户名称
                    bak3 = tel,//固话
                    bak4 = zhiwu,//职务
                    PhoneNumber = phonenumber,//手机
                };
                bll.Add(bean);
                context.Response.Write("{\"success\":true,\"msg\":\"注册成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:UserReg,描述:用户注册方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"注册失败！\"}");
            }
        }

        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="context"></param>
        public void UserReg1(HttpContext context)
        {
            var bll = new UserRegBll();
            var qymc = context.Request["qymc"] ?? "";
            var lxr = context.Request["lxr"] ?? "";
            var phonenumber = context.Request["phonenumber"] ?? "";
            try
            {
                var bean = new UserReg()
                {
                    Wxid = qymc,
                    RNumber = qymc,//用户编号
                    bak2 = lxr,//用户名称
                    PhoneNumber = phonenumber,
                };
                bll.Add(bean);
                context.Response.Write("{\"success\":true,\"msg\":\"注册成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:UserReg,描述:用户注册方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"注册失败！\"}");
            }
        }

        /// <summary>
        /// 获取用户注册列表
        /// </summary>
        /// <param name="context"></param>
        public void GetUserRegList(HttpContext context)
        {
            var wxid = context.Request["wxid"] ?? "";
            var phone = context.Request["phone"] ?? "";

            try
            {
                UserRegBll bll = new UserRegBll();
                DataTable dt = bll.Query(new UserReg() { Wxid = wxid, PhoneNumber = phone });
                var list = ConvertHelper<UserReg>.ConvertToList(dt);
                hashtable["rows"] = list.ToList();
                hashtable["total"] = list.Count;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:GetUserRegList,描述:获取用户注册列表方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"查询失败！\"}");
            }
        }

        /// <summary>
        /// 删除绑定
        /// </summary>
        /// <param name="context"></param>
        public void DelUserReg(HttpContext context)
        {
            var wxid = context.Request["wxid"] ?? "";
            var qymc = context.Request["qymc"] ?? "";
            var lxr = context.Request["lxr"] ?? "";
            var dh = context.Request["dh"] ?? "";
            var phone = context.Request["phone"] ?? "";
            try
            {
                UserRegBll bll = new UserRegBll();
                bll.Del(new UserReg()
                {
                    Wxid = wxid,
                });
                context.Response.Write("{\"success\":true,\"msg\":\"删除成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:DelUserReg,描述:删除绑定方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"删除失败！\"}");
            }
        }

        /// <summary>
        /// 删除绑定
        /// </summary>
        /// <param name="context"></param>
        public void DelUserReg2(HttpContext context)
        {
            var wxid = context.Request["wxid"] ?? "";
            var qymc = context.Request["qymc"] ?? "";
            var lxr = context.Request["lxr"] ?? "";
            var phone = context.Request["phone"] ?? "";
            try
            {
                UserRegDal bll = new UserRegDal();
                bll.Del2(new UserReg()
                {
                    Wxid = wxid,
                    bak1 = qymc,
                    bak2 = lxr,
                    PhoneNumber = phone
                });
                context.Response.Write("{\"success\":true,\"msg\":\"删除成功！\"}");
            }
            catch (Exception e)
            {
                Log.Debug("方法名:DelUserReg,描述:删除绑定方法出错,错误原因:" + e.Message);
                context.Response.Write("{\"success\":false,\"msg\":\"删除失败！\"}");
            }
        }

        /// <summary>
        /// 查询用户信息和业务号
        /// </summary>
        /// <param name="context"></param>
        public void QueryUserInfoAndYwh(HttpContext context)
        {
            var userInfoDal = new UsersInfoDal();
            var groupid = context.Request.Params["groupid"];
            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var data = userInfoDal.QueryUserInfoAndYwh2(groupid, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   wxid = da.Field<string>("wxid"),
                                   nickname = da.Field<string>("nickname"),
                                   sjh = da.Field<string>("phonenumber"),
                                  // qy = da.Field<string>("qy"),
                                   zw = da.Field<int?>("zw")
                               };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }

        /// <summary>
        /// 添加用户到信息发布表
        /// </summary>
        /// <param name="context"></param>
        public void AddPersonToInfoRelease(HttpContext context)
        {
            var hashTable = new Hashtable();
            var weChatData = new Hashtable();

            var infoReleaseDal = new InfoReleaseDal();
            var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();

            var id = context.Request.Params["id"];
            var ppid = context.Request.Params["ppid"];
            var key = context.Request.Params["key"];
            var title = context.Request.Params["title"];
            var ms = context.Request.Params["ms"];
            try
            {
                infoReleaseDal.Modify(new InfoRelease()
                {
                    Id = id,
                    FlagRelease = "1",
                    SendPerson = ppid
                });
                var tmp = JsonConvert.DeserializeObject<List<Tmp>>(ppid);
                if (tmp.Count > 0)
                {
                    infoReleaseDetailsDal.Del(new InfoReleaseDetailsModel() { InfoReleaseId = id });
                    foreach (var t in tmp)
                    {
                        var weChatDataList = new List<object>();
                        weChatDataList.Add(new
                        {
                            title,
                            description = ms,
                            url = "http://218.22.27.236/views/messagelist/messagedetail_fxgz.htm?id=" + id + "&wxid=" + t.wxid,
                            picurl = "http://218.22.27.236/tl/UploadImages/" + key + ".jpg"
                        });
                        weChatData["touser"] = t.wxid;
                        weChatData["msgtype"] = "news";
                        weChatData["news"] = new
                        {
                            articles = weChatDataList
                        };
                        var json = _jss.Serialize(weChatData);
                        json = json.Replace("\\u0026", "&");
                        var token = accessToken.GetExistAccessToken();
                        var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                        var backData = JsonConvert.DeserializeObject<WeChatBackData>(back);
                        var infoReleaseDetailsModel = new InfoReleaseDetailsModel();
                        if (backData.errcode == 0 && backData.errmsg == "ok")
                        {
                            infoReleaseDetailsModel.State = "2";//待确认
                        }
                        else
                        {
                            infoReleaseDetailsModel.State = "1";//待推送
                        }
                        infoReleaseDetailsModel.Id = Guid.NewGuid().ToString();
                        infoReleaseDetailsModel.OpenId = t.wxid;
                        infoReleaseDetailsModel.NickName = t.nickname;
                        infoReleaseDetailsModel.RNumber = t.ywh;
                        infoReleaseDetailsModel.InfoReleaseId = id;

                        infoReleaseDetailsDal.Add(infoReleaseDetailsModel);
                    }
                }

                hashTable["isSuccess"] = true;
                hashTable["jsMethod"] = "ajax_AddPersonToInfoRelease";
                var json2 = _jss.Serialize(hashTable);
                context.Response.Write(json2);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }

        /// <summary>
        /// 查询人员
        /// </summary>
        /// <param name="context"></param>
        public void QueryPerson(HttpContext context)
        {
            var txt = context.Request.Params["txt"];
            var userInfoDal = new UsersInfoDal();
            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var data = userInfoDal.QueryPerson(txt, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               wxid = da.Field<string>("wxid"),
                               nickname = da.Field<string>("nickname"),
                               qy = da.Field<string>("qy"),
                               zw = da.Field<string>("zw")
                           };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }


        public class GroupBackData
        {
            public string errcode { get; set; }
            public string errmsg { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class Tmp
    {
        public string wxid { get; set; }

        public string nickname { get; set; }

        public string ywh { get; set; }
    }

    public class WeChatBackData
    {
        //{"errcode":0,"errmsg":"ok"}
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}