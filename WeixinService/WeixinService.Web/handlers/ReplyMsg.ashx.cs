﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using WeixinService.Model.common;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// ReplyMsg 的摘要说明
    /// </summary>
    public class ReplyMsg : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        public void QueryReceiveInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var bll = new ReceiveMsgBll();
            var keyWords = context.Request.Params["keyWords"];
            var unReply = context.Request["UnReply"] ?? "2";//1-没有回复，2-已回复
            string tmp = context.Request["dateBegin"] ?? "";
            DateTime dateBegin = DateTime.Now.AddDays(-1).Date;
            DateTime dateEnd = DateTime.Now.AddDays(1).Date;
            if (!string.IsNullOrEmpty(tmp))
            {
                dateBegin = DateTime.Parse(tmp);
            }
            tmp = context.Request["dateEnd"] ?? "";
            if (!string.IsNullOrEmpty(tmp))
            {
                dateEnd = DateTime.Parse(tmp);
            }

            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "20");
            try
            {
                var data = bll.QueryByPage(new ReceiveMsg()
                {
                    DateBegin = dateBegin,
                    DateEnd = dateEnd,
                    KeyWords = keyWords,
                    bak1 = unReply,
                }, page, rows, ref total);
                var list = ConvertHelper<ReceiveMsg>.ConvertToList(data);
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["error"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Error(e);
            }
        }

        /// <summary>
        /// 回复消息
        /// </summary>
        /// <param name="context"></param>
        private void ReplyMsg1(HttpContext context)
        {
            var hashtable = new Hashtable();
            var msgId = context.Request["msgId"] ?? "";
            var openId = context.Request["openId"] ?? "";
            var replyContent = context.Request["replyContent"] ?? "";
            try
            {
                var jsonWx = "{\"touser\":\"" + openId + "\",\"msgtype\":\"text\",\"text\":{\"content\":\"" +
                             replyContent + "\"}}";
                var accessToken = new AccessToken();
                var sendDataToWeChat = new SendDataToWeChat();

                var token = accessToken.GetExistAccessToken();
                var back = sendDataToWeChat.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, jsonWx);

                hashtable = new Hashtable();
                hashtable["data"] = back;
                hashtable["error"] = false;
                ReplyMsgBll bll = new ReplyMsgBll();
                bll.Add(new Model.common.ReplyMsg()
                    {
                        ReceiveMsgId = msgId,
                        AcceptName = openId,
                        TextMsg = replyContent,
                        IsSend = 2,
                    });
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["error"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Error(e);
            }
        }

        private void QueryReplyInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var msgId = context.Request["msgId"] ?? "";
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "20");
            try
            {
                ReplyMsgBll bll = new ReplyMsgBll();
                DataTable dt = bll.Query(new Model.common.ReplyMsg()
                    {
                        ReceiveMsgId = msgId,
                    });
                var list = ConvertHelper<Model.common.ReplyMsg>.ConvertToList(dt);
                hashtable["rows"] = list.ToArray();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug(e);
                context.Response.Write("{\"success\":false,\"msg\":\"查询失败！\"}");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}