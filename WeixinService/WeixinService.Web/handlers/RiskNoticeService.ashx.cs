﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using Newtonsoft.Json;
using WeixinService.Model.common;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for RiskNoticeService
    /// </summary>
    public class RiskNoticeService : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        private readonly Hashtable _hashtable = new Hashtable();
        private readonly RiskNoticeDal _riskNoticeDal = new RiskNoticeDal();
        private readonly RiskNoticeRIDal _riskNoticeRiDal = new RiskNoticeRIDal();
        private readonly RiskNoticeSignerDal _riskNoticeSignerDal = new RiskNoticeSignerDal();
        readonly Bll.SendMessage _sendMessage = new Bll.SendMessage();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 添加风险告知通知单审核人和签发人数据
        /// </summary>
        /// <param name="context"></param>
        public void UpdateRiskNoticeRI(HttpContext context)
        {
            var type = context.Request.Params["type"];

            if (type == "1")
            {
                try
                {
                    var wxid = context.Request.Params["wxid"];
                    var sjh = context.Request.Params["sjh"];
                    var name = context.Request.Params["name"];
                    _riskNoticeRiDal.Modify(new RiskNoticeRI()
                    {
                        ID = "101",
                        PhoneNum = sjh,
                        WeiXin = wxid,
                        Name = name,
                        Type = 1
                    });
                    _hashtable["isSuccess"] = true;
                    context.Response.Write(_jss.Serialize(_hashtable));
                }
                catch (Exception e)
                {
                    _hashtable["isSuccess"] = false;
                    context.Response.Write(_jss.Serialize(_hashtable));
                    Log.Debug("出错原因：" + e.Message);
                }
            }
            if (type == "2")
            {
                try
                {
                    var wxid = context.Request.Params["wxid2"];
                    var sjh = context.Request.Params["sjh"];
                    var name = context.Request.Params["name2"];
                    _riskNoticeRiDal.Modify(new RiskNoticeRI()
                    {
                        ID = "102",
                        PhoneNum = sjh,
                        WeiXin = wxid,
                        Name = name,
                        Type = 2
                    });
                    _hashtable["isSuccess"] = true;
                    context.Response.Write(_jss.Serialize(_hashtable));
                }
                catch (Exception e)
                {
                    _hashtable["isSuccess"] = false;
                    context.Response.Write(_jss.Serialize(_hashtable));
                    Log.Debug("出错原因：" + e.Message);
                }
            }

        }

        /// <summary>
        /// 查找审核人、签发人 
        /// </summary>
        /// <param name="context"></param>
        public void QueryRiskNoticeRI(HttpContext context)
        {
            var user = context.Session["user"];
            try
            {
                var data = _riskNoticeRiDal.QueryRiskNoticeRI();
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   openid = da.Field<string>("t_weixin"),
                                   phonename = da.Field<string>("t_phonenum"),
                                   name = da.Field<string>("t_name"),
                                   type = da.Field<int>("t_type")
                               };
                _hashtable["data"] = list.ToList();
                _hashtable["bzr"] = user.ToString();
                _hashtable["jsMethod"] = "ajax_QueryRiskNoticeRI";
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                _hashtable["isSuccess"] = false;
                context.Response.Write(_jss.Serialize(_hashtable));
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 保存风险告知通知单
        /// </summary>
        /// <param name="context"></param>
        public void SaveRiskNotice(HttpContext context)
        {
            var type = context.Request.Params["type"];
            var bh = context.Request.Params["bh2"];
            var rq = context.Request.Params["rq"];
            var zs = context.Request.Params["zs"];
            //停电设备及工期
            var tdsbgq = context.Request.Params["tdsbgq"];
            //运行风险分析
            var yxfxfx = context.Request.Params["yxfxfx"];
            //风险预控措施
            var fxykcs = context.Request.Params["fxykcs"];
            var bzr = context.Request.Params["bzr"];

            if (type == "add")
            {
                _riskNoticeDal.Add(new Model.RiskNotice()
                    {
                        ID = Guid.NewGuid().ToString(),
                        Rq = rq,
                        Fxykcs = fxykcs,
                        Number = bh,
                        State = 0,
                        Tdsb_Gq = tdsbgq,
                        Zs = zs,
                        Yxfxfx = yxfxfx,
                        CreateDt = DateTime.Now,
                        Operator = bzr
                    });
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }

            if (type == "edit")
            {
                var id = context.Request.Params["id"];
                _riskNoticeDal.Modify(new Model.RiskNotice()
                {
                    ID = id,
                    Rq = rq,
                    Fxykcs = fxykcs,
                    Number = bh,
                    Tdsb_Gq = tdsbgq,
                    Zs = zs,
                    Yxfxfx = yxfxfx,
                    Operator = bzr
                });
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }
        }

        /// <summary>
        /// 查询风险告知通知单
        /// </summary>
        /// <param name="context"></param>
        public void QueryRiskNotice(HttpContext context)
        {
            var kssj = context.Request.Params["kssj"];
            var jssj = context.Request.Params["jssj"];
            var bh = context.Request.Params["bh"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var data = _riskNoticeDal.QueryByPage(new RiskNoticeTmp3()
                {
                    kssj = kssj,
                    jssj = jssj,
                    bh = bh
                }, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               Number = da.Field<string>("t_number"),
                               Rq = da.Field<string>("t_date"),
                               Zs = da.Field<string>("t_zs"),
                               Tdsb_Gq = da.Field<string>("t_tdsb_gq"),
                               Yxfxfx = da.Field<string>("t_yxfxfx"),
                               Fxykcs = da.Field<string>("t_fxykcs"),
                               State = da.Field<int>("t_state"),
                               Send_Dt = da.Field<DateTime?>("t_send_dt"),
                               CreateDt = da.Field<DateTime?>("t_createdt"),
                               Operator = da.Field<string>("t_operator")
                           };
                _hashtable["rows"] = list.ToList();
                _hashtable["total"] = total;
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryRiskNotick,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
        }

        /// <summary>
        /// 删除风险告知通知单和发送的相应人员数据(级联删除)
        /// </summary>
        /// <param name="context"></param>
        public void DelRiskNotice(HttpContext context)
        {
            var id = context.Request.Params["id"];
            try
            {
                _riskNoticeDal.Del(new Model.RiskNotice()
                    {
                        ID = id
                    });
                _riskNoticeSignerDal.Del(new RiskNoticeSigner()
                    {
                        RNID = id
                    });
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_DelRiskNotice";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:DelRiskNotice,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
        }

        /// <summary>
        /// 查找审核人、签发人 和编制人
        /// </summary>
        /// <param name="context"></param>
        public void QueryRiskNoticeRIB(HttpContext context)
        {
            var type = context.Request.Params["type"];
            try
            {
                var data = _riskNoticeRiDal.QueryRiskNoticeRI();
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               openid = da.Field<string>("t_weixin"),
                               phonename = da.Field<string>("t_phonenum"),
                               name = da.Field<string>("t_name"),
                               type = da.Field<int>("t_type")
                           };
                _hashtable["data"] = list.ToList();
                if (type == "add")
                {
                    var user = context.Session["user"].ToString();
                    UsersDal usersDal = new UsersDal();
                    var tmp = usersDal.QueryUserInfo(user);
                    var listtmp = from t in tmp.AsEnumerable()
                                  select new
                                      {
                                          name = t.Field<string>("name")
                                      };
                    _hashtable["bzr"] = listtmp.ToList()[0].name;
                }
                if (type == "edit")
                {
                    var id = context.Request.Params["id"];
                    var data2 = _riskNoticeDal.QueryBzr(id);
                    var bzr = from da2 in data2.AsEnumerable()
                              select new
                              {
                                  bzr = da2.Field<string>("t_operator"),
                              };
                    _hashtable["bzr"] = bzr.ToList();
                }
                _hashtable["jsMethod"] = "ajax_QueryRiskNoticeRIB";
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                _hashtable["isSuccess"] = false;
                context.Response.Write(_jss.Serialize(_hashtable));
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 添加签名人，分别有运检部、营销部、安质部、客户和审核人、签发人
        /// 注意：用户可能点击重新发送，所以要先删除RiskNoticeSigner表里面相关的所有
        /// 这条通知单的数据，再重新添加，就没有问题了，要不然就会造成数据冗余和脏数据
        /// </summary>
        /// <param name="context"></param>
        public void AddRiskNoticeSigner(HttpContext context)
        {
            var data = context.Request.Params["data"];
            var id = context.Request.Params["id"];//通知单id
            var num = context.Request.Params["num"];//通知单编号 
            try
            {
                var json = JsonConvert.DeserializeObject<List<RiskNoticeTmp>>(data);

                //1：先删除所有RiskNoticeSigner表中相关的通知单数据(可能有重发)
                _riskNoticeSignerDal.Del(new RiskNoticeSigner()
                    {
                        RNID = id
                    });

                //2：然后向RiskNoticeSigner表中添加审核人签发人的数据
                var dt = _riskNoticeRiDal.QueryRiskNoticeRI();
                var list = from da in dt.AsEnumerable()
                           select new RiskNoticeSigner
                           {
                               ID = Guid.NewGuid().ToString(),
                               RNID = id,
                               WeiXin = da.Field<string>("t_weixin"),
                               PhoneNum = da.Field<string>("t_phonenum"),
                               Name = da.Field<string>("t_name"),
                               Type = da.Field<int>("t_type"),
                               State = 0
                           };
                foreach (var riskNoticeSigner in list)
                {
                    _riskNoticeSignerDal.Add(riskNoticeSigner);
                    //5.另外还要发送微信给审核人
                    if (riskNoticeSigner.Type == 1)
                    {
                        Log.Debug("给审核人发送微信");
                        _sendMessage.SendTxtMessage(riskNoticeSigner.WeiXin, "尊敬的用户您好：您有铜陵电网运行风险预警通知单，编号：" + num + "，请您签字确认！");
                    }
                }

                //3：接着向RiskNoticeSigner表中添加运检部、营销部、安质部、客户
                foreach (var riskNoticeTmp in json)
                {
                    //过滤已经添加的审核人和签发人
                    if (list.Any(s => s.WeiXin == riskNoticeTmp.wxid))
                    {
                        continue;
                    }
                    _riskNoticeSignerDal.Add(new RiskNoticeSigner()
                    {
                        ID = Guid.NewGuid().ToString(),
                        Name = riskNoticeTmp.nickname,
                        PhoneNum = riskNoticeTmp.sjh,
                        RNID = riskNoticeTmp.id,
                        State = 0,
                        WeiXin = riskNoticeTmp.wxid,
                        Type = riskNoticeTmp.type
                    });
                }

                //4：最后根据id号修改通知单状态为已发送
                _riskNoticeDal.UpdateRiskNoticeState(new Model.RiskNotice()
                    {
                        ID = id,
                        State = 1,
                        Send_Dt = DateTime.Now
                    });

                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_AddRiskNoticeSigner";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                _hashtable["isSuccess"] = false;
                context.Response.Write(_jss.Serialize(_hashtable));
                Log.Debug("出错原因：" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 导出word文档
        /// </summary>
        /// <param name="context"></param>
        public void ExportWord(HttpContext context)
        {
            var data = context.Request.Params["data"];
            var riskNoticTmp2 = JsonConvert.DeserializeObject<RiskNoticeTmp2>(data);
            try
            {
                string path = context.Server.MapPath("/RiskNoticeDoc");
                Log.Debug("path=" + path);
                string templatePath = path + @"\RiskNoticeModel.dot";
                Log.Debug("完整路径为=" + templatePath);
                var report = new WordOp();
                report.CreateNewDocument(templatePath); //模板路径

                report.InsertValue("bh", riskNoticTmp2.Number);
                report.InsertValue("fxykcs", riskNoticTmp2.Fxykcs);
                report.InsertValue("bz", riskNoticTmp2.Operator);//编制人
                report.InsertValue("rq", riskNoticTmp2.Rq);
                report.InsertValue("tdsbjgq", riskNoticTmp2.Tdsb_Gq);
                report.InsertValue("yxfxfx", riskNoticTmp2.Yxfxfx);
                report.InsertValue("zs", riskNoticTmp2.Zs);

                //获取签名人图片并插入到word文档中
                var dt = _riskNoticeSignerDal.QueryRiskNoticeSignerByRnid(riskNoticTmp2.Id);
                if (dt.Rows.Count > 0)
                {
                    var list = from dts in dt.AsEnumerable()
                               select new RiskNoticeSigner()
                               {
                                   Img = dts.Field<string>("t_img"),
                                   Type = dts.Field<int?>("t_type")
                               };
                    //根据签字状态判断用户是否全部签字，如果签字修改数据库状态为[已备案]
                    if (list.All(s => s.State == 1))
                    {
                        _riskNoticeDal.Modify(new Model.RiskNotice()
                        {
                            ID = riskNoticTmp2.Id,
                            State = 5
                        });
                    }
                    foreach (var riskNoticeSigner in list)
                    {
                        switch (riskNoticeSigner.Type)
                        {
                            case 1:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("sh", riskNoticeSigner.Img);
                                }
                                break;
                            case 2:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("qf", riskNoticeSigner.Img);
                                }
                                break;
                            case 3:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("yjb", riskNoticeSigner.Img);
                                }
                                break;
                            case 4:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("yxb", riskNoticeSigner.Img);
                                }
                                break;
                            case 5:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("azb", riskNoticeSigner.Img);
                                }
                                break;
                            case 6:
                                if (!string.IsNullOrEmpty(riskNoticeSigner.Img))
                                {
                                    report.InsertPicture("kh", riskNoticeSigner.Img);
                                }
                                break;
                        }
                    }
                }
                report.SaveDocument(path + @"\" + riskNoticTmp2.Number + ".doc");

                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_ExportWord";
                _hashtable["data"] = "/RiskNoticeDoc/" + riskNoticTmp2.Number + ".doc";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = e.Message;
                context.Response.Write(_jss.Serialize(_hashtable));
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        ///  保存图片到指定文件夹
        /// </summary>
        public void SavePictureBase64(HttpContext context)
        {
            string picName = context.Request.Params["picName"];
            string imgstr = context.Request.Params["img"];
            if (string.IsNullOrEmpty(picName) || string.IsNullOrEmpty(imgstr))
            {
                Log.Debug("上传图片为空，不能保存");
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "上传图片为空，不能保存";
                context.Response.Write(_jss.Serialize(_hashtable));
                return;
            }
            //string savePath = "/RiskNoticeImg"; ;//目标图片路径
            //string dirPath = HttpContext.Current.Server.MapPath(savePath);
            //string filePath = dirPath + @"\" + picName;
            string filePath = System.Configuration.ConfigurationSettings.AppSettings["imgdiskpath"] + @"\" + picName;
            Log.Debug("**********************************filePath=" + filePath);
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);
                BinaryWriter sw = new BinaryWriter(fs);
                fs.SetLength(0);//首先把文件清空了。
                //sw.Write(imgstr);//写你的字符串。
                sw.Write(Convert.FromBase64String(imgstr));
                sw.Close();

                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "上传图片成功";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePictureBase64,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "上传图片失败，原因：" + e.Message;
                context.Response.Write(_jss.Serialize(_hashtable));
            }

        }

        /// <summary>
        ///  保存图片到指定文件夹
        /// </summary>
        public void SavePicture(HttpContext context)
        {
            var picName = context.Request.Params["picName"];
            //const string savePath = "/RiskNoticeImg"; //目标图片路径
            //string dirPath = context.Server.MapPath(savePath);
            string filePath = System.Configuration.ConfigurationSettings.AppSettings["imgdiskpath"] + @"\" + picName;

            if (context.Request.Files[0].ContentLength <= 0)
            {
                Log.Debug("上传图片为空，不能保存");
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "上传图片为空，不能保存";
                context.Response.Write(_jss.Serialize(_hashtable));
                return;
            }
            try
            {
                context.Request.Files[0].SaveAs(filePath);
                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "上传图片成功";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePicture,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "上传图片失败，原因：" + e.Message;
                context.Response.Write(_jss.Serialize(_hashtable));
            }

        }

        /// <summary>
        /// 预览通知单
        /// </summary>
        /// <param name="context"></param>
        public void ViewRiskNotice(HttpContext context)
        {
            var id = context.Request.Params["id"];
            try
            {
                var dt = _riskNoticeSignerDal.QueryRiskNoticeSignerByRnid(id);
                var list = from da in dt.AsEnumerable()
                           select new
                               {
                                   Img = da.Field<string>("t_img"),
                                   Type = da.Field<int?>("t_type")
                               };
                _hashtable["isSuccess"] = true;
                _hashtable["data"] = list.ToList();
                _hashtable["jsMethod"] = "ajax_ViewRiskNotice";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = e.Message;
                context.Response.Write(_jss.Serialize(_hashtable));
                Log.Debug("出错原因：" + e.Message);
            }

        }

        /// <summary>
        /// 根据ID查询通知单信息
        /// </summary>
        /// <param name="context"></param>
        public void QueryRiskNoticebyID(HttpContext context)
        {
            try
            {
                string id = context.Request.Params["id"];
                var dt = _riskNoticeDal.QueryRiskNoticeByID(id);
                var list = from da in dt.AsEnumerable()
                           select new
                           {
                               ID = da.Field<string>("ID"),
                               t_number = da.Field<string>("t_number"),
                               t_date = da.Field<string>("t_date"),
                               t_zs = da.Field<string>("t_zs"),
                               t_tdsb_gq = da.Field<string>("t_tdsb_gq"),
                               t_yxfxfx = da.Field<string>("t_yxfxfx"),
                               t_fxykcs = da.Field<string>("t_fxykcs"),
                               t_operator = da.Field<string>("t_operator"),
                               t_send_dt = da.Field<DateTime?>("t_send_dt"),
                               t_createdt = da.Field<DateTime?>("t_createdt"),
                               t_state = da.Field<int?>("t_state")
                           };
                _hashtable["isSuccess"] = true;
                _hashtable["msg"] = "获取成功";
                _hashtable["data"] = list.ToList();
                //解决乱码代码：
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryRiskNotice,错误原因:" + e.Message);
                _hashtable["isSuccess"] = false;
                _hashtable["msg"] = "获取失败";
                _hashtable["data"] = null;
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
        }


        /// <summary>
        /// 查询签名人详细信息
        /// </summary>
        /// <param name="context"></param>
        public void QueryRiskNoticeDetail(HttpContext context)
        {
            var id = context.Request.Params["id"];
            try
            {
                var data = _riskNoticeSignerDal.QueryRiskNoticeSignerByRnid(id);
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   sjh = da.Field<string>("t_phonenum"),
                                   name = da.Field<string>("t_name"),
                                   type = da.Field<int?>("t_type"),
                                   state = da.Field<int?>("t_state"),
                                   dt = da.Field<DateTime?>("t_signerdt"),
                               };
                _hashtable["rows"] = list.ToList();
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("错误原因：" + e.Message);
            }
        }
    }

    public class RiskNoticeTmp
    {
        public string id { get; set; }

        public string wxid { get; set; }

        public string nickname { get; set; }

        public string sjh { get; set; }

        public int? type { get; set; }
    }

    public class RiskNoticeTmp2
    {
        public string Id { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string Number { get; set; }
        public string Rq { get; set; }
        public string Zs { get; set; }
        public string Tdsb_Gq { get; set; }
        public string Yxfxfx { get; set; }
        public string Fxykcs { get; set; }
        public string State { get; set; }
        public string Send_Dt { get; set; }
        public string CreateDt { get; set; }
        public string Operator { get; set; }
    }
}