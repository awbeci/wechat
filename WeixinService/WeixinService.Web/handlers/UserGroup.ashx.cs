﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for UserGroup
    /// </summary>
    public class UserGroup : IHttpHandler, IRequiresSessionState
    {

        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询所有组数据
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryAllGroup(HttpContext httpContext)
        {
            var hashtable = new Hashtable();
            var userGroupDal = new UserGroupDal();
            var infoReleaseDal = new InfoReleaseDal();
            var id = httpContext.Request.Params["id"];
            try
            {
                var data = userGroupDal.QueryAll();
                var dataList = from da in data.AsEnumerable()
                               select new
                                   {
                                       id = da.Field<string>("id"),
                                       name = da.Field<string>("name"),
                                       pid = da.Field<string>("pid")
                                   };
                var groupData = infoReleaseDal.QuerySendGroup(id);
                if (groupData.Rows.Count > 0 && groupData.Rows[0]["SendGroup"] != null && !string.IsNullOrEmpty(groupData.Rows[0]["SendGroup"].ToString().Trim()))
                {
                    var groupDataList = from groupDa in groupData.AsEnumerable()
                                        select new
                                        {
                                            sendGroup = groupDa.Field<string>("SendGroup")
                                        };
                    hashtable["sendGroup"] = groupDataList.ToList();
                }
                hashtable["data"] = dataList.ToList();
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryAllGroup";
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }

        /// <summary>
        /// 查询所有选中人员数据
        /// </summary>
        /// <param name="httpContext"></param>
        public void QuerySelectedNodes(HttpContext httpContext)
        {
            var hashtable = new Hashtable();
            var riskNoticeSignerDal = new RiskNoticeSignerDal();
            var riskNoticeRiDal = new RiskNoticeRIDal();
            var userGroupDal = new UserGroupDal();
            var id = httpContext.Request.Params["id"];
            try
            {
                var data = userGroupDal.QueryAll();
                var dataList = from da in data.AsEnumerable()
                               select new
                               {
                                   id = da.Field<string>("id"),
                                   name = da.Field<string>("name"),
                                   pid = da.Field<string>("pid")
                               };
                //这里要查询已经发送给的用户列表，显示在[发送面板]的[已选择用户]列表中。
                //如果审核人或签发人改变，那么前台要提示用户审核人或签发人已经修改，要先删除通知单重新添加
                var data3 = riskNoticeRiDal.QueryRiskNoticeRI();
                var dataList3 = from da3 in data3.AsEnumerable()
                                select new
                                    {
                                        openid = da3.Field<string>("t_weixin"),
                                        type = da3.Field<int?>("t_type")
                                    };

                var data2 = riskNoticeSignerDal.QueryRiskNoticeSignerByRnid(id);
                if (data2.Rows.Count <= 0)
                {
                    data2 = riskNoticeRiDal.QueryRiskNoticeRI();
                    var dataList2 = from da2 in data2.AsEnumerable()
                                    select new
                                    {
                                        openid = da2.Field<string>("t_weixin"),
                                        nickname = da2.Field<string>("t_name"),
                                        type = da2.Field<int?>("t_type"),
                                        sjh = da2.Field<string>("t_phonenum")
                                    };
                    hashtable["data3"] = true;
                    hashtable["data2"] = dataList2.ToList();
                }
                else
                {
                    var dataList2 = from da2 in data2.AsEnumerable()
                                    select new
                                    {
                                        openid = da2.Field<string>("t_weixin"),
                                        nickname = da2.Field<string>("t_name"),
                                        type = da2.Field<int?>("t_type"),
                                        sjh = da2.Field<string>("t_phonenum")
                                    };
                    var tmp = dataList3.Any(s => (dataList2.Any(k => k.openid == s.openid && k.type == 1 && s.type == 1)));
                    tmp &= dataList3.Any(s => (dataList2.Any(k => k.openid == s.openid && k.type == 2 && s.type == 2)));
                    hashtable["data3"] = tmp;
                    hashtable["data2"] = dataList2.ToList();
                }
                hashtable["data"] = dataList.ToList();
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QuerySelectedNodes";
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }

        /// <summary>
        /// 添加组到信息发布表
        /// </summary>
        /// <param name="context"></param>
        public void AddGroupToInfoRelease(HttpContext context)
        {
            var hashTable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();

            var id = context.Request.Params["id"];
            var groupsId = context.Request.Params["groupsId"];
            try
            {
                infoReleaseDal.Modify(new InfoRelease()
                    {
                        Id = id,
                        FlagRelease = "1",
                        SendGroup = groupsId
                    });
                hashTable["isSuccess"] = true;
                hashTable["jsMethod"] = "ajax_AddGroupToInfoRelease";
                var json = _jss.Serialize(hashTable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错信息：" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}