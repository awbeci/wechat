﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using Newtonsoft.Json;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;
using WeixinService.Model.common;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for PublishInfo
    /// </summary>
    public class PublishInfo : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("PowerInfo.ashx,描述:映射方法名出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryPublishInfo(HttpContext httpContext)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var businesstype = httpContext.Request.Params["businesstype"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(httpContext.Request["page"] ?? "1");
            var rows = int.Parse(httpContext.Request["rows"] ?? "10");
            try
            {
                var data = infoReleaseDal.QueryByPage(new InfoRelease()
                {
                    BusinessType = businesstype
                }, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               Title = da.Field<string>("Title"),
                               MaterialContent = da.Field<string>("MaterialContent"),
                               MaterialType = da.Field<string>("MaterialType"),
                               FlagRelease = da.Field<string>("FlagRelease"),
                               MessageDescription = da.Field<string>("MessageDescription"),
                               CreateDt = da.Field<DateTime?>("CreateDt"),
                               CreatePerson = da.Field<string>("CreatePerson"),
                               Bak1 = da.Field<string>("Bak1"),
                               Bak2 = da.Field<string>("Bak2"),
                               Bak3 = da.Field<string>("Bak3"),
                               Bak4 = da.Field<string>("Bak4"),
                               SendGroup = da.Field<string>("SendGroup")
                               
                           };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryPowerInfo,描述:查询停电信息方法出错,错误原因:" + e.Message);
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }

        }

        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="context"></param>
        public void SavePublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var type = context.Request.Params["type"];
            var key = context.Request.Params["key"];
            var messageDescription = context.Request.Params["messageDescription"];
            var title = context.Request.Params["title"];
            var content = context.Request.Params["contents"];

            var infoRelease = new InfoRelease();
            infoRelease.Id = Guid.NewGuid().ToString();
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;

            infoRelease.CreatePerson = context.Session["user"].ToString();
            infoRelease.FlagRelease = "0";
            infoRelease.MaterialContent = content;
            infoRelease.MaterialType = "news";
            infoRelease.MessageDescription = messageDescription;
            infoRelease.SendGroup = "";
            infoRelease.SendPerson = "";
            infoRelease.Title = title;

            try
            {
                infoReleaseDal.Add(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="context"></param>
        public void SavePublishInfoPhone(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var key = context.Request.Params["key"];
            var people = context.Request.Params["people"];
            var title = context.Request.Params["title"];
            var content = context.Request.Params["contents"];

            var infoRelease = new InfoRelease();
            infoRelease.Id = Guid.NewGuid().ToString();
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;

            infoRelease.CreatePerson = people;
            infoRelease.FlagRelease = "1";
            infoRelease.MaterialContent = content;
            infoRelease.MaterialType = "news";
            infoRelease.Title = title;

            try
            {
                infoReleaseDal.Add(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePublishInfoPhone,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="context"></param>
        public void EditPublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var id = context.Request.Params["id"];
            var key = context.Request.Params["key"];
            var type = context.Request.Params["type"];
            var messageDescription = context.Request.Params["messageDescription"];
            var title = context.Request.Params["title"];
            var content = context.Request.Params["contents"];

            var infoRelease = new InfoRelease();
            infoRelease.Id = id;
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;
            infoRelease.CreatePerson = "";
            //infoRelease.FlagRelease = "0";
            infoRelease.MaterialContent = content;
            infoRelease.MaterialType = type;
            infoRelease.MessageDescription = messageDescription;
            infoRelease.SendGroup = "";
            infoRelease.SendPerson = "";
            infoRelease.Title = title;

            try
            {
                infoReleaseDal.Modify(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="context"></param>
        public void DelPublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var infoReleaseDetails = new InfoReleaseDetailsDal();
            var id = context.Request.Params["id"];

            try
            {
                infoReleaseDal.Del(new InfoRelease() { Id = id });
                infoReleaseDetails.Del(new InfoReleaseDetailsModel() { InfoReleaseId = id });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_DelPowerInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 发布信息到人(实际上只是修改了下发布状态，仅此而已)
        /// </summary>
        /// <param name="context"></param>
        public void PublishInfoToPerson(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var userInfoDal = new UserInfoDal();
            var id = context.Request.Params["id"];
            try
            {
                //var data = userInfoDal.QueryAll();
                //var tmp = string.Join(",",
                //    (
                //        from DataRow da in data.Rows
                //        select da["openid"].ToString()
                //    ).ToArray());

                infoReleaseDal.Modify(new InfoRelease()
                    {
                        Id = id,
                        FlagRelease = "1"
                    });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_PublishInfoToPerson";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:PublishInfoToPerson,,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 查找信息发布表详细数据
        /// </summary>
        /// <param name="context"></param>
        public void QueryInfoReleaseDetails(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
            var id = context.Request.Params["id"];
            try
            {
                var data = infoReleaseDetailsDal.QueryByInfoReleaseId(id);
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   wxh = da.Field<string>("openid"),
                                   nickname = da.Field<string>("nickname"),
                                   yyzt = da.Field<string>("bk1"),
                                   yy = da.Field<string>("bk3"),
                                   state = da.Field<string>("State"),
                                   confirmdt = da.Field<DateTime?>("confirmdt")
                               };
                hashtable["rows"] = list.ToList();
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryInfoReleaseDetails,错误原因:" + e.Message);
            }
        }

        public void GetUserRiskMessage(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
            var state = context.Request.Params["state"];

            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "20");
            try
            {
                var data = infoReleaseDetailsDal.QueryUserRiskMessage(state, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               Title = da.Field<string>("Title"),
                               MessageDescription = da.Field<string>("MessageDescription"),
                               State = da.Field<string>("State"),
                               Nickname = da.Field<string>("nickname"),
                               Openid = da.Field<string>("openid"),
                               bak1 = da.Field<string>("bak1"),
                               bak2 = da.Field<string>("bak2"),
                               bak3 = da.Field<string>("bak3"),
                               bak4 = da.Field<string>("bak4"),//语音路径
                               bak5 = da.Field<string>("bak5"),//语音确认，1-已确认，0或null-未确认
                               PhoneNumber = da.Field<string>("PhoneNumber"),
                               confirmdt = da.Field<DateTime?>("confirmdt")
                           };
                hashtable["total"] = total;
                hashtable["rows"] = list.ToList();
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryInfoReleaseDetails,,错误原因:" + e.Message);
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\",\"rows\":[]}");
            }
        }

        private void Send(HttpContext context)
        {
            var userid = context.Request["userid"] ?? "";
            var Id = context.Request["Id"] ?? "";
            var Title = context.Request["title"] ?? "";
            var messageDescription = context.Request["MessageDescription"] ?? "";
            var key = context.Request["key"] ?? "";
            if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(Id) || string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(messageDescription))
            {
                context.Response.Write("{\"error\":true,\"msg\":\"信息不完整。\"}");
                return;
            }

            var hashTable = new Hashtable();
            var listnews = new List<object>();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();

            try
            {
                listnews.Add(new
                    {
                        title = Title,
                        description = messageDescription,
                        url = "http://218.22.27.236/views/messagelist/messagedetail_fxgz.htm?id=" + Id + "&wxid=" + userid + "&state=2",
                        picurl = "http://218.22.27.236/tl/UploadImages/" + key + ".jpg"
                    });

                hashTable["touser"] = userid;
                hashTable["msgtype"] = "news";
                hashTable["news"] = new
                {
                    articles = listnews
                };
                var json = _jss.Serialize(hashTable);
                json = json.Replace("\\u0026", "&");
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                var backData = JsonConvert.DeserializeObject<WeChatBackData>(back);
                if (backData.errcode != 0)
                {
                    context.Response.Write("{\"error\":true,\"msg\":\"" + backData.errmsg + "\"}");
                }
                else
                {
                    context.Response.Write("{\"error\":false}");
                }
                var infoReleaseDetailsModel = new InfoReleaseDetailsModel();
                if (backData.errcode == 0 && backData.errmsg == "ok")
                {
                    infoReleaseDetailsModel.State = "2";//待确认
                }
                var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
                infoReleaseDetailsModel.Id = Id;
                infoReleaseDetailsDal.Modify(infoReleaseDetailsModel);
                Log.Debug("调试信息:" + back);
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Error(e);
            }
        }

        /// <summary>
        /// 修改信息发布详细表里状态
        /// </summary>
        /// <param name="context"></param>
        public void ModifyInfoReleaseDetails(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDetailsDal = new InfoReleaseDetailsDal();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var wxid = context.Request.Params["wxid"];
            var id = context.Request.Params["id"];
            try
            {
                //修改信息详细表内容为已确认
                infoReleaseDetailsDal.ModifyStateByWxid(new InfoReleaseDetailsModel()
                    {
                        OpenId = wxid,
                        InfoReleaseId = id
                    });

                hashtable["touser"] = wxid;
                hashtable["msgtype"] = "text";
                hashtable["text"] = new
                {
                    content = "尊敬的客户，请按以下标准导语进行语音确认，“某某单位某某某已签收并确认某月某日高压用户风险告知通知书”。"
                };
                var json = _jss.Serialize(hashtable);
                var token = accessToken.GetExistAccessToken();
                var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                var backData = JsonConvert.DeserializeObject<WeChatBackData>(back);
                if (backData.errcode == 0 && backData.errmsg == "ok")
                {
                    hashtable["isSuccess"] = true;
                    hashtable["jsMethod"] = "ajax_ModifyInfoReleaseDetails";
                }
                else
                {
                    hashtable["isSuccess"] = false;
                    hashtable["msg"] = "发送文本消息失败";
                }
                json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:ModifyInfoReleaseDetails,,错误原因:" + e.Message);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}