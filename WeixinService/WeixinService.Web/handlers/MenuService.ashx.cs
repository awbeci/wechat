﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;
using System.Web.SessionState;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for MenuService
    /// </summary>
    public class MenuService : IHttpHandler, IRequiresSessionState
    {

        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        readonly MenuDal _menuDal = new MenuDal();
        Hashtable _hashtable = new Hashtable();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 根据角色查询菜单
        /// </summary>
        /// <param name="context"></param>
        public void QueryMenuByRoles(HttpContext context)
        {
            var html = "";
            var session = context.Session["user"];
            if (session == null)
            {
                return;
            }
            try
            {
                var mainData = _menuDal.QueryMainMenu();
                var mainList = from main in mainData.AsEnumerable()
                               select new
                                   {
                                       mainid = main.Field<string>("id"),
                                       maintitle = main.Field<string>("title")
                                   };
                var count = 0;
                foreach (var m in mainList.ToList())
                {
                    count++;
                    var subData = _menuDal.QuerySubMenu(session.ToString(), m.mainid);
                    if (subData.Rows.Count <= 0)
                        continue;
                    var subList = from sub in subData.AsEnumerable()
                                  select new
                                      {
                                          subtitle = sub.Field<string>("title"),
                                          suburl = sub.Field<string>("Url"),
                                          icon = sub.Field<string>("icon")
                                      };
                    if (count == 1)
                    {
                        html += "<div title='" + m.maintitle + "' data-options='selected:true' style='padding: 10px; overflow: auto;'>";
                    }
                    else
                    {
                        html += "<div title='" + m.maintitle + "' style='padding: 10px; '>";
                    }
                    foreach (var s in subList.ToList())
                    {
                        html += "<div class='mylistbt'>";
                        html += "<a href='javascript:void(0);' src='" + s.suburl + "' class='easyui-linkbutton cs-navi-tab' data-options='iconCls:\"" + s.icon + "\"'>" + s.subtitle + "</a>";
                        html += "</div>";
                    }
                    html += "</div>";
                }
                _hashtable["isSuccess"] = true;
                _hashtable["data"] = html;
                _hashtable["jsMethod"] = "ajax_QueryMenuByRoles";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}