﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Dal;
using WeixinService.Model;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for ManageService
    /// </summary>
    public class ManageService : IHttpHandler, IRequiresSessionState
    {

        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        Hashtable _hashtable = new Hashtable();
        DepartmentDal _departmentDal = new DepartmentDal();
        UsersDal _usersDal = new UsersDal();
        MenuDal _menuDal = new MenuDal();
        RoleMenuDal _roleMenuDal = new RoleMenuDal();
        RolesDal _rolesDal = new RolesDal();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询部门
        /// </summary>
        /// <param name="context"></param>
        public void QueryDepartment(HttpContext context)
        {
            try
            {
                var data = _departmentDal.QueryAll();
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   id = da.Field<string>("id"),
                                   name = da.Field<string>("name"),
                                   pid = da.Field<string>("pid")
                               };
                _hashtable["isSuccess"] = true;
                _hashtable["data"] = list.ToList();
                _hashtable["jsMethod"] = "ajax_QueryDepartment";
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 添加部门
        /// </summary>
        /// <param name="context"></param>
        public void AddDepartment(HttpContext context)
        {
            var id = context.Request.Params["id"];
            var pId = context.Request.Params["pId"];
            var name = context.Request.Params["name"];
            try
            {
                _departmentDal.Add(new DepartmentModel()
                    {
                        Id = id,
                        Name = name,
                        PId = pId
                    });
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_AddDepartment";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 更新部门
        /// </summary>
        /// <param name="context"></param>
        public void UpdateDepartment(HttpContext context)
        {
            var id = context.Request.Params["id"];
            var name = context.Request.Params["name"];
            try
            {
                _departmentDal.Modify(new DepartmentModel()
                {
                    Id = id,
                    Name = name
                });
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_UpdateDepartment";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="context"></param>
        public void DelDepartment(HttpContext context)
        {
            var id = context.Request.Params["id"];
            try
            {
                _departmentDal.Del(new DepartmentModel()
                {
                    Id = id
                });
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_DelDepartment";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询用户
        /// </summary>
        /// <param name="context"></param>
        public void QueryUser(HttpContext context)
        {
            var id = context.Request.Params["uid"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var data = _usersDal.QueryPageByDepartmentId(id, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               id = da.Field<string>("id"),
                               openid = da.Field<string>("openid"),
                               name = da.Field<string>("name"),
                               loginname = da.Field<string>("loginname"),
                               password = da.Field<string>("password"),
                               deptname = da.Field<string>("deptname"),
                               rolename = da.Field<string>("rolename")
                           };
                _hashtable["total"] = total;
                _hashtable["rows"] = list.ToList();
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 添加人员
        /// </summary>
        /// <param name="context"></param>
        public void AddUser(HttpContext context)
        {
            var name = context.Request["name"];
            var loginname = context.Request["loginname"];
            var sel_role = context.Request["sel_role"];
            var bmid = context.Request["bmid"];
            var openid = context.Request["openid"];
            try
            {
                var boolData = _usersDal.ExistLoginName(new Users() { LoginName = loginname });
                if (!boolData)
                {
                    _usersDal.Add(new Users()
                    {
                        Id = Guid.NewGuid().ToString(),
                        LoginName = loginname,
                        Name = name,
                        DepartmentId = bmid,
                        RoleId = sel_role,
                        OpenId = openid
                    });
                    _hashtable["isSuccess"] = true;
                    context.Response.Write(_jss.Serialize(_hashtable));
                }
                else
                {
                    _hashtable["isSuccess"] = false;
                    _hashtable["msg"] = "该用户已经存在";
                    context.Response.Write(_jss.Serialize(_hashtable));
                }

            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 更新人员
        /// </summary>
        /// <param name="context"></param>
        public void UpdateUser(HttpContext context)
        {
            var name = context.Request["name"];
            var loginname = context.Request["loginname"];
            var sel_role = context.Request["sel_role"];
            var id = context.Request["id"];
            var openid = context.Request["openid"];
            try
            {
                var Data = _usersDal.QueryUserByLName(new Users() { LoginName = loginname });
                if (Data.Rows.Count > 0 && !id.Equals(Data.Rows[0]["id"]))
                {
                    _hashtable["isSuccess"] = false;
                    _hashtable["msg"] = "该用户已经存在";
                    context.Response.Write(_jss.Serialize(_hashtable));
                }
                else
                {
                    _usersDal.Modify(new Users()
                    {
                        Id = id,
                        RoleId = sel_role,
                        LoginName = loginname,
                        Name = name,
                        OpenId = openid
                    });
                    _hashtable["isSuccess"] = true;
                    context.Response.Write(_jss.Serialize(_hashtable));
                }
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }


        private void QueryUnbindWeixin(HttpContext context)
        {
            try
            {
                var wxid = context.Request["wxid"] ?? "";
                UserGroupDal userGroupDal = new UserGroupDal();
                var data = userGroupDal.QueryUnbindInternalUser(wxid);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               openid = da.Field<string>("openid"),
                               nickname = da.Field<string>("nickname"),
                               sex = da.Field<int>("sex"),
                               TrueName = da.Field<string>("TrueName"),
                               selected = da.Field<string>("openid") == wxid ? true : false,
                           };

                _hashtable["total"] = list.Count();
                _hashtable["rows"] = list.ToList();
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 删除人员
        /// </summary>
        /// <param name="context"></param>
        public void DelUser(HttpContext context)
        {
            var id = context.Request["id"];
            try
            {
                _rolesDal.Del(new Roles() { Id = id });
                _hashtable["isSuccess"] = true;
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 获取所有菜单项，显示在ztree中
        /// </summary>
        /// <param name="context"></param>
        private void Menu(HttpContext context)
        {
            try
            {
                var str = new StringBuilder();
                var roleid = context.Request["roleid"] ?? "";
                var dtRoot = _menuDal.Query(new MenuModel() { PId = "0" });
                str.Append("[");
                if (dtRoot.Rows.Count > 0)
                {
                    for (var i = 0; i < dtRoot.Rows.Count; i++)
                    {
                        var rId = dtRoot.Rows[i]["id"].ToString();
                        var rName = dtRoot.Rows[i]["title"].ToString();
                        if (i > 0)
                        {
                            str.Append(",");
                        }
                        str.Append("{id:'" + rId + "',pId:'0' ,name:'" + rName + "',icon:'../../../../images/1.png',isParent:true,open:true }");

                        var dtNodes = _menuDal.Query(new MenuModel() { PId = rId });
                        if (dtNodes.Rows.Count <= 0) continue;
                        for (var j = 0; j < dtNodes.Rows.Count; j++)
                        {
                            var nId = dtNodes.Rows[j]["id"].ToString(); // 节点id
                            var nName = dtNodes.Rows[j]["Title"].ToString(); // 节点名称 
                            var pId = dtNodes.Rows[j]["pid"].ToString(); // 上级节点id

                            str.Append(",");
                            if (IsCheckedNode(roleid, nId))
                            {
                                str.Append("{id:'" + nId + "',pId:'" + pId + "',name:'" + nName + "',icon:'../../../../images/2.png',checked:true,open:true }");

                                var dtNodes3 = _menuDal.Query(new MenuModel() { PId = nId });
                                if (dtNodes3.Rows.Count <= 0) continue;
                                for (var k = 0; k < dtNodes3.Rows.Count; k++)
                                {
                                    var nId3 = dtNodes3.Rows[k]["id"].ToString(); // 节点id
                                    var nName3 = dtNodes3.Rows[k]["title"].ToString(); // 节点名称 
                                    var pId3 = dtNodes3.Rows[k]["pid"].ToString(); // 上级节点id

                                    str.Append(",");
                                    if (IsCheckedNode(roleid, nId3))
                                    {
                                        str.Append("{id:'" + nId3 + "',pId:'" + pId3 + "',name:'" + nName3 + "',icon:'../../../../images/2.png',checked:true,open:true }");
                                    }
                                    else
                                    {
                                        str.Append("{id:'" + nId3 + "',pId:'" + pId3 + "',name:'" + nName3 + "',icon:'../../../../images/2.png',open:true }");
                                    }

                                }
                            }
                            else
                            {
                                str.Append("{id:'" + nId + "',pId:'" + pId + "',name:'" + nName + "',icon:'../../../../images/2.png',open:true }");

                                var dtNodes3 = _menuDal.Query(new MenuModel() { PId = nId });

                                if (dtNodes3.Rows.Count <= 0) continue;
                                for (var k = 0; k < dtNodes3.Rows.Count; k++)
                                {
                                    var nId3 = dtNodes3.Rows[k]["id"].ToString(); // 节点id
                                    var nName3 = dtNodes3.Rows[k]["title"].ToString(); // 节点名称 
                                    var pId3 = dtNodes3.Rows[k]["pid"].ToString(); // 上级节点id

                                    str.Append(",");
                                    if (IsCheckedNode(roleid, nId3))
                                    {
                                        str.Append("{id:'" + nId3 + "',pId:'" + pId3 + "',name:'" + nName3 + "',icon:'../../../../images/2.png',checked:true,open:true }");
                                    }
                                    else
                                    {
                                        str.Append("{id:'" + nId3 + "',pId:'" + pId3 + "',name:'" + nName3 + "',icon:'../../../../images/2.png',open:true }");
                                    }

                                }
                            }

                        }
                    }

                }
                str.Append("]");
                context.Response.Write(str.ToString());
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Error(e);
            }
        }

        public bool IsCheckedNode(string roleid, string menuid)
        {
            var roleMenuDal = new RoleMenuDal();
            var roleMenu = new RoleMenu
            {
                MenuId = menuid,
                RoleId = roleid
            };
            return roleMenuDal.Exist(roleMenu);
        }

        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <param name="context"></param>
        public void QueryRoles(HttpContext context)
        {
            var str = new StringBuilder();
            var dtRoot = _rolesDal.Query(new Roles());
            str.Append("[");
            if (dtRoot.Rows.Count > 0)
            {
                for (var i = 0; i < dtRoot.Rows.Count; i++)
                {
                    var rId = dtRoot.Rows[i]["id"].ToString();
                    var rName = dtRoot.Rows[i]["name"].ToString();
                    if (i > 0)
                    {
                        str.Append(",");
                    }
                    str.Append("{'id':'" + rId + "','pId':'0','name':'" + rName + "','icon':'../../../../images/renyuan.png','open':true }");
                }
            }
            str.Append("]");
            context.Response.Write(str.ToString());
        }

        /// <summary>
        /// 保存菜单角色对应关系
        /// </summary>
        /// <param name="context"></param>
        private void SaveMenuRole(HttpContext context)
        {
            try
            {
                var roleId = context.Request["roleid"] ?? "";
                var menuIdStr = context.Request["menuid"] ?? "";
                var menuId = menuIdStr.Split(',');
                _roleMenuDal.Del(new RoleMenu() { RoleId = roleId });
                foreach (var s in menuId)
                {
                    _roleMenuDal.Add(new RoleMenu() { RoleId = roleId, MenuId = s });
                }
                context.Response.Write("{\"error\":false}");
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Error(e);
            }

        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="context"></param>
        private void DelRole(HttpContext context)
        {
            try
            {
                var nodeId = context.Request["nodeId"] ?? "";
                _roleMenuDal.Del(new RoleMenu() { RoleId = nodeId });
                _rolesDal.Del1(new Roles() { Id = nodeId });
                context.Response.Write("{\"error\":false}");
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Error(e);
            }
        }

        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="context"></param>
        private void SaveRole(HttpContext context)
        {
            try
            {
                var jsmc = context.Request["jsmc"] ?? "";
                _rolesDal.Add(new Roles() { Name = jsmc });
                context.Response.Write("{\"error\":false}");
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Error(e);
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <param name="context"></param>
        public void QueryRole(HttpContext context)
        {
            try
            {
                var data = _rolesDal.QueryAll();
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               id = da.Field<string>("id"),
                               val = da.Field<string>("Name")
                           };
                _hashtable["data"] = list.ToList();
                _hashtable["isSuccess"] = true;
                _hashtable["jsMethod"] = "ajax_QueryRoles";
                context.Response.Write(_jss.Serialize(_hashtable));
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="context"></param>
        public void ResetPass(HttpContext context)
        {
            var id = context.Request.Params["id"];
            try
            {
                _usersDal.ResetPass(id);
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        public void QueryUnbindWeixin2(HttpContext context)
        {
            var wxid = context.Request.Params["wxid"];
            try
            {
                var userGroupDal = new UserGroupDal();
                var data = userGroupDal.QueryUnbindInternalUser();
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               openid = da.Field<string>("openid"),
                               nickname = da.Field<string>("nickname"),
                               sex = da.Field<int>("sex"),
                               trueName = da.Field<string>("TrueName"),
                               phonenumber = da.Field<string>("phonenumber"),
                               selected = da.Field<string>("openid") == wxid,
                           };

                _hashtable["total"] = list.Count();
                _hashtable["rows"] = list.ToList();
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + e.Message + "\"}");
                Log.Debug("出错原因：" + e.Message);
            }
        }
    }
}