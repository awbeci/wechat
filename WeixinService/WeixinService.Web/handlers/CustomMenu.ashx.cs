﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;
using System.Web.SessionState;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for CustomMenu
    /// </summary>
    public class CustomMenu : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 查询菜单并拼接数据
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryMenuAddStitching(HttpContext httpContext)
        {
            var hashTable = new Hashtable();
            var customMenuDal = new CustomMenuDal();
            var data = customMenuDal.Query(new Model.CustomMenu());
            var tmp = from da in data.AsEnumerable()
                      select new
                          {
                              Id = da.Field<string>("id"),
                              Pid = da.Field<string>("pid"),
                              Type = da.Field<string>("type"),
                              Name = da.Field<string>("name"),
                              Nkey = da.Field<string>("nkey"),
                              Url = da.Field<string>("url")
                          };
            hashTable["isSuccess"] = true;
            hashTable["data"] = tmp.ToList();
            hashTable["jsMethod"] = "ajax_QueryMenuAddStitching";
            var json = _jss.Serialize(hashTable);
            httpContext.Response.Write(json);
        }

        /// <summary>
        /// 发送菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void SendMenu(HttpContext httpContext)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var hashTableBack = new Hashtable();
            var sendDataToWeChat = new SendDataToWeChat();
            var customMenuDal = new CustomMenuDal();
            var data = customMenuDal.QueryByFirstMenu();//查询一级菜单
            var tmp = from da in data.AsEnumerable()
                      select new
                      {
                          id = da.Field<string>("id"),
                          pid = da.Field<string>("pid"),
                          type = da.Field<string>("type"),
                          name = da.Field<string>("name"),
                          key = da.Field<string>("nkey"),
                          url = da.Field<string>("url")
                      };

            var list = new List<object>();
            foreach (var info in tmp.ToList())
            {
                //  算法：
                //    首先构造一个List<object>，然后，通过一级菜单数据查询二级菜单，
                //    如果有二级菜单就构造一个MainMenuParameter()类，将它加到List<object>中
                //    如果没有则构造一个SubMenuParameter类，将它加到List<object>中
                var dataSub = customMenuDal.QueryBySecondMenu(info.id);//查询二级菜单
                if (dataSub.Rows.Count != 0)
                {
                    var mainMenuParameter = new MainMenuParameter();
                    var tmpSub = from da in dataSub.AsEnumerable()
                                 select new
                                 {
                                     id = da.Field<string>("id"),
                                     pid = da.Field<string>("pid"),
                                     type = da.Field<string>("type"),
                                     name = da.Field<string>("name"),
                                     key = da.Field<string>("nkey"),
                                     url = da.Field<string>("url")
                                 };
                    var subList = new List<object>();
                    foreach (var tm in tmpSub)
                    {
                        switch (tm.type)
                        {
                            case "click":
                                {
                                    var menuClick = new MenuClick { name = tm.name, key = tm.key, type = tm.type };
                                    subList.Add(menuClick);
                                }
                                break;
                            case "view":
                                {
                                    var menuView = new MenuView { name = tm.name, url = tm.url, type = tm.type };
                                    subList.Add(menuView);
                                }
                                break;
                        }
                    };

                    mainMenuParameter.name = info.name;
                    mainMenuParameter.sub_button = subList.ToList();
                    list.Add(mainMenuParameter);
                }
                else
                {

                    switch (info.type)
                    {
                        case "click":
                            {
                                var menuClick = new MenuClick { name = info.name, key = info.key, type = info.type };
                                list.Add(menuClick);
                            }
                            break;
                        case "view":
                            {
                                var menuView = new MenuView { name = info.name, url = info.url, type = info.type };
                                list.Add(menuView);
                            }
                            break;
                    }
                }
            }

            hashTable["button"] = list.ToList();
            var json = _jss.Serialize(hashTable);
            json = json.Replace("\\u0026", "&");
            var token = accessToken.GetExistAccessToken();
            var back = sendDataToWeChat.GetPage(" https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + token, json);

            hashTableBack["isSuccess"] = true;
            hashTableBack["data"] = back;
            hashTableBack["jsMethod"] = "ajax_SendMenu";
            var jsonBack = _jss.Serialize(hashTableBack);
            httpContext.Response.Write(jsonBack);
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void AddMenu(HttpContext httpContext)
        {
            var customMenu = new Model.CustomMenu();
            var customMenuDal = new CustomMenuDal();
            var hashTable = new Hashtable();
            var pid = httpContext.Request.Params["pid"];
            var type = httpContext.Request.Params["type"];
            var name = httpContext.Request.Params["name"];
            var nkey = httpContext.Request.Params["nkey"];
            var nurl = httpContext.Request.Params["nurl"];

            customMenu.Id = nkey;
            customMenu.Name = name;
            customMenu.Nkey = nkey;
            customMenu.Pid = pid;
            customMenu.Type = type;
            customMenu.Url = nurl;

            customMenuDal.Add(customMenu);

            hashTable["isSuccess"] = true;
            hashTable["jsMethod"] = "ajax_AddMenu";
            var json = _jss.Serialize(hashTable);
            httpContext.Response.Write(json);
        }

        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void EditMenu(HttpContext httpContext)
        {
            var customMenu = new Model.CustomMenu();
            var customMenuDal = new CustomMenuDal();
            var hashTable = new Hashtable();
            var id = httpContext.Request.Params["id"];
            var type = httpContext.Request.Params["type"];
            var name = httpContext.Request.Params["name"];
            //var nkey = httpContext.Request.Params["nkey"];
            var nurl = httpContext.Request.Params["nurl"];

            customMenu.Name = name;
            //customMenu.Nkey = nkey;
            customMenu.Id = id;
            customMenu.Type = type;
            customMenu.Url = nurl;

            customMenuDal.Modify(customMenu);

            hashTable["isSuccess"] = true;
            hashTable["jsMethod"] = "ajax_EditMenu";
            var json = _jss.Serialize(hashTable);
            httpContext.Response.Write(json);
        }

        /// <summary>
        /// 根据id删除菜单
        /// </summary>
        /// <param name="httpContext"></param>
        public void DelMenu(HttpContext httpContext)
        {
            var customMenu = new Model.CustomMenu();
            var customMenuDal = new CustomMenuDal();
            var hashTable = new Hashtable();
            var id = httpContext.Request.Params["id"];
            customMenu.Id = id;
            customMenuDal.Del(customMenu);
            hashTable["isSuccess"] = true;
            hashTable["jsMethod"] = "ajax_DelMenu";
            var json = _jss.Serialize(hashTable);
            httpContext.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
