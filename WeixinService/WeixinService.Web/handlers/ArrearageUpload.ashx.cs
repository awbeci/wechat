﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Dal;
using WeixinService.Model;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// ArrearageUpload 的摘要说明
    /// </summary>
    public class ArrearageUpload : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();

        const string savePath = "../Excel/";
        const string saveUrl = "/Excel/";
        const string fileTypes = "xls,xlsx";
        const int maxSize = 100000000;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("ArrearageUpload.ashx,描述:映射方法名出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="context"></param>
        public void Add(HttpContext context)
        {
            Hashtable hash;
            try
            {
                HttpPostedFile file = HttpContext.Current.Request.Files["Exclefile"];
                if (file == null)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "请选择文件";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string dirPath = HttpContext.Current.Server.MapPath(savePath);
                if (!Directory.Exists(dirPath))
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传目录不存在";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string fileName = file.FileName;
                string fileExt = Path.GetExtension(fileName).ToLower();

                ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

                if (file.InputStream == null || file.InputStream.Length > maxSize)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传文件大小超过限制";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传文件扩展名是不允许的扩展名";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff") + fileExt;
                string filePath = dirPath + newFileName;
                file.SaveAs(filePath);
                string fileUrl = saveUrl + newFileName;

                hash = new Hashtable();
                hash["error"] = false;
                hash["url"] = fileUrl;

                var title = HttpContext.Current.Request["title"] ?? "";
                var month = context.Request["month"] ?? DateTime.Now.ToString("yyyy-MM");
                InfoReleaseDal infoReleaseDal = new InfoReleaseDal();
                string user = (string)context.Session["user"];
                infoReleaseDal.Add(new InfoRelease()
                    {
                        Title = title,
                        MaterialContent = fileUrl,
                        CreateDt = DateTime.Now,
                        BusinessType = context.Request["key"],
                        CreatePerson = user,
                        FlagRelease = "0",
                        Bak1 = month,
                    });

                var json = _jss.Serialize(hash);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");
                Log.Error(ex);
            }
        }

        private void Edit(HttpContext context)
        {
            Hashtable hash;
            try
            {
                string fileUrl = "";
                HttpPostedFile file = HttpContext.Current.Request.Files["Exclefile"];
                if (file != null && file.ContentLength != 0)
                {
                    string dirPath = HttpContext.Current.Server.MapPath(savePath);
                    if (!Directory.Exists(dirPath))
                    {
                        hash = new Hashtable();
                        hash["error"] = true;
                        hash["msg"] = "上传目录不存在";
                        var data = _jss.Serialize(hash);
                        context.Response.Write(data);
                        return;
                    }

                    string fileName = file.FileName;
                    string fileExt = Path.GetExtension(fileName).ToLower();

                    ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

                    if (file.InputStream == null || file.InputStream.Length > maxSize)
                    {
                        hash = new Hashtable();
                        hash["error"] = 1;
                        hash["msg"] = "上传文件大小超过限制";
                        var data = _jss.Serialize(hash);
                        context.Response.Write(data);
                        return;
                    }

                    if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                    {
                        hash = new Hashtable();
                        hash["error"] = 1;
                        hash["msg"] = "上传文件扩展名是不允许的扩展名";
                        var data = _jss.Serialize(hash);
                        context.Response.Write(data);
                        return;
                    }

                    string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff") + fileExt;
                    string filePath = dirPath + newFileName;
                    file.SaveAs(filePath);
                    fileUrl = saveUrl + newFileName;
                }

                hash = new Hashtable();
                hash["error"] = false;
                hash["url"] = fileUrl;

                var title = HttpContext.Current.Request["title"] ?? "";
                var id = HttpContext.Current.Request["id"] ?? "";
                var month = context.Request["month"] ?? DateTime.Now.ToString("yyyy-MM");
                InfoReleaseDal infoReleaseDal = new InfoReleaseDal();

                infoReleaseDal.Modify(new InfoRelease()
                {
                    Id = id,
                    Title = title,
                    MaterialContent = fileUrl,
                    Bak1 = month,
                });

                var json = _jss.Serialize(hash);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");
                Log.Error(ex);
            }
        }


        private void Del(HttpContext context)
        {
            var hash = new Hashtable();
            try
            {
                var id = HttpContext.Current.Request["id"] ?? "";
                InfoReleaseDal infoReleaseDal = new InfoReleaseDal();
                infoReleaseDal.Del(new InfoRelease()
                    {
                        Id = id,
                    });
                hash["error"] = false;
                hash["msg"] = "";
                var json = _jss.Serialize(hash);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                hash["error"] = true;
                hash["msg"] = ex.Message;
                var json = _jss.Serialize(hash);
                context.Response.Write(json);
                Log.Debug(ex);
            }
        }


        private void Release(HttpContext context)
        {
            var hash = new Hashtable();
            try
            {
                var id = HttpContext.Current.Request["id"] ?? "";
                InfoReleaseDal infoReleaseDal = new InfoReleaseDal();
                infoReleaseDal.Modify(new InfoRelease()
                {
                    Id = id,
                    FlagRelease = "1",
                });
                hash["error"] = false;
                hash["msg"] = "";
                var json = _jss.Serialize(hash);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                hash["error"] = true;
                hash["msg"] = ex.Message;
                var json = _jss.Serialize(hash);
                context.Response.Write(json);
                Log.Debug(ex);
            }
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="context"></param>
        public void UploadPicture(HttpContext context)
        {
            const string savePath = "../images/";
            const string saveUrl = "../images/";
            const string fileTypes = "jpg";
            Hashtable hash;
            try
            {
                HttpPostedFile file = HttpContext.Current.Request.Files["picture"];
                if (file == null)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "请选择文件";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string dirPath = HttpContext.Current.Server.MapPath(savePath);
                if (!Directory.Exists(dirPath))
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传目录不存在";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string fileName = file.FileName;
                string fileExt = Path.GetExtension(fileName).ToLower();

                ArrayList fileTypeList = ArrayList.Adapter(fileTypes.Split(','));

                if (file.InputStream == null || file.InputStream.Length > maxSize)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传文件大小超过限制";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                {
                    hash = new Hashtable();
                    hash["error"] = true;
                    hash["msg"] = "上传文件扩展名是不允许的扩展名";
                    var data = _jss.Serialize(hash);
                    context.Response.Write(data);
                    return;
                }

                string newFileName = "bindpicture.jpg";
                string filePath = dirPath + newFileName;
                file.SaveAs(filePath);
                string fileUrl = saveUrl + newFileName;

                hash = new Hashtable();
                hash["error"] = false;
                hash["url"] = fileUrl;

                //var title = HttpContext.Current.Request["title"] ?? "";
                //var month = context.Request["month"] ?? DateTime.Now.ToString("yyyy-MM");
                //InfoReleaseDal infoReleaseDal = new InfoReleaseDal();
                //string user = (string)context.Session["user"];
                //infoReleaseDal.Add(new InfoRelease()
                //{
                //    Title = title,
                //    MaterialContent = fileUrl,
                //    CreateDt = DateTime.Now,
                //    BusinessType = context.Request["key"],
                //    CreatePerson = user,
                //    FlagRelease = "0",
                //    Bak1 = month,
                //});

                var json = _jss.Serialize(hash);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                context.Response.Write("{\"error\":true,\"msg\":\"" + ex.Message + "\"}");
                Log.Error(ex);
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}