﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for SendMessage
    /// </summary>
    public class SendMessage : IHttpHandler
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 发送文本信息
        /// </summary>
        /// <param name="context"></param>
        public void SendTxtMessage(HttpContext context)
        {
            var hashTable = new Hashtable();
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var userInfoDal = new UserInfoDal();
            try
            {
                var data = userInfoDal.QueryAll();
                var list = from da in data.AsEnumerable()
                           select new
                               {
                                   openid = da.Field<string>("openid"),
                               };
                foreach (var li in list)
                {
                    hashTable["touser"] = li.openid;
                    hashTable["msgtype"] = "text";
                    hashTable["text"] = new
                        {
                            content = "你好张威，这是你的测试内容"
                        };
                    var json = _jss.Serialize(hashTable);
                    var token = accessToken.GetExistAccessToken();
                    var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                    Log.Debug("调试信息:" + back);
                }
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }

        /// <summary>
        /// 发送图文信息
        /// </summary>
        /// <param name="context"></param>
        public void SendNewsMessage(HttpContext context)
        {
            var hashTable = new Hashtable();
            var key = context.Request.Params["key"];
            var type = context.Request.Params["type"];
            var count = context.Request.Params["count"];
            var accessToken = new AccessToken();
            var sendDataToUser = new SendDataToWeChat();
            var userInfoDal = new UserInfoDal();
            var infoReleaseDal = new InfoReleaseDal();
            var listnews = new List<object>();
            try
            {
                //获取发布信息数据
                var dataDt = infoReleaseDal.QueryInfo(new InfoRelease()
                {
                    BusinessType = key,
                    FlagRelease = type
                }, "2");//todo:替换count
                var listDt = from ldt in dataDt.AsEnumerable()
                             select new
                             {
                                 Title = ldt.Field<string>("Title"),
                                 MessageDescription = ldt.Field<string>("MessageDescription")
                             };
                foreach (var ldt in listDt)
                {
                    listnews.Add(new
                    {
                        title = ldt.Title,
                        description = ldt.MessageDescription,
                        url = "http://218.22.27.236/views/messagelist/messagelist.htm?key=" + key + "&name=停电信息列表",
                        picurl = "http://218.22.27.236/tl/UploadImages/topleft.jpg"
                    });
                }

                //获取用户数据
                var data = userInfoDal.QueryAll();
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               openid = da.Field<string>("openid"),
                           };
                foreach (var li in list)
                {
                    hashTable["touser"] = li.openid;
                    hashTable["msgtype"] = "news";
                    hashTable["news"] = new
                    {
                        articles = listnews
                    };
                    var json = _jss.Serialize(hashTable);
                    var token = accessToken.GetExistAccessToken();
                    var back = sendDataToUser.GetPage("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + token, json);
                    Log.Debug("调试信息:" + back);
                }
            }
            catch (Exception ex)
            {
                Log.Debug("错误信息:" + ex.Message);
            }
        }

        public void SendTest(HttpContext context)
        {
            var sendMessage = new Bll.SendMessage();
            sendMessage.SendNewsMessage("id",new Articles()
                {
                    description = "description",
                    picurl = "picurl",
                    title = "title",
                    url = "url"
                });
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}