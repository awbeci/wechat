﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Dal;
using WeixinService.Model;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for PowerInfo
    /// </summary>
    public class PowerInfo : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var session = context.Session["user"];
            if (session == null)
            {
                context.Response.Write("location.href='../views/main/login.html';");
                return;
            }
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("PowerInfo.ashx,描述:映射方法名出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 查询停电信息
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryPowerInfo(HttpContext httpContext)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var businesstype = httpContext.Request.Params["businesstype"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(httpContext.Request["page"] ?? "1");
            var rows = int.Parse(httpContext.Request["rows"] ?? "10");
            try
            {
                var data = infoReleaseDal.QueryByPage(new InfoRelease()
                    {
                        BusinessType = businesstype
                    }, page, rows, ref total);
                var list = from da in data.AsEnumerable()
                           select new
                           {
                               Id = da.Field<string>("Id"),
                               Title = da.Field<string>("Title"),
                               MaterialContent = da.Field<string>("MaterialContent"),
                               MaterialType = da.Field<string>("MaterialType"),
                               FlagRelease = da.Field<string>("FlagRelease"),
                               MessageDescription = da.Field<string>("MessageDescription"),
                               CreateDt = da.Field<DateTime?>("CreateDt"),
                               CreatePerson = da.Field<string>("CreatePerson"),
                               Bak1 = da.Field<string>("Bak1"),
                               Bak2 = da.Field<string>("Bak2"),
                               Bak3 = da.Field<string>("Bak3"),
                               Bak4 = da.Field<string>("Bak4"),
                               SendGroup = da.Field<string>("SendGroup"),
                               SendPerson = da.Field<string>("SendPerson")
                           };
                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryPowerInfo,描述:查询停电信息方法出错,错误原因:" + e.Message);
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }

        }

        /// <summary>
        /// 保存停电信息
        /// </summary>
        /// <param name="context"></param>
        public void SavePowerInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var type = context.Request.Params["type"];
            var key = context.Request.Params["key"];
            var messageDescription = context.Request.Params["messageDescription"];
            var title = context.Request.Params["title"];
            var content = context.Request.Params["contents"];

            var infoRelease = new InfoRelease();
            infoRelease.Id = Guid.NewGuid().ToString();
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;
            infoRelease.CreatePerson = "";
            infoRelease.FlagRelease = "0";
            infoRelease.MaterialContent = content;
            infoRelease.MaterialType = type;
            infoRelease.MessageDescription = messageDescription;
            infoRelease.SendGroup = "";
            infoRelease.SendPerson = "";
            infoRelease.Title = title;

            try
            {
                infoReleaseDal.Add(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 保存停电信息
        /// </summary>
        /// <param name="context"></param>
        public void EditPowerInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var id = context.Request.Params["id"];
            var key = context.Request.Params["key"];
            var type = context.Request.Params["type"];
            var messageDescription = context.Request.Params["messageDescription"];
            var title = context.Request.Params["title"];
            var content = context.Request.Params["contents"];

            var infoRelease = new InfoRelease();
            infoRelease.Id = id;
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;
            infoRelease.CreatePerson = "";
            infoRelease.FlagRelease = "0";
            infoRelease.MaterialContent = content;
            infoRelease.MaterialType = type;
            infoRelease.MessageDescription = messageDescription;
            infoRelease.SendGroup = "";
            infoRelease.SendPerson = "";
            infoRelease.Title = title;

            try
            {
                infoReleaseDal.Modify(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 删除停电信息
        /// </summary>
        /// <param name="context"></param>
        public void DelPowerInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new InfoReleaseDal();
            var id = context.Request.Params["id"];

            try
            {
                infoReleaseDal.Del(new InfoRelease() { Id = id });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_DelPowerInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}