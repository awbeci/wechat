﻿using System.Web.Services;
using WeixinService.Bll;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// WebService1 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : WebService
    {

        [WebMethod]
        public string GetToken()
        {
            var accessToken = new AccessToken();
            var token = accessToken.GetExistAccessToken();
            return token;
        }

        [WebMethod]
        public string GetTokenToUpload()
        {
            try
            {
                var accessToken = new AccessToken();
                var token = accessToken.GetExistAccessToken();
                UsersInfo userInfo = new UsersInfo();
                userInfo.UploadGroup(token);
                return "成功";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
