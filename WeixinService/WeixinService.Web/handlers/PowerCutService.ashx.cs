﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using WeixinService.Bll;
using WeixinService.Dal;
using WeixinService.Model;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// PowerCutService 的摘要说明
    /// </summary>
    public class PowerCutService : IHttpHandler, IRequiresSessionState
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("PowerCutService.ashx,描述:映射方法名出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="httpContext"></param>
        public void QueryPublishInfo(HttpContext httpContext)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var businesstype = httpContext.Request.Params["businesstype"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(httpContext.Request["page"] ?? "1");
            var rows = int.Parse(httpContext.Request["rows"] ?? "10");
            try
            {
                var data = infoReleaseDal.QueryByPage(new PowerCut()
                {
                    BusinessType = businesstype
                }, page, rows, ref total);
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryPowerInfo,描述:查询停电信息方法出错,错误原因:" + e.Message);
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                httpContext.Response.Write(json);
            }
        }

        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="context"></param>
        public void SavePublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var key = context.Request.Params["key"];
            var title = context.Request.Params["title"];
            var jd = context.Request.Params["jd"];
            var wd = context.Request.Params["wd"];
            var sm = context.Request.Params["sm"];
            var state = context.Request["State"] ?? "";
            var tmp = context.Request["PowerCutTime"] ?? "";
            DateTime PowerCutTime = DateTime.Now;
            if (!string.IsNullOrEmpty(tmp))
            {
                PowerCutTime = DateTime.Parse(tmp);
            }
            var timeArea = context.Request["TimeArea"] ?? "";
            var area = context.Request["Area"] ?? "";
            var device = context.Request["Device"] ?? "";

            var infoRelease = new PowerCut();
            infoRelease.Id = Guid.NewGuid().ToString();
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;
            infoRelease.CreatePerson = context.Session["user"].ToString();
            infoRelease.State = "1";
            infoRelease.SendGroup = "";
            infoRelease.SendPerson = "";
            infoRelease.Title = title;
            infoRelease.PowerCutTime = PowerCutTime;
            infoRelease.TimeArea = timeArea;
            infoRelease.Area = area;
            infoRelease.Device = device;
            infoRelease.Jd = jd;
            infoRelease.Wd = wd;
            infoRelease.Description = sm;

            try
            {
                infoReleaseDal.Add(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 保存信息
        /// </summary>
        /// <param name="context"></param>
        public void EditPublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var id = context.Request.Params["id"];
            var key = context.Request.Params["key"];
            var title = context.Request.Params["title"];
            var jd = context.Request.Params["jd"];
            var wd = context.Request.Params["wd"];
            var sm = context.Request.Params["sm"];
            var state = context.Request["State"] ?? "";
            var tmp = context.Request["PowerCutTime"] ?? "";
            DateTime PowerCutTime = DateTime.Now;
            if (!string.IsNullOrEmpty(tmp))
            {
                PowerCutTime = DateTime.Parse(tmp);
            }
            var timeArea = context.Request["TimeArea"] ?? "";
            var area = context.Request["Area"] ?? "";
            var device = context.Request["Device"] ?? "";

            var infoRelease = new PowerCut();
            infoRelease.Id = id;
            infoRelease.BusinessType = key;
            infoRelease.CreateDt = DateTime.Now;
            infoRelease.CreatePerson = "";
            infoRelease.State = state;
            infoRelease.Title = title;
            infoRelease.PowerCutTime = PowerCutTime;
            infoRelease.TimeArea = timeArea;
            infoRelease.Area = area;
            infoRelease.Device = device;
            infoRelease.Jd = jd;
            infoRelease.Wd = wd;
            infoRelease.Description = sm;

            try
            {
                infoReleaseDal.Modify(infoRelease);
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="context"></param>
        public void DelPublishInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var infoReleaseDetails = new InfoReleaseDetailsDal();
            var id = context.Request.Params["id"];

            try
            {
                infoReleaseDal.Del(new PowerCut() { Id = id });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_DelPowerInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:SavePowerInfo,描述:保存停电信息方法出错,错误原因:" + e.Message);
            }
        }

        /// <summary>
        /// 发布信息到人(实际上只是修改了下发布状态，仅此而已)
        /// </summary>
        /// <param name="context"></param>
        public void PublishInfoToPerson(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var userInfoDal = new UserInfoDal();
            var id = context.Request.Params["id"];
            try
            {
                infoReleaseDal.Modify(new PowerCut()
                {
                    Id = id,
                    FlagRelease = "2"
                });
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_PublishInfoToPerson";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:PublishInfoToPerson,,错误原因:" + e.Message);
            }
        }

        private void Query(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var businesstype = context.Request.Params["key"];
            var tmp = context.Request["kssj"] ?? "";
            var dateBegin = DateTime.Now.Date;
            var dateEnd = DateTime.MinValue;
            var dateTmp = DateTime.MinValue;
            if (!string.IsNullOrEmpty(tmp))
            {
                dateTmp = DateTime.Parse(tmp);
                dateBegin = dateTmp;
            }
            tmp = context.Request["jssj"] ?? "";
            if (!string.IsNullOrEmpty(tmp))
            {
                dateTmp = DateTime.Parse(tmp);
                dateEnd = dateTmp;
            }

            try
            {
                var data = infoReleaseDal.Query(new PowerCut()
                {
                    BusinessType = businesstype,
                    DateBegin = dateBegin,
                    DateEnd = dateEnd,
                    State = "1",
                    FlagRelease = "'2','3'",
                });
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["data"] = list.ToList();
                hashtable["total"] = list.Count;
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryMessageList";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Debug(e);
            }
        }

        private void Query1(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var businesstype = context.Request.Params["key"];

            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");

            try
            {
                var data = infoReleaseDal.QueryByPage(new PowerCut()
                {
                    BusinessType = businesstype,
                }, page, rows, ref total);
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["rows"] = list.ToList();
                hashtable["total"] = total;
                hashtable["isSuccess"] = true;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Debug(e);
            }
        }

        private void GetInfo(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var id = context.Request["id"] ?? "";

            try
            {
                var data = infoReleaseDal.Query(new PowerCut()
                {
                    Id = id,
                });
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["data"] = list.ToList();
                hashtable["total"] = list.Count;
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryDetailInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Debug(e);
            }
        }

        /// <summary>
        /// 查询故障停电和计划停电
        /// </summary>
        /// <param name="context"></param>
        public void QueryMap(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var businesstype = context.Request.Params["key"];

            try
            {
                DateTime dateBegin;
                DateTime dateEnd;
                switch (businesstype)
                {
                    //计划停电key
                    case "011_youo_tdtz":
                        dateBegin = DateTime.Now.Date;
                        dateEnd = DateTime.Now.AddDays(7).Date;
                        break;
                    //故障停电key
                    case "011_youo_tdtz2":
                        dateBegin = DateTime.Now.AddDays(-3).Date;
                        dateEnd = DateTime.Now.Date;
                        break;
                    default: return;
                }
                var data = infoReleaseDal.Query(new PowerCut()
                {
                    BusinessType = businesstype,
                    DateBegin = dateBegin,
                    DateEnd = dateEnd,
                    State = "1",
                    FlagRelease = "'2','3'",
                });
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["data"] = list.ToList();
                hashtable["key"] = businesstype;
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryMap";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Debug(e);
            }
        }

        private void GetInfo2(HttpContext context)
        {
            var hashtable = new Hashtable();
            var infoReleaseDal = new PowerCutDal();
            var id = context.Request["id"] ?? "";
            var key = context.Request["key"] ?? "";

            try
            {
                var data = infoReleaseDal.QueryDataById(id, key);
                var list = ConvertHelper<PowerCut>.ConvertToList(data);

                hashtable["data"] = list.ToList();
                hashtable["total"] = list.Count;
                hashtable["isSuccess"] = true;
                hashtable["jsMethod"] = "ajax_QueryDetailInfo";
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                hashtable["isSuccess"] = false;
                var json = _jss.Serialize(hashtable);
                context.Response.Write(json);
                Log.Debug(e);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}