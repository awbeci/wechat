﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Xml;
using System.IO;
using System.Text;
using NPOI;
using NPOI.HPSF;
using NPOI.HSSF;
using NPOI.HSSF.UserModel;
using NPOI.POIFS;
using NPOI.Util;
using NPOI.DDF;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Newtonsoft.Json;
using WeixinService.Model;
using WeixinService.Model.common;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.handlers
{
    /// <summary>
    /// Summary description for UserRegService
    /// </summary>
    public class UserRegService : IHttpHandler
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly JavaScriptSerializer _jss = new JavaScriptSerializer();
        Hashtable _hashtable = new Hashtable();
        UserRegDal _userRegDal = new UserRegDal();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            var action = context.Request.Params["action"];
            Type curType = GetType();
            try
            {
                MethodInfo method = curType.GetMethod(action, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                method.Invoke(this, new object[] { HttpContext.Current });
            }
            catch (Exception e)
            {
                Log.Debug("出错原因：" + e.Message);
            }
        }

        /// <summary>
        /// 用户绑定查询
        /// </summary>
        /// <param name="context"></param>
        public void QueryReg(HttpContext context)
        {
            var kssj = context.Request.Params["kssj"];
            var jssj = context.Request.Params["jssj"];
            var hh = context.Request.Params["hh"];

            //获取分页数据
            var total = 0;
            var page = int.Parse(context.Request["page"] ?? "1");
            var rows = int.Parse(context.Request["rows"] ?? "10");
            try
            {
                var dataTable = _userRegDal.QueryUserReg(kssj, jssj, hh, page, rows, ref total);
                var list = from da in dataTable.AsEnumerable()
                           select new
                               {
                                   RNumber = da.Field<string>("RNumber"),
                                   PhoneNumber = da.Field<string>("PhoneNumber"),
                                   CreateDt = da.Field<DateTime?>("CreateDt")
                               };
                _hashtable["rows"] = list.ToList();
                _hashtable["total"] = total;
                var json = _jss.Serialize(_hashtable);
                context.Response.Write(json);
            }
            catch (Exception e)
            {
                Log.Debug("方法名:QueryReg,错误原因:" + e.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}