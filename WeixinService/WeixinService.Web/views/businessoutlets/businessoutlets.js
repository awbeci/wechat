﻿$(function () {
    name = queryUrlName("name");
    name = decodeURI(name);
    initTable();
    initDlg();
});

var actiontype; //全局变量
var key; //全局变量
var name; //全局变量
var dgObj = {
    url: "../../handlers/BusinessOutletsService.ashx",
    queryParams: { action: "QueryBusinessOutlets" },
    fit: true,
    title: '信息列表',
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: [{
        text: '新建',
        iconCls: 'icon-add',
        handler: function () {
            addDlg('add');
        }
    }, '-', {
        text: '编辑',
        iconCls: 'icon-edit',
        handler: function () {
            addDlg('edit');
        }
    }, '-', {
        text: '删除',
        iconCls: 'icon-remove',
        handler: function () {
            del();
        }
    }
    ],
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "Id", hidden: true },
        { field: "AreaName", title: "区域名称", width: 200, align: 'center' },
        { field: "Address", title: "地址", width: 200, align: 'center' },
        { field: "Name", title: "名称", width: 120, align: 'center' },
        { field: "Longitude", title: "经度", width: 120, align: 'center' },
        { field: "Latitude", title: "纬度", width: 120, align: 'center' }
    ]]
};

function initTable() {
    $("#dg").datagrid(dgObj);
}

//初始化dlg
function initDlg() {
    $('#dlg-map').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });
}
var map, point, marker;
var xx1, yy1;   //东北点
var xx2, yy2;   //西南点
function addDlg(data) {
    $('#dlg-map').dialog("open");
    actiontype = data;
    $('#fm-map').form('reset');
    if (data == "edit") {
        var selected = $("#dg").datagrid("getSelected");
        if (selected == null) {
            alert("请选择一条数据进行编辑");
            return false;
        }
        $("#qy").val(selected.AreaName);
        $("#dz").val(selected.Address);
        $("#name").val(selected.Name);
        $("#jd").val(selected.Longitude);
        $("#wd").val(selected.Latitude);
        // 百度地图API功能
        map = new BMap.Map("allmap");            // 创建Map实例
        map.enableScrollWheelZoom();
        point = new BMap.Point(selected.Longitude, selected.Latitude);
        map.centerAndZoom(point, 17);
        addMarker(point, map, selected.Name, selected.Address);
    }
    if (data == "add") {
        addBm();
    }
}

/***  标记该坐标  方法
*/
function addMarker(point, map, name, address) {
    marker = new BMap.Marker(point);
    map.addOverlay(marker);
    var sContent =
	"<h4 style='margin:0 0 5px 0;padding:0.2em 0'>" + name + "</h4>" +
	"<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>" + address + "</p>" +
	"</div>";
    var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
    marker.addEventListener("click", function () {
        this.openInfoWindow(infoWindow);
    });
    var label = new BMap.Label(sContent, { offset: new BMap.Size(20, -10) });
    marker.setLabel(label);
    map.addEventListener("click", function (e) {
        deletePoint();             //删除所有已标记点
        $("#jd").val(e.point.lng);
        $("#wd").val(e.point.lat);
        point = new BMap.Point(e.point.lng, e.point.lat);
        marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);
    });

    //当点击页面   删除所有点
    function deletePoint() {
        var allOverlay = map.getOverlays();
        for (var i = 0; i < allOverlay.length; i++) {
            map.removeOverlay(allOverlay[i]);
        }
    }
}

function addBm() {
    // 百度地图API功能
    map = new BMap.Map("allmap");
    point = new BMap.Point(117.071675, 30.553076);
    map.centerAndZoom(point, 17);
    marker = new BMap.Marker(point);  // 创建标注
    map.addOverlay(marker);              // 将标注添加到地图中
    marker.addEventListener("click", getAttr);
    map.enableScrollWheelZoom(true);
    map.addEventListener("click", function (e) {
        deletePoint();             //删除所有已标记点
        $("#jd").val(e.point.lng);
        $("#wd").val(e.point.lat);
        point = new BMap.Point(e.point.lng, e.point.lat);
        marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);
    });

    //当点击页面   删除所有点
    function deletePoint() {
        var allOverlay = map.getOverlays();
        for (var i = 0; i < allOverlay.length; i++) {
            map.removeOverlay(allOverlay[i]);
        }
    }

    function getAttr() {
        var p = marker.getPosition();       //获取marker的位置
        $("#jd").val(p.lng);
        $("#wd").val(p.lat);
    }
}

function del() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    if (confirm("确认删除此条数据吗？")) {
        zwobj.url = "../../handlers/BusinessOutletsService.ashx?action=DelBusinessOutlets";
        zwobj.data = { id: selected.Id };
        ajaxData();
    }
}

//保存方法
function save() {
    var myurl;
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        myurl = '../../handlers/BusinessOutletsService.ashx?action=AddBusinessOutlets';
    } else if (actiontype == "edit") {
        myurl = '../../handlers/BusinessOutletsService.ashx?action=EditBusinessOutlets&id=' + selected.Id;
    }

    $('#fm-map').form('submit', {
        url: myurl,
        onSubmit: function (data) {
            if ($("#name").val().length <= 0) {
                alert("请输入名称");
                return false;
            }
            if ($("#jd").val().length <= 0) {
                alert("请输入经度");
                return false;
            }
            if ($("#wd").val().length <= 0) {
                alert("请输入纬度");
                return false;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                //$('#dlg-map').dialog('close');
                //$("#dg").datagrid("reload");
            }
            $('#dlg-map').dialog('close');
            $("#dg").datagrid("reload");
        }
    });
}

function ajax_DelBusinessOutlets(obj) {
    $("#dg").datagrid("reload");
}
