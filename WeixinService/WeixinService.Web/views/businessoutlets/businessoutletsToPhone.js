﻿var map, point, marker;
$(function () {
    position();
    $("#myposition").click(function () {
        myposition();
    });
});

function myposition() {
    var point1;
    try {
        if (typeof window.Android != 'undefined' && window.Android != null) {
            var aa = window.Android.getLocationToStr();
            if (typeof aa != 'undefined' && aa != null) {
                var ss = aa.split(',');
                if (typeof ss != 'undefined' && ss != null) {
                    point1 = new BMap.Point(ss[0], ss[1]);
                    map.centerAndZoom(point1, 17);
                    addMarker(point1, '我的位置', '');
                } else {
                    elseMyposition();
                }
            } else {
                elseMyposition();
            }
        } else {
            elseMyposition();
        }
    } catch (e) {
        console.log(e.message);
    }
}

function elseMyposition() {
    var point1;
    point1 = new BMap.Point('117.07216', '30.553495');
    map.centerAndZoom(point1, 17);
    //addMarker(point1, '默认位置', '');
}

function initMap() {
    try {
        if (typeof window.Android != 'undefined' && window.Android != null) {
            var aa = window.Android.getLocationToStr();
            var ss = aa.split(',');
            zwobj.url = "../../handlers/BusinessOutletsService.ashx?action=QueryBusinessOutByJdAndWd";
            zwobj.data = { jd: ss[0], wd: ss[1] };
            ajaxData();
        } else {
            zwobj.url = "../../handlers/BusinessOutletsService.ashx?action=QueryBusinessOutByJdAndWd";
            zwobj.data = { jd: '117.07216', wd: '30.553495' };
            ajaxData();
        }
    } catch (e) {
        console.log(e.message);
    }

}

function ajax_QueryBusinessOutByJdAndWd(obj) {
    var arr = [];
    for (var j = 0, len2 = obj.data.length; j < len2; j++) {
        arr.push({ jd: obj.data[j].longitude, wd: obj.data[j].latitude, name: obj.data[j].name, dz: obj.data[j].address });
    }
    // 标记该坐标
    for (var i = 0, len = arr.length; i < len; i++) {
        var point1 = new BMap.Point(arr[i].jd, arr[i].wd);
        addMarker(point1, arr[i].name, arr[i].dz);
    }
}

function position() {
    // 百度地图API功能
    try {
        map = new BMap.Map("allmap");            // 创建Map实例
        map.enableScrollWheelZoom();
        myposition();
        initMap();
    } catch (e) {
        console.log(e.message);
    }
}

/**
*  标记该坐标  方法
*/
function addMarker(point, name, address) {
    var marker;
    try {
        if (name == "我的位置") {
            var myIcon = new BMap.Icon("../../images/location2.png", new BMap.Size(28, 28));
            marker = new BMap.Marker(point, { icon: myIcon });
        } else {
            marker = new BMap.Marker(point);
        }

        map.addOverlay(marker);
        var sContent =
	"<h4 style='margin:0 0 5px 0;padding:0.2em 0'>" + name + "</h4>" +
	"<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>" + address + "</p>" +
	"</div>";
        var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
        marker.addEventListener("click", function () {
            this.openInfoWindow(infoWindow);
        });
        var label = new BMap.Label(sContent, { offset: new BMap.Size(20, -10) });
        marker.setLabel(label);
    } catch (e) {
        console.log(e.message);
    }
}

