﻿$(function () {
    queryMenu();
    $.fn.zTree.init($("#treeDemo"), setting, zNodes);

    $("#d_main").fadeIn(1000);
    getfz("cx");
    var fzcx = $("#fzcx").val();
    var nickname = $("#namecx").val();
    var truename = $("#cxtruename").val();
    var begintime = $("#begintime").val();
    var endtime = $("#endtime").val();
    $("#t_user").datagrid({
        url: "../../handlers/UsersInfo.ashx",
        queryParams: { action: "QueryUserInfo", fz: fzcx, nickname: nickname, truename: truename, begintime: begintime, endtime: endtime },
        fit: true,
        pagination: true,
        nowrap: false,
        fitColumns: true,
        rownumbers: true,
        //singleSelect: true,
        striped: true,
        border: false,
        pageList: [10, 20, 30, 50],
        pageSize: 20,
        columns: [[
                    { field: 'ck', checkbox: true, width: 50 },
                    { field: "openid", title: "用户号", width: 150, align: 'center' },
                    { field: "nickname", title: "用户昵称", width: 100, align: 'center' },
                    { field: "truename", title: "实名", width: 100, align: 'center' },
                    { field: "sex", title: "性别", width: 50, align: 'center',
                        formatter: function (value) {
                            if (value == "1")
                                return "男";
                            else if (value == "2")
                                return "女";
                            else
                                return "未知";
                        }
                    },
                    { field: "city", title: "城市", width: 80, align: 'center' },
                    { field: "country", title: "国家", width: 80, align: 'center' },
                    { field: "province", title: "省份", width: 50, align: 'center' },
                    { field: "language", title: "语言", width: 50, align: 'center' },
                    { field: "subscribe_time", title: "用户关注时间", width: 120, align: 'center',
                        formatter: function (value) {
                            var dt = eval("new " + value.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
                            if (dt == "1-01-01")
                                return "-";
                            else
                                return dt;
                        }
                    },
                    { field: "groupID", title: "分组id", width: 50, align: 'center', hidden: true },
                    { field: "groupName", title: "分组名称", width: 80, align: 'center',
                        formatter: function (value) {
                            if (value == null)
                                return "未分组";
                            else
                                return value;
                        }
                    },
             { field: "zw", title: "职位", width: 100, align: 'center',
                 formatter: function (val, obj) {
                     if (val == 1) {
                         return "正职";
                     }
                     if (val == 2) {
                         return "副职";
                     }
                 }
             },
                    { field: "RNumber", title: "关联钞资产号", width: 100, align: 'center', hidden: true },
                    { field: "msgFrom", title: "用户来源", width: 100, align: 'center', hidden: true }
                ]],
        onLoadSuccess: function (data) {
            $($('#t_user').datagrid("getPanel")).find('a.easyui-linkbutton').linkbutton();
        },
        view: detailview,
        detailFormatter: function (index, row) {
            return "<div style='padding:2px;'><table id='ddv-" + index + "'></table></div>";
        },
        onExpandRow: function (index, row) {
            $("#ddv-" + index).datagrid({
                url: "../../handlers/UsersInfo.ashx",
                queryParams: {
                    action: "GetUserRegList",
                    wxid: row.openid,
                    r: Math.random()
                },
                fitColums: true,
                singleSelect: true,
                rownumbers: true,
                loadMsg: "加载中...",
                height: "auto",
                columns: [[
                    { field: "bak1", title: "企业名称", width: 150, align: 'center' },
                    { field: "bak2", title: "联系人", width: 150, align: 'center' },
                    { field: "bak3", title: "固定电话", width: 150, align: 'center' },
                    { field: "RNumber", title: "用户号", width: 150, align: 'center', hidden: true },
                    { field: "PhoneNumber", title: "手机号", width: 200, align: 'center' },
                    { field: "bak4", title: "职务", width: 150, align: 'center' }
                ]],
                onLoadSuccess: function () {
                    setTimeout(function () {
                        $("#t_user").datagrid("fixDetailRowHeight", index);
                    });
                },
                onResize: function () {
                    $("#t_user").datagrid("fixDetailRowHeight", index);
                }
            });
        }
    });
});

//查询
function search() {
    var fzcx = $("#fzcx").val();
    var nickname = $("#namecx").val();
    var truename = $("#cxtruename").val();
    var begintime = $("#begintime").val();
    var endtime = $("#endtime").val();
    if (begintime == "" && endtime != "") {
        $.messager.alert('提示', '请选择完整的关注时间段！');
        return;
    }
    if (begintime != "" && endtime == "") {
        $.messager.alert('提示', '请选择完整的关注时间段！');
        return;
    }
    $("#t_user").datagrid('load', {
        action: "QueryUserInfo",
        fz: fzcx, nickname: nickname, truename: truename, begintime: begintime, endtime: endtime
    });
}

//打开分组框
function openfz() {
    var rows = $("#t_user").datagrid("getSelections");
    if (rows.length < 1) {
        $.messager.alert('提示', '请选中一行或多行数据！');
        return;
    }
    $("#dlgexpall").css("display", "block");
    getfz("tj");
    $("#dlgexpall").dialog("open").dialog("setTitle", "用户分组");
}

//保存分组
function savefz() {
    //采集用户所要分到的组别信息
    var groupidrows = $("#TB_FZ").datagrid("getSelections");
    if (groupidrows.length < 1) {
        $.messager.alert('提示', '请选中一行或多行数据！');
        return;
    }
    var groupids = "";
    for (var i = 0; i < groupidrows.length; i++) {
        var row = groupidrows[i];
        var groupid = row.id;

        if (groupid == 0 && groupidrows.length > 1) {
            $.messager.alert('提示', '未分组不能和其他组同时被选择！');
            return;
        }
        groupids += groupid + ":";
    }
    groupids = groupids.substr(0, groupids.length - 1);

    //采集需要分组的微信用户号
    var openidrows = $("#t_user").datagrid("getSelections");
    var openids = "";
    for (var i = 0; i < openidrows.length; i++) {
        var row = openidrows[i];
        var openid = row.openid;
        openids += openid + ":";
    }
    openids = openids.substr(0, openids.length - 1);

    $.ajax({
        url: "../../handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "SaveFz", openids: openids, groupids: groupids },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $.messager.alert('提示', result.msg);
            } else {
                $.messager.alert("提示", result.msg);
            }
            $('#dlgexpall').dialog('close');
            $("#t_user").datagrid("reload");
        }
    });
}


//打开实名编辑框
function openTrueName() {
    var rows = $("#t_user").datagrid("getSelections");
    if (rows.length != 1) {
        $.messager.alert('提示', '请选中一行数据！');
        return;
    } else {
        var row = $("#t_user").datagrid("getSelected");
        $("#dlgtruename").css("display", "block");
        $("#dlgtruename").dialog("open").dialog("setTitle", "实名编辑");
        $("#truename").val(row.truename);
        switch (row.zw) {
            case 1:
                bindSelectOption("zw", "正职");
                break;
            case 2:
                bindSelectOption("zw", "副职");
                break;
            default:
                bindSelectOption("zw", "无");
                break;
        }
    }
}


//保存实名
function saveTrueName() {
    var row = $("#t_user").datagrid("getSelected");
    var truename = $("#truename").val();
    var zw = $("#zw").val();
    var openid = row.openid;
    $.ajax({
        url: "../../handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "SaveTrueName", truename: truename, openid: openid, zw: zw },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $.messager.alert('提示', result.msg);
                $('#dlgtruename').dialog('close');
                $("#t_user").datagrid("reload");
            } else {
                $.messager.alert("提示", result.msg);
            }

        }
    });
}


//加载分组项
function getfz(id) {
    $.ajax({
        url: "../../handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "QueryGroup", fz: 0 },
        dataType: "html",
        success: function (html) {
            var val = "";
            eval("val=" + html);
            var length = val.rows.length;
            if (id == "cx") {
                $("#fzcx").html("<option value='all'>全部</option>");
                for (var i = 0; i < length; i++) {
                    if (val.rows[i].id != "-1")
                        $("#fzcx").append("<option value=" + val.rows[i].id + ">" + val.rows[i].name + "</option>");
                }
            }
            else if (id == "tj") {
                getFZTable();
            }
        }
    });
}

//获取所有分组信息
function getFZTable() {
    $("#TB_FZ").datagrid({
        url: "../../handlers/UsersInfo.ashx",
        queryParams: { action: "QueryGroup", fz: -1 },
        fit: true,
        pagination: true,
        nowrap: false,
        fitColumns: true,
        rownumbers: true,
        //singleSelect: true,
        striped: true,
        border: false,
        pageList: [10, 20, 30, 50],
        pageSize: 20,
        columns: [[
                    { field: 'ck', checkbox: true, width: 50 },
                    { field: "id", title: "分组id", width: 150, align: 'center', hidden: true },
                    { field: "name", title: "分组名称", width: 150, align: 'center' }
                ]]
    });
}



//日期时间格式化方法，可以格式化年、月、日、时、分、秒、周
Date.prototype.Format = function (formatStr) {
    var week = ['日', '一', '二', '三', '四', '五', '六'];
    return formatStr.replace(/yyyy|YYYY/, this.getFullYear())
 	             .replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100))
 	             .replace(/MM/, (this.getMonth() + 1) > 9 ? (this.getMonth() + 1).toString() : '0' + (this.getMonth() + 1)).replace(/M/g, (this.getMonth() + 1))
 	             .replace(/w|W/g, week[this.getDay()])
 	             .replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate()).replace(/d|D/g, this.getDate())
 	             .replace(/HH|hh/g, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours()).replace(/H|h/g, this.getHours())
 	             .replace(/mm/g, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes()).replace(/m/g, this.getMinutes())
 	             .replace(/ss/g, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds()).replace(/S|s/g, this.getSeconds());
};



//===================================================ZTREE树========================================================================================

var setting = {
    view: {
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        selectedMulti: false
    },
    edit: {
        enable: true,
        editNameSelectAll: true,
        showRemoveBtn: showRemoveBtn,
        showRenameBtn: showRenameBtn
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeDrag: beforeDrag,
        beforeEditName: beforeEditName,
        beforeRemove: beforeRemove,
        beforeRename: beforeRename,
        onRemove: onRemove,
        onRename: onRename,
        onClick: onClick
    }
};

var zNodes = [];
var log, className = "dark";
function beforeDrag(treeId, treeNodes) {
    return false;
}

//===删改按钮触发事件
function beforeEditName(treeId, treeNode) {
    className = (className === "dark" ? "" : "dark");
    showLog("[ " + getTime() + " beforeEditName ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.selectNode(treeNode);
    openEditFz(treeNode);
    //return confirm("进入节点 -- " + treeNode.name + " 的编辑状态吗？");
}
function beforeRemove(treeId, treeNode) {
    className = (className === "dark" ? "" : "dark");
    showLog("[ " + getTime() + " beforeRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.selectNode(treeNode);
    return confirm("确认删除 节点 -- " + treeNode.name + " 吗？");
}
function onRemove(e, treeId, treeNode) {
    showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    DelFz(treeNode.id)
}
function beforeRename(treeId, treeNode, newName, isCancel) {
    className = (className === "dark" ? "" : "dark");
    showLog((isCancel ? "<span style='color:red'>" : "") + "[ " + getTime() + " beforeRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>" : ""));
    if (newName.length == 0) {
        alert("节点名称不能为空.");
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        setTimeout(function () { zTree.editName(treeNode) }, 10);
        return false;
    }
    return true;
}
function onRename(e, treeId, treeNode, isCancel) {
    showLog((isCancel ? "<span style='color:red'>" : "") + "[ " + getTime() + " onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>" : ""));
    //ModifyFz(treeNode.id, treeNode.name);

    if (flag == "add") {
        AddFz(treeNode.id, treeNode.pId, treeNode.name);
    }
    else if (flag == "edit") {
        showLog((isCancel ? "<span style='color:red'>" : "") + "[ " + getTime() + " onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>" : ""));
        ModifyFz(treeNode.id, treeNode.name);
    }
}

//===控制删改按钮显示
function showRemoveBtn(treeId, treeNode) {
    if (treeNode.level == 0) {
        return false;
    }
    if (treeNode.level == 1) {
        return false;
    }
    if (treeNode.level == 2) {
        return true;
    }
}
function showRenameBtn(treeId, treeNode) {
    if (treeNode.id == 0) {
        return false;
    }
    if (treeNode.id == "0_0") {
        return false;
    }
    if (treeNode.level == 1) {
        return true;
    }
    if (treeNode.level == 2) {
        return true;
    }

}

//===辅助方法
function showLog(str) {
    if (!log) log = $("#log");
    log.append("<li class='" + className + "'>" + str + "</li>");
    if (log.children("li").length > 8) {
        log.get(0).removeChild(log.children("li")[0]);
    }
}
function getTime() {
    var now = new Date(),
			h = now.getHours(),
			m = now.getMinutes(),
			s = now.getSeconds(),
			ms = now.getMilliseconds();
    return (h + ":" + m + ":" + s + " " + ms);
}

var newCount = 1;
var flag = "";
//===增触发事件和增按钮是否显示
function addHoverDom(treeId, treeNode) {
    if (treeNode.level == 2) {
        return false;
    }
    if (treeNode.id == 0) {
        return false;
    }
    if (treeNode.id == "0_0") {
        return false;
    }
    if (treeNode.pid == -1) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_" + treeNode.tId);
        if (btn) btn.bind("click", function () {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            var id = newGuid();
            zTree.addNodes(treeNode, { id: id, pId: treeNode.id, name: "new node", tid: "xd_" + id, icon: "../../images/page.png" });
            openAddFz(treeNode);
            return false;
        });
    }
    else if (treeNode.pid != -1) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_" + treeNode.tId);
        if (btn) btn.bind("click", function () {
            //        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            //        var id = newGuid();
            //        zTree.addNodes(treeNode, { id: id, pId: treeNode.id, name: "new node", tid: "xd_" + id, icon: "../../images/page.png" });
            //        var node = zTree.getNodeByParam("id", id, treeNode);
            //        zTree.editName(node);
            //        flag = "add";
            openAddFz(treeNode);
            return false;
        });
    }




};
function removeHoverDom(treeId, treeNode) {
    $("#addBtn_" + treeNode.tId).unbind().remove();
};
function selectAll() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.setting.edit.editNameSelectAll = $("#selectAll").attr("checked");
}
//======================================================数据交互=====================================================================================

//节点点击事件
function onClick(event, treeId, treeNode, clickFlag) {
    var fzcx = treeNode.id;
    $("#t_user").datagrid('load', {
        action: "QueryUserInfo",
        fz: fzcx
    });

}

//打开添加组框
function openAddFz(treeNode) {
    $("#dlgadd").css("display", "block");
    $("#dlgadd").dialog("open").dialog("setTitle", "新建用户组");
    $("#afzname").val("");
    MyNode = treeNode;
}

//保存新建组
var NodeId = "";
var MyNode = "";
function SaveNewFz() {
    var name = $("#afzname").val();
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    if (MyNode.pid == -1) {
        $.ajax({
            url: "/handlers/UsersInfo.ashx",
            type: "post",
            async: false,
            data: { action: "GetId", name: name },
            dataType: "html",
            success: function (html) {
                var val = "";
                eval("val=" + html);
                if (val.group) {
                    NodeId = val.group.id;
                    zTree.addNodes(MyNode, { id: NodeId, pId: MyNode.id, name: name, icon: "../../images/page.png" });
                    AddFz(NodeId, MyNode.id, name);
                }
                else if (val.errcode) {
                    $.messager.alert("提示", "新建分组失败！");
                }
            }
        });
    }
    else if (MyNode.pid != -1) {
        var id = newGuid();
        zTree.addNodes(MyNode, { id: id, pId: MyNode.id, name: name, icon: "../../images/page.png" });
        AddFz(id, MyNode.id, name);
    }

}

//打开修改组框
function openEditFz(treeNode) {
    $("#dlgedit").css("display", "block");
    $("#dlgedit").dialog("open").dialog("setTitle", "新建用户组");
    $("#efzname").val(treeNode.name);
    MyNode = treeNode;
}

//查询节点方法
function queryMenu() {
    zwobj.url = "../../handlers/UsersInfo.ashx?action=QueryUserGroup";
    ajaxData();
}
function ajax_QueryUserGroup(obj) {
    var len = obj.data.length;
    for (var i = 0; i < len; i++) {
        zNodes.push({
            id: obj.data[i].Id,
            pId: obj.data[i].Pid,
            name: obj.data[i].Name,
            icon: "../../images/page.png",
            open: true
        });
    }
}

//增加节点方法
function AddFz(id, pid, name) {
    $.ajax({
        url: "/handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "AddFz", id: id, name: name, pid: pid },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $.messager.alert('提示', result.msg);
                getfz("cx");
                $('#dlgadd').dialog('close');
                $('#t_user').datagrid('reload');
            } else {
                $.messager.alert("提示", result.msg);
            }
        }
    });
}



//编辑节点方法
function ModifyFz() {
    var name = $("#efzname").val();
    $.ajax({
        url: "/handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "ModifyFz", id: MyNode.id, name: name },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $.messager.alert('提示', result.msg);
                $('#dlgedit').dialog('close');
                location.reload();
            } else {
                $.messager("提示", result.msg);
            }
        }
    });
}

//删除节点方法
function DelFz(id) {
    $.ajax({
        url: "/handlers/UsersInfo.ashx",
        type: "post",
        async: false,
        data: { action: "DelFz", id: id },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $.messager.alert('提示', result.msg);
            } else {
                $.messager.alert("提示", result.msg);
            }
        }
    });
}



//js中guid生成方法
function newGuid() {
    var guid = "";
    for (var i = 1; i <= 32; i++) {
        var n = Math.floor(Math.random() * 16.0).toString(16);
        guid += n;
        if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
            guid += "-";
    }
    return guid;
}