﻿var url_id;
$(function () {
    url_id = queryUrlName("id");
    queryDetailInfo();
    var divwidth = $("#content").width();
    
    $("#content img").each(function () {
        if ($(this).width() > divwidth) {
            $(this).width(divwidth);
            $(this).css("width", divwidth); //或者这么用
        }
    })
});

function queryDetailInfo() {
    zwobj.url = "../../handlers/MessageList.ashx?action=QueryDetailInfo&id=" + url_id;
    ajaxData();
}

function ajax_QueryDetailInfo(obj) {
    $("#title").html(obj.data[0].Title);
    $("#time").html(eval("new " + obj.data[0].CreateDt.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss"));
    $("#content").html(obj.data[0].MaterialContent);
    $(document).attr("title", obj.data[0].Title);
}