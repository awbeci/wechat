﻿var key;
var name;
var flag;
var wxid;
$(function () {
    key = queryUrlName("key");
    name = queryUrlName("name");
    name = decodeURI(name);
    flag = queryUrlName("flag");
    wxid = flag == "0" ? queryUrlName("wxid") : "";
    $("#listtitle").html(name);
    queryMessageList();
});

function queryMessageList() {
    zwobj.url = "../../handlers/MessageList.ashx?action=QueryMessageList";
    zwobj.data = { key: key, wxid: wxid, flag: flag };
    ajaxData();
}

function ajax_QueryMessageList(obj) {
    var html = "";
    if (obj == null || obj.data == null || obj.data.length <= 0) {
        //alert("数据为空，请联系管理人员");
        return false;
    }
    
    for (var i = 0; i < obj.data.length; i++) {
        html += '<li onclick="myclick(\'' + obj.data[i].Id + '\')"><div style="font-size:16px;font-weight:bold;margin-left:20px;margin-bottom:10px;">' + obj.data[i].Title + '</div>';
        if (obj.data[i].MessageDescription != null) {
            html += '<div class="mydescription">' + obj.data[i].MessageDescription + '</div></li>';
        }
    }
    $("#myul").html(html);
}

function myclick(id) {
    switch (flag) {
        case "0":
            location.href = "messagedetail_fxgz.htm?id=" + id + "&wxid=" + wxid;
            break;
        case "1":
            location.href = "messagedetail.htm?id=" + id;
            break;
        default:
    }

}