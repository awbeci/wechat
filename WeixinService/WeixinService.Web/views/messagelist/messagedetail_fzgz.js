﻿var url_id;
var state; //1,待推送;2,待确认;3,已确认
var wxid;
$(function () {
    url_id = queryUrlName("id");
    wxid = queryUrlName("wxid");
    queryDetailInfo();
    $("body > div").css("background", "white");
});

function queryDetailInfo() {
    zwobj.url = "../../handlers/MessageList.ashx?action=QueryDetailInfo&id=" + url_id + "&wxid=" + wxid;
    ajaxData();
}

function ajax_QueryDetailInfo(obj) {
    $("#title").html(obj.data[0].Title);
    $("#time").html(eval("new " + obj.data[0].CreateDt.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss"));
    $("#content").html(obj.data[0].MaterialContent);
    if (typeof (obj.state) != "undefined" && obj.state != null && obj.state.length > 0) {
        $("#bt").empty();
        switch (obj.state) {
            case "1":
                $("#bt").append('<input type="button" id="isok" name="isok" class="mysubmit" value="请确认" onclick="myok()" />');
                break;
            case "2":
                $("#bt").append('<input type="button" id="isok" name="isok" class="mysubmit" value="请确认" onclick="myok()" />');
                break;
            case "3":
                $("#bt").append('<input type="button" id="isok" name="isok" class="mysubmit2" value="已确认"/>');
                break;
            default:
                $("#bt").append('<input type="button" id="isok" name="isok" class="mysubmit2" value="未知" />');
                break;
        }
    }
}

function myok() {
    zwobj.url = "../../handlers/PublishInfo.ashx?action=ModifyInfoReleaseDetails";
    zwobj.data = { wxid: wxid, id: url_id };
    ajaxData();
}

function ajax_ModifyInfoReleaseDetails(obj) {
    $("#bt").empty();
    $("#bt").append('<input type="button" id="isok" name="isok" class="mysubmit2" value="已确认"/>');
}
