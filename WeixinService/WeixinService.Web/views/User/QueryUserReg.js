﻿$(function () {
    initTable();
});

var dgObj = {
    url: "../../handlers/UserRegService.ashx",
    fit: true,
    queryParams: { action: "QueryReg" },
    title: '用户绑定查询',
    pagination: true,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "RNumber", title: "户号", width: 120, align: 'center' },
        { field: "PhoneNumber", title: "联系电话", width: 120, align: 'center' },
        { field: "CreateDt", title: "绑定时间", width: 120, align: 'center',
            formatter: function (val, obj) {
                if (val != null) {
                    return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd");
                }
            } 
        }
    ]]
};

function initTable() {
    $("#dg").datagrid(dgObj);
}

function search() {
    $("#dg").datagrid('load', {
        action: "QueryReg",
        kssj: $("#kssj").val(),
        jssj: $("#jssj").val(),
        hh: $("#hh").val()
    });
}
