﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeixinService.Bll;
using WeixinService.Dal;

namespace WeixinService.Web.views.User
{
    public partial class QueryUserReg : System.Web.UI.Page
    {
        protected log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        readonly UserRegDal _userRegDal = new UserRegDal();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 导出查询数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportQueryExcel_Click(object sender, EventArgs e)
        {
            var kssj = Request.Params["kssj"];
            var jssj = Request.Params["jssj"];
            var hh = Request.Params["hh"];
            try
            {
                var dataTable = _userRegDal.QueryUserReg(kssj, jssj, hh);
                dataTable.Columns[0].ColumnName = "用户号";
                dataTable.Columns[1].ColumnName = "联系电话";
                dataTable.Columns[2].ColumnName = "绑定时间";
                NPOIHelper.DataTableToExcel("用户绑定查询数据", dataTable, "数据表", Response, "用户绑定查询数据");
            }
            catch (Exception ex)
            {
                Log.Debug("方法名:Button1_Click,错误原因:" + ex.Message);
            }
        }

        /// <summary>
        /// 导出全部数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportAllExcel_Click(object sender, EventArgs e)
        {
            try
            {
                var dataTable = _userRegDal.QueryUserRegAll();
                dataTable.Columns[0].ColumnName = "用户号";
                dataTable.Columns[1].ColumnName = "联系电话";
                dataTable.Columns[2].ColumnName = "绑定时间";
                NPOIHelper.DataTableToExcel("用户绑定全部数据", dataTable, "数据表", Response, "用户绑定全部数据");
            }
            catch (Exception ex)
            {
                Log.Debug("方法名:Button2_Click,错误原因:" + ex.Message);
            }
        }
    }
}