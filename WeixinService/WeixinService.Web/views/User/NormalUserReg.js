﻿var List = null;
var phone = queryUrlName("phone");
$(function () {
    $("#PhoneNumber").val(phone);
    GetList();
    $("#Reg").click(function () {
        $("#Reg").attr("disabled", true);
        if (List) {
            for (var i = 0; i < List.rows.length; i++) {
                if (qymc == List.rows[i].RNumber) {
                    alert("用户号已经绑定");
                    $("#Reg").attr("disabled", false);
                    return;
                }
            }
        }
        if (List && List.rows.length >= 3) {
            alert("最多只能注册三个用户号；如需绑定，请先删除一个绑定。");
            return;
        }
        var qymc = $("#qymc").val();

        var phonenumber = $("#PhoneNumber").val();
        if (qymc.trim().length <= 0) {
            alert("用户编号不能为空");
            $("#Reg").attr("disabled", false);
            return;
        }
        if (!check_Number(qymc)) {
            alert("用户编号只能为数字，请检查。");
            $("#Reg").attr("disabled", false);
            return;
        }
        if (qymc.length != 10) {
            alert("用户编号为10位数字。");
            $("#Reg").attr("disabled", false);
            return;
        }


        if (!check_Number(phonenumber)) {
            alert("手机号只能为数字，请检查。");
            $("#Reg").attr("disabled", false);
            return;
        }
        if (phonenumber.length != 11) {
            alert("手机号为11位数字。");
            $("#Reg").attr("disabled", false);
            return;
        }
        phone = phonenumber;
        $.ajax({
            url: "../../handlers/UsersInfo.ashx",
            type: "POST",
            data: { action: "UserReg1",
                qymc: qymc,
                lxr: "",
                phonenumber: phonenumber,
                r: Math.random()
            },
            dataType: "json",
            success: function (data) {
                $("#Reg").attr("disabled", false);
                if (data.success) {
                    $("#qymc").val("");
                    if (confirm(data.msg + '点“确定”返回到主页，点“取消”继续绑定！')) {
                        window.Android.goMain();
                    }
                } else {
                    alert(data.msg);
                }
                try {
                    window.Android.savePcToSD(phonenumber);
                }
                catch (ex) {
                    alert(ex);
                }
                GetList();
            },
            error: function (x, h, e) {
                $("#Reg").attr("disabled", false);
            }
        });
    });
});

function getUserName() {
    var yhid = $("#qymc").val();
    if (yhid.length > 0) {
        $.getJSON("../../handlers/ArrearageQuery.ashx",
        { action: "GetUserNameByDb", yhid: $("#qymc").val(), r: Math.random() },
        function (data) {
            if (!data.error) {
                var str = data.name.substr(data.name.length - 1, 1);
                for (var i = 1; i < data.name.length; i++) {
                    str = "*" + str;
                }
                $("#lxr").val(str);
            } else {
                $("#lxr").val("");
                //alert(data.msg);
            }
        });
    }
}

//js验证只能输入数字
function check_Number(value) {
    //定义正则表达式部分    
    var reg = /^\d+$/;
    if (value.constructor === String) {
        var re = reg.test(value);
        return re;
    }
    return false;
}

function GetList() {
    if (phone.length == 0) {
        $("#InfoList").html("");
        return;
    }
    $.ajax({
        url: "../../handlers/UsersInfo.ashx",
        type: "POST",
        data: { action: "GetUserRegList", phone: phone, r: Math.random() },
        dataType: "json",
        success: function (data) {
            List = data;
            var html = "";
            for (var i = 0; i < data.rows.length; i++) {
                html += ' <div class="m-item2"><div class="mynum">' + data.rows[i].RNumber + ',' + data.rows[i].PhoneNumber + '</div>'
                    + '<div class="mydel"><input type="button" class="mysubmit2" value="删 除" onclick="DelReg(\''
                    + data.rows[i].Wxid + '\')"/></div></div>';
            }
            $("#InfoList").html(html.replace("null", ""));
        },
        error: function (x, h, e) {
        }
    });
}

//删除绑定
function DelReg(wxid) {
    if (confirm("确认删除该用户？"))
        $.ajax({
            url: "../../handlers/UsersInfo.ashx",
            type: "POST",
            data: { action: "DelUserReg",
                wxid: wxid,
                r: Math.random()
            },
            dataType: "json",
            success: function (data) {
                alert(data.msg);
                GetList();
            },
            error: function (x, h, e) {
            }
        });
}

/*
* 获取url参数值 
*/
function queryUrlName(name) {
    var url = location.href.replace(/\\u0026/g, "&");
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}