﻿var map, point, marker;

$(function () {
    initMap();
    initMap2();
});

function initMap() {
    // 百度地图API功能
    map = new BMap.Map("allmap");
    point = new BMap.Point(117.81876, 30.951177);
    map.centerAndZoom(point, 12); //创建中心点
    //    marker = new BMap.Marker(point);  // 创建中心点标注
    //    map.addOverlay(marker);              // 将中心点标注添加到地图中
    map.enableScrollWheelZoom(true);

    zwobj.url = "../../handlers/PowerCutService.ashx?action=QueryMap";
    zwobj.data = { key: '011_youo_tdtz' };
    ajaxData();
}

function initMap2() {
    zwobj.url = "../../handlers/PowerCutService.ashx?action=QueryMap";
    zwobj.data = { key: '011_youo_tdtz2' };
    ajaxData();
}

function ajax_QueryMap(obj) {
    if (obj.data.length <= 0) {
        return;
    }
    try {
        for (var i = 0, len = obj.data.length; i < len; i++) {
            if ((obj.data[i].Jd != null && typeof obj.data[i].Jd != "undefined" && obj.data[i].Jd != '0') &&
            (obj.data[i].Wd != null && typeof obj.data[i].Wd != "undefined" && obj.data[i].Jd != '0')) {
                point = new BMap.Point(obj.data[i].Jd, obj.data[i].Wd);
                addMarker(point, obj.data[i], obj.key);
            }
        }
    } catch (e) {
        alert(e.message);
    }
}

/***  标记该坐标  方法
*/
function addMarker(point, data, type) {
    if (type == "011_youo_tdtz") {
        var myIcon = new BMap.Icon("../../images/location2.png", new BMap.Size(22, 28));
        marker = new BMap.Marker(point, { icon: myIcon });
    }
    else if (type == "011_youo_tdtz2") {
        //marker = new BMap.Marker(point);
        var myIcon = new BMap.Icon("../../images/location3.png", new BMap.Size(22, 28));
        marker = new BMap.Marker(point, { icon: myIcon });
    }

    map.addOverlay(marker);
    if (data.BusinessType == "011_youo_tdtz") {
        var sContent =
        "<p style='margin:0 0 5px 0;padding:0.2em 0'>【计划停电】" + data.TimeArea + "</p>" +
            "<p style='margin:0 0 5px 0;padding:0.2em 0'>停电区域：" + data.Area + "</p>";
    }
    else {
        var sContent =
        "<p style='margin:0 0 5px 0;padding:0.2em 0'>【故障停电】" + data.TimeArea + "</p>" +
            "<p style='margin:0 0 5px 0;padding:0.2em 0'>停电区域：" + data.Area + "</p>";
    }
    var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
    marker.addEventListener("click", function () {
        this.openInfoWindow(infoWindow, point);
    });
}