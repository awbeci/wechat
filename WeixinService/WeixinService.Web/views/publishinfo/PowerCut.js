﻿function ajax_PublishInfoToPerson(obj) {
    alert("发布成功");
    $("#dg").datagrid('reload');
}

$(function () {
    key = queryUrlName("key");
    name = queryUrlName("name");
    name = decodeURI(name);
    try {
        initTable();
    } catch (e) { }
    initDlg();
    $("#ilable").text(name + "内容");
});

var map, point, marker;
var jd = '117.81876', wd = '30.951177'; //在这里配置默认坐标

function openMap() {
    $('#dlg-map').dialog('open');
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        addBm();
    }
    if (actiontype == "edit") {
        $("#jd").val(selected.Jd);
        $("#wd").val(selected.Wd);
        $("#sm").val(selected.Description);
        // 百度地图API功能
        map = new BMap.Map("allmap");            // 创建Map实例
        map.enableScrollWheelZoom();
        if (typeof selected.Jd == "undefined" || typeof selected.Wd == "undefined" || selected.Jd == null || selected.Wd == null) {
            point = new BMap.Point(jd, wd);
            map.centerAndZoom(point, 13);
            marker = new BMap.Marker(point);
        } else {
            point = new BMap.Point(selected.Jd, selected.Wd);
            map.centerAndZoom(point, 13);
            marker = new BMap.Marker(point);
            map.addOverlay(marker);
        }
        addMarker();
    }
}
function KeyQuery() {
    var local = new BMap.LocalSearch(map, {
        renderOptions: { map: map }
    });
    local.search($("#IKey").val());
}
/***  标记该坐标  方法
*/
function addMarker() {
    map.addEventListener("click", function (e) {
        deletePoint();             //删除所有已标记点
        $("#jd").val(e.point.lng);
        $("#wd").val(e.point.lat);
        point = new BMap.Point(e.point.lng, e.point.lat);
        marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);
    });

    //当点击页面   删除所有点
    function deletePoint() {
        var allOverlay = map.getOverlays();
        for (var i = 0; i < allOverlay.length; i++) {
            map.removeOverlay(allOverlay[i]);
        }
    }
}

function addBm() {
    $("#fm-map").form("reset");
    // 百度地图API功能
    map = new BMap.Map("allmap");
    point = new BMap.Point(jd, wd);
    map.centerAndZoom(point, 13);
    marker = new BMap.Marker(point);  // 创建标注
    //map.addOverlay(marker);              // 将标注添加到地图中
    marker.addEventListener("click", getAttr);
    map.enableScrollWheelZoom(true);
    map.addEventListener("click", function (e) {
        deletePoint();             //删除所有已标记点
        $("#jd").val(e.point.lng);
        $("#wd").val(e.point.lat);
        point = new BMap.Point(e.point.lng, e.point.lat);
        marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);
    });

    //当点击页面   删除所有点
    function deletePoint() {
        var allOverlay = map.getOverlays();
        for (var i = 0; i < allOverlay.length; i++) {
            map.removeOverlay(allOverlay[i]);
        }
    }

    function getAttr() {
        var p = marker.getPosition();       //获取marker的位置
        $("#jd").val(p.lng);
        $("#wd").val(p.lat);
    }
}

var actiontype; //全局变量
var key; //全局变量
var name; //全局变量

var dgObj = {
    url: "../../handlers/PowerCutService.ashx",
    queryParams: { action: "Query1", key: key, r: Math.random() },
    fit: true,
    title: '信息列表',
    pagination: true,
    border: false,
    iconCls: 'icon-applicationviewcolumns',
    nowrap: false,
    rownumbers: true,
    singleSelect: true,
    striped: true,
    toolbar: [{
        text: '新建',
        iconCls: 'icon-add',
        handler: function () {
            addDlg('add');
        }
    }, '-', {
        text: '编辑',
        iconCls: 'icon-edit',
        handler: function () {
            addDlg('edit');
        }
    }, '-', {
        text: '删除',
        iconCls: 'icon-remove',
        handler: function () {
            del();
        }
    }, '-', {
        text: '发布',
        iconCls: 'icon-world',
        handler: function () {
            var selected = $("#dg").datagrid("getSelected");
            if (selected == null) {
                alert("请选择一条数据进行发布");
                return false;
            }
            zwobj.url = "../../handlers/PowerCutService.ashx";
            zwobj.data = { action: "PublishInfoToPerson", id: selected.Id };
            ajaxData();
        }
    }
    ],
    pageList: [10, 20, 30, 50],
    pageSize: 20,
    columns: [[
        { field: "Id", hidden: true },
        {
            field: "Title",
            title: "标题",
            width: 220,
            align: 'left',
            formatter: function (val, obj) {
                return name + ":" + val;
            }
        },
      {
          field: "State",
          title: "送电状态",
          width: 60,
          align: 'center',
          formatter: function (val, obj) {
              switch (val) {
                  case "1":
                      return "未送电";
                  case "2":
                      return "已送电";
                  default:
                      return "未知";
              }
          }
      },
        {
            field: "PowerCutTime",
            title: "停电日期",
            width: 100,
            align: 'center',
            formatter: function (val, obj) {
                if (val != null) {
                    return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd");
                }
            }
        },
        { field: "TimeArea", title: "停电时间范围", width: 120 },
        { field: "Area", title: "停电范围", width: 120 },
        { field: "Device", title: "停电设备", width: 120 },
        {
            field: "FlagRelease",
            title: "发布状态",
            width: 70,
            align: 'center',
            formatter: function (val, obj) {
                switch (val) {
                    case "1":
                        return "未发布";
                    case "2":
                        return "已发布";
                    case "3":
                        return "已推送";
                    default:
                        return "未知";
                }
            }
        },
          { field: "Jd", title: "经度", width: 80, align: 'center' },
        { field: "Wd", title: "纬度", width: 80, align: 'center' },
        {
        field: "CreateDt",
        title: "创建日期",
        width: 140,
        align: 'center',
        formatter: function (val, obj) {
            if (val != null) {
                return eval("new " + val.split('/')[1]).Format("yyyy-MM-dd hh:mm:ss");
            }
        }
    },
        { field: "CreatePerson", title: "创建人", width: 100, align: 'center' }
    ]]
};

function initTable() {
    dgObj.queryParams.key = key;
    dgObj.title = name + "列表";
    $("#dg").datagrid(dgObj);
}

//初始化dlg
function initDlg() {
    $('#dlg').dialog({
        title: ' 添 加',
        modal: true,
        closed: true,
        buttons: "#dlg-buttons"
    });

    $('#dlg-map').dialog({
        title: ' 添 加',
        modal: true,
        closed: true
    });
}

function addDlg(data) {
    actiontype = data;
    $('#fm').form('reset');
    if (data == "edit") {
        if ($(".panel-icon").hasClass("icon-add")) {
            $(".panel-icon").removeClass("icon-add").addClass("icon-edit");
        }
        var selected = $("#dg").datagrid("getSelected");
        if (selected == null) {
            alert("请选择一条数据进行编辑");
            return false;
        }
        $('#dlg').dialog("setTitle", "编 辑");
        $("#title").val(selected.Title);
        $("#State").val(selected.State);
        $("#PowerCutTime").val(eval("new " + selected.PowerCutTime.split('/')[1]).Format("yyyy-MM-dd hh:mm"));
        $("#TimeArea").val(selected.TimeArea);
        $("#Area").val(selected.Area);
        $("#Device").val(selected.Device);
    }
    if (data == "add") {
        var date = new ZwGetDate();
        var dt = date.getZwDate();
        if (key == "011_youo_tdtz") {
            $("#title").val("国网铜陵供电公司" + dt + "停电公告");
        }
        if (key == "011_youo_tdtz2") {
            $("#title").val("国网铜陵供电公司" + dt + "停电公告");
        }

        if ($(".panel-icon").hasClass("icon-edit")) {
            $(".panel-icon").removeClass("icon-edit").addClass("icon-add");
        }
    }
    $('#dlg').dialog("open");
}

function del() {
    var selected = $("#dg").datagrid("getSelected");
    if (selected == null) {
        alert("请选择一条数据进行删除");
        return false;
    }
    if (confirm("确认删除此条数据吗？")) {
        zwobj.url = "../../handlers/PowerCutService.ashx?action=DelPublishInfo";
        zwobj.data = { id: selected.Id };
        ajaxData();
    }
}

//保存方法
function save() {
    var myurl;
    var selected = $("#dg").datagrid("getSelected");
    if (actiontype == "add") {
        myurl = '../../handlers/PowerCutService.ashx?action=SavePublishInfo&key=' + key;
    } else if (actiontype == "edit") {
        myurl = '../../handlers/PowerCutService.ashx?action=EditPublishInfo&key=' + key + '&id=' + selected.Id;
    }

    $('#fm').form('submit', {
        url: myurl,
        onSubmit: function (data) {
            data.jd = $("#jd").val();
            data.wd = $("#wd").val();
            data.sm = $("#sm").val();
            if ($("#title").val().length <= 0) {
                alert("请输入标题");
                return false;
            }
            return true;
        },
        success: function (result) {
            result = eval('(' + result + ')');
            if (result.isSuccess) {
                $('#dlg').dialog('close');
                $("#dg").datagrid("reload");
            }
        }
    });
}

function ajax_DelPowerInfo(obj) {
    $("#dg").datagrid("reload");
}

function ZwGetDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth();
    var mymonth;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();

    this.getZwDate = function () {
        switch (month) {
            case 0:
                mymonth = "01";
                break;
            case 1:
                mymonth = "02";
                break;
            case 2:
                mymonth = "03";
                break;
            case 3:
                mymonth = "04";
                break;
            case 4:
                mymonth = "05";
                break;
            case 5:
                mymonth = "06";
                break;
            case 6:
                mymonth = "07";
                break;
            case 7:
                mymonth = "08";
                break;
            case 8:
                mymonth = "09";
                break;
            case 9:
                mymonth = "10";
                break;
            case 10:
                mymonth = "11";
                break;
            case 11:
                mymonth = "12";
                break;
            default:
        }
        return mymonth + "月" + day + "日";
    };
}