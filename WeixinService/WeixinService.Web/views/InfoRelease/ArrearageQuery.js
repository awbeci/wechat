﻿$(function () {
    var phone = queryUrlName("phone");
    if (phone.length == 0) {
        try {
            phone = window.Android.readPcInSD();
            for (var i = 0; i < 10; i++) {
                setTimeout(function () {
                    if ($("#PhoneNumber").val().length == 0) {
                        $("#PhoneNumber").val(phone);
                    }
                }, i * 1000);
            }
        }
        catch (e) {
            alert(e);
        }
    }
    $("#PhoneNumber").val(phone);
    $("#Query").click(function () {
        phone = $("#PhoneNumber").val();
        if (phone.length == 0) {
            alert("手机号不能为空.");
            return;
        }
        if (!check_Number(phone)) {
            alert("手机号只能为数字，请检查。");
            return;
        }
        if (phone.length != 11) {
            alert("手机号为11位数字。");
            return;
        }
        try {
            window.Android.savePcToSD(phone);
        }
        catch (ex) {
            alert(ex);
        }
        $.ajax({
            url: "../../handlers/ArrearageQuery.ashx",
            type: "POST",
            data: { action: "Query", phone: phone, r: Math.random() },
            dataType: "json",
            success: function (data) {
                if (!data.error) { //已注册，
                    var html = "截止到：" + data.createDt.split(" ")[0];
                    for (var obj in data.rows) {
                        if (data.rows[obj].key == "用户编号") {
                            html += "<br/>" + data.rows[obj].key + "： " + data.rows[obj].value;
                        } else if (data.rows[obj].key == "欠费") {
                            html += data.rows[obj].key + "： " + data.rows[obj].value + "(不含违约金)";
                        } else {
                            html += data.rows[obj].key + "： " + data.rows[obj].value;
                        }
                        html += ", ";
                    }
                    $("#InfoList").html(html);
                } else {
                    if (data.msg == "unreg") { //未注册，跳转到注册界面
                        location.href = '../User/NormalUserReg.html?key=011_youo_dfzd&phone=' + phone;
                        return;
                    }
                    $("#InfoList").html(data.msg);
                }
            },
            error: function (x, h, ee) {
            }
        });
    });
});

function Reg() {
    phone = $("#PhoneNumber").val();
    if (phone.length == 0) {
        alert("手机号不能为空.");
        return;
    }
    location.href = '../User/NormalUserReg.html?phone=' + phone;
}

/*
* 获取url参数值 
*/
function queryUrlName(name) {
    var url = location.href.replace(/\\u0026/g, "&");
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (var i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}
//js验证只能输入数字
function check_Number(value) {
    //定义正则表达式部分    
    var reg = /^\d+$/;
    if (value.constructor === String) {
        var re = reg.test(value);
        return re;
    }
    return false;
}