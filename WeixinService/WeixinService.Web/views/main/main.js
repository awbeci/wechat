﻿
function addTab(title, url, icon) {
    if ($('#tabs').tabs('exists', title)) {
        $('#tabs').tabs('select', title); //选中并刷新
        var currTab = $('#tabs').tabs('getSelected');
        var url = $(currTab.panel('options').content).attr('src');
        if (url != undefined && currTab.panel('options').title != 'Home') {
            $('#tabs').tabs('update', {
                tab: currTab,
                options: {
                    content: createFrame(url)
                }
            });
        }
    } else {
        var content = createFrame(url);
        $('#tabs').tabs('add', {
            title: title,
            iconCls: icon,
            content: content,
            closable: true
        });
    }
}

function createFrame(url) {
    var s = '<iframe name="myframe" scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
    return s;
}

function modifyPass() {
    addTab('修改密码', '../manage/background/ModifyPass.htm', 'icon-key');
}

$(function () {
    queryMenu();
    $('.cs-navi-tab').click(function () {
        var $this = $(this);
        var href = $this.attr('src');
        var icon = $this.attr('data-options').split(":")[1].replace(/"/g,'');//删除所有";
        var title = $this.text();
        addTab(title, href, icon);
    });
    zwobj.url = "../../handlers/UsersService.ashx?action=VerifySession";
    ajaxData();
});

function ajax_VerifySession(obj) {
    if (typeof (obj.data) == "undefined") {
        location.href = "login.html";
    } else {
        $("#username a").append(obj.data);
    }
}

function queryMenu() {
    zwobj.url = "../../handlers/MenuService.ashx?action=QueryMenuByRoles";
    ajaxData();
}

function ajax_QueryMenuByRoles(obj) {
    $("#mymenu").empty();
    var targetObj = $("#mymenu").html('<div id="mymenu" class="easyui-accordion" data-options="fit:true,border:false">' + obj.data);
    $.parser.parse(targetObj); //重新渲染
}