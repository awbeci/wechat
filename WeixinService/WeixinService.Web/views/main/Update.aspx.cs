﻿using System;

namespace WeixinService.Web.views.main
{
    public partial class Update : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var versions = Request.QueryString["versions"] ?? "";
                var strV = System.Configuration.ConfigurationSettings.AppSettings["versions"];
                var url = System.Configuration.ConfigurationSettings.AppSettings["url"];
                float ver = float.Parse(strV);//最新版本
                var sourceVersions = float.Parse(versions);
                var ret = ver <= sourceVersions ? "" : url;
                Response.Write(ret);
            }
            catch (Exception)
            {
                Response.Write("");
            }
        }
    }
}