﻿//全局变量
var key;
var name;

$(function () {
    key = queryUrlName("key");
    name = queryUrlName("name");
    name = decodeURI(name);
    initDate();
    $("#listtitle").html(name + "信息查询");
    $(".ui-btn:contains('查询')").empty().append(' <input type="button" id="publish" name="publish" class="mysubmit" onclick="query()"value="查询" />');
    $(".ui-loader").css("display", "none");
    $("body > div").css("background", "#fff");
    if (key == "011_youo_tdtz") {
        $(".mycontent").append('<p>5、尊敬的电力客户，因供电公司电力施工，安排以下计划停电工作安排。未经铜陵供电公司有关部门许可，严禁任何单位和个人在停电线路及设备上工作。为此造成的不便，敬请各客户给予谅解和支持。如有任何疑问，请致电供电公司24小时服务热线95598。</p>');
    }
    if (key == "011_youo_tdtz2") {
        $(".mycontent").append('<p>5、尊敬的电力客户，因突发电力故障，安排以下故障抢修工作安排。未经铜陵供电公司有关部门许可，严禁任何单位和个人在停电线路及设备上工作。为此造成的不便，敬请各客户给予谅解和支持。如有任何疑问，请致电供电公司24小时服务热线95598。</p>');
    }
});

function initDate() {
    var date = new ZwDate();
    if (key == "011_youo_tdtz") {
        var kssj = date.getZwDate();
        $("#kssj").val(kssj);

        date.setDay(7);

        var jssj = date.getZwDate();
        $("#jssj").val(jssj);

        $('.demo-test-date2').mobiscroll().date({
            defaultValue: new Date(jssj),
            preset: 'date', //日?期ú
            dateFormat: 'yy-mm-dd', // 日?期ú格?式?
            dateOrder: 'yymmdd', //面?板?中D日?期ú排?列格?式?
            theme: 'android-holo',
            mode: 'scroller',
            lang: 'zh',
            display: 'bottom',
            animate: '',
            maxDate: new Date(jssj),
            minDate: new Date(kssj)
        });

        $('.demo-test-date1').mobiscroll().date({
            defaultValue: new Date(kssj),
            preset: 'date', //日期
            dateFormat: 'yy-mm-dd', // 日期格式
            dateOrder: 'yymmdd', //面板中日期排列格式
            theme: 'android-holo', //皮肤样式
            mode: 'scroller', //日期选择模式
            lang: 'zh',
            display: 'bottom', //显示方式
            animate: '',
            maxDate: new Date(jssj),
            minDate: new Date()

        });
    }
    else if (key == "011_youo_tdtz2") {
        var jssj2 = date.getZwDate();
        $("#jssj").val(jssj2);

        date.setDay(-3);
        var kssj2 = date.getZwDate();
        $("#kssj").val(kssj2);

        $('.demo-test-date1').mobiscroll().date({
            defaultValue: new Date(kssj2),
            preset: 'date', //日?期ú
            dateFormat: 'yy-mm-dd', // 日?期ú格?式?
            dateOrder: 'yymmdd', //面?板?中D日?期ú排?列格?式?
            theme: 'android-holo',
            mode: 'scroller',
            lang: 'zh',
            display: 'bottom',
            animate: '',
            minDate: new Date(kssj2),
            maxDate: new Date(jssj2)
        });

        $('.demo-test-date2').mobiscroll().date({
            defaultValue: new Date(jssj2),
            preset: 'date', //日?期ú
            dateFormat: 'yy-mm-dd', // 日?期ú格?式?
            dateOrder: 'yymmdd', //面?板?中D日?期ú排?列格?式?
            theme: 'android-holo',
            mode: 'scroller',
            lang: 'zh',
            display: 'bottom',
            animate: '',
            minDate: new Date(kssj2),
            maxDate: new Date()
        });
    }
}

function query() {
    var kssj = $("#kssj").val();
    var jssj = $("#jssj").val();
    location.href = "querypoweruplist.htm?name=" + name + "&key=" + key + "&kssj=" + kssj + "&jssj=" + jssj;
}